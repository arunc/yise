/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : abc_truth.cpp
 * @brief  : Truth table manipulations for ABC ported from axekit
 */
//-------------------------------------------------------------------------------

#include "abc_truth.hpp"
#include <functions/truth/yig_truth.hpp>

#include <array>
#include <iomanip>


std::string to_str (
  const std::vector<int> &list,
  const std::unordered_map<int, int> &pi_vals
  )
{
  auto to_y = [&] ( int i )
    {
      if ( i == 1 ) return 'a'; 
      if ( i == 2 ) return 'b'; 
      if ( i == 3 ) return 'c'; 
      if ( i == 4 ) return 'd'; 
      if ( i == 5 ) return 'e'; 
      if ( i == 6 ) return 'f';
      return 'm';
    };

  
  std::stringstream str;
  str << "{ ";

  for ( const auto &e : pi_vals )
  {
    auto p = std::find ( list.begin (), list.end (), e.first );
    if ( p == list.end () )
    {
      str << to_y ( e.first ) << ":" << e.second << " ";
    }
  }
  
  for ( const auto &e : list )
  {
    auto v = pi_vals.find ( e );
    assert ( v != pi_vals.end () );
    std::string vs = "x";
    if ( v->second == 1 ) vs = "1";
    if ( v->second == 0 ) vs = "0";
    
    str << to_y ( e ) << ":" << vs << " ";
  }
  str <<"}";
  
  return str.str ();
}

std::string to_str ( const std::vector<int> &list )
{
  std::string str = "{";
  for ( const auto &e : list )
  {
    str = str + std::to_string ( e ) + " ";
  }
  str = str + "}";
  return str;
}


// clang-format off

namespace abc
{
static inline word *Gla_ObjTruthNodeId(Gia_Man_t *p, int Id) { return Vec_WrdArray(p->vTtMemory) + p->nTtWords * Id; }
static inline word *Gla_ObjTruthNode(Gia_Man_t *p, Gia_Obj_t *pObj) { return Vec_WrdArray(p->vTtMemory) + p->nTtWords * Gia_ObjNum(p, pObj); }

static inline word *Gla_ObjTruthElem(Gia_Man_t *p, int i) { return (word *)Vec_PtrEntry(p->vTtInputs, i); }

  
void ObjCollectInternalCut_rec(abc::Gia_Man_t *p, int iObj)
{
  if (Gia_ObjHasNumId(p, iObj))
  {
    return;
  }

  assert(Gia_ObjIsAnd(Gia_ManObj(p, iObj)));
  ObjCollectInternalCut_rec(p, Gia_ObjFaninId0(Gia_ManObj(p, iObj), iObj));
  ObjCollectInternalCut_rec(p, Gia_ObjFaninId1(Gia_ManObj(p, iObj), iObj));
  Gia_ObjSetNumId(p, iObj, Vec_IntSize(p->vTtNodes));
  Vec_IntPush(p->vTtNodes, iObj);
}

void ObjCollectInternalCut(abc::Gia_Man_t *p, int iRoot, abc::Vec_Int_t *vLeaves)
{
  int i, iObj;
  assert(!Gia_ObjHasNumId(p, iRoot));
  assert(Gia_ObjIsAnd(Gia_ManObj(p, iRoot)));
  Vec_IntForEachEntry(vLeaves, iObj, i)
  {
    assert(!Gia_ObjHasNumId(p, iObj));
    Gia_ObjSetNumId(p, iObj, -i);
  }
  assert(!Gia_ObjHasNumId(p, iRoot)); // the root cannot be one of the leaves
  Vec_IntClear(p->vTtNodes);
  Vec_IntPush(p->vTtNodes, -1);
  ObjCollectInternalCut_rec(p, iRoot);
}

word *ObjComputeTruthTableCut ( Gia_Man_t *p, Gia_Obj_t *pRoot,
                                Vec_Int_t *vLeaves, Vec_Wrd_t *vTtLeaves )
{
  Gia_Obj_t *pTemp;
  word *pTruth, *pTruthL, *pTruth0, *pTruth1;
  int i, iObj, Id0, Id1;
  assert ( p->vTtMemory != NULL );
  assert ( Vec_IntSize ( vLeaves ) <= p->nTtVars );
  // extend ID numbers
  if ( Vec_IntSize ( p->vTtNums ) < Gia_ManObjNum ( p ) )
    Vec_IntFillExtra ( p->vTtNums, Gia_ManObjNum ( p ), -ABC_INFINITY );
  // collect internal nodes
  ObjCollectInternalCut ( p, Gia_ObjId ( p, pRoot ), vLeaves );
  // extend TT storage
  if ( Vec_WrdSize ( p->vTtMemory ) < p->nTtWords * ( Vec_IntSize ( p->vTtNodes ) + 2 ) )
    Vec_WrdFillExtra ( p->vTtMemory, p->nTtWords * ( Vec_IntSize ( p->vTtNodes ) + 2 ), 0 );
  // compute the truth table for internal nodes
  Vec_IntForEachEntryStart ( p->vTtNodes, iObj, i, 1 )
  {
    assert ( i == Gia_ObjNumId ( p, iObj ) );
    pTemp = Gia_ManObj ( p, iObj );
    pTruth = Gla_ObjTruthNodeId ( p, i );
    pTruthL = pTruth + p->nTtWords;
    Id0 = Gia_ObjNumId ( p, Gia_ObjFaninId0 ( pTemp, iObj ) );
    Id1 = Gia_ObjNumId ( p, Gia_ObjFaninId1 ( pTemp, iObj ) );
    pTruth0 = ( Id0 > 0 ) ?
      Gla_ObjTruthNodeId ( p, Id0 ) : Vec_WrdEntryP ( vTtLeaves, -Id0 );
    pTruth1 = ( Id1 > 0 ) ?
      Gla_ObjTruthNodeId ( p, Id1 ) : Vec_WrdEntryP ( vTtLeaves, -Id1 );
    if ( Gia_ObjFaninC0 ( pTemp ) )
    {
      if ( Gia_ObjFaninC1 ( pTemp ) )
        while ( pTruth < pTruthL ) *pTruth++ = ~*pTruth0++ & ~*pTruth1++;
      else
        while ( pTruth < pTruthL ) *pTruth++ = ~*pTruth0++ & *pTruth1++;
    }
    else
    {
      if ( Gia_ObjFaninC1 ( pTemp ) )
        while ( pTruth < pTruthL ) *pTruth++ = *pTruth0++ & ~*pTruth1++;
      else
        while ( pTruth < pTruthL ) *pTruth++ = *pTruth0++ & *pTruth1++;
    }
  }
  pTruth = Gla_ObjTruthNode ( p, pRoot );
  // unmark leaves marked by Gia_ObjCollectInternal()
  Vec_IntForEachEntry ( vLeaves, iObj, i ) Gia_ObjResetNumId ( p, iObj );
  Vec_IntForEachEntryStart ( p->vTtNodes, iObj, i, 1 ) Gia_ObjResetNumId ( p, iObj );
  return pTruth;
}

// Abc code. due to static linkage copied here.
// Don't touch
swapInfo *setSwapInfoPtr ( int varsN )
{
  int i;
  swapInfo *x = (swapInfo *)malloc ( sizeof ( swapInfo ) );
  x->posArray = (varInfo *)malloc ( sizeof ( varInfo ) * ( varsN + 2 ) );
  x->realArray = (int *)malloc ( sizeof ( int ) * ( varsN + 2 ) );
  x->varN = varsN;
  x->realArray[0] = varsN + 100;
  for ( i = 1; i <= varsN; i++ )
  {
    x->posArray[i].position = i;
    x->posArray[i].direction = -1;
    x->realArray[i] = i;
  }
  x->realArray[varsN + 1] = varsN + 10;
  return x;
}
void freeSwapInfoPtr ( swapInfo *x )
{
  free ( x->posArray );
  free ( x->realArray );
  free ( x );
}


}



namespace axekit
{
// clang-format on

void path_rec ( std::vector<int> &pi_list, abc::Gia_Man_t *gia, abc::Gia_Obj_t *currObj )
{

  assert ( currObj );
  assert ( !abc::Gia_IsComplement ( currObj ) );
  assert ( !abc::Gia_ObjIsConst0 ( currObj ) );

  if ( abc::Gia_ObjIsTravIdCurrent ( gia, currObj ) ) return;
  abc::Gia_ObjSetTravIdCurrent ( gia, currObj );

  if ( abc::Gia_ObjIsCi ( currObj ) )
  {
    // pi_list.emplace_back ( abc::Gia_Obj2Lit (gia, currObj) );
    pi_list.emplace_back ( abc::Gia_ObjId ( gia, currObj ) );
    return;
  }
  assert ( abc::Gia_ObjIsAnd ( currObj ) );
  path_rec ( pi_list, gia, abc::Gia_ObjFanin0 ( currObj ) );
  path_rec ( pi_list, gia, abc::Gia_ObjFanin1 ( currObj ) );
}


//------------------------------------------------------------------------------
// Find the support for a given PO
// support is the list of all PIs in the fanin cone
// NOTE: the returned list is NOT the pi-index. Its the objectId of pis
// Hence starts from 1 to numPi, not 0 to numPi-1; for 0 is const
std::vector<int> get_pis_for_po ( abc::Gia_Man_t *gia, int po_id )
{
  assert ( po_id < abc::Gia_ManPoNum ( gia ) );
  std::vector<int> pi_list;
  auto pObj = abc::Gia_ManPo ( gia, po_id );
  pObj = abc::Gia_ObjFanin0 ( pObj );
  if ( abc::Gia_ObjIsConst0 ( pObj ) ) // const0 (const1 cannot come here)
  {
    return pi_list;
  }
  abc::Gia_ManIncrementTravId ( gia );
  path_rec ( pi_list, gia, abc::Gia_Regular ( pObj ) );
  assert ( pi_list.size () <= abc::Gia_ManPiNum ( gia ) );
  return pi_list;
}

//------------------------------------------------------------------------------
// create a map of all PIs with don't care assignment
std::unordered_map<int, int> create_pi_vals ( abc::Gia_Man_t *gia )
{
  std::unordered_map<int, int> pi_vals;
  for ( auto i = 0; i < abc::Gia_ManPiNum ( gia ); i++ )
  {
    auto objid = abc::Gia_ObjId ( gia, abc::Gia_ManPi ( gia, i ) );
    pi_vals[objid] = 2;
  }
  return pi_vals;
}

// reassign all the values in pi_vals map to don't care
void assign_x_to_pi_vals ( std::unordered_map<int, int> &pi_vals )
{
  assert ( pi_vals.find ( 0 ) == pi_vals.end () ); // do not map 0
  for ( auto &pi : pi_vals )
  {
    pi.second = 2;
  }
}

// Finds the local function truth-table of the PO
abc::word get_truth_table_for_po ( abc::Gia_Man_t *gia, const int po_id )
{
  assert ( po_id < abc::Gia_ManPoNum ( gia ) );
  auto pi_list = get_pis_for_po ( gia, po_id );
  
  // NOTE: if an order of PIs is needed, then need to put back this sort.
  // But this will not work, if PIs have constants or shared PIs,
  // which are missing from pi_list, but treated as free variables.
  //std::sort ( pi_list.begin (), pi_list.end () );
  auto pi_vals = create_pi_vals ( gia );

  return get_truth_table_for_po ( gia, po_id, pi_list, pi_vals );
}

// Finds the local function tt for po, for a given support and
// pre-defined overridden values for its PIs.
// PIs can be overridden with a user defined value using the map pi_vals.
// The tt is expressed in terms of the input PI order, as given in the support
//
// The support is the list of PIs on which the given PO depend on.
//
// pi_vals is a map of PI-ObjID to its value.
// For e.g., to set the 0th pi as 0, find the 0th PIs objid and map it to 0.
// 0 for 0, 1 for 1 and  2 for dont-care
abc::word get_truth_table_for_po (
    abc::Gia_Man_t *gia, const int po_id,
    const std::vector<int> &pi_list,            // list of PiObjIds
    const std::unordered_map<int, int> &pi_vals // map of values for PiObjIds
    )
{

  assert ( po_id < abc::Gia_ManPoNum ( gia ) );
  assert ( pi_vals.size () <= abc::Gia_ManPiNum ( gia ) );
  assert ( pi_list.size () <= 16 );

  if ( pi_list.empty () ) // PO is a const
  {
    if ( abc::Gia_ManPoIsConst0 ( gia, po_id ) ) return abc::word ( 0 );
    if ( abc::Gia_ManPoIsConst1 ( gia, po_id ) ) return ~abc::word ( 0 );
    assert ( false );
  }

  std::vector<int> dummy;
  return get_truth_table_for_po ( gia, po_id, pi_list, dummy, pi_vals );
}


std::vector<int> cat_pi_list (
  const std::vector<int> &non_common_pi_list, // list of non-common PiObjIds
  const std::vector<int> &shared_pi_list      // PiObjIds that are common
  )
{
  // Now, both giving me the same result. why?
  // Probably it doesnt matter which order PIs are.
  // Only const has to be continuous, else curr_var will be wrong.
  //
  // Note: below one is wrong order of PIs!
  // for common PIs to be in the MSB positions
  auto pi_list = non_common_pi_list;
  for ( auto &elem : shared_pi_list )
  {
    pi_list.emplace_back ( elem );
  }
  //auto pi_list = shared_pi_list; // correct order non-common in LSB positions
  //for ( auto &elem : non_common_pi_list )
  //{
  //  pi_list.emplace_back ( elem );
  //}
  return pi_list;
}


// Finds the local function tt for po, for a given support and
// pre-defined overridden values for its PIs.
// PIs can be overridden with a user defined value using the map pi_vals.
// The tt is expressed in terms of the input PI order, as given in the support
//
// The support is the list of PIs on which the given PO depend on.
//
// pi_vals is a map of PI-ObjID to its value.
// For e.g., to set the 0th pi as 0, find the 0th PIs objid and map it to 0.
// 0 for 0, 1 for 1 and  2 for dont-care
//
// all common PIs will be computed as appreaing in the MSB positions
// all non-common PIs will be computed as appearing in LSB positions
// these two list should be mutually exclusive
abc::word get_truth_table_for_po (
    abc::Gia_Man_t *gia, const int po_id,
    const std::vector<int> &non_common_pi_list, // list of non-common PiObjIds
    const std::vector<int> &shared_pi_list,     // PiObjIds that are common
    const std::unordered_map<int, int> &pi_vals // map of values for PiObjId
    )
{
  assert ( po_id < abc::Gia_ManPoNum ( gia ) );
  assert ( pi_vals.size () <= abc::Gia_ManPiNum ( gia ) );

  auto pi_list = cat_pi_list ( non_common_pi_list, shared_pi_list );
  assert ( pi_list.size () <= 16 );
  
#define DEBUG_GET_TRUTH_TABLE_FOR_PO_PI_LIST
#ifdef DEBUG_GET_TRUTH_TABLE_FOR_PO_PI_LIST
  auto cpy = pi_list; // copy and check if unique
  std::sort ( std::begin ( cpy ), std::end ( cpy ) );
  assert ( std::adjacent_find ( cpy.begin (), cpy.end () ) == cpy.end () );
#endif

  if ( pi_list.empty () ) // PO is a const
  {
    if ( abc::Gia_ManPoIsConst0 ( gia, po_id ) ) return abc::word ( 0 );
    if ( abc::Gia_ManPoIsConst1 ( gia, po_id ) ) return ~abc::word ( 0 );
    assert ( false );
  }

  auto fanin = abc::Gia_ObjChild0 ( abc::Gia_ManPo ( gia, po_id ) );
  Yise::abc_vec_int leaves = Yise::to_abcVec ( pi_list );
  Yise::abc_vec_word tt_leaves ( pi_list.size () );
  abc::word tt_word = 0;


  
  // assign tt to leaves
  // abc::Gia_ObjComputeTruthTableStart ( gia, abc::Gia_ManPiNum ( gia ) );
  abc::Gia_ObjComputeTruthTableStart ( gia, pi_list.size () );
  int cur_var = 0;
  // the pi_list has to come in the order of the PI variables.
  // else the position of i wrt. pi_list will change
  auto pi_list_cpy = pi_list;
  //std::sort ( pi_list_cpy.begin (), pi_list_cpy.end () );

  //std::cout << "pi_list = " << to_str ( pi_list ) << " :: ";
  //std::cout << to_str ( pi_list, pi_vals ) << "\n";
  
  for ( auto i = 0; i < pi_list_cpy.size (); i++ )
  {
    auto itr = pi_vals.find ( pi_list_cpy[i] );
    if ( itr == pi_vals.end () )
    {
      assert ( false );
    }
    else if ( itr->second == 0 )
    {
      tt_leaves[i] = abc::word ( 0 );
    }
    else if ( itr->second == 1 )
    {
      tt_leaves[i] = ~abc::word ( 0 );
    }
    else
    {
      //tt_leaves[i] = ( *Gla_ObjTruthElem ( gia, i ) );

      tt_leaves[i] = ( *Gla_ObjTruthElem ( gia, cur_var ) );
      cur_var++;
    }
  }

  if ( pi_list.size () == 1 ) // depends only on 1 PI
  {
    tt_word = tt_leaves[0];
  }
  else
  {
    tt_word = *( abc::ObjComputeTruthTableCut ( gia, fanin, leaves, tt_leaves ) );
  }

  if ( abc::Gia_IsComplement ( fanin ) ) tt_word = ~tt_word;

  abc::Gia_ObjComputeTruthTableStop ( gia );
  return tt_word;
}


std::string get_permut_str ( const std::vector<char> &permPos )
{
  auto permPosCopy = permPos;
  std::reverse ( permPosCopy.begin (), permPosCopy.end () );
  std::string str;
  for ( auto &c : permPosCopy )
  {
    str = str + std::to_string ( static_cast<int> ( c ) );
  }
  return str;
}

std::string get_hex_str ( const abc::word &tt )
{
  std::stringstream str;
  str << std::hex << std::setw ( 16 ) << std::setfill ( '0' ) << tt;
  return str.str ();
}

// update the position of the permPos based on adjacent location.
// adjacent location/swap is always b/w i and i+1
void updatePermPos ( std::vector<char> &permPos, int pos )
{
  assert ( ( pos + 1 ) < permPos.size () );
  auto p1 = permPos[pos];
  auto p2 = permPos[pos + 1];
  permPos[pos] = p2;
  permPos[pos + 1] = p1;
}

// Returns a pair, NPN truth-table and array of permutation positions
// not maintaining the negation info for now
std::pair<abc::word, std::vector<char>> get_npn ( const abc::word &tt, const int nVars )
{
  assert ( nVars <= 6 );
  abc::word x = tt;     // work only on copy
  abc::word x_bar = ~x; // work on both and x and neg_x per loop iteration.
  abc::word minimal = std::min ( x, x_bar );
  PermInfo pi ( nVars );
  std::vector<char> permPos;
  for ( auto i = 0; i < nVars; i++ ) // initial permutation positions
  {
    permPos.emplace_back ( i );
  }

  int i, j;
  // swap both x and x_bar and take the minimum
  for ( i = pi.totalSwaps - 1; i >= 0; i-- )
  {
    auto pos = pi.swapArray[i];
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x, nVars, pi.swapArray[i] );
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x_bar, nVars, pi.swapArray[i] );
    minimal = std::min ( {x, x_bar, minimal} );
    updatePermPos ( permPos, pos );
  }

  // start negations and take the minimum
  for ( j = pi.totalFlips - 1; j >= 0; j-- )
  {
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x, nVars, 0 );
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x_bar, nVars, 0 );
    abc::Kit_TruthChangePhase_64bit ( &x, nVars, pi.flipArray[j] );
    abc::Kit_TruthChangePhase_64bit ( &x_bar, nVars, pi.flipArray[j] );
    minimal = std::min ( {x, x_bar, minimal} );
    updatePermPos ( permPos, 0 );

    for ( i = pi.totalSwaps - 1; i >= 0; i-- )
    {
      auto pos = pi.swapArray[i];
      abc::Kit_TruthSwapAdjacentVars_64bit ( &x, nVars, pi.swapArray[i] );
      abc::Kit_TruthSwapAdjacentVars_64bit ( &x_bar, nVars, pi.swapArray[i] );
      minimal = std::min ( {x, x_bar, minimal} );
      updatePermPos ( permPos, pos );
    }
  }
  return std::make_pair ( minimal, permPos );
}

// clang-format off

namespace buggy
{
int to_yiggg ( const std::vector<char> &permpos, const std::vector<char> &negpos )
{
  assert ( permpos.size () == 6 );
  assert ( negpos.size () == 6 );

  auto in0 = ( static_cast<int> ( permpos[0] ) << 1 ) |
             ( static_cast<int> ( negpos[0] ) & 1 );
  auto in1 = ( static_cast<int> ( permpos[1] ) << 1 ) |
             ( static_cast<int> ( negpos[1] ) & 1 );
  auto in2 = ( static_cast<int> ( permpos[2] ) << 1 ) |
             ( static_cast<int> ( negpos[2] ) & 1 );
  auto in3 = ( static_cast<int> ( permpos[3] ) << 1 ) |
             ( static_cast<int> ( negpos[3] ) & 1 );
  auto in4 = ( static_cast<int> ( permpos[4] ) << 1 ) |
             ( static_cast<int> ( negpos[4] ) & 1 );
  auto in5 = ( static_cast<int> ( permpos[5] ) << 1 ) |
             ( static_cast<int> ( negpos[5] ) & 1 );
  return Yise::make_tt_encoding ( in0, in1, in2, in3, in4, in5 );
  
}


// clang-format on

// based on NPN computation in ABC
// negation has some bug. Debug later
void add_to_tt_map ( const int num_const_msbs, const abc::word &tt,
                     std::unordered_map<abc::word, int> &tt_map,
                     std::unordered_map<int, int> &pi_vals )
{

  const auto nConsts = std::count_if (
      pi_vals.begin (), pi_vals.end (),
      [&]( const std::pair<int, int> &i ) { return i.second < 2; } );
  assert ( num_const_msbs == nConsts ); // chumma!
  assert ( num_const_msbs <= 6 && num_const_msbs >= 0 );
  const auto nVars = 6;
  abc::word x = tt;

  // working on xbar affects consts. Hence can't do
  PermInfo pi ( nVars );
  std::vector<char> permPos;  // permutation of positions
  std::vector<char> negPos_x; // negation of positions for x

  auto check_permPos = [&]() {
    for ( auto i = 0; i < 6; i++ )
    {
      assert ( permPos[i] < 16 || permPos[i] >= 0 );
    }
  };

  for ( auto i = 0; i < 6; i++ ) // initial permutation positions
  {
    if ( pi_vals[i+1] == 0  ) // value 6 shows its a constant
      permPos.emplace_back ( 7 );
    else if ( pi_vals[i+1] == 1  )
      permPos.emplace_back ( 6 );
    else 
      permPos.emplace_back ( i );
  }
  check_permPos ();

  for ( auto i = 0; i < 6; i++ ) // initial negation positions
  {
    if ( pi_vals[i+1] == 0 )
      negPos_x.emplace_back ( 1 );
    else 
      negPos_x.emplace_back ( 0 );
  }
  auto flipNegPos = [&]( int pos ) {
    assert ( pos < nVars );
    if ( negPos_x[pos] == 0 )
      negPos_x[pos] = 1;
    else
      negPos_x[pos] = 0;
  };

// update the position of the permPos based on adjacent location.
// adjacent location/swap is always b/w i and i+1
  auto  swapPermPos = [&] ( int pos )
    {
      assert ( ( pos + 1 ) < permPos.size () );
      assert ( ( pos + 1 ) < nVars );
      auto p1 = permPos[pos];
      auto p2 = permPos[pos + 1];
      permPos[pos] = p2;
      permPos[pos + 1] = p1;
    };
  
  
  auto print_perm = [&] ()
  {
    //return;
    auto cpy = permPos;
    std::reverse ( cpy.begin (), cpy.end () );
    auto neg_cpy = negPos_x;
    std::reverse ( neg_cpy.begin (), neg_cpy.end () );
    //std::cout << "Pi_vals = ";
    //for ( auto &pi : pi_vals )
    //{
    //  std::cout << pi.first << ":" << pi.second << " ";
    //}
    std::cout << "x = " << axekit::get_hex_str ( x );
    std::cout << "\n";
    std::cout << "Perm = ";
    for ( auto i = 0; i < cpy.size (); i++ )
    {
      //std::cout << axekit::get_hex_str ( cpy[i] ) << ":";
      std::cout << (int)cpy[i] << ":";
    }
    std::cout <<"\n";
    std::cout << " Neg = ";
    for ( auto &n : neg_cpy )
    {
      std::cout << (int)n << ":";
    }
    std::cout <<"\n";
    std::cout << " Enc = "
    << axekit::get_hex_str ( to_yiggg ( permPos, negPos_x ) ) << "\n";
  };
  
  auto check_x = [&] ()
  {
    if (x == 0x7171717171717171 )
    {
      print_perm ();
      std::exit ( 0 );
    }
  };
  
  check_permPos ();
  // assert ( permPos.size () == nVars );
  tt_map[x] = to_yiggg ( permPos, negPos_x );
  std::cout << "=================== START ===================0\n";
  print_perm ();
  int i, j;
  for ( i = pi.totalSwaps - 1; i >= 0; i-- )
  {
    auto pos = pi.swapArray[i];
    swapPermPos ( pos );
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x, nVars, pi.swapArray[i] );
    tt_map[x] = to_yiggg ( permPos, negPos_x );
    check_x ();
  }

  // start negations
  for ( j = pi.totalFlips - 1; j >= 0; j-- )
  {
    swapPermPos ( 0 );
    flipNegPos ( pi.flipArray[j] );
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x, nVars, 0 );
    abc::Kit_TruthChangePhase_64bit ( &x, nVars, pi.flipArray[j] );
    tt_map[x] = to_yiggg ( permPos, negPos_x );
    check_x ();
    
    for ( i = pi.totalSwaps - 1; i >= 0; i-- )
    {
      auto pos = pi.swapArray[i];
      swapPermPos ( pos );
      abc::Kit_TruthSwapAdjacentVars_64bit ( &x, nVars, pi.swapArray[i] );
      tt_map[x] = to_yiggg ( permPos, negPos_x );
      check_x ();
    }
  }
}
} 
}

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
