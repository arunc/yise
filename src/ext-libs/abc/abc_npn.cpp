/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : abc_npn.cpp
 * @brief  : NPN functions for ABC ported from axekit
 */
//-------------------------------------------------------------------------------

#include "abc_npn.hpp"

#include <algorithm>
#include <sstream>
#include <fstream>

#include <utils/common_utils.hpp>

namespace axekit
{
namespace npn
{

std::string to_rev ( const std::string &str )
{
  auto cpy = str;
  std::reverse ( cpy.begin (), cpy.end () );
  return cpy;
}

std::string to_str ( const std::array<int, 6> &pos )
{
  std::stringstream str;
  str << "{ a:" << pos[0]
      << " b:" << pos[1]
      << " c:" << pos[2]
      << " d:" << pos[3]
      << " e:" << pos[4]
      << " f:" << pos[5]
      << " }";
  return str.str ();
}

std::string to_str (
  const Permutation_t &perm,
  const std::unordered_map<int, int> &pi_vals
  )
{
  auto to_y = [&] ( int i )
    {
      if ( i == 1 ) return 'a'; 
      if ( i == 2 ) return 'b'; 
      if ( i == 3 ) return 'c'; 
      if ( i == 4 ) return 'd'; 
      if ( i == 5 ) return 'e'; 
      if ( i == 6 ) return 'f';
      return 'm';
    };

  
  std::stringstream str;
  str << "{ ";

  for ( const auto &p : perm )
  {
    auto e = p.first + 1; 
    auto v = pi_vals.find ( e );
    assert ( v != pi_vals.end () );
    std::string vs = " x";
    if ( v->second == 1 )
    {
      //assert ( !p.second ); // should not negate a constant
      
      vs = p.second ? " 0" : " 1";
    }
    else if ( v->second == 0 )
    {
      //assert ( !p.second ); // should not negate a constant
      vs = p.second ? " 1" : " 0";
    }
    else if ( p.second )
    {
      vs = "~x";
    }
    
    str << to_y ( e ) << ":" << vs << " ";
  }
  str <<"}";
  
  return str.str ();
}

std::string to_str ( const std::vector<int> &list )
{
  std::string str = "{";
  for ( const auto &e : list )
  {
    str = str + std::to_string ( e ) + " ";
  }
  str = str + "}";
  return str;
}

std::string to_str ( const Permutation_t &perm )
{
  std::string str;
  for ( const auto &e : perm )
  {
    std::string sign = e.second ? "~" : "";
    str = str + sign + std::to_string ( e.first ) + " "; 
  }
  return str;
}


std::string to_str ( const abc::word &tt )
{
  return get_hex_str ( tt );
}


std::string to_str ( const std::unordered_map<int, int> &pi_vals )
{
  std::string str;
  for ( auto i = 0; i < pi_vals.size (); i++ )
  {
    auto obj_id = i+1;
    assert ( pi_vals.find ( obj_id ) != pi_vals.end () );
    std::string v = "x";
    if ( pi_vals.find ( obj_id )->second == 0 ) v = "0";
    if ( pi_vals.find ( obj_id )->second == 1 ) v = "1";
    str = str + v;
  }
  std::reverse ( str.begin (),  str.end () );
  return to_rev ( str );
}


template<typename F>
void foreach_negation (
  const abc::word &tt,
  const PermInfo &pi,
  Permutation_t &perm,
  F f
  )
{

  abc::word x = tt;
  abc::word x_bar = ~tt;

  // this position is always from LSB, irresepective of pi_vals and pi_list
  // if there are non-permutable vars, they should be in MSB position
  // of p and PermInfo should have created taking this into account
  auto flipNegPos = [&] ( Permutation_t &p, int pos )
  {
    assert ( pos < p.size () );
    p[pos].second = !p[pos].second;
  };

  f ( perm, false, x  ); 
  f ( perm, true, x_bar  );
  
  for ( auto j = pi.totalFlips - 1; j >= 0; j-- )
  {
    abc::Kit_TruthChangePhase_64bit ( &x, perm.size (), pi.flipArray[j] );
    abc::Kit_TruthChangePhase_64bit ( &x_bar, perm.size (), pi.flipArray[j] );
    flipNegPos ( perm, pi.flipArray[j] );
    f ( perm, false, x  ); 
    f ( perm, true, x_bar  ); 
  }

  flipNegPos ( perm, pi.varN - 1 );
}


Permutation_t identity_permutation ( const std::vector<int> &pi_list )
{
  Permutation_t perm;
  for ( auto i = 0; i < pi_list.size (); i++ ) // initial permutation positions
  {
    auto pi_id = pi_list[i] - 1;
    auto p = std::make_pair ( pi_id, false ); 
    perm.emplace_back ( p );
  }
  return perm;
}
	 

template<typename F>
void foreach_permutation (
  const abc::word &tt,
  const PermInfo &pi,
  Permutation_t &perm,
  F f
  )
{

  abc::word x = tt;
  auto updatePermPos = [&] ( Permutation_t &p, int pos )
  {
    assert ( ( pos + 1 ) < p.size () );
    auto p1 = p[pos];
    auto p2 = p[pos + 1];
    p[pos] = p2;
    p[pos + 1] = p1;
  };

  f ( perm, x ); 

  for ( auto i = pi.totalSwaps - 1; i >= 0; i-- ) // all initial permutations
  {
    abc::Kit_TruthSwapAdjacentVars_64bit ( &x, perm.size (), pi.swapArray[i] );
    updatePermPos ( perm, pi.swapArray[i] );
    f ( perm, x  ); 
  }

  updatePermPos ( perm, 0 );
}

// foreach_npn is really foreach_permutation_negation ();
template<typename F>
void foreach_npn (
  const abc::word &tt,
  const int n, // total vars
  const int k, // permutation vars
  Permutation_t &perm,
  F f
  )
{
  if ( k == 0 ) return; // nothing to permute or negate. all constants

  PermInfo pi ( k );
  
  foreach_negation (
    tt, pi, perm, 
    [&] ( Permutation_t &perm, bool po_negation, const abc::word &nptt )
    {
      f ( perm, po_negation, nptt );
    }
    );
  

  //foreach_permutation (
  //  tt, pi, perm,
  //  [&] ( Permutation_t &perm,  const abc::word &ptt )
  //  {
  //    foreach_negation (
  //	ptt, pi, perm, 
  //	[&] ( Permutation_t &perm, bool po_negation, const abc::word &nptt )
  //	{
  //	  f ( perm, po_negation, nptt );
  //	}
  //	);
  //
  //  }
  //  );

}



// find all the truthtables with constants assigned to respective inputs
// we need some gray code to do this. 
template<typename F>
void foreach_const_pi_tt (
  abc::Gia_Man_t *gia,
  const bool konst, // either 0 or 1
  const std::unordered_map<int, int> &orig_pi_vals,
  const std::vector<int> &orig_pi_list, // all pis, instead of computing each time
  F f
  )
{
  // only for yise, hence single output. We dont consider multiple outputs here
  assert ( abc::Gia_ManPoNum ( gia ) == 1 );
  assert ( orig_pi_list.size () == 6 ); // yise specific
  
  // we assume the entire PI vars of gia is given a value in orig_pi_vals.
  // else this routine will not work -> we take pos + 1 as PiObjId
  assert ( abc::Gia_ManPiNum ( gia ) == orig_pi_vals.size () );
  //assert ( abc::Gia_ManPiNum ( gia ) == orig_pi_list.size () );
  
  const auto konst_bar = ( konst ) ? false : true;
  auto pi_vals = orig_pi_vals; // work in a copy.
  auto pi_list = orig_pi_list; // work in a copy.

  
  const PermInfo pi ( abc::Gia_ManPiNum ( gia ) ); // instead of negation lets use const
  // flip is between normal-var and konst in pi_vals
  auto flipConstPos = [&] ( std::unordered_map<int, int> &pi_vals, int pos )
  {
    auto obj_id = pos + 1;
    assert ( pi_vals.find ( obj_id ) != pi_vals.end () );
    if ( pi_vals[obj_id] == konst_bar ) return false; // leave pre-assigned const as it is
    if ( pi_vals[obj_id] == konst )
      pi_vals[obj_id] = 2; // don't care
    else if ( pi_vals[obj_id] == 2 ) 
      pi_vals[obj_id] = konst; // put our const
    else
      assert ( false );  // should never hit

    return true;
  };

  // all constants moved to MSB, permutation not allowed in constants
  std::vector<int> const_pis;
  std::vector<int> partitioned_pis;

  auto partitionPiList = [&] ()
  {
    pi_list = orig_pi_list;
    partitioned_pis = orig_pi_list;
    return;
    
    pi_list.clear ();
    const_pis.clear ();
    for ( const auto &pi : orig_pi_list )
    {
      if ( pi_vals[pi] == 2 )
	pi_list.push_back ( pi );
      else
	const_pis.push_back ( pi );
    }
    partitioned_pis = axekit::cat_pi_list ( pi_list, const_pis );
  };
  
  // direct one without any const substitution
  partitionPiList ();
  auto tt = axekit::get_truth_table_for_po ( gia, 0, pi_list, const_pis, pi_vals );
  f ( tt, -1, pi_vals, partitioned_pis );
  for ( auto j = pi.totalFlips - 1; j >= 0; j-- )
  {
    auto flipped = flipConstPos ( pi_vals, pi.flipArray[j] );
    if ( !flipped ) continue;

    partitionPiList (); // we put a const, we move it to MSB
    auto tt = axekit::get_truth_table_for_po ( gia, 0, pi_list, const_pis, pi_vals );
    f ( tt, pi.flipArray[j], pi_vals, partitioned_pis );
  }
  
}

// we deal with only single output Y function
// none of the PIs should be assigned with a const.
template<typename F>
void foreach_const_pi_combination_tt (
  abc::Gia_Man_t *gia,
  F f
  )
{
  assert ( abc::Gia_ManPoNum ( gia ) == 1 );
  assert ( abc::Gia_ManPiNum ( gia ) == 6 );
  const auto const0 = false;
  const auto &pi_vals = axekit::create_pi_vals ( gia );
  auto pi_list = axekit::get_pis_for_po ( gia, 0 );
  assert ( pi_list.size () == 6 ); // none of the PI is a constant
  std::sort ( pi_list.begin (), pi_list.end () ); // Get the PIs in correct order
  
  foreach_const_pi_tt (
    gia, const0, pi_vals, pi_list,
    [&] ( const abc::word &tt_k0, int pos,
	  const std::unordered_map<int, int> &new_pi_vals_k0,
	  const std::vector<int> &new_pi_list_k0
      )
    {
      foreach_const_pi_tt (
	gia, !const0, new_pi_vals_k0, new_pi_list_k0,
	[&] ( const abc::word &tt, int pos,
	      const std::unordered_map<int, int> &new_pi_vals,
	      const std::vector<int> &new_pi_list )
	{
	  f ( tt, new_pi_vals, new_pi_list );
	}
	);
    }
    );
  
}


// goes through all pi-permutations, pi-negations, also with pi-constants
// Y function is self dual, so we ignore all po_negation
// we deal with only single output for now
template<typename F>
void foreach_tt_combination (
  abc::Gia_Man_t *gia,
  F f
  )
{
  assert ( abc::Gia_ManPoNum ( gia ) == 1 );

  foreach_const_pi_combination_tt ( // loop for assigning PI consts
    gia, 
    [&] ( const abc::word &tt,
	  const std::unordered_map<int, int> &pi_vals,
	  const std::vector<int> &pi_list
      )
    {
      auto k = std::count_if (
	pi_vals.begin (), pi_vals.end (),
	[&] ( const std::pair<int, int> &e ) { return  e.second == 2; }
	);

      auto initial_perm = identity_permutation ( pi_list ); // w.r.t the order in pi_list

      foreach_npn ( // loop for permutations + negations
	tt, pi_vals.size (), k, initial_perm,
	[&] ( const Permutation_t &perm, bool po_negation, const abc::word &cur_tt )
	{
	  if ( po_negation ) return; // Y function is self-dual, avoid repetition

	  
	  f ( cur_tt, pi_vals, pi_list, perm );
	}
	);
    }
    );

}

//// same as above with unique truthtable
//template<typename F>
//void foreach_unique_tt_combination (
//  abc::Gia_Man_t *gia,
//  F f
//  )
//{
//  assert ( abc::Gia_ManPoNum ( gia ) == 1 );
//  std::unordered_set<abc::word> tt_set;
//  
//  foreach_tt_combination (
//    gia, 
//    [&] ( const abc::word &tt, const Permutation_t &perm,
//	  const std::unordered_map<int, int> &pi_vals )
//    {
//      if ( tt_set.find ( tt ) != tt_set.end () ) return; // pick only the unqiue
//      tt_set.emplace ( tt );
//      f ( tt, perm, pi_vals );
//    }
//    );
//}


// stable function
int to_yise_encoding ( const Permutation_t &perm,
		       const std::unordered_map<int, int> &pi_vals )
{
  // loop: first const and then permutation

  const auto k0_enc = 13; // const0 = 1101 = 0xD 
  const auto k1_enc = 12; // const1 = 1100 = 0xC
  std::array<int, 6> pos = {0, 2, 4, 6, 8, 10}; // positions for a normal Y gate
  
  for ( auto &v : pi_vals ) // 1. assign all consts
  {
    auto obj_id = v.first;
    const auto val = v.second;
    const auto pos_id = obj_id - 1;
    assert ( pos_id < pos.size () );
    if ( val == 0 )
      pos[pos_id] = k0_enc;
    else if ( val == 1 )
      pos[pos_id] = k1_enc;
    else if ( val != 2 )
      assert ( false );
  }

  auto flipEnc = [&] ( int enc )
  {
    if ( 1 == ( enc & 1 ) )
      return (enc >> 1 ) << 1; // or & FFFFE
    else
      return enc | 1;
  };
  auto isConst = [&] ( const int val )
  {
    if ( val == k0_enc ) return true;
    if ( val == k1_enc ) return true;
    return false;
  };

  
  std::array<int, 6> permuted_pos = {-1, -1, -1, -1, -1, -1};
  for ( auto i = 0; i < 6; i++ )
  {
    permuted_pos[i] = pos[perm[i].first];
    // constant cannot have permutation
    //if ( isConst ( permuted_pos[i] ) )  assert ( !perm[i].second );

    if ( perm[i].second )
      permuted_pos[i] = flipEnc ( permuted_pos[i] );
  }

  assert ( permuted_pos[0] >= 0 && permuted_pos[0] <= 0xD );
  assert ( permuted_pos[1] >= 0 && permuted_pos[1] <= 0xD );
  assert ( permuted_pos[2] >= 0 && permuted_pos[2] <= 0xD );
  assert ( permuted_pos[3] >= 0 && permuted_pos[3] <= 0xD );
  assert ( permuted_pos[4] >= 0 && permuted_pos[4] <= 0xD );
  assert ( permuted_pos[5] >= 0 && permuted_pos[5] <= 0xD );

  return Yise::make_tt_encoding ( permuted_pos );
  
}


template<typename F>
void foreach_simulated_tt (
  abc::Gia_Man_t *gia,
  F f
  )
{
  std::unordered_set<std::string> uniq_tt_list;
  assert ( abc::Gia_ManPoNum ( gia ) == 1 );

  foreach_tt_combination (
    gia,
    [&] ( const abc::word &tt,
  	  const std::unordered_map<int, int> &pi_vals,
  	  const std::vector<int> &pi_list,
  	  const Permutation_t &perm
      )
    {
      auto nvar = std::count_if (
	pi_vals.begin (), pi_vals.end (),
	[&] ( const std::pair<int, int> vv ) { return vv.second == 2; }
	);
      if ( nvar > 3 ) return;
     const auto enc = to_yise_encoding ( perm, pi_vals );
     const auto sim_tt = encoding_to_simulation_tt ( enc );
     if ( uniq_tt_list.find ( sim_tt ) == uniq_tt_list.end () )
     {
       uniq_tt_list.emplace ( sim_tt );
       f ( sim_tt, perm, pi_vals, enc, to_str ( tt ) );
     }
      
  
    }
    );
}

void test_truth_table ( abc::Gia_Man_t *gia )
{
  assert ( abc::Gia_ManPoNum ( gia ) == 1 );
  assert ( abc::Gia_ManPiNum ( gia ) == 6 );

  foreach_tt_combination (
    gia,
    [&] ( const abc::word &tt,
  	  const std::unordered_map<int, int> &pi_vals,
  	  const std::vector<int> &pi_list,
  	  const Permutation_t &perm
      )
    {

      if ( tt != 0x8a8a8a8a8a8a8a8a ) return;

      const auto enc = to_yise_encoding ( perm, pi_vals );
      std::cout << "TT: " << to_str ( tt ) 
		<< " :: " << to_str ( perm, pi_vals )
		<< " :: " << to_str ( enc )
		<< "\n";
      
      //std::exit ( 0 );
      
    }
    );
  





  return;




  
  std::ofstream ofs ( "all_3var_tt_combinations_wo_permut.sim" );

  long count = 0;
  foreach_simulated_tt (
    gia,
    [&] ( const std::string &sim_tt,
	  const Permutation_t &perm,
	  const std::unordered_map<int, int> &pi_vals,
	  const int enc,
	  const std::string &orig_tt
      )
    {
      ofs << "TT: " << sim_tt
	  << " :: " << to_str ( perm, pi_vals )
	  << " :: " << to_str ( enc )
	  << " :: " << orig_tt
	  << "\n";

      count++;
      if  ( count % 500 == 0 )
      {
	std::cout << "TT: " << sim_tt
		  << " :: " << to_str ( perm, pi_vals )
		  << " :: " << to_str ( enc )
		  << "\n";
      }

    }
    );
  
  return;
  

  
  print_const_pi_permutations ( gia );
  return;

  assert ( abc::Gia_ManPoNum ( gia ) == 1 );

  foreach_tt_combination (
    gia,
    [&] ( const abc::word &tt,
  	  const std::unordered_map<int, int> &pi_vals,
  	  const std::vector<int> &pi_list,
  	  const Permutation_t &perm
      )
    {

      if ( tt != 0x57ff57ff57ff57ff ) return;
      //if ( to_yise_encoding ( perm, pi_vals )  != 0x0000000000adc420 )
      //{
      //	return;
      //}

      const auto enc = to_yise_encoding ( perm, pi_vals );
      std::cout << "TT: " << to_str ( tt ) 
		<< " :: " << to_str ( perm, pi_vals )
		<< " :: " << to_str ( enc )
		<< "\n";
      
      
    }
    );
  
  
  return;
  

  Permutation_t perm = { {0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0} };
  using pi_list_vals_t = std::pair<std::vector<int>, std::unordered_map<int, int>>;
  std::unordered_map<abc::word, pi_list_vals_t> unique_list;
  foreach_const_pi_combination_tt (
    gia,
    [&] ( const abc::word &tt,
	  const std::unordered_map<int, int> &pi_vals,
	  const std::vector<int> &pi_list
      )
    {
      unique_list[tt] = {pi_list, pi_vals};
    }
    );

  for ( const auto &e : unique_list )
  {
    std::cout << "TT: " << to_str ( e.first ) 
	      << " :: " << to_str ( perm, e.second.second )
	      << " :: " << to_str ( to_yise_encoding ( perm, e.second.second ) )
	      << "\n";
    
  }

  
  return;
  

  const auto const0 = false;
  const auto const1 = true;
  const auto &pi_vals = axekit::create_pi_vals ( gia );
  const auto &pi_list = axekit::get_pis_for_po ( gia, 0 );

  int i = 0;

  return;

  /*
  foreach_unique_tt_combination (
    gia, 
    [&] ( const abc::word &tt, const Permutation_t &perm,
	  const std::unordered_map<int, int> &pi_vals )
    {
      auto perm_str = to_str ( perm );
      std::reverse ( perm_str.begin (), perm_str.end () );
      std::cout << "TT: " << to_str ( tt )
		<< " pi_vals: " << to_str ( pi_vals )
		<< " pi_perm: " << perm_str
		<< "  enc: " << to_str ( to_yise_encoding ( perm, pi_vals ) )
		<< "\n";
    }
    );
  */

  return;



  foreach_const_pi_combination_tt (
    gia, 
    [&] ( const abc::word &tt,
	  const std::unordered_map<int, int> &pi_vals,
	  const std::vector<int> &pi_list
      )
    {
      std::cout << "TT: " << to_str ( tt )
		<< " pi_vals: " << to_str ( pi_vals ) << "\n";
    }
    );


  return;
  

  foreach_const_pi_tt (
    gia, const0, pi_vals, pi_list,
    [&] ( const abc::word &tt_a, int pos_a,
	  const std::unordered_map<int, int> &new_pi_vals_a,
	  const std::vector<int> &new_pi_list_a
      )
    {
      foreach_const_pi_tt (
	gia, ~const0, new_pi_vals_a, new_pi_list_a,
	[&] ( const abc::word &tt_b, int pos_b,
	      const std::unordered_map<int, int> &new_pi_vals_b,
	      const std::vector<int> &new_pi_list_b
	  )
	{
	  
	  std::cout << "TT: " << to_str ( tt_b )
		    << " pi_vals: " << to_str ( new_pi_vals_b ) << "\n";
	  
	}
	);
    }
    );


  return;
  
  
}


// stable function
void print_const_pi_permutations ( abc::Gia_Man_t *gia )
{
  assert ( abc::Gia_ManPoNum ( gia ) == 1 );

  Permutation_t perm = { {0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0} };
  using pi_list_vals_t = std::pair<std::vector<int>, std::unordered_map<int, int>>;
  std::unordered_map<abc::word, pi_list_vals_t> unique_list;
  foreach_const_pi_combination_tt (
    gia,
    [&] ( const abc::word &tt,
	  const std::unordered_map<int, int> &pi_vals,
	  const std::vector<int> &pi_list
      )
    {
      unique_list[tt] = {pi_list, pi_vals};
    }
    );

  for ( const auto &e : unique_list )
  {
    const auto enc = to_yise_encoding ( perm, e.second.second );
    std::cout << "TT: " << to_str ( e.first ) 
	      << " :: " << to_str ( perm, e.second.second )
	      << " :: " << to_str ( enc )
	      << "\n";

    const auto tt = to_str ( e.first );
    const auto sim_tt = encoding_to_simulation_tt ( enc );
    if ( tt != sim_tt ) 
    {
      std::cout << "TruthTable failed!\n";
      std::cout << "   orig tt: " << tt << "\n"
		<< "   sim  tt: " << sim_tt << "\n";
      assert ( false );
    }
    
  }

}

std::string to_verilog_str ( const int yise_enc, const std::string &top ) 
{
  std::stringstream str;
  const auto k1 = 6; 
  std::array<std::string, 6> arr = {"a", "b", "c", "d", "e", "f"};
  std::array<std::string, 6> vars;

  str << "module " << top << " ( a, b, c, d, e, f, y );\n";
  str << "   input a, b, c, d, e, f;\n";
  str << "   output y;\n\n";
  str << "   wire   m1, m2, m3;\n";
  str << "   wire   aa, bb, cc, dd, ee, ff;\n\n";

  for ( auto i = 0; i < 6; i++ )
  {
    auto t = Yise::leaf_permutation ( yise_enc, i );
    auto s = Yise::leaf_negation ( yise_enc, i );
    if ( t == k1 )
    {
      vars[i] = s ? "0" : "1";
    }
    else
    {
      vars[i] = s ? ( std::string ("~") + arr[t] ) : arr[t];
    }
  }

  str << "   assign aa = " << vars[0] << ";\n";
  str << "   assign bb = " << vars[1] << ";\n";
  str << "   assign cc = " << vars[2] << ";\n";
  str << "   assign dd = " << vars[3] << ";\n";
  str << "   assign ee = " << vars[4] << ";\n";
  str << "   assign ff = " << vars[5] << ";\n";
  

  str << "   assign m1 = ( aa & bb ) | ( bb & cc ) | ( cc & aa );\n";
  str << "   assign m2 = ( bb & dd ) | ( dd & ee ) | ( ee & bb );\n";
  str << "   assign m3 = ( cc & ee ) | ( ee & ff ) | ( ff & cc );\n";
  str << "   assign y = ( m1 & m2 ) | ( m2 & m3 ) | ( m3 & m1 ); \n\n";

  str << "endmodule\n";
  
  return str.str ();
}


void write_to_file ( const std::string &file, const std::string &str )
{
  std::ofstream ofs ( file );
  ofs << "\n";
  ofs << str;
  ofs << "\n";
}


std::string encoding_to_simulation_tt ( // simulate and find the truthtable
  const int enc,       // Yise encoding
  bool cleanup         // cleanup the temporary files or note
  )
{
  const std::string top = "dingo";
  auto vlog =  to_verilog_str ( enc, top );
  auto vlog_file = "./" + top + ".v"; 
  auto aig_file = "./" + top + ".aig"; 
  write_to_file ( vlog_file, vlog );
  
  std::stringstream cmd_str;
  cmd_str << "vlogToAiger  " << "./" << top << ".v" << "  " << top;
  Yise::exec ( cmd_str.str () );


  std::string cmd_sim = "truthTableFromSimulation " + aig_file;
  auto res = Yise::exec ( cmd_sim );

  if ( cleanup )
  {
    //Yise::delete_work_dir ( vlog_file );
    //Yise::delete_work_dir ( aig_file );
    
  }

  if ( !res.empty () && res[res.length () - 1] == '\n') {
    res.erase ( res.length () - 1 );
  }

  return res;
}

abc::Gia_Man_t *encoding_to_gia (
  const std::string &top, // Top module of Gia will be in this top
  const int enc,       // Yise encoding
  bool cleanup         // cleanup the temporary files or note
  )
{

  auto vlog =  to_verilog_str ( enc, top );
  auto vlog_file = "./" + top + ".v"; 
  auto aig_file = "./" + top + ".aig"; 
  write_to_file ( vlog_file, vlog );
  
  std::stringstream cmd_str;
  cmd_str << "vlogToAiger  " << "./" << top << ".v" << "  " << top;
  Yise::exec ( cmd_str.str () );

  auto gia = Yise::read_aiger ( aig_file );

  if ( cleanup )
  {
    //Yise::delete_work_dir ( vlog_file );
    //Yise::delete_work_dir ( aig_file );
    
  }
  return gia;
}


} // namespace npn
} // namespace axekit


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
