/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : abc_npn.hpp
 * @brief  : NPN functions for ABC ported from axekit
 */
//-------------------------------------------------------------------------------

#include "abc_truth.hpp"
#include "luckyInt.h"

#include <ext-libs/abc/abc_api.hpp>
#include <functions/truth/yig_truth.hpp>

#include <algorithm>
#include <array>
#include <iomanip>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>


namespace axekit
{
namespace npn
{
// Permutation_t is a list of Obj-Ids (PI or PI) and its corresponding negation
using Permutation_t = std::vector<std::pair <int, bool>>;
void test_truth_table ( abc::Gia_Man_t *gia );
void print_const_pi_permutations ( abc::Gia_Man_t *gia );

std::string to_verilog_str ( // verilog file for the given Yig encoding
  const int yise_enc, const std::string &top = "yy" );


abc::Gia_Man_t *encoding_to_gia ( // get a Gia corresponding to the Yig encoding
  const std::string &top, // Top module of Gia will be in this top
  const int enc,       // Yise encoding
  bool cleanup = true  // cleanup the temporary files or note
  );

std::string encoding_to_simulation_tt ( // simulate and find the truthtable
  const int enc,       // Yise encoding
  bool cleanup = true  // cleanup the temporary files or note
  );


} // namespace npn

} // namespace axekit


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
