/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : abc_truth.cpp
 * @brief  : Truth table manipulations for ABC ported from axekit
 */
//-------------------------------------------------------------------------------
#include "luckyInt.h"
#include <ext-libs/abc/abc_api.hpp>

#include <algorithm>
#include <sstream>
#include <unordered_map>
#include <utility>
#include <vector>


namespace abc
{
// these 2 functions have static linkage in Abc, define separately.
swapInfo *setSwapInfoPtr ( int vars );
void freeSwapInfoPtr ( swapInfo *x );
int nextSwap ( swapInfo *x );
int oneBitPosition ( int x, int size );
}

namespace axekit
{

abc::word get_truth_table_for_po (
    abc::Gia_Man_t *gia, const int po_id,
    const std::vector<int> &non_common_pi_list, // list of non-common PiObjIds
    const std::vector<int> &shared_pi_list,     // PiObjIds that are common
    const std::unordered_map<int, int> &pi_vals // map of values for PiObjId
    );
abc::word get_truth_table_for_po (
    abc::Gia_Man_t *gia, const int po_id,
    const std::vector<int> &pi_list,            // list of PiObjIds
    const std::unordered_map<int, int> &pi_vals // map of values for PiObjIds
    );
abc::word get_truth_table_for_po ( abc::Gia_Man_t *gia, const int po_id );
void assign_x_to_pi_vals ( std::unordered_map<int, int> &pi_vals );

std::unordered_map<int, int> create_pi_vals ( abc::Gia_Man_t *gia );
std::vector<int> get_pis_for_po ( abc::Gia_Man_t *gia, int po_id );

std::vector<int> cat_pi_list (
  const std::vector<int> &non_common_pi_list, // list of non-common PiObjIds
  const std::vector<int> &shared_pi_list      // PiObjIds that are common
  );

class PermInfo
{
public:
  PermInfo ( int nVars )
  {
    varN = nVars;
    flipCtr = 0;
    totalFlips = ( 1 << varN ) - 1;
    swapCtr = 0;
    totalSwaps = fact ( nVars ) - 1;
    flipArray = new int[totalFlips];
    swapArray = new int[totalSwaps];
    fillInSwapArray ();
    fillInFlipArray ();
  }
  ~PermInfo ()
  {
    delete[] flipArray;
    delete[] swapArray;
  }

  int varN;
  int *swapArray;
  int swapCtr;
  int totalSwaps;
  int *flipArray;
  int flipCtr;
  int totalFlips;

private:
  inline int fact ( int n )
  {
    return ( n == 1 || n == 0 ) ? 1 : fact ( n - 1 ) * n;
  }
  void fillInSwapArray ()
  {
    int counter = totalSwaps - 1;
    auto x = abc::setSwapInfoPtr ( varN );
    while ( abc::nextSwap ( x ) == 1 )
    {
      if ( x->positionToSwap1 < x->positionToSwap2 )
        swapArray[counter--] = x->positionToSwap1;
      else
        swapArray[counter--] = x->positionToSwap2;
    }
    abc::freeSwapInfoPtr ( x );
  }

  void fillInFlipArray ()
  {
    int i, temp = 0, grayNumber;
    for ( i = 1; i <= totalFlips; i++ )
    {
      grayNumber = i ^ ( i >> 1 );
      flipArray[totalFlips - i] = abc::oneBitPosition ( temp ^ grayNumber, varN );
      temp = grayNumber;
    }
  }
};

// clang-format off

std::string get_permut_str ( const std::vector<char> &permPos );
void print_hex ( const abc::word &tt_word );
std::string get_hex_str ( const abc::word &tt );
std::pair<abc::word, std::vector<char>>
  get_npn ( const abc::word &tt, const int nVars );
std::unordered_map<abc::word, int> all_permutations ( const abc::word &tt );
void add_to_tt_map ( const int num_const_msbs, const abc::word &tt,
                     std::unordered_map<abc::word, int> &tt_map,
                     std::unordered_map<int, int> &pi_vals );

void simple_test ( abc::word *x, int nVars );
void simple_compare_test ( const abc::word &x, const abc::word &y, int nVars );



}

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
