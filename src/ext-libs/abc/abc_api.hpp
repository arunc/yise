/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : abc_static.hpp
 * @brief  : abc api header + extra abc routines
 */

// todo: cleanup this file
//------------------------------------------------------------------------------
#ifndef ABC_API_HPP
#define ABC_API_HPP

#ifndef __cplusplus
#define __cplusplus
#endif

#include <cassert>
#include <iostream>
#include <string>
#include <vector>

#define LIN64
#include "misc/util/abc_global.h"

#include "abc.h"
#include "aig.h"
#include "gia.h"
#include "ioAbc.h"


namespace abc
{
// minimum stuff ported from "main.h"
typedef struct Abc_Frame_t_ Abc_Frame_t;
extern void Abc_Start ();
extern void Abc_Stop ();
extern Abc_Ntk_t *Abc_FrameReadNtk ( Abc_Frame_t *p );
extern Gia_Man_t *Abc_FrameReadGia ( Abc_Frame_t *p );
extern Abc_Frame_t *Abc_FrameGetGlobalFrame ();
}


// Put all the static linkage C functions here.
namespace abc
{
Aig_Man_t *Gia_ManToAig ( Gia_Man_t *p, int fChoices );
Gia_Man_t *Gia_ManFromAig ( Aig_Man_t *p );
Abc_Ntk_t *Abc_NtkFromAigPhase ( Aig_Man_t *pMan );
Aig_Man_t *Abc_NtkToDar ( Abc_Ntk_t *pNtk, int fExors, int fRegisters );
}


namespace Yise
{
bool start_abc ();
void stop_abc ();
bool is_abc_started ();

void write_verilog ( abc::Abc_Ntk_t *ntk, char *file );
void write_verilog ( abc::Gia_Man_t *gia, char *file );
void write_verilog ( abc::Abc_Ntk_t *ntk, const std::string &file );
void write_verilog ( abc::Gia_Man_t *gia, const std::string &file );

// TODO: get this routine properly
void print_ckt ( abc::Gia_Man_t *gia );
abc::Gia_Man_t *read_aiger ( const std::string &file );
abc::Abc_Ntk_t *convert_gia_to_ntk ( abc::Gia_Man_t *gia );


//------------------------------------------------------------------------------
//
template <typename T>
struct abc_vec_traits;

template <>
struct abc_vec_traits<int>
{
  typedef int type_name;
  typedef abc::Vec_Int_t *abc_vec_type;

  static abc_vec_type create ( int size )
  {
    return abc::Vec_IntStart ( size );
  }

  static void destroy ( abc_vec_type v )
  {
    abc::Vec_IntFree ( v );
  }

  static type_name entry ( abc_vec_type v, int i )
  {
    return abc::Vec_IntEntry ( v, i );
  }

  static type_name *entryp ( abc_vec_type v, int i )
  {
    return abc::Vec_IntEntryP ( v, i );
  }
};

template <>
struct abc_vec_traits<abc::word>
{
  typedef abc::word type_name;
  typedef abc::Vec_Wrd_t *abc_vec_type;

  static abc_vec_type create ( int size )
  {
    return abc::Vec_WrdStart ( size );
  }

  static void destroy ( abc_vec_type v )
  {
    abc::Vec_WrdFree ( v );
  }

  static type_name entry ( abc_vec_type v, int i )
  {
    return abc::Vec_WrdEntry ( v, i );
  }

  static type_name *entryp ( abc_vec_type v, int i )
  {
    return abc::Vec_WrdEntryP ( v, i );
  }
};

template <typename T>
class abc_vec
{
public:
  typedef typename abc_vec_traits<T>::abc_vec_type vec_type;

  abc_vec ( int size = 16 ) : v ( abc_vec_traits<T>::create ( size ) )
  {
  }

  abc_vec ( vec_type v ) : v ( v )
  {
  }

  abc_vec ( abc_vec &&other ) : v ( other.v )
  {
    other.v = nullptr;
  }

  abc_vec ( const abc_vec & ) = delete;
  abc_vec &operator= ( const abc_vec & ) = delete;

  abc_vec &operator= ( abc_vec &&other )
  {
    std::swap ( v, other.v );
    return *this;
  }

  T operator[] ( int i ) const
  {
    return abc_vec_traits<T>::entry ( v, i );
  }

  T &operator[] ( int i )
  {
    return *abc_vec_traits<T>::entryp ( v, i );
  }

  ~abc_vec ()
  {
    if ( v )
    {
      abc_vec_traits<T>::destroy ( v );
    }
  }

  operator vec_type () const
  {
    return v;
  }

private:
  vec_type v;
};

typedef abc_vec<int> abc_vec_int;
typedef abc_vec<abc::word> abc_vec_word;
//------------------------------------------------------------------------------

template <typename T>
abc_vec<T> to_abcVec ( const std::vector<T> &v )
{
  abc_vec<T> vv ( v.size () );

  for ( int i = 0; i < v.size (); i++ )
  {
    vv[i] = v[i];
  }

  return vv;
}


// abc::Vec_Int_t *to_abcVec(abc::Gia_Man_t *gia, const std::vector<int> &v,
// const int size)
// {
//   assert(v.size() == size);
//   auto vv = abc::Vec_IntStart(size);
//   int i = 0;
//   for (auto &elem : v)
//   {
//     vv->pArray[i++] = abc::Abc_Lit2Var(
//         abc::Gia_Obj2Lit(gia, abc::Gia_ManObj(gia, elem)));
//   }
//
//   return vv;
// }


} // Yise

#endif


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
