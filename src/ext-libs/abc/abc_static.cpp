/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : abc_static.cpp
 * @brief  : extra abc routines
 */

//------------------------------------------------------------------------------

#include "abc_api.hpp"


// Add all the static linkage ABC routines here
namespace abc
{
}


// If we are using Ntk, then mostly we need to start ABC.
// When we use Gia, somewhere in between this is taken care of (??)
// Also, many cases its not even necessary to start ABC explicitly.
namespace Yise
{

void write_verilog ( abc::Gia_Man_t *gia, char *file )
{
  auto kk = abc::Gia_ManToAig ( gia, 0 );
  abc::Aig_ManDumpVerilog ( kk, file );
}
void write_verilog ( abc::Gia_Man_t *gia, const std::string &file )
{
  write_verilog ( gia, (char *)file.c_str () );
}
void write_verilog ( abc::Abc_Ntk_t *origNtk, char *file )
{

  assert ( abc::Abc_NtkIsLogic ( origNtk ) );
  auto ntk = abc::Abc_NtkDup ( origNtk );
  if ( ntk == nullptr ) return;
  abc::Abc_NtkStrash ( ntk, 1, 1, 0 );
  abc::Abc_NtkCleanup ( ntk, 0 );
  ntk = abc::Abc_NtkStrash ( ntk, 1, 1, 0 );
  abc::Abc_AigCleanup ( (abc::Abc_Aig_t *)ntk->pManFunc );
  ntk = abc::Abc_NtkStrash ( ntk, 1, 1, 0 );
  auto dar = abc::Abc_NtkToDar ( ntk, 0, 0 ); // 8. network_to_gia()
  abc::Abc_NtkDelete ( ntk );
  auto gia = abc::Gia_ManFromAig ( dar );

  write_verilog ( gia, file );
}

void write_verilog ( abc::Abc_Ntk_t *ntk, const std::string &file )
{
  write_verilog ( ntk, (char *)file.c_str () );
}


bool start_abc ()
{
  abc::Abc_Start ();
  auto frame = abc::Abc_FrameGetGlobalFrame ();
  if ( frame == nullptr )
  {
    abc::Abc_Stop ();
    return false;
  }
  return true;
}

void stop_abc ()
{
  abc::Abc_Stop ();
}

bool is_abc_started ()
{
  if ( abc::Abc_FrameGetGlobalFrame () == nullptr ) return false;
  return true;
}

//------------------------------------------------------------------------------
// do the below in 2 steps. Else errors will not be easy to understand.
abc::Gia_Man_t *read_aiger ( const std::string &file )
{
  char name[] = "top";
  auto gia = abc::Gia_AigerRead ( (char *)file.c_str (), 1, 1, 1 );
  gia->pName = abc::Abc_UtilStrsav ( name );
  assert ( gia != nullptr );
  if ( gia->nRegs > 0 )
  {
    assert ( false && "Golden circuit is sequential!" );
  }
  return gia;
}

// TODO: rewrite the entire code based on &get in ABC.
abc::Abc_Ntk_t *convert_gia_to_ntk ( abc::Gia_Man_t *gia )
{

  /*
  // below is the actual procedure. But its a segfault.
  // hence taking this route.
  auto aigman = abc::Gia_ManToAig (gia, 0);
  auto ntk =  abc::Abc_NtkFromAigPhase ( aigman );
  */

  std::string ntk_file = "/tmp/tmp_aig.aig";
  abc::Gia_AigerWrite ( gia, (char *)ntk_file.c_str (), 1, 0 );
  auto ntk = abc::Io_ReadAiger ( (char *)ntk_file.c_str (), 1 );
  ntk = abc::Abc_NtkToLogic ( ntk );         // Convert to Logic form.
  ntk = abc::Abc_NtkStrash ( ntk, 1, 1, 0 ); // Strash
  abc::Abc_AigCleanup ( (abc::Abc_Aig_t *)ntk->pManFunc ); // Cleanup
  ntk = abc::Abc_NtkStrash ( ntk, 1, 1, 0 );               // Strash again
  ntk->pName = abc::Abc_UtilStrsav ( gia->pName );
  return ntk;
}

//------------------------------------------------------------------------------
// TODO: get this routine properly
void print_ckt ( abc::Gia_Man_t *gia )
{
  abc::Gia_Obj_t *pObj;
  int i;

  std::cout << "PI ids: \n";
  Gia_ManForEachCi ( gia, pObj, i )
  {
    std::cout << abc::Gia_ObjId ( gia, pObj ) << "\n";
  }
  std::cout << "\n";

  std::cout << "AND ids: fanId0 : fanId1\n";
  Gia_ManForEachAnd ( gia, pObj, i )
  {
    auto id = abc::Gia_ObjId ( gia, pObj );
    std::cout << id << "  " << abc::Gia_ObjFaninId0 ( pObj, id ) << " :"
              << abc::Gia_ObjFaninId1 ( pObj, id ) << "\n";
  }
  std::cout << "\n";

  std::cout << "PO ids:\n";
  Gia_ManForEachCo ( gia, pObj, i )
  {
    auto id = abc::Gia_ObjId ( gia, pObj );
    std::cout << id << "  " << abc::Gia_ObjFaninId0 ( pObj, id ) << " :"
              << abc::Gia_ObjFaninId1 ( pObj, id ) << "\n";
  }
}
}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
