/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : sat_solver.hpp
 * @brief  : Header for all sat solvers
 */

/*-------------------------------------------------------------------------------*/

#ifndef ALL_SAT_SOLVERS_HPP
#define ALL_SAT_SOLVERS_HPP

#include <sat/msat/satSolver.hpp>
#include <types/aig.hpp>

//------------------------------------------------------------------------------
// Normal sat solver
namespace Sat
{
/*
  lbool l_Undef   =  0;  -> TIMEOUT
  lbool l_True    =  1;  -> SAT
  lbool l_False   = -1;  -> UNSAT
*/

static const auto SAT   =  1; // l_True
static const auto UNSAT = -1; // l_False
static const auto UNDEF =  0; // l_Undef

sat_solver *to_solver ( Yise::Aig::Aig &miter ); // AIG miter to sat-solver
std::vector<int> get_cex ( Yise::Aig::Aig &miter, Sat::sat_solver *solver );

// Variables in SAT solver are like the id/index or node
// Literals are for edge i.e., lit: ( var << 1 | polarity )
// Creates a new sat solver
sat_solver* sat_solver_new ();
int sat_solver_solve (
  sat_solver* s,
  lit* assumptions_begin, lit* assumptions_end,
  long nConfLimit, long nInsLimit,
  long nConfLimitGlobal, long nInsLimitGlobal );
int sat_solver_solve_internal ( sat_solver* s );
inline int sat_solver_solve ( sat_solver *s ) { return sat_solver_solve_internal (s); }


int sat_solver_push ( sat_solver* s, int p );
void sat_solver_pop ( sat_solver* s );
int sat_solver_simplify ( sat_solver* s );


}
//------------------------------------------------------------------------------
#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
