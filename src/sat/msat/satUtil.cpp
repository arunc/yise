// Copyright to be written properly (MiniSAT + ABC)

#include <cstdio>
#include <cassert>
#include "satSolver.hpp"

// clang-format off

namespace Sat
{

void Sat_SolverWriteDimacs( sat_solver * p, char * pFileName, lit* assumpBegin, lit* assumpEnd, int incrementVars )
{
  assert ( false );
} 
  

inline double Sat_Wrd2Dbl( long w )  { return (double)(unsigned)(w&0x3FFFFFFF) + (double)(1<<30)*(unsigned)(w>>30); }

void Sat_SolverPrintStats( FILE * pFile, sat_solver * p )
{
    printf( "starts        : %16.0f\n", Sat_Wrd2Dbl(p->stats.starts) );
    printf( "conflicts     : %16.0f\n", Sat_Wrd2Dbl(p->stats.conflicts) );
    printf( "decisions     : %16.0f\n", Sat_Wrd2Dbl(p->stats.decisions) );
    printf( "propagations  : %16.0f\n", Sat_Wrd2Dbl(p->stats.propagations) );
}

int * Sat_SolverGetModel( sat_solver * p, int * pVars, int nVars )
{
    int * pModel;
    int i;
    pModel = __ABC__CALLOC__ ( int, nVars+1 );
    for ( i = 0; i < nVars; i++ )
        pModel[i] = sat_solver_var_value(p, pVars[i]);
    return pModel;    
}

void Sat_SolverDoubleClauses( sat_solver * p, int iVar )
{
    assert ( false );
}

}


// clang-format on

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
