/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : All copyrights and code ownership to Alan Mischenko, ABC team
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : utilSort.cpp
 * @brief  : Sort utilities taken from ABC
 * 
 */

/*-------------------------------------------------------------------------------*/
// IMPORTANT: All original copyrights to Alan Mischenko and ABC team

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cassert>

#include <functions/io/common_io.hpp>

namespace Sat
{


/**Function*************************************************************
  Synopsis    [Merging two lists of entries.]
***********************************************************************/
void Abc_SortMerge( int * p1Beg, int * p1End, int * p2Beg, int * p2End, int * pOut )
{
    int nEntries = (p1End - p1Beg) + (p2End - p2Beg);
    int * pOutBeg = pOut;
    while ( p1Beg < p1End && p2Beg < p2End )
    {
        if ( *p1Beg == *p2Beg )
            *pOut++ = *p1Beg++, *pOut++ = *p2Beg++; 
        else if ( *p1Beg < *p2Beg )
            *pOut++ = *p1Beg++; 
        else // if ( *p1Beg > *p2Beg )
            *pOut++ = *p2Beg++; 
    }
    while ( p1Beg < p1End )
        *pOut++ = *p1Beg++; 
    while ( p2Beg < p2End )
        *pOut++ = *p2Beg++;
    assert( pOut - pOutBeg == nEntries );
}

/**Function*************************************************************
  Synopsis    [Recursive sorting.]
***********************************************************************/
void Abc_Sort_rec( int * pInBeg, int * pInEnd, int * pOutBeg )
{
    int nSize = pInEnd - pInBeg;
    assert( nSize > 0 );
    if ( nSize == 1 )
        return;
    if ( nSize == 2 )
    {
         if ( pInBeg[0] > pInBeg[1] )
         {
             pInBeg[0] ^= pInBeg[1];
             pInBeg[1] ^= pInBeg[0];
             pInBeg[0] ^= pInBeg[1];
         }
    }
    else if ( nSize < 8 )
    {
        int temp, i, j, best_i;
        for ( i = 0; i < nSize-1; i++ )
        {
            best_i = i;
            for ( j = i+1; j < nSize; j++ )
                if ( pInBeg[j] < pInBeg[best_i] )
                    best_i = j;
            temp = pInBeg[i]; 
            pInBeg[i] = pInBeg[best_i]; 
            pInBeg[best_i] = temp;
        }
    }
    else
    {
        Abc_Sort_rec( pInBeg, pInBeg + nSize/2, pOutBeg );
        Abc_Sort_rec( pInBeg + nSize/2, pInEnd, pOutBeg + nSize/2 );
        Abc_SortMerge( pInBeg, pInBeg + nSize/2, pInBeg + nSize/2, pInEnd, pOutBeg );
        memcpy( pInBeg, pOutBeg, sizeof(int) * nSize );
    }
}

/**Function*************************************************************
  Synopsis    [Returns the sorted array of integers.]
  Description [This procedure is about 10% faster than qsort().]
***********************************************************************/
void Abc_MergeSort( int * pInput, int nSize )
{
    int * pOutput;
    if ( nSize < 2 )
        return;
    pOutput = (int *) malloc( sizeof(int) * nSize );
    Abc_Sort_rec( pInput, pInput + nSize, pOutput );
    free( pOutput );
}



/**Function*************************************************************
  Synopsis    [Merging two lists of entries.]
***********************************************************************/
void Abc_MergeSortCostMerge( int * p1Beg, int * p1End, int * p2Beg, int * p2End, int * pOut )
{
    int nEntries = (p1End - p1Beg) + (p2End - p2Beg);
    int * pOutBeg = pOut;
    while ( p1Beg < p1End && p2Beg < p2End )
    {
        if ( p1Beg[1] == p2Beg[1] )
            *pOut++ = *p1Beg++, *pOut++ = *p1Beg++, *pOut++ = *p2Beg++, *pOut++ = *p2Beg++; 
        else if ( p1Beg[1] < p2Beg[1] )
            *pOut++ = *p1Beg++, *pOut++ = *p1Beg++; 
        else // if ( p1Beg[1] > p2Beg[1] )
            *pOut++ = *p2Beg++, *pOut++ = *p2Beg++; 
    }
    while ( p1Beg < p1End )
        *pOut++ = *p1Beg++, *pOut++ = *p1Beg++; 
    while ( p2Beg < p2End )
        *pOut++ = *p2Beg++, *pOut++ = *p2Beg++;
    assert( pOut - pOutBeg == nEntries );
}

/**Function*************************************************************
  Synopsis    [Recursive sorting.]
***********************************************************************/
void Abc_MergeSortCost_rec( int * pInBeg, int * pInEnd, int * pOutBeg )
{
    int nSize = (pInEnd - pInBeg)/2;
    assert( nSize > 0 );
    if ( nSize == 1 )
        return;
    if ( nSize == 2 )
    {
         if ( pInBeg[1] > pInBeg[3] )
         {
             pInBeg[1] ^= pInBeg[3];
             pInBeg[3] ^= pInBeg[1];
             pInBeg[1] ^= pInBeg[3];
             pInBeg[0] ^= pInBeg[2];
             pInBeg[2] ^= pInBeg[0];
             pInBeg[0] ^= pInBeg[2];
         }
    }
    else if ( nSize < 8 )
    {
        int temp, i, j, best_i;
        for ( i = 0; i < nSize-1; i++ )
        {
            best_i = i;
            for ( j = i+1; j < nSize; j++ )
                if ( pInBeg[2*j+1] < pInBeg[2*best_i+1] )
                    best_i = j;
            temp = pInBeg[2*i]; 
            pInBeg[2*i] = pInBeg[2*best_i]; 
            pInBeg[2*best_i] = temp;
            temp = pInBeg[2*i+1]; 
            pInBeg[2*i+1] = pInBeg[2*best_i+1]; 
            pInBeg[2*best_i+1] = temp;
        }
    }
    else
    {
        Abc_MergeSortCost_rec( pInBeg, pInBeg + 2*(nSize/2), pOutBeg );
        Abc_MergeSortCost_rec( pInBeg + 2*(nSize/2), pInEnd, pOutBeg + 2*(nSize/2) );
        Abc_MergeSortCostMerge( pInBeg, pInBeg + 2*(nSize/2), pInBeg + 2*(nSize/2), pInEnd, pOutBeg );
        memcpy( pInBeg, pOutBeg, sizeof(int) * 2 * nSize );
    }
}

/**Function*************************************************************
  Synopsis    [Sorting procedure.]
  Description [Returns permutation for the non-decreasing order of costs.]
***********************************************************************/
int * Abc_MergeSortCost( int * pCosts, int nSize )
{
    int i, * pResult, * pInput, * pOutput;
    pResult = (int *) calloc( sizeof(int), nSize );
    if ( nSize < 2 )
        return pResult;
    pInput  = (int *) malloc( sizeof(int) * 2 * nSize );
    pOutput = (int *) malloc( sizeof(int) * 2 * nSize );
    for ( i = 0; i < nSize; i++ )
        pInput[2*i] = i, pInput[2*i+1] = pCosts[i];
    Abc_MergeSortCost_rec( pInput, pInput + 2*nSize, pOutput );
    for ( i = 0; i < nSize; i++ )
        pResult[i] = pInput[2*i];
    free( pOutput );
    free( pInput );
    return pResult;
}


// this implementation uses 3x less memory but is 30% slower due to cache misses

#if 0  

/**Function*************************************************************
  Synopsis    [Merging two lists of entries.]
***********************************************************************/
void Abc_MergeSortCostMerge( int * p1Beg, int * p1End, int * p2Beg, int * p2End, int * pOut, int * pCost )
{
    int nEntries = (p1End - p1Beg) + (p2End - p2Beg);
    int * pOutBeg = pOut;
    while ( p1Beg < p1End && p2Beg < p2End )
    {
        if ( pCost[*p1Beg] == pCost[*p2Beg] )
            *pOut++ = *p1Beg++, *pOut++ = *p2Beg++; 
        else if ( pCost[*p1Beg] < pCost[*p2Beg] )
            *pOut++ = *p1Beg++; 
        else // if ( pCost[*p1Beg] > pCost[*p2Beg] )
            *pOut++ = *p2Beg++; 
    }
    while ( p1Beg < p1End )
        *pOut++ = *p1Beg++; 
    while ( p2Beg < p2End )
        *pOut++ = *p2Beg++;
    assert( pOut - pOutBeg == nEntries );
}

/**Function*************************************************************
  Synopsis    [Recursive sorting.]
***********************************************************************/
void Abc_MergeSortCost_rec( int * pInBeg, int * pInEnd, int * pOutBeg, int * pCost )
{
    int nSize = pInEnd - pInBeg;
    assert( nSize > 0 );
    if ( nSize == 1 )
        return;
    if ( nSize == 2 )
    {
         if ( pCost[pInBeg[0]] > pCost[pInBeg[1]] )
         {
             pInBeg[0] ^= pInBeg[1];
             pInBeg[1] ^= pInBeg[0];
             pInBeg[0] ^= pInBeg[1];
         }
    }
    else if ( nSize < 8 )
    {
        int temp, i, j, best_i;
        for ( i = 0; i < nSize-1; i++ )
        {
            best_i = i;
            for ( j = i+1; j < nSize; j++ )
                if ( pCost[pInBeg[j]] < pCost[pInBeg[best_i]] )
                    best_i = j;
            temp = pInBeg[i]; 
            pInBeg[i] = pInBeg[best_i]; 
            pInBeg[best_i] = temp;
        }
    }
    else
    {
        Abc_MergeSortCost_rec( pInBeg, pInBeg + nSize/2, pOutBeg, pCost );
        Abc_MergeSortCost_rec( pInBeg + nSize/2, pInEnd, pOutBeg + nSize/2, pCost );
        Abc_MergeSortCostMerge( pInBeg, pInBeg + nSize/2, pInBeg + nSize/2, pInEnd, pOutBeg, pCost );
        memcpy( pInBeg, pOutBeg, sizeof(int) * nSize );
    }
}

/**Function*************************************************************
  Synopsis    [Sorting procedure.]
  Description [Returns permutation for the non-decreasing order of costs.]
***********************************************************************/
int * Abc_MergeSortCost( int * pCosts, int nSize )
{
    int i, * pInput, * pOutput;
    pInput = (int *) malloc( sizeof(int) * nSize );
    for ( i = 0; i < nSize; i++ )
        pInput[i] = i;
    if ( nSize < 2 )
        return pInput;
    pOutput = (int *) malloc( sizeof(int) * nSize );
    Abc_MergeSortCost_rec( pInput, pInput + nSize, pOutput, pCosts );
    free( pOutput );
    return pInput;
}

#endif


int Abc_SortNumCompare( int * pNum1, int * pNum2 )
{
    return *pNum1 - *pNum2;
}

/**Function*************************************************************
  Synopsis    [Testing the sorting procedure.]
***********************************************************************/
void Abc_SortTest()
{
    int fUseNew = 0;
    int i, nSize = 50000000;
    int * pArray = (int *)malloc( sizeof(int) * nSize );
    int * pPerm;
    // generate numbers
    srand( 1000 );
    for ( i = 0; i < nSize; i++ )
        pArray[i] = rand();

    // try sorting
    if ( fUseNew )
    {
        int fUseCost = 1;
        if ( fUseCost )
        {
            pPerm = Abc_MergeSortCost( pArray, nSize );
            // check
            for ( i = 1; i < nSize; i++ )
                assert( pArray[pPerm[i-1]] <= pArray[pPerm[i]] );
            free( pPerm );
        }
        else
        {
            Abc_MergeSort( pArray, nSize );
            // check
            for ( i = 1; i < nSize; i++ )
                assert( pArray[i-1] <= pArray[i] );
        }
    }
    else
    {
        qsort( (void *)pArray, nSize, sizeof(int), (int (*)(const void *, const void *)) Abc_SortNumCompare );
        // check
        for ( i = 1; i < nSize; i++ )
            assert( pArray[i-1] <= pArray[i] );
    }

    free( pArray );
}




/**Function*************************************************************
  Synopsis    [QuickSort algorithm as implemented by qsort().]
***********************************************************************/
int Abc_QuickSort1CompareInc( long * p1, long * p2 )
{
    if ( (unsigned)(*p1) < (unsigned)(*p2) )
        return -1;
    if ( (unsigned)(*p1) > (unsigned)(*p2) )
        return 1;
    return 0;
}
int Abc_QuickSort1CompareDec( long * p1, long * p2 )
{
    if ( (unsigned)(*p1) > (unsigned)(*p2) )
        return -1;
    if ( (unsigned)(*p1) < (unsigned)(*p2) )
        return 1;
    return 0;
}
void Abc_QuickSort1( long * pData, int nSize, int fDecrease )
{
    int i, fVerify = 0;
    if ( fDecrease )
    {
        qsort( (void *)pData, nSize, sizeof(long), (int (*)(const void *, const void *))Abc_QuickSort1CompareDec );
        if ( fVerify )
            for ( i = 1; i < nSize; i++ )
                assert( (unsigned)pData[i-1] >= (unsigned)pData[i] );
    }
    else
    {
        qsort( (void *)pData, nSize, sizeof(long), (int (*)(const void *, const void *))Abc_QuickSort1CompareInc );
        if ( fVerify )
            for ( i = 1; i < nSize; i++ )
                assert( (unsigned)pData[i-1] <= (unsigned)pData[i] );
    }
}

/**Function*************************************************************

  Synopsis    [QuickSort algorithm based on 2/3-way partitioning.]

  Description [This code is based on the online presentation 
  "QuickSort is Optimal" by Robert Sedgewick and Jon Bentley.
  http://www.sorting-algorithms.com/static/QuicksortIsOptimal.pdf 

  The first 32-bits of the input data contain values to be compared. 
  The last 32-bits contain the user's data. When sorting is finished, 
  the 64-bit words are ordered in the increasing order of their value ]
               
***********************************************************************/
static inline void Abc_SelectSortInc( long * pData, int nSize )
{
    int i, j, best_i;
    for ( i = 0; i < nSize-1; i++ )
    {
        best_i = i;
        for ( j = i+1; j < nSize; j++ )
            if ( (unsigned)pData[j] < (unsigned)pData[best_i] )
                best_i = j;
        __ABC__SWAP__( long, pData[i], pData[best_i] );
    }
}
static inline void Abc_SelectSortDec( long * pData, int nSize )
{
    int i, j, best_i;
    for ( i = 0; i < nSize-1; i++ )
    {
        best_i = i;
        for ( j = i+1; j < nSize; j++ )
            if ( (unsigned)pData[j] > (unsigned)pData[best_i] )
                best_i = j;
        __ABC__SWAP__( long, pData[i], pData[best_i] );
    }
}

void Abc_QuickSort2Inc_rec( long * pData, int l, int r )
{
    long v = pData[r];
    int i = l-1, j = r;
    if ( l >= r )
        return;
    assert( l < r );
    if ( r - l < 10 )
    {
        Abc_SelectSortInc( pData + l, r - l + 1 );
        return;
    }
    while ( 1 )
    {
        while ( (unsigned)pData[++i] < (unsigned)v );
        while ( (unsigned)v < (unsigned)pData[--j] )
            if ( j == l )
                break;
        if ( i >= j )
            break;
        __ABC__SWAP__( long, pData[i], pData[j] );
    }
    __ABC__SWAP__( long, pData[i], pData[r] ); 
    Abc_QuickSort2Inc_rec( pData, l, i-1 );
    Abc_QuickSort2Inc_rec( pData, i+1, r );
}
void Abc_QuickSort2Dec_rec( long * pData, int l, int r )
{
    long v = pData[r];
    int i = l-1, j = r;
    if ( l >= r )
        return;
    assert( l < r );
    if ( r - l < 10 )
    {
        Abc_SelectSortDec( pData + l, r - l + 1 );
        return;
    }
    while ( 1 )
    {
        while ( (unsigned)pData[++i] > (unsigned)v );
        while ( (unsigned)v > (unsigned)pData[--j] )
            if ( j == l )
                break;
        if ( i >= j )
            break;
        __ABC__SWAP__( long, pData[i], pData[j] );
    }
    __ABC__SWAP__( long, pData[i], pData[r] ); 
    Abc_QuickSort2Dec_rec( pData, l, i-1 );
    Abc_QuickSort2Dec_rec( pData, i+1, r );
}

void Abc_QuickSort3Inc_rec( long * pData, int l, int r )
{
    long v = pData[r];
    int k, i = l-1, j = r, p = l-1, q = r;
    if ( l >= r )
        return;
    assert( l < r );
    if ( r - l < 10 )
    {
        Abc_SelectSortInc( pData + l, r - l + 1 );
        return;
    }
    while ( 1 )
    {
        while ( (unsigned)pData[++i] < (unsigned)v );
        while ( (unsigned)v < (unsigned)pData[--j] )
            if ( j == l )
                break;
        if ( i >= j )
            break;
        __ABC__SWAP__( long, pData[i], pData[j] );
        if ( (unsigned)pData[i] == (unsigned)v ) 
            { p++; __ABC__SWAP__( long, pData[p], pData[i] ); }
        if ( (unsigned)v == (unsigned)pData[j] ) 
            { q--; __ABC__SWAP__( long, pData[j], pData[q] ); }
    }
    __ABC__SWAP__( long, pData[i], pData[r] ); 
    j = i-1; i = i+1;
    for ( k = l; k < p; k++, j-- ) 
        __ABC__SWAP__( long, pData[k], pData[j] );
    for ( k = r-1; k > q; k--, i++ ) 
        __ABC__SWAP__( long, pData[i], pData[k] );
    Abc_QuickSort3Inc_rec( pData, l, j );
    Abc_QuickSort3Inc_rec( pData, i, r );
}
void Abc_QuickSort3Dec_rec( long * pData, int l, int r )
{
    long v = pData[r];
    int k, i = l-1, j = r, p = l-1, q = r;
    if ( l >= r )
        return;
    assert( l < r );
    if ( r - l < 10 )
    {
        Abc_SelectSortDec( pData + l, r - l + 1 );
        return;
    }
    while ( 1 )
    {
        while ( (unsigned)pData[++i] > (unsigned)v );
        while ( (unsigned)v > (unsigned)pData[--j] )
            if ( j == l )
                break;
        if ( i >= j )
            break;
        __ABC__SWAP__( long, pData[i], pData[j] );
        if ( (unsigned)pData[i] == (unsigned)v ) 
            { p++; __ABC__SWAP__( long, pData[p], pData[i] ); }
        if ( (unsigned)v == (unsigned)pData[j] ) 
            { q--; __ABC__SWAP__( long, pData[j], pData[q] ); }
    }
    __ABC__SWAP__( long, pData[i], pData[r] ); 
    j = i-1; i = i+1;
    for ( k = l; k < p; k++, j-- ) 
        __ABC__SWAP__( long, pData[k], pData[j] );
    for ( k = r-1; k > q; k--, i++ ) 
        __ABC__SWAP__( long, pData[i], pData[k] );
    Abc_QuickSort3Dec_rec( pData, l, j );
    Abc_QuickSort3Dec_rec( pData, i, r );
}

void Abc_QuickSort2( long * pData, int nSize, int fDecrease )
{
    int i, fVerify = 0;
    if ( fDecrease )
    {
        Abc_QuickSort2Dec_rec( pData, 0, nSize - 1 );
        if ( fVerify )
            for ( i = 1; i < nSize; i++ )
                assert( (unsigned)pData[i-1] >= (unsigned)pData[i] );
    }
    else
    {
        Abc_QuickSort2Inc_rec( pData, 0, nSize - 1 );
        if ( fVerify )
            for ( i = 1; i < nSize; i++ )
                assert( (unsigned)pData[i-1] <= (unsigned)pData[i] );
    }
}
void Abc_QuickSort3( long * pData, int nSize, int fDecrease )
{
    int i, fVerify = 1;
    if ( fDecrease )
    {
        Abc_QuickSort2Dec_rec( pData, 0, nSize - 1 );
        if ( fVerify )
            for ( i = 1; i < nSize; i++ )
                assert( (unsigned)pData[i-1] >= (unsigned)pData[i] );
    }
    else
    {
        Abc_QuickSort2Inc_rec( pData, 0, nSize - 1 );
        if ( fVerify )
            for ( i = 1; i < nSize; i++ )
                assert( (unsigned)pData[i-1] <= (unsigned)pData[i] );
    }
}

/**Function*************************************************************
  Synopsis    [Wrapper around QuickSort to sort entries based on cost.]
***********************************************************************/
void Abc_QuickSortCostData( int * pCosts, int nSize, int fDecrease, long * pData, int * pResult )
{
    int i;
    for ( i = 0; i < nSize; i++ )
        pData[i] = ((long)i << 32) | pCosts[i];
    Abc_QuickSort3( pData, nSize, fDecrease );
    for ( i = 0; i < nSize; i++ )
        pResult[i] = (int)(pData[i] >> 32);
}
int * Abc_QuickSortCost( int * pCosts, int nSize, int fDecrease )
{
    long * pData = __ABC__ALLOC__( long, nSize );
    int * pResult = __ABC__ALLOC__( int, nSize );
    Abc_QuickSortCostData( pCosts, nSize, fDecrease, pData, pResult );
    __ABC__FREE__( pData );
    return pResult;
}

//        extern void Abc_QuickSortTest();
//        Abc_QuickSortTest();

void Abc_QuickSortTest()
{
    int nSize = 1000000;
    int fVerbose = 0;
    long * pData1, * pData2;
    int i;
    // generate numbers
    pData1 = __ABC__ALLOC__( long, nSize );
    pData2 = __ABC__ALLOC__( long, nSize );
    srand( 1111 );
    for ( i = 0; i < nSize; i++ )
        pData2[i] = pData1[i] = ((long)i << 32) | rand();
    // perform sorting
    Abc_QuickSort3( pData1, nSize, 1 );
    // print the result
    if ( fVerbose )
    {
        for ( i = 0; i < nSize; i++ )
            printf( "(%d,%d) ", (int)(pData1[i] >> 32), (int)pData1[i] );
        printf( "\n" );
    }
    // create new numbers
    Abc_QuickSort1( pData2, nSize, 1 );
    // print the result
    if ( fVerbose )
    {
        for ( i = 0; i < nSize; i++ )
            printf( "(%d,%d) ", (int)(pData2[i] >> 32), (int)pData2[i] );
        printf( "\n" );
    }
    __ABC__FREE__( pData1 );
    __ABC__FREE__( pData2 );
}


} // namespace Sat

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

