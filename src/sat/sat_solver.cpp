/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : sat_solver.cpp
 * @brief  : Sat solver routines
 */

/*-------------------------------------------------------------------------------*/

#include "sat_solver.hpp"
//------------------------------------------------------------------------------
// Normal sat solver
namespace Sat
{
/*
  lbool l_Undef   =  0;  -> TIMEOUT
  lbool l_True    =  1;  -> SAT
  lbool l_False   = -1;  -> UNSAT
*/
using namespace Yise;

inline Aig::Node &node0  ( Aig::Node &n ) { 
  assert ( Aig::to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ Aig::to_id ( n.fanins[0] )];
}
inline Aig::Node &node1  ( Aig::Node &n ) { 
  assert ( Aig::to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ Aig::to_id ( n.fanins[1] )];
}
inline Aig::Node &node_from_id ( Aig::Aig *p_aig, int id )
{
  if ( id >= p_aig->node_list.size () )
  {
    std::cout << "Failed ID: " << id << " node_list.size(): "
	      << p_aig->node_list.size () << "\n";
    std::exit ( 0 );
  }
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}


inline int max_id ( Aig::Aig &aig )
{
  int max = 0;
  for ( auto &po : aig.po_list )
  {
    const auto &n = node_from_id ( &aig, Aig::to_id (po.node_id ()) );
    if ( n.id > max ) max = n.id;
  }
  assert ( max > 0 );
  return max;
}

sat_solver *to_solver ( Aig::Aig &miter )
{
  assert ( miter.po_list.size () == 1 );  // only for single output miter
  assert ( miter.node_list.size () > 1 ); // const PO not allowed
  const auto &po_edge = miter.po_list[0].edge;
  
  auto solver = sat_solver_new ();
  sat_solver_setnvars ( solver, max_id ( miter ) );
  for ( auto &n : miter.node_list )
  {
    if ( is_const ( n ) ) continue;
    if ( is_pipo ( n ) )
    {
      std::cout << "[i] Direct PI to PO. Trivial tautology\n";
      return nullptr;
    }
    if ( is_pi ( n ) ) continue;
    const auto &n0 = node0 ( n );
    const auto &n1 = node1 ( n );
    auto iVar0 = n0.id;
    auto iVar1 = n1.id;
    auto iVar = n.id;
    auto fCompl0 = Aig::is_complement ( n.fanins[0] );
    auto fCompl1 = Aig::is_complement ( n.fanins[1] );
    auto fCompl = false;
    if ( Aig::is_po ( n ) && Aig::is_complement ( po_edge ) ) fCompl = true; 
    sat_solver_add_and ( solver, iVar, iVar0, iVar1, fCompl0, fCompl1, fCompl );
  }

  return solver;
}

std::vector<int> get_cex ( Aig::Aig &miter, Sat::sat_solver *solver )
{
  std::vector<int> cex;
  cex.reserve ( miter.pi_list.size () );
  for ( auto &pi : miter.pi_list )
  {
    auto v = Aig::to_id ( pi );
    assert ( v > 0 );
    const auto &val = Sat::sat_solver_var_value ( solver, v );
    cex.emplace_back ( val ? v : -v );
  }
  return cex;
}



}
//------------------------------------------------------------------------------

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

