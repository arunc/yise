/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : simulate_aig.hpp
 * @brief  : AIG simulator: event-driven and plain justification
 */

/*
  Notes: Simulation values: 0:'0', 1:'1', 2:don't care (X) and -1:Unknown (U)
  pi_vals is a list of initial values for the PI
*/

//------------------------------------------------------------------------------

#ifndef SIMULATE_AIG_HPP
#define SIMULATE_AIG_HPP

#include <types/aig.hpp>
#include <utils/common_utils.hpp>

namespace Yise
{
namespace Aig
{

// pi_vals is a list of initial pi values.
// Remaining unspecified PIs, if any, are set as 'X'
// returns a vector of simulation value at the POs
std::vector<char> simulate (
  Aig &aig, const std::vector<char> &pi_vals
  );

// Incremental event driven simulation.
// Should have completed atleast one round of plain simulation
std::vector<char> simulate_incr (
  Aig &aig, const std::unordered_map<int, char> &pi_vals_new
  );

inline std::vector<char> simulate_event (
  Aig &aig, const std::unordered_map<int, char> &pi_vals_new
  )
{
  return simulate_incr ( aig, pi_vals_new );
}

inline char complement_sim_value ( const char v )
{
  if ( v == 0 ) return 1;
  if ( v == 1 ) return 0;
  if ( v == 2 ) return 2;
  if ( v == -1 ) return -1;
  assert ( false && "Sim value should be 0/1/2/-1");
  return -1;
}

inline char and_sim_values ( const char v0, const char v1 )
{
  if ( v0 == -1 || v1 == -1 ) return -1;
  if ( v0 ==  0 || v1 ==  0 ) return 0; // 0 & X/1/0
  if ( v0 ==  2 || v1 ==  2 ) return 2; // X & 1
  if ( v0 ==  1 && v1 ==  1 ) return 1; // Only remaining combination
  assert ( false && "Check sim values in v0 and v1 ");
  return -1;
}

inline char or_sim_values ( const char v0, const char v1 )
{
  if ( v0 == -1 || v1 == -1 ) return -1;
  if ( v0 ==  1 || v1 ==  1 ) return 1; // 1 | X/1/0
  if ( v0 ==  2 || v1 ==  2 ) return 2; // X | 0
  if ( v0 ==  0 && v1 ==  0 ) return 1; // Only remaining combination
  assert ( false && "Check sim values in v0 and v1 ");
  return -1;
}

inline char nand_sim_values ( const char v0, const char v1 )
{
  return complement_sim_value ( and_sim_values ( v0, v1 ) );
}

inline char nor_sim_values ( const char v0, const char v1 )
{
  return complement_sim_value ( or_sim_values ( v0, v1 ) );
}

}
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
