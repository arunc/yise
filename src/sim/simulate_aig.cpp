/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : simulate_aig.cpp
 * @brief  : AIG simulator: event-driven and plain justification
 */

//------------------------------------------------------------------------------
/*
  Notes: Simulation values: 0:'0', 1:'1', 2:don't care (X) and -1:Unknown (U)
  pi_vals is a list of initial values for the PI
*/

#include "simulate_aig.hpp"

namespace Yise
{
namespace Aig
{
inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node_from_id ( Aig *p_aig, int id )
{
  if ( id >= p_aig->node_list.size () )
  {
    std::cout << "Failed ID: " << id << " node_list.size(): "
	      << p_aig->node_list.size () << "\n";
    std::exit ( 0 );
  }
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}




inline char sim_rev_topo_rec ( Node &n )
{
  if ( is_trav_id_curr ( n ) ) return n.sim_value;
  
  auto &n0 = node0 ( n );
  auto &n1 = node1 ( n );
  auto v0 = sim_rev_topo_rec ( n0 );
  auto v1 = sim_rev_topo_rec ( n1 );
  assert ( v0 != -1 ); // no unknowns
  assert ( v1 != -1 ); // no unknowns
  v0 = is_complement ( n.fanins[0] ) ? complement_sim_value ( v0 ) : v0;
  v1 = is_complement ( n.fanins[1] ) ? complement_sim_value ( v1 ) : v1;
  n.sim_value = and_sim_values ( v0, v1 );
  set_curr_trav_id ( n );
  return n.sim_value;
}

// pi_vals is a list of initial pi values.
// Remaining unspecified PIs, if any, are set as 'X'
// returns a vector of simulation value at the POs
std::vector<char> simulate (
  Aig &aig, const std::vector<char> &pi_vals
  )
{
  assert ( aig.good () );
  assert ( pi_vals.size () <= aig.pi_list.size () );
  inc_trav_id ( aig );
  aig.node_list[0].sim_value = 1; // const-1
  set_curr_trav_id ( aig.node_list[0] );
  int i;
  for ( i = 0; i < pi_vals.size (); i++ ) // assign all PI values
  {
    const auto &pi = aig.pi_list[i];
    auto &n = node_from_id ( &aig, to_id ( pi ) );
    n.sim_value = pi_vals[i];
    set_curr_trav_id ( n );
  }
  for ( ; i < aig.pi_list.size (); i++ )
  {
    const auto &pi = aig.pi_list[i];
    auto &n = node_from_id ( &aig, to_id ( pi ) );
    n.sim_value = 2;
    set_curr_trav_id ( n );
  }
  
  std::vector<char> po_sim_vals;
  for ( auto &po : aig.po_list ) // simulate in rev-topo order
  {
    auto &n = node_from_id ( &aig, po.node_id () );
    auto v = sim_rev_topo_rec ( n );
    if ( po.is_complement () ) v = complement_sim_value ( v );
    po_sim_vals.emplace_back ( v );
  }
  
  return po_sim_vals;
}



inline char sim_topo_rec ( Node &n )
{
  assert ( false ); // TOOD: to complete
}

// Incremental event driven simulation.
// Should have completed atleast one round of plain simulation
std::vector<char> simulate_incr (
  Aig &aig, const std::unordered_map<int, char> &pi_vals_new
  )
{
  assert ( false );
  // Apply only the affected values, and use sim_topo_rec
}

} // namespace Aig
} // namespace Yise

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
