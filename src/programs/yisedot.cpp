/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : yisedot.cpp
 * @brief  : Yise MIG/AIG to dot converter
 */
//------------------------------------------------------------------------------


#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <types/gate.hpp>
#include <types/mig.hpp>
#include <types/aig.hpp>


int main ( int argc, char *argv[] )
{
  const std::string usage = "Usage: yisedot <design>\nConverts AIG/MIG/Netlist to DOT form. Supports design.aig, design.mig and design.ckt\n";
  if ( argc < 2 )
  {
    std::cout << usage << "\n";
    std::exit ( -1 );
  }
  const auto &file = std::string ( argv[1] );
  if ( file.size () < 5 ) // x.aig
  {
    std::cout << "[e] " <<  file << ": Not Aig/Mig/Ckt (Need .aig/.mig/.ckt extension)" << '\n';
    std::cout << usage << "\n";
    std::exit ( -1 );
  }
  const auto &ext = file.substr ( file.size () - 3 ); // take last 3 char
  const auto &dot_name = file.substr ( 0, file.size () - 4 ) + ".dot";
  if ( ext == "aig" )
  {
    const auto &aig = Yise::Aig::read_aig ( file );
    Yise::Aig::write_dot ( aig, dot_name, true );

  }
  else if ( ext == "mig" )
  {
    const auto &mig = Yise::Mig::read_mig ( file );
    Yise::Mig::write_dot ( mig, dot_name, true );
  }
  else if ( ext == "ckt" )
  {
    assert ( false ); // Need yise-plus for parser
  }
  else
  {
    std::cout << "[e] " <<  file << ": Not Aig/Mig/Ckt (Need .aig/.mig/.ckt extension)" << '\n';
    std::cout << usage << "\n";
    std::exit ( -1 );
  }

  std::cout << "yisedot successful\ninput: " << file << "   output: " << dot_name << '\n';
  return 0;
  
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
 
