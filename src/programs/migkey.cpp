/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : yise_mig.cpp
 * @brief  : Yise MIG main application
 */
//------------------------------------------------------------------------------


#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <types/gate.hpp>
#include <types/mig.hpp>
#include <types/aig.hpp>
#include <types/common.hpp>

#include <functions/aig/cut.hpp>
#include <functions/aig/truth.hpp>
#include <functions/aig/opt.hpp>
#include <functions/aig/npn.hpp>
#include <functions/aig/tt_maps.hpp>
#include <utils/common_utils.hpp>

using Edge = Yise::Mig::Edge;
using Node = Yise::Mig::Node;
namespace Yise
{
namespace Mig
{
Mig test ( const std::string &file );
Mig get_full_adder ();
}
namespace Aig
{
std::string cut_str ( const Aig &aig, const Cut &cut );
Aig balancio ( Aig &aig_orig );

void edge_permutations (
  std::unordered_map<Truth_t, unsigned> &tts, // Input TT map
  const Truth_t &tt, const int num_vars
  );

int re_encode_with_const ( unsigned enc, int pos, unsigned const_val );
std::string to_str ( const std::vector<unsigned> &perm );
void print_enc ( const std::unordered_map<Truth_t, unsigned> &tts );
Mig::Mig to_opt_mig ( Aig &aig );
void generate_permut ( const std::string &file, const int stage );
void generate_adder_permut ( const std::string &file );
void print_permut ( const std::string &file, const int stage, const int input_neg_pos );
Aig opt_tt3 ( Aig &orig_aig );
Aig get_full_adder ();
}
namespace Gate
{
Netlist get_full_adder ();
}
}



int main ( int argc, char *argv[] )
{
  auto adder1 = Yise::Gate::get_full_adder ();
  Yise::Gate::write_dot ( adder1, "adder_netlist.dot", true );

  auto adder2 = Yise::Aig::get_full_adder ();
  adder2 = Yise::Aig::balance ( adder2 );
  Yise::Aig::write_dot ( adder2, "adder_aig.dot", true );
  Yise::Aig::write_aig ( adder2, "adder_aig.aig" );


  auto adder3 = Yise::Mig::get_full_adder ();
  Yise::Mig::write_dot ( adder3, "adder_mig.dot", true );

  return 0;

  auto adder1_aig = Yise::Gate::to_aig ( adder1 );
  Yise::Aig::write_dot ( adder1_aig, "adder_aig_netlist.dot" );
  
  const std::string usage = "Usage: migkey input.aig output.aig\n";

  if ( argc < 3 )
  {
    std::cout << usage << "\n";
    std::exit ( -1 );
  }

  /*
  int stage = std::stoi ( std::string ( argv[2] ) );
  Yise::Aig::generate_permut ( std::string ( argv[1] ), stage );
  return 0;
  */

  
  std::string aig_in_file = argv[1];
  std::string aig_out_file = argv[2];
  aig_out_file = "migkey.aig";

  auto aig1 = Yise::Aig::read_aig ( aig_in_file );
  aig1 = Yise::Aig::strash ( aig1 );
  Yise::Aig::write_dot ( aig1, "orig.dot" );
  auto aig2 = Yise::Aig::balance ( aig1 );
  Yise::Aig::write_aig ( aig2, aig_out_file );
  Yise::Aig::write_dot ( aig2, "migkey.dot" );
  std::cout << "Orig: max_level: " << Yise::Aig::get_max_level ( aig1 )
	    << " num_nodes: " << aig1.num_regular_nodes () << "\n";
  std::cout << "Optm: max_level: " << Yise::Aig::get_max_level ( aig2 )
	    << " num_nodes: " << aig2.num_regular_nodes () << "\n";

  std::cout << "\n\n--------------------------------------\n\n";

  /*
  std::ofstream ofs ( "cut.list", std::ios::out | std::ios::app );

  auto aig1 = Yise::Aig::read_aig ( aig_in_file );
  aig1 = Yise::Aig::strash ( aig1 );
  //Yise::Aig::write_dot ( aig1, "migkey.dot" );
  auto t1 = Yise::curr_time ();
  Yise::Aig::init_cuts ( aig1 );
  int d3 = 0, d4 = 0, d5 = 0, d6 = 0;
  std::vector<Yise::Aig::Cut> cut_list;
  for ( auto &n: aig1.node_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    if ( is_pipo ( n ) ) continue;
    auto cut = get_cut ( n, 6 );
    cut_list.push_back ( cut );
    if ( cut.leaves.size () == 6 ) d6++;
    else if ( cut.leaves.size () == 5 ) d5++;
    else if ( cut.leaves.size () == 4 ) d4++;
    else if ( cut.leaves.size () == 3 ) d3++;
    const auto &tt = Yise::Aig::compute_cut_tt ( n, cut );
    const auto &npn = Yise::Aig::compute_npn ( tt, cut.leaves.size () );
    auto itr = Yise::Aig::TtMap::cut_size_3.find ( tt );
    if ( itr == Yise::Aig::TtMap::cut_size_3.end () )
    {
      if ( cut.leaves.size () == 3 )
      {
	ofs << Yise::to_hex_str ( tt ) << " "
	    << Yise::to_hex_str ( npn ) << " "
	    << cut.leaves.size () << " "
	    << cut.volume << "\n";
      }
    }
  }
  const auto CUT_time = Yise::elapsed_time ( t1 );
  */
  
  /*
  auto aig1 = Yise::Aig::read_aig ( aig_in_file );
  aig1 = Yise::Aig::strash ( aig1 );
  //Yise::Aig::write_dot ( aig1, "migkey.dot" );
  auto t1 = Yise::curr_time ();
  Yise::Aig::init_cuts ( aig1 );
  int d3 = 0, d4 = 0, d5 = 0, d6 = 0;
  for ( auto &n: aig1.node_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    if ( is_pipo ( n ) ) continue;
    auto cut = get_cut ( n, 6 );
    if ( cut.leaves.size () == 6 ) d6++;
    else if ( cut.leaves.size () == 5 ) d5++;
    else if ( cut.leaves.size () == 4 ) d4++;
    else if ( cut.leaves.size () == 3 ) d3++;
  }
  const auto CUT_time = Yise::elapsed_time ( t1 );

  auto t2 = Yise::curr_time ();
  auto cut_manager = enumerate_cuts ( aig1, 3 );
  const auto cut_time = Yise::elapsed_time ( t2 );
  
  const auto &cut_list = cut_manager.cut_list;
  int c3 = 0, c4 = 0, c5 = 0, c6 = 0;
  for ( const auto &elem : cut_list )
  {
    const auto &cut = elem.second;
    if ( cut.leaves.size () == 6 ) c6++;
    else if ( cut.leaves.size () == 5 ) c5++;
    else if ( cut.leaves.size () == 4 ) c4++;
    else if ( cut.leaves.size () == 3 ) c3++;
    //else assert ( false );
  }
  std::cout << "cut  6:" << c6 << "  5:" << c5 << "  4:" << c4 << "  3:" << c3 << " || ";
  const auto p6 = (float) c6 / (float) cut_list.size () * 100;
  const auto p5 = (float) c5 / (float) cut_list.size () * 100;
  const auto p4 = (float) c4 / (float) cut_list.size () * 100;
  const auto p3 = (float) c3 / (float) cut_list.size () * 100;
  std::cout << "%  6:" << p6 << "  5:" << p5 << "  4:" << p4 << "  3:" << p3 << " || ";
  std::cout << cut_time << "\n";


  std::cout << "CUT  6:" << d6 << "  5:" << d5 << "  4:" << d4 << "  3:" << d3 << " || ";
  const auto q6 = (float) d6 / (float) cut_list.size () * 100;
  const auto q5 = (float) d5 / (float) cut_list.size () * 100;
  const auto q4 = (float) d4 / (float) cut_list.size () * 100;
  const auto q3 = (float) d3 / (float) cut_list.size () * 100;
  std::cout << "%  6:" << q6 << "  5:" << q5 << "  4:" << q4 << "  3:" << q3 << " || ";
  std::cout << CUT_time << "\n";
  */
  
  /*
  auto mig1 = Yise::Mig::read_aig ( aig_in_file );
  mig1 = Yise::Mig::strash ( mig1 );
  auto aig1 = Yise::Mig::to_aig ( mig1 );
  Yise::Aig::write_aig ( aig1, aig_out_file );
  //Yise::Aig::write_dot ( aig1, "migkey.dot" );
  */
  
  /*
  auto aig1 = Yise::Aig::read_aig ( aig_in_file );
  aig1 = Yise::Aig::strash ( aig1 );
  //Yise::Aig::write_dot ( aig1, "orig.dot" );
  auto aig2 = Yise::Aig::opt_tt ( aig1 );
  Yise::Aig::write_aig ( aig2, aig_out_file );
  //Yise::Aig::write_dot ( aig2, "migkey.dot" );
  std::cout << "Orig: max_level: " << Yise::Aig::get_max_level ( aig1 )
	    << " num_nodes: " << aig1.num_nodes () << "\n";
  std::cout << "Optm: max_level: " << Yise::Aig::get_max_level ( aig2 )
	    << " num_nodes: " << aig2.num_nodes () << "\n";
  */
  

  /*
  auto aig1 = Yise::Aig::read_aig ( aig_in_file );
  aig1 = Yise::Aig::strash ( aig1 );
  auto mig1 = Yise::Aig::to_mig ( aig1 );
  Yise::Mig::write_mig ( mig1, "migkey.mig", true );
  Yise::Mig::write_aig ( mig1, aig_out_file );
  //auto aig2 = Yise::Mig::to_aig ( mig1 );
  //Yise::Mig::write_dot ( mig1, "migkey_mig.dot" );
  //Yise::Aig::write_dot ( aig2, "migkey.dot" );
  //Yise::Aig::write_dot ( aig1, "orig.dot" );

  const auto &aig_level = Yise::Aig::get_max_level ( aig1 );
  const auto &mig_level = Yise::Mig::get_max_level ( mig1 );
  const auto &aig_nodes = aig1.num_nodes ();
  const auto &mig_nodes = mig1.num_nodes ();
  const float level_change = (float) aig_level / (float) mig_level;
  const float node_change = (float) aig_nodes / (float) mig_nodes;
  std::cout << "Aig: max_level: " << aig_level << " num_nodes: " << aig_nodes << "\n";
  std::cout << "Mig: max_level: " << mig_level << " num_nodes: " << mig_nodes << "\n";
  std::cout << "Level change: " << level_change << " node_change: " << node_change << "\n";
  */
  
  /*
  auto mig1 = Yise::Mig::read_aig ( aig_in_file );
  mig1 = Yise::Mig::strash ( mig1 );
  Yise::Mig::write_mig ( mig1, "migkey.mig" );
  auto mig2 = Yise::Mig::read_mig ( "migkey.mig" );
  mig2 = Yise::Mig::strash ( mig2 );
  Yise::Mig::write_dot ( mig2, "migkey.dot" );
  Yise::Mig::write_mig ( mig2, "migkey.mig", true );
  Yise::Mig::write_aig ( mig2, aig_out_file );
  return 0;
  */

  /*
  auto mig1 = Yise::Mig::read_aig ( aig_in_file );
  mig1 = Yise::Mig::strash ( mig1 );
  Yise::Mig::write_aig ( mig1, "migkey.aig" );
  //Yise::Aig::write_dot ( aig1, "migkey.dot" );
  return 0;
  */

  /*
  Yise::Aig::write_dot ( aig1, "orig.dot" );
  auto mig1 = Yise::Aig::to_opt_mig ( aig1 );
  //auto mig1 = Yise::Aig::to_mig ( aig1 );
  Yise::Mig::write_dot ( mig1, "migkey.dot" );

  auto aig2 = Yise::Mig::to_aig ( mig1 );
  Yise::Aig::write_aig ( aig2, aig_out_file );
  //Yise::Aig::write_dot ( aig2, "migkey.dot" );
  
  Yise::Mig::write_mig ( mig1, "migkey.mig", true );
  
  //Yise::Mig::write_aig ( mig1, aig_out_file );
  return 0;
  */


  /*
  //std::cout << "Max level1 = " << Yise::Aig::get_max_level ( aig1 ) << "\n";

  return 0;

  */
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
 
