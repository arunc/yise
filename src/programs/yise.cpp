/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : yise.cpp
 * @brief  : Yise main application
 */
//------------------------------------------------------------------------------


#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <ext-libs/abc/abc_api.hpp>
#include <types/yig.hpp>
#include <types/experimental.hpp>

#include <functions/truth/yig_truth.hpp>

#include <utils/common_utils.hpp>


namespace Yise
{
namespace iwls // iwls competition index is from 1, not 0.
{
void write_yag ( const Yig &yig, std::ostream &ofs );
}
}

int main ( int argc, char *argv[] )
{
  std::string aig_file, yag_file;
  const std::string usage = "Usage: yise <circuit.aig> [<circuit.yag>]       # default: circuit.aig.yag \nNote: provide a 3rd argument to write-out verilog for equivalence checking\n";
    
  if ( argc < 2 )
  {
    std::cout << usage << "\n";
    std::exit ( -1 );
  }
  aig_file = argv[1];
  if ( argc > 2 )
  {
    yag_file = argv[2];
  }
  else
  {
    yag_file = aig_file + ".yag";
  }

  std::cout << "Starting Yise: " << Yise::curr_time_str () << "\n";
  auto t1 = Yise::curr_time ();
							   
  auto ntk = Yise::convert_gia_to_ntk ( Yise::read_aiger ( aig_file ) );
  auto ofs = std::ofstream ( yag_file );
  
  auto yig = Yise::to_yig ( ntk );
  //Yise::write_yag2 ( yig, ofs );
  Yise::iwls::write_yag ( yig, ofs );

  const auto node_count = Yise::node_count ( yig ) - Yise::pi_count ( yig ) - 1;
  
  std::cout << "Finished Yise: " << Yise::curr_time_str () << "\n";
  std::cout << "Statistics:\n";
  std::cout << "   time taken:  " << Yise::elapsed_time ( t1 ) << " ms\n";
  std::cout << "   node count:  " << node_count << "\n";
  std::cout << "   PI count:    " << Yise::pi_count ( yig ) << "\n";
  std::cout << "   PO count:    " << Yise::po_count ( yig ) << "\n";
  std::cout << "   max level:   " << Yise::get_max_level ( yig ) << "\n";
  std::cout << "   qca inv :    " << Yise::count_qca_inversions ( yig ) << "\n";
  std::cout << "   qca delay:   " << Yise::max_qca_delay ( yig ) << "\n";
  std::cout << "   qca maj:     " << Yise::count_qca_maj ( yig ) << "\n";
  std::cout << "   qca cost:    " << Yise::qca_cost_function ( yig ) << "\n";

  
  if ( argc > 3 ) // for equivalence checking
  {
    Yise::write_verilog ( yig, argv[3] ); 
  }
    
  //Yise::write_dot ( yig, "ckt.dot" );
  
  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
