/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : count_sat.cpp
 * @brief  : #SAT model counter
 */
//------------------------------------------------------------------------------


#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <types/cnf.hpp>
#include <types/common.hpp>

#include <utils/common_utils.hpp>


int main ( int argc, char *argv[] )
{
  const std::string usage = "Usage: count_sat input.cnf output.cnf\n";

  if ( argc < 3 )
  {
    std::cout << usage << "\n";
    std::exit ( -1 );
  }

  std::string cnf_in_file = argv[1];
  std::string cnf_out_file = argv[2];
  cnf_out_file = "count_sat.cnf";

  auto cnf1 = Yise::Cnf::read_cnf ( cnf_in_file );
  auto aig1 = Yise::Cnf::to_aig ( cnf1 );
  auto cnf2 = Yise::Cnf::to_cnf ( aig1 );
  auto aig2 = Yise::Cnf::to_aig ( cnf2 );

  Yise::Cnf::write_cnf ( cnf1, "count_sat1.cnf" );
  Yise::Cnf::write_cnf ( cnf2, "count_sat2.cnf" );

  
  Yise::Aig::write_aig ( aig1, "count_sat.aig" );
  Yise::Aig::write_aig ( aig2, "count_sat2.aig" );
  Yise::Aig::write_dot ( aig1, "count_sat.dot" );
  
  auto solver = Yise::Cnf::to_solver ( cnf1 );
  auto res = Sat::sat_solver_solve_internal ( solver );
  if ( res == Sat::SAT )
  {
    Yise::Cnf::update_cex ( cnf1, solver );
    std::cout << "Sat Cex: " << Yise::to_cex_str ( cnf1.cex ) << "\n";
  }
  else if ( res == Sat::UNSAT )
  {
    std::cout << "Unsat \n";
  }
  else
  {
    std::cout << "Undef \n";
  }

  auto solver2 = Sat::to_solver ( aig1 );
  auto res2 = Sat::sat_solver_solve_internal ( solver2 );
  if ( res2 == Sat::SAT )
  {
    const auto &cex2 = Sat::get_cex ( aig1, solver2 );
    std::cout << "Sat Cex2: " << Yise::to_cex_str ( cex2 ) << "\n";
  }
  else if ( res2 == Sat::UNSAT )
  {
    std::cout << "Unsat2 \n";
  }
  else
  {
    std::cout << "Undef2 \n";
  }


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
 
