/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : synq.cpp
 * @brief  : QCA Synthesis main application
 */
//------------------------------------------------------------------------------


#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>

#include <types/qca.hpp>
#include <types/common.hpp>
#include <utils/common_utils.hpp>

#include <functions/qca/opt.hpp>

int main ( int argc, char *argv[] )
{
  const std::string usage = "Usage: synq input.aig output.qca\n";

  if ( argc < 3 )
  {
    std::cout << usage << "\n";
    std::exit ( -1 );
  }

  std::string aig_in_file = argv[1];
  std::string qca_out_file = argv[2];
  qca_out_file = "synq.qca";

  auto aig1 = Yise::Aig::read_aig ( aig_in_file );
  auto qca1 = Yise::Qca::read_aig ( aig_in_file );
  auto qca2 = Yise::Qca::opt_maj ( aig1 );
  Yise::Qca::opt_inv_inplace ( qca2 );
  
  //qca1 = Yise::Qca::strash ( qca1 );
  //auto qca2 = Yise::Qca::opt_inv ( qca1 );
  Yise::Qca::write_dot ( qca1, "input.dot" ); 
  Yise::Qca::write_dot ( qca2, "synq.dot" ); 
  
  
  //Yise::Qca::write_qca ( qca1, qca_out_file );
  Yise::Qca::write_qca ( qca2, qca_out_file );

  const auto &d1 = Yise::Qca::get_max_delay ( qca1 );
  const auto &d2 = Yise::Qca::get_max_delay ( qca2 );
  const double dd = (double)( d1 - d2 ) / (double)d1 * 100;

  const auto &c1 = Yise::Qca::compute_cost ( qca1 );
  const auto &c2 = Yise::Qca::compute_cost ( qca2 );
  const double cc = (double) ( c1 - c2 ) / (double)c1 * 100;
  
  
  std::cout << "Qca delay improvement: " << dd << "%"
	    << "\n";
  std::cout << "Qca cost improvement:  " << cc << "%"
	    << "\n";
  auto aig2 = Yise::Qca::to_aig ( qca2 );
  Yise::Aig::write_aig ( aig2, "synq.aig" );
  
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
 
