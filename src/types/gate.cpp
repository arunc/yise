/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : gate.cpp
 * @brief  : Gate type for structural representation
 */

/*-------------------------------------------------------------------------------*/

//clang-format off

#include "gate.hpp"

#include <utils/common_utils.hpp>

#include <string>
#include <sstream>


namespace Yise
{
namespace Gate
{


int add_gate (
  Netlist &ckt, const Type &type, bool inv,
  const std::vector<int> &fanins, const std::string &name
  )
{
  ckt.gate_list.emplace_back ( Gate ( ckt.gate_list.size (), fanins, type, &ckt, inv ) );
  const auto id = ckt.gate_list.size () - 1;
  if ( name != "" )
    ckt.name_list.emplace ( std::make_pair ( id, name ) );
  //assert ( ckt.gate_list.back ().good () );
  auto &g = ckt.gate_list.back ();
  for ( auto &f : g.fanins )
  {
    auto &fog = to_gate ( &ckt, f );
    fog.fanouts.emplace_back ( id );
  }
  
  if ( is_pi ( g ) ) ckt.pi_list.emplace_back ( id );
  if ( is_po ( g ) ) ckt.po_list.emplace_back ( id );

  int max_level = -2;
  for ( auto &ff : g.fanins )
  {
    const auto &fog = to_gate ( &ckt, ff );
    if ( fog.level > max_level ) max_level = fog.level;
  }
  g.level = max_level + 1;
  if ( is_pi ( g ) ) g.level = 0;
  if ( is_const0 ( g ) ) g.level = -1;
  if ( is_const1 ( g ) ) g.level = -1;
  
  return id;
}

bool check ( const Netlist &ckt )
{
  const auto &num_pis = ckt.num_pis ();
  const auto &num_pos = ckt.num_pos ();
  auto npi = 0;
  auto npo = 0;
  for ( const auto &g : ckt.gate_list )
  {
    if ( !g.good () ) return false;
    if ( is_pi ( g ) ) npi++;
    if ( is_po ( g ) ) npo++;
  }
  if ( npi != num_pis ) return false;
  if ( npo != num_pos ) return false;
  
  return true;
}

int prune_rec (
  const Gate &g_orig,
  Netlist &ckt_new,
  std::unordered_map<int, int> &added_gates
  )
{
  const auto &itr = added_gates.find ( g_orig.id );
  if ( itr != added_gates.end () ) return itr->second;

  std::vector<int> fanins_new;
  for ( auto &f : g_orig.fanins )
  {
    auto &gg = to_gate ( g_orig.p_netlist, f );
    fanins_new.emplace_back ( prune_rec ( gg, ckt_new, added_gates ) );
  }
  std::string name = has_name ( g_orig ) ? to_name ( g_orig ) : "";
  const auto &id = add_gate ( ckt_new, g_orig.type, g_orig.inv, fanins_new, name );
  added_gates[g_orig.id] = id;
  return id;
}


// Remove all the floating gates
Netlist prune ( const Netlist &ckt_orig )
{
  Netlist ckt_new ( ckt_orig.name, ckt_orig.num_gates () );
  std::unordered_map<int, int> added_gates;
  added_gates.reserve ( ckt_orig.num_gates () );
  for ( auto &pi : ckt_orig.pi_list )
  {
    auto pi_new = add_pi ( ckt_new, to_name ( ckt_orig, pi ) );
    added_gates[pi] = pi_new;
  }

  for ( auto &po : ckt_orig.po_list )
  {
    const auto &g = to_gate ( ckt_orig, po );
    const auto &po_new = prune_rec ( g, ckt_new, added_gates );
  }
  
  assert ( check ( ckt_new ) );
  return ckt_new;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

inline Aig::Node &node_from_id ( Aig::Aig *p_aig, int id )
{
  if ( id >= p_aig->node_list.size () )
  {
    std::cout << "Failed ID: " << id << " node_list.size(): "
	      << p_aig->node_list.size () << "\n";
    std::exit ( 0 );
  }
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}


inline Aig::Edge gate_to_aig (
  const Gate &g,
  Aig::Aig &aig_new,
  const std::vector<Aig::Edge> &fanins
  )
{
  if ( is_buf (g) )  { assert (fanins.size () == 1); return fanins[0]; }
  if ( is_inv (g) )  { assert (fanins.size () == 1); return Aig::complement (fanins[0]); }
  if ( is_and (g) )  { assert (fanins.size () == 2); return Aig::add_and ( aig_new, fanins[0], fanins[1] );}
  if ( is_nand (g) ) { assert (fanins.size () == 2); return Aig::add_nand ( aig_new, fanins[0], fanins[1] );}
  if ( is_or (g) )   { assert (fanins.size () == 2); return Aig::add_or ( aig_new, fanins[0], fanins[1] );}
  if ( is_nor (g) )  { assert (fanins.size () == 2); return Aig::add_nor ( aig_new, fanins[0], fanins[1] );}
  if ( is_xor (g) )  { assert (fanins.size () == 2); return Aig::add_xor ( aig_new, fanins[0], fanins[1] );}
  if ( is_xnor (g) ) { assert (fanins.size () == 2); return Aig::add_xnor ( aig_new, fanins[0], fanins[1] );}
  if ( is_fanout(g)) { assert (fanins.size () == 1); return fanins[0]; }
  assert ( false );
  return Aig::edge_undef;
}

Aig::Edge to_aig_rec (
  Gate &g_orig,
  Aig::Aig &aig_new
  )
{
  if ( is_const0 ( g_orig ) ) { g_orig.aig_edge = Aig::edge0; return Aig::edge0; }
  if ( is_const1 ( g_orig ) ) { g_orig.aig_edge = Aig::edge0; return Aig::edge1; }
  if ( g_orig.aig_edge != Aig::edge_undef ) return g_orig.aig_edge;
  assert ( !is_po ( g_orig ) );
  assert ( !is_pi ( g_orig ) );

  std::vector<Aig::Edge> fanins;
  for ( auto &f : g_orig.fanins )
  {
    auto &gg = to_gate ( g_orig.p_netlist, f );
    fanins.emplace_back ( to_aig_rec ( gg, aig_new ) ) ;
  }
  auto e_new = gate_to_aig ( g_orig, aig_new, fanins );
  g_orig.aig_edge = e_new;
  return e_new;
}

// Convert to a corresponding AIG
Aig::Aig to_aig ( Netlist &ckt )
{
  Aig::Aig aig_new ( ckt.name, ckt.num_gates () );
  for ( auto &g : ckt.gate_list )
  {
    g.aig_edge = Aig::edge_undef;
  }
  
  for ( auto &pi : ckt.pi_list )
  {
    auto &g = to_gate ( &ckt, pi ); 
    auto e = Aig::add_pi ( aig_new, to_name ( ckt, pi ) );
    g.aig_edge = e;
  }

  for ( auto &po : ckt.po_list )
  {
    auto &g = to_gate ( &ckt, po );
    auto po_new = to_aig_rec ( g, aig_new );
    g.aig_edge = po_new;
    Aig::tag_as_po ( aig_new, po_new, to_name ( ckt, po ) );
  }
  
  return aig_new;
}

bool check_level ( const Netlist &ckt )
{
  for ( const auto &n : ckt.gate_list )
  {
    if ( is_const0 ( n ) && n.level != -1 )  return false;
    if ( is_const1 ( n ) && n.level != -1 )  return false;
    if ( is_pi ( n ) && n.level != 0 ) return false;
    if ( is_pi ( n ) ) continue;
    if ( is_const0 ( n ) ) continue;
    if ( is_const1 ( n ) ) continue;
    
    for ( auto &f : n.fanins )
    {
      assert ( f < n.p_netlist->gate_list.size () );
      const auto &nn =  n.p_netlist->gate_list[f];
      if ( nn.level >= n.level ) return false;
    }
  }
  
  return true;
}


inline std::string to_shape ( const Gate &g )
{
  return "house";
}

void write_dot ( const Netlist &ckt, const std::string &file, bool pretty )
{
  //assert ( check ( ckt ) );
  if ( !check_level ( ckt ) )
  {
    std::cout << "[e] Wrong level info.\n";
    return;
  }
  if ( ckt.po_list.empty () )
  {
    std::cout << "[e] No Primary Output to writeout!\n";
    return;
  }
  if ( ckt.pi_list.empty () ) // ToDo: remove this restriction
  {
    std::cout << "[e] No Primary Input. Plain const outputs are ignored!\n";
    return;
  }
  if ( ckt.gate_list.size () > 200 )
  {
    std::cout << "[e] Cannot write dot files for #gates > 200!\n";
    return;
  }
  auto ofs = fopen ( (char *)file.c_str (), "w" );
  if ( ofs  == NULL )
  {
    std::cout << "[e] Cannot open " << file << " for writing!\n";
    return;
  }

  std::vector<int> levels;
  auto max_level = 0;
  for ( auto &g : ckt.gate_list )
  {
    if ( g.level > max_level ) max_level = g.level;
    levels.emplace_back ( g.level );
  }
  
  const auto min_level = 0;
  const auto num_gates = ckt.num_gates (); // including 0, and PIs
  
  // 1. write the DOT header
  std::fprintf ( ofs, "# %s\n",  "GraphViz network by Yise" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "digraph network {\n" );
  if ( pretty )
  {
    std::fprintf ( ofs, "size = \"5,5\";\n" );
    std::fprintf ( ofs, "fontsize=26;\n" );
  }
  else
  {
    std::fprintf ( ofs, "size = \"7.5,10\";\n" );
  }

  std::fprintf ( ofs, "center = true;\n" );
  std::fprintf ( ofs, "edge [dir = back];\n" );
  std::fprintf ( ofs, "\n" );
  
  // labels on the left of the picture
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  node [shape = plaintext];\n" );
  std::fprintf ( ofs, "  edge [style = invis];\n" );
  std::fprintf ( ofs, "  LevelTitle1 [label=\"\"];\n" );
  
  // 2. generate node names with labels
  // labels are provided later with groups
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    // the visible node name
    std::fprintf ( ofs, "  Level%d", Level );
    std::fprintf ( ofs, " [label = " );
    // label name
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "];\n" );
  }

  // 2. generate the levels for nodes
   std::fprintf ( ofs, "  LevelTitle1 ->" );
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    std::fprintf ( ofs, "  Level%d",  Level );
    if ( Level != min_level )
      std::fprintf ( ofs, " ->" );
    else
      std::fprintf ( ofs, ";" );
  }
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

  
  // 3. generate title box on top
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  rank = same;\n" );
  std::fprintf ( ofs, "  LevelTitle1;\n" );
  std::fprintf ( ofs, "  title1 [shape=plaintext,\n" );
  if ( pretty )
  {
    std::fprintf ( ofs, "          fontsize=26,\n" );
  }
  //std::fprintf ( ofs, "          fontsize=26,\n" );
  //std::fprintf ( ofs, "          fontname = \"Times-Roman\",\n" );
  std::fprintf ( ofs, "          label=\"" );
  std::fprintf ( ofs, "Netlist: %s ", (char *)ckt.name.c_str () );
  if ( !pretty )
  {
    std::fprintf ( ofs, "by Yise @ %s ",  (char *)curr_time_str ().c_str () );
  }

  std::fprintf ( ofs, "\n(#gates: %d #levels: %d #PI: %d #PO: %d)",
		 ckt.num_gates () - ckt.num_pis () - ckt.num_pos (),
		 max_level - 1, ckt.num_pis (), ckt.num_pos () );
  std::fprintf ( ofs, "\"\n" );
  std::fprintf ( ofs, "         ];\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

   // 4. generate the POs, the topmost level.
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", max_level );
  for ( auto i = 0; i < ckt.po_list.size (); i++ )
  {
    const auto &po = ckt.po_list[i];
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", 
		   num_gates + i ,              // each PO is a taken as a Node
		   (char *)to_name ( ckt, po ).c_str () );  // name of the PO
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
      std::fprintf ( ofs, ", shape = hexagon, style = filled, color = gray22" );
    }
    else
    {
      std::fprintf ( ofs, ", shape = triangle, style = filled, color = gray22" );
    }
    std::fprintf ( ofs, ", fillcolor = bisque];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  
  std::unordered_map<int, int> labels; // Nobody cares about efficiency
  for ( auto i = 0; i < ckt.gate_list.size (); i++ )
  {
    const auto &n = to_gate ( ckt, i );
    if ( !pretty )
    {
      labels.emplace ( std::make_pair ( n.id, n.id ) );
    }
    else
    {
      if ( is_pi ( n ) ) continue;
      if ( is_po ( n ) ) continue;
      if ( is_const0 ( n ) ) continue;
      if ( is_const1 ( n ) ) continue;
      labels.emplace ( std::make_pair ( n.id, labels.size () + 1 ) );
    }
  }

  // 5. generate nodes of each rank
  for ( int Level = max_level - 1; Level >= min_level && Level > 0; Level-- )
  {
    std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", Level ); 
    for ( const auto &n : ckt.gate_list )
    {
      if ( n.type == Type::PI ) continue;
      if ( n.type == Type::PO ) continue;
      if ( n.level !=  Level ) continue;
      auto num = labels.find ( n.id )->second;
      std::fprintf ( ofs, "  Node%d [label = \"%s:%d\"", n.id ,
		     (char *)to_type_str ( n ).c_str (), num );  

      if ( pretty )
      {
	std::fprintf ( ofs, ", fontsize=26" );
      }
      std::fprintf ( ofs, ", shape = %s];\n", (char *)to_shape ( n ).c_str () );
    }
    std::fprintf ( ofs, "}\n\n" );
  }

  // 6. generate the PI nodes, the lowest level
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", min_level );
  for ( const auto &pi : ckt.pi_list )
  {
    const auto &n = to_gate ( ckt, pi );
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", n.id, to_name ( ckt, pi ).c_str () );
    std::fprintf ( ofs, ", shape = %s", "hexagon, style = filled" );
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
    }
    std::fprintf ( ofs, ", color = gray22, fillcolor = lightblue];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );

  // 7. Order the top 3 levels using invisible edges
   for ( auto i = 0; i < ckt.po_list.size (); i++ )             // title2 -> POs
  {
     std::fprintf ( ofs, "title1 -> Node%d [style = invis];\n", num_gates + i );
  }
  for ( auto i = 1; i < ckt.po_list.size (); i++ )             // among all POs
  {
    auto j = i - 1;
    std::fprintf ( ofs, "Node%d -> Node%d [style = invis];\n",
		   num_gates + j , num_gates + i );
  }
  
  // 8. generate all edges using fanin details
  for ( const auto &n : ckt.gate_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_po ( n ) ) continue;
    for ( auto &e : n.fanins )
    {
      const auto &ee = to_gate ( ckt, e );
      std::fprintf ( ofs, "Node%d",  n.id );
      std::fprintf ( ofs, " -> " );
      std::fprintf ( ofs, "Node%d",  ee.id );
      std::fprintf ( ofs, " [style = %s", ee.inv ? "dotted" : "bold" );
      std::fprintf ( ofs, "];\n" );
    }
  }
  
  // 9. Finally generate all the PO edges
  for ( auto i = 0; i < ckt.po_list.size (); i++ )
  {
    const auto &po = to_gate ( ckt, ckt.po_list[i] );

    for ( auto &f : po.fanins )
    {
      const auto &ff = to_gate ( ckt, f );
      std::fprintf ( ofs, "Node%d", num_gates + i );
      std::fprintf ( ofs, " -> " );
      std::fprintf ( ofs, "Node%d", ff.id );
      std::fprintf ( ofs, " [style = %s", ff.inv ? "dotted" : "bold" );
      std::fprintf ( ofs, "];\n" );
    }
    
  }
  std::fprintf ( ofs, "}\n\n" );
  fclose( ofs );
}

// For the purpose of my thesis :)
Netlist get_full_adder ()
{
  Netlist ckt ( "full_adder", 20 );
  auto a = add_pi ( ckt, "a" );
  auto b = add_pi ( ckt, "b" );
  auto cin = add_pi ( ckt, "cin" );
  auto xor1 = add_xor ( ckt, a, b );
  auto xor2 = add_xor ( ckt, cin, xor1 );
  add_po ( ckt, {xor2}, "sum" );
  auto and1 = add_and ( ckt, cin, xor1 );
  auto and2 = add_and ( ckt, a, b );
  auto or1 = add_or ( ckt, and1, and2 );
  add_po ( ckt, {or1}, "cout" );

  return ckt;
}

//------------------------------------------------------------------------------
} // Gate namespace
//------------------------------------------------------------------------------
} // Yise namespace

//clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

