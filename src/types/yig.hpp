/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : yig.hpp
 * @brief  : The Y inverter Gate. Base type
 */

/*-------------------------------------------------------------------------------
Y2::
      a
     /  \
    /    \
   b ----- c
  / \     / \
 /   \   /   \
d----- e ----- f

Y2  = Maj (  Maj(a, b, c), Maj(b, d, e), Maj(c, e, f) )
Inorder to reduce to a Y1, i.e. a plain MAJ gate connect e=a, f=b and d=c
Y2 = abd + abe + bcd + bce + cde + def + bef + bcf + ace + acf
Y1 = ab + bc + ac = Y2 (a, b, c, c, a, b)

Reduction rules
Unit clauses and consts
1) Y2 (b=c=e)   = b
special Y2 (b=c=e=0) = 0
special Y2 (b=c=e=1) = 1
2) Y2 (a=b=d) = a
3) Y2 (d=e=f) = d
4) Y2 (a=c=f) = a

Y2 (a=0, e=0, f=0) = bcd       = 3 input AND
Y2 (a=1, e=1, f=1) = b + c + d = 3 input OR
TODO: find other gates also, minimum XOR and MUX

From the majority propery
Y2 (a, b, c, c, a, b) = 3 input MAJ
Y2 (a, b, 0, 0, a, b) = 2 input AND
Y2 (a, b, 1, 1, a, b) = 2 input OR

Axiom:
Inversion of Y = Y of inversion, follows from the majority property

-------------------------------------------------------------------------------*/
//------------------------------------------------------------------------------

#ifndef YIG_HPP
#define YIG_HPP

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include <ext-libs/abc/abc_api.hpp>

namespace Yise
{
// clang-format off

//------------------------------------------------------------------------------
enum class NodeType
{
  CONST0,     // const-0
  PI,         // primary input
  PO,         // primary output
  CONST0_PO,  // const0 can also be a PO
  PI_PO,      // PI can also be a PO
  PLAIN       // internal node; anything other than the above types
};

// Edge represents a fanout/fanin node + complement
struct Edge  // node-id + complement
{
  int node_id;      // store the corresponding node-id
  bool complement;  // complement edge or regular edge, true = complement

  inline Edge operator!  () const { return {node_id, !complement}; }
  inline bool operator== ( const Edge &other ) const { return node_id == other.node_id && complement == other.complement; }
  inline bool operator!= ( const Edge &other ) const { return node_id != other.node_id || complement != other.complement; }
  inline Edge operator^  ( bool value ) const { return {node_id, complement != value}; }
  inline bool operator<  ( const Edge &other ) const  {  // Needed for the sort.
    return node_id < other.node_id || ( node_id == other.node_id && complement < other.complement );
  }
};

// type to hold po information: edge and name
using po_info_type = std::pair<Edge, std::string>;

struct Node
{
  int id;                      // node id
  std::array<Edge, 6> fanins;  // fanin edges: a, b, c, d, e, f
  int value;                   // user defined value
  int trav_id;                 // traversal id
  NodeType type;               // NodeType type = NodeType::PLAIN;

  inline bool operator== ( const Node &other ) const { return id == other.id; }
  inline bool operator!= ( const Node &other ) const { return id != other.id; }
  inline bool operator<  ( const Node &other ) const { return id < other.id; }
  inline bool operator>  ( const Node &other ) const { return id > other.id; }
};

//----------------------------------------------------------------------------------------
inline bool edgehash_predicate ( const std::array<Edge,3> &e1, const std::array<Edge, 3> &e2 )
{
  return e1[0] < e2[0];
}

// Reduction rules must be applied before calling hash
// TODO: how to speed this up?
struct EdgeHash
{
  size_t operator () ( const std::array<Edge, 6> &edges ) const
  {
    // 1. sort the 3 majority gates first, <a, b, c>, <b, d, e>, <c, e, f>
    auto &a = edges[0]; auto &b = edges[1]; auto &c = edges[2];
    auto &d = edges[3]; auto &e = edges[4]; auto &f = edges[5];
    std::array<Edge, 3> m1 = {a, b, c};
    std::sort ( &m1[0], &m1[2] ); // TODO: think of manual swap
    std::array<Edge, 3> m2 = {b, d, e};
    std::sort ( &m2[0], &m2[2] ); // TODO: think of manual swap
    std::array<Edge, 3> m3 = {c, e, f};
    std::sort ( &m3[0], &m3[2] ); // TODO: think of manual swap

    // 2. arrange the majorities min to max of first element
    std::array<std::array<Edge, 3>, 3> mmm = {m1, m2, m3};
    std::sort ( &mmm[0], &mmm[2], edgehash_predicate); // mmm should be unique for all valid combinations now

    // 3. compute the hash of individual elements and combine
    auto h0 = std::hash<int>() ( mmm[0][0].node_id );
    auto h1 = std::hash<int>() ( mmm[0][1].node_id );
    auto h2 = std::hash<int>() ( mmm[0][2].node_id );
    auto h3 = std::hash<int>() ( mmm[1][0].node_id );
    auto h4 = std::hash<int>() ( mmm[1][1].node_id );
    auto h5 = std::hash<int>() ( mmm[1][2].node_id );
    auto h6 = std::hash<int>() ( mmm[2][0].node_id );
    auto h7 = std::hash<int>() ( mmm[2][1].node_id );
    auto h8 = std::hash<int>() ( mmm[2][2].node_id );

    auto hash0 = (h0 ^ (h1 << 1)) ^ (h2 << 1);
    auto hash1 = (h3 ^ (h4 << 1)) ^ (h5 << 1);
    auto hash2 = (h6 ^ (h7 << 1)) ^ (h8 << 1);
    auto yig_hash = (hash0 ^ (hash1 << 1) ) ^ (hash2 << 1);
    return yig_hash;
  }
};

struct EdgeEqual
{
  size_t operator () ( const std::array<Edge, 6> &e1, const std::array<Edge, 6> &e2 ) const
  {
    return (e1[0] == e2[0]) && (e1[1] == e2[1]) && (e1[2] == e2[2]) && 
           (e1[3] == e2[3]) && (e1[4] == e2[4]) && (e1[5] == e2[5]);
  }
};

struct Yig
{
  std::string name = "";              // circuit name
  std::string file = "";              // source filename
  std::vector<Node> node_list;        // list of all nodes
  int curr_trav_id = 0;               // current traversal id
  std::vector<int> pi_nodeid_list;    // list of primary inputs.
  std::vector<po_info_type> po_list;  // list of primary outputs.
  // Hash table: reduction rules must be applied before calling hash
  std::unordered_map<std::array<Edge, 6>, int, EdgeHash, EdgeEqual> strash; 
  std::unordered_map<int, std::string> node_name_list;  // list of node names
  // list of fanout edges of each node. Key is node-id
  std::unordered_map<int, std::vector<Edge>> fanout_edge_list;
};

//----------------------------------------------------------------------------------------
// Note:: In general any function returning a Node returns a reference.
// Edge, node-id etc returns values.

// All utility functions
void init ( Yig &yig, std::string name = "NA", unsigned num_nodes = 20 );
inline bool is_initialized ( const Yig &yig ) { return yig.node_list.size () >= 2; }
bool check_yig ( const Yig &yig );
inline void set_network_name ( Yig &yig, const std::string &name ) { yig.name = name; }
inline std::string get_network_name ( const Yig &yig ) { return yig.name; }
inline void set_file ( Yig &yig, const std::string &name ) { yig.file = name; }
inline std::string get_file ( const Yig &yig ) { return yig.file; }
inline void set_trav_id ( Yig &yig, int trav_id ) { yig.curr_trav_id = trav_id; }
inline void inc_trav_id ( Yig &yig ) { yig.curr_trav_id = yig.curr_trav_id + 1; }
inline int get_trav_id ( const Yig &yig ) { return yig.curr_trav_id; }
inline void set_curr_trav_id ( Yig &yig, Node &node ) { node.trav_id = yig.curr_trav_id; }
inline bool good ( const Yig &yig )
{
  return yig.node_list.size () >= 1 && yig.node_list[0].id == 0;
}
inline int pi_count ( const Yig &yig ) { return yig.pi_nodeid_list.size(); }
inline int po_count ( const Yig &yig ) { return yig.po_list.size(); }
inline int node_count ( const Yig &yig ) { return yig.node_list.size(); }
inline int edge_count ( const Yig &yig );
inline Edge edge_one  () { return {0, true}; }
inline Edge edge_zero () { return {0, false}; }

// These are for C++11 iterations.
inline std::vector<Node> &all_nodes  ( Yig &yig ) { return yig.node_list; }
inline std::vector<Node> all_nodes   ( const Yig &yig ) { return yig.node_list; }
inline std::vector<int>  &all_pi_ids ( Yig &yig ) { return yig.pi_nodeid_list; }
inline std::vector<int>  all_pi_ids  ( const Yig &yig ) { return yig.pi_nodeid_list; }
inline std::vector<po_info_type> &all_pos ( Yig &yig ) { return yig.po_list; }
inline std::vector<po_info_type> all_pos ( const Yig &yig ) { return yig.po_list; }

// Adding/Updating new nodes, PI, POs
int add_yig ( Yig &yig, std::array<Edge, 6> &fanins ); // returns a new node-id
int add_pi  ( Yig &yig, const std::string &name = ""); // returns a new node-id
void tag_as_po  ( Yig &yig, const Edge &edge, const std::string &name = "");
void remove_all_po_tags_for_edge ( Yig &yig, const Edge &edge );
void remove_nth_po_tag ( Yig &yig, int n );
void remove_first_po_tag ( Yig &yig );
void add_fanins ( Yig &yig, Node &node, std::array<Edge, 6> &fanins ); // add fanins to an already created node
void update_fanins ( Yig &yig, Node &node, std::array<Edge, 6> &fanins ); // modify fanins to an already created node

// Other gates
inline int add_maj ( Yig &yig, Edge &a, Edge &b, Edge &c ) // Majority gate
{
  assert ( good (yig) );
  std::array<Edge, 6> arr = {a, b, c, c, a, b};
  return add_yig ( yig, arr );
}
inline int add_and ( Yig &yig, Edge &a, Edge &b ) { auto z = edge_zero(); return add_maj ( yig, a, b, z ); }
inline int add_or  ( Yig &yig, Edge &a, Edge &b ) { auto n = edge_one(); return add_maj ( yig, a, b, n ); }
inline int add_and ( Yig &yig, Edge &x, Edge &y, Edge &z ) // 3-input AND
{
  assert ( good (yig) );
  std::array<Edge, 6> arr = { edge_zero(), x, y, z, edge_zero(), edge_zero() };
  return add_yig ( yig, arr );
}
inline int add_or ( Yig &yig, Edge &x, Edge &y, Edge &z ) // 3-input OR
{
  assert ( good (yig) );
  std::array<Edge, 6> arr = { edge_one(), x, y, z, edge_one(), edge_one() };
  return add_yig ( yig, arr );
}

// add the nodes, without any connection, return the node-id
int add_node ( Yig &yig );
// Add num_nodes to the network, all nodes dangling
inline void add_nodes ( Yig &yig, int num_nodes )
{
  for ( auto i=0; i < num_nodes; i++ ) {
    add_node ( yig );
  }
}

//-----------------------------------------------------------------------------
// is_two_and ()
// is_three_and ( )
// is_two_or ()
// is_three_or ( )
// nand, nor, xor, xnor, majority, mux


//-----------------------------------------------------------------------------

// Edge-node-id conversions
inline Edge to_edge  ( const Node &node, bool value ) { return {node.id, value}; }
inline Edge to_edge  ( const int node_id, bool value ) { return {node_id, value}; }
inline Edge to_edge  ( const po_info_type &po ) { return po.first; }
inline Edge to_regular_edge ( const Node &node ) { return to_edge ( node.id, false ); }
inline Edge to_complement_edge ( const Node &node ) { return to_edge ( node.id, true ); }
inline Edge to_regular_edge ( const int node_id ) { return to_edge ( node_id, false ); }
inline Edge to_complement_edge ( const int node_id ) { return to_edge ( node_id, true ); }
inline int to_id ( const Node &node ) { return node.id; }
inline int to_id ( const Edge &edge ) { return edge.node_id; }
inline int to_id ( const po_info_type &po ) { return to_id ( po.first ); }
inline Node &to_node ( Yig &yig, int id )
{
  assert ( id < yig.node_list.size () );
  return yig.node_list[id];
}
inline Node &to_node ( Yig &yig, const Edge &edge ) 
{
  return to_node ( yig, edge.node_id );
}

inline Node to_node ( const Yig &yig, int id )
{
  assert ( id < yig.node_list.size () );
  return yig.node_list[id];
}
inline Node to_node ( const Yig &yig, const Edge &edge ) 
{
  return to_node ( yig, edge.node_id );
}
inline Node to_node ( const Yig &yig, const po_info_type &po )
{
  assert ( good ( yig ) );
  return to_node ( yig, po.first );
}
inline Node &to_node ( Yig &yig, po_info_type &po )
{
  assert ( good ( yig ) );
  return to_node ( yig, po.first );
}

inline std::string to_name ( const po_info_type &po ) { return po.second; }
std::string to_name ( const Yig &yig, const Edge &edge );
inline std::string to_name ( const Yig &yig, const Node &node )
{
  Edge e = to_edge ( node, false );
  return to_name ( yig, e );
}

std::string to_yig_string ( const Yig &yig, const Node &node );
std::string to_yig_string ( const Yig &yig, const po_info_type &po );

//-----------------------------------------------------------------------------
inline bool is_regular ( const Edge &edge ) { return !edge.complement; }
inline bool is_complement ( const Edge &edge ) { return edge.complement; }
inline bool is_const1 ( const Edge &edge ) { return edge.node_id == 0 && edge.complement; }
inline bool is_const0 ( const Edge &edge ) { return edge.node_id == 0 && !edge.complement; }
inline bool is_const ( const Edge &edge ) { return edge.node_id == 0; }
inline bool is_const0 ( const Node &node ) { return node.id == 0; }
inline bool is_const0 ( int node_id ) { return node_id == 0; }
inline bool is_pi ( const Node &node )
{
  return node.type == NodeType::PI || node.type == NodeType::PI_PO;
}
inline bool is_pi ( const Yig &yig, int id ) { return is_pi ( to_node (yig, id) ); };
inline bool is_node_pipo ( const Node &node ) { return node.type == NodeType::PI_PO; }
inline bool is_node_pipo ( const Yig &yig, int id ) { return is_node_pipo ( to_node (yig, id) ); };
inline bool is_node_constpo ( const Node &node ) { return node.type == NodeType::CONST0_PO; }
inline bool is_node_constpo ( const Yig &yig, int id ) { return is_node_constpo ( to_node (yig, id) ); };
inline bool is_node_po ( const Node &node )
{
  auto type = node.type;
  return type == NodeType::PO || type == NodeType::PI_PO || type == NodeType::CONST0_PO;
}
inline bool is_node_po ( const Yig &yig, int id ) { return is_node_po ( to_node (yig, id) ); };

// is_po () is costly. Use maximum is_node_*po() versions
inline bool is_po ( const Yig &yig, const Edge &edge )
{
  if ( !is_node_po ( to_node ( yig, edge ) ) ) return false;
  // however true does not guarantee that edge is a PO
  for ( const auto &po : all_pos ( yig ) ) {
    // below is wrong, since same node can have both false and true PO
    // if ( to_id ( po ) == to_id ( edge ) ) return edge == po; 
    if ( edge == po.first ) return true;
  }
  return false;
}

inline bool is_po_regular ( const po_info_type &po ) { return is_regular ( po.first );}
inline bool is_po_complement ( const po_info_type &po ) { return is_complement ( po.first );}

inline bool is_trav_id_curr ( const Yig &yig, const Node &node ) { return node.trav_id == yig.curr_trav_id; }
inline bool is_trav_id_curr ( const Yig &yig, const Edge &edge ) { return to_node ( yig, edge ).trav_id == yig.curr_trav_id; }
inline bool is_trav_id_curr ( const Yig &yig, int &node_id ) { return to_node ( yig, node_id ).trav_id == yig.curr_trav_id; }
inline bool is_trav_id_last ( const Yig &yig, const Node &node ) { return node.trav_id == ( yig.curr_trav_id - 1 ); }
inline bool is_trav_id_last ( const Yig &yig, const Edge &edge ) { return to_node ( yig, edge ).trav_id == (yig.curr_trav_id - 1); }
inline bool is_trav_id_last ( const Yig &yig, int node_id ) { return to_node ( yig, node_id ).trav_id == (yig.curr_trav_id - 1); }

inline bool has_fanout_edges ( const Yig &yig, int node_id )
{
  auto itr = yig.fanout_edge_list.find ( node_id );
  if ( itr == yig.fanout_edge_list.end () ) return true;
  return itr->second.empty (); // can have empty output edge list also
}
inline bool has_fanout_edges ( const Yig &yig, const Node &node ) { return has_fanout_edges ( yig, to_id ( node ) );}

inline bool has_name ( const Yig &yig, int node_id )
{
  return yig.node_name_list.find ( node_id ) != yig.node_name_list.end ();
}
inline bool has_name ( const Yig &yig, Node &node ) { return has_name ( yig, to_id ( node ) );}
inline bool has_name ( const Yig &yig, Edge &edge ) { return has_name ( yig, to_id ( edge ) );}

//-----------------------------------------------------------------------------
// Special nodes: const1
inline Node &get_const0 ( Yig &yig ) { assert ( good ( yig ) ); return yig.node_list[0]; }
inline Node get_const0 ( const Yig &yig ) { assert ( good ( yig ) ); return yig.node_list[0]; }
inline Edge get_edge_const1 ( Yig &yig ) { assert ( good ( yig ) ); return {yig.node_list[0].id, true}; }
inline Edge get_edge_const0 ( Yig &yig ) { assert ( good ( yig ) ); return {yig.node_list[0].id, false}; }

//-----------------------------------------------------------------------------
// Fanouts
inline std::vector<Edge> &get_fanout_edges ( Yig &yig, int node_id )
{
  assert ( good ( yig ) );
  assert ( node_id < yig.node_list.size () );
  auto edges = yig.fanout_edge_list.find ( node_id );
  //assert ( edges != yig.fanout_edge_list.end () );
  // Only way to get out of this is to create empty vec and return tht ref
  if ( edges == yig.fanout_edge_list.end () ) {
    yig.fanout_edge_list [node_id] = {};
    return yig.fanout_edge_list [node_id];
  }
  return edges->second;
}
inline std::vector<Edge> &get_fanout_edges ( Yig &yig, const Node &node )
{
  return get_fanout_edges ( yig, to_id ( node ) );
}

inline std::vector<Edge> get_fanout_edges ( const Yig &yig, int node_id )
{
  assert ( good ( yig ) );
  assert ( node_id < yig.node_list.size () );
  auto edges = yig.fanout_edge_list.find ( node_id );
  //assert ( edges != yig.fanout_edge_list.end () );
  if ( edges == yig.fanout_edge_list.end () ) return {};
  return edges->second;
}
inline std::vector<Edge> get_fanout_edges ( const Yig &yig, const Node &node )
{
  return get_fanout_edges ( yig, to_id ( node ) );
}

//-----------------------------------------------------------------------------
// Fanin
inline std::array<Edge, 6> &get_fanin_edges ( Node &node )
{
  return node.fanins;
}
inline std::array<Edge, 6> &get_fanin_edges ( Yig &yig, int node_id )
{
  assert ( good ( yig ) );
  return get_fanin_edges ( to_node ( yig, node_id ) );
}

inline std::array<Edge, 6> get_fanin_edges ( const Node &node )
{
  return node.fanins;
}
inline std::array<Edge, 6> get_fanin_edges ( const Yig &yig, int node_id )
{
  assert ( good ( yig ) );
  return get_fanin_edges ( to_node ( yig, node_id ) );
}


//-----------------------------------------------------------------------------
// Primary Inputs and Outputs
inline int id_to_pi_num ( const Yig &yig, const int id )
{
  assert ( good ( yig ) );
  const auto &pis = all_pi_ids ( yig );
  for ( auto i = 0u; i < pis.size (); i++ )
    if ( pis[i] == id ) return i;
  
  return -1;
}

inline int node_to_pi_num ( const Yig &yig, const Node &node )
{
  assert ( good ( yig ) );
  return id_to_pi_num ( yig, to_id ( node ) );
}

  /*
inline int id_to_po_num ( const Yig &yig, const int id )
{
  assert ( false );
  return -1;
}
  */

inline int pi_num_to_id ( const Yig &yig, int pi_num )
{
  assert ( false );
  assert ( good ( yig ) );
  assert ( pi_num < yig.pi_nodeid_list.size () );
  auto nodeid = yig.pi_nodeid_list[pi_num];
  assert ( nodeid < yig.node_list.size() );
  return nodeid;
}


inline Node &pi_num_to_node ( Yig &yig, int pi_num )
{
  auto nodeid = pi_num_to_id ( yig, pi_num );
  return to_node ( yig, nodeid );
}

inline Node pi_num_to_node ( const Yig &yig, int pi_num )
{
  auto nodeid = pi_num_to_id ( yig, pi_num );
  return to_node ( yig, nodeid );
}

inline Edge pi_num_to_edge ( const Yig &yig, int pi_num )
{
  auto nodeid = pi_num_to_id ( yig, pi_num );
  return to_edge ( nodeid, false );
}

inline std::string pi_num_to_name ( const Yig &yig, int pi_num )
{
  auto edge = pi_num_to_edge ( yig, pi_num );
  return to_name ( yig, edge );
}

inline po_info_type po_num_to_po ( const Yig &yig, int po_num )
{
  assert ( false );
  assert ( good ( yig ) );
  assert ( po_num < yig.po_list.size () );
  return yig.po_list[po_num];
}

inline Edge po_num_to_edge ( const Yig &yig, int po_num ) 
{
  return po_num_to_po ( yig, po_num ).first;
}

inline std::string po_num_to_name ( const Yig &yig, int po_num )
{
  return po_num_to_po ( yig, po_num ).second;
}

//----------------------------------------------------------------------------------------
// compute the levels, similar to BFS
std::vector<int> compute_levels ( const Yig &yig );
int get_max_level ( const Yig &yig, const std::vector<int> &levels );
inline int get_max_level ( const Yig &yig )
{
  const auto &levels = compute_levels ( yig );
  return get_max_level ( yig, levels );
}

//----------------------------------------------------------------------------------------

// this is very basic I/O
void write_yag2 ( const Yig &yig, std::ostream &ofs );

// from ygate_io.cpp
// read and write from Yig format
Yig read_yag ( const std::string &file );
void write_yag ( const Yig &yig, const std::string &file );
void write_yag ( const Yig &yig, std::ostream &ofs );
inline void write_yag ( const Yig &yig, const std::string &file )
{
  assert ( good ( yig ) );
  std::ofstream ofs ( file );
  write_yag ( yig, ofs );
}

void print_yig ( const Yig &yig );

//----------------------------------------------------------------------------------------
// from conversions.cpp
// these are the naive direct versions
// Yig ntk_to_yig_topo ( abc::Abc_Ntk_t * ntk );
Yig ntk_to_yig_rev_topo ( abc::Abc_Ntk_t * ntk ); // prefer this one mostly
abc::Abc_Ntk_t *yig_to_ntk ( const Yig &yig );
abc::Gia_Man_t *yig_to_gia ( const Yig &yig );
Yig gia_to_yig ( abc::Gia_Man_t *gia );

// Use these versions for interconversion
inline Yig to_yig ( abc::Abc_Ntk_t *ntk ) { return ntk_to_yig_rev_topo ( ntk ); }
inline Yig to_yig ( abc::Gia_Man_t *gia )        { return gia_to_yig ( gia ); }
inline abc::Abc_Ntk_t *to_ntk ( const Yig &yig ) { return yig_to_ntk ( yig ); }
inline abc::Gia_Man_t *to_gia ( const Yig &yig ) { return yig_to_gia ( yig ); }


void yig_to_verilog ( const Yig &yig, std::ofstream &ofs );
inline void yig_to_verilog ( const Yig &yig, const std::string &file )
{
  std::ofstream ofs ( file );
  yig_to_verilog ( yig, ofs );
}
inline void write_verilog ( const Yig &yig, std::ofstream &ofs )
{
  yig_to_verilog ( yig, ofs );
}
inline void write_verilog ( const Yig &yig, const std::string &file )
{
  yig_to_verilog ( yig, file );
}
abc::Abc_Ntk_t *yig_to_ntk ( Yig &yig, bool dummy );
// clang-format on

// For QCA
int max_qca_delay ( Yig &yig );
int count_qca_inversions ( Yig &yig );
long qca_cost_function ( Yig &yig );
int count_qca_maj ( Yig &yig );

//----------------------------------------------------------------------------------------
} // Yise namespace

#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

