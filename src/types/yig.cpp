/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : yig.cpp
 * @brief  : The Y inverter Gate. Base type
 */
//-------------------------------------------------------------------------------

#include "yig.hpp"
#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iterator>
#include <regex>
#include <string>
#include <sstream>

#include <utils/common_utils.hpp>


namespace Yise
{

void init ( Yig &yig, std::string name, unsigned num_nodes )
{
  yig.name = name;
  yig.node_list.reserve ( num_nodes );
  yig.node_list.emplace_back ( Node{0, {}, 0, 0, NodeType::CONST0} ); // const0
  yig.strash.reserve ( num_nodes );
  yig.fanout_edge_list.reserve ( num_nodes );
}

bool check_yig ( const Yig &yig ) // TODO: put more checks here
{
  if ( yig.node_list.size () < 2 ) return false; // Uninitialized YIG
  if ( yig.node_list[0].id != 0 ) return false;  // const-0 node
  if ( yig.pi_nodeid_list.size () < 1 )
    return false; // shoule be atleast 1 pi and 1 po
  if ( yig.po_list.size () < 1 )
    return false; // shoule be atleast 1 pi and 1 po
  for ( auto i = 0; i < yig.node_list.size (); i++ )
  {
    if ( yig.node_list[i].id != i ) return false;
  }
  return true;
}

std::string to_name ( const Yig &yig, const Edge &edge )
{
  auto id = edge.node_id;
  assert ( id < yig.node_list.size () );

  if ( is_const1 ( edge ) ) return "1";
  if ( is_const0 ( edge ) ) return "0";

  auto itr = yig.node_name_list.find ( id );
  std::string name = "";

  if ( itr != yig.node_name_list.end () )
    name = itr->second;
  else
    name = std::string ( "w" ) + std::to_string ( id );

  if ( edge.complement ) return std::string ( "~" ) + name;
  return name;
}

void __add_to_fanout ( Yig &yig, Edge &__edge, Node &node )
{
  auto driver = to_id ( __edge );
  Edge e = {to_id ( node ), __edge.complement}; // new fanout edge
  auto e_itr = yig.fanout_edge_list.find ( driver );
  if ( e_itr != yig.fanout_edge_list.end () ) // already present
  {
    auto &edges = e_itr->second;
    // 1. check if there is enough space
    // for special nodes, we assume lot of fanouts
    if ( edges.size () == edges.capacity () )
    {
      if ( is_const0 ( driver ) )
        edges.reserve ( edges.size () + 32 );
      else
        edges.reserve ( edges.size () + 4 );
    }

    edges.emplace_back ( std::move ( e ) );
    yig.fanout_edge_list[driver] = std::move ( edges );
  }
  else // create a new entry
  {
    if ( is_const0 ( driver ) )
      yig.fanout_edge_list[driver].reserve ( 32 ); // expect a lot
    else
      yig.fanout_edge_list[driver].reserve ( 4 ); // reserve space for 4 fanouts

    yig.fanout_edge_list[driver].emplace_back ( std::move ( e ) );
  }
}

void add_fanins ( Yig &yig, Node &node, std::array<Edge, 6> &fanins )
{
  // 1. add fanins of this node
  node.fanins = fanins;
  // 2. for each of these fanin, append node as a fanout
  Edge edge;
  // std::cout << "\n  add_fanin: " << to_name (yig, node) << " = Y3("
  //	    << to_name (yig, fanins[0]) << ", "
  //  	    << to_name (yig, fanins[1]) << ", "
  //  	    << to_name (yig, fanins[2]) << ", "
  //  	    << to_name (yig, fanins[3]) << ", "
  //  	    << to_name (yig, fanins[4]) << ", "
  //  	    << to_name (yig, fanins[5]) << "\n";
  // print_yig (yig);

  edge = fanins[0];
  __add_to_fanout ( yig, edge, node );
  edge = fanins[1];
  __add_to_fanout ( yig, edge, node );
  edge = fanins[2];
  __add_to_fanout ( yig, edge, node );
  edge = fanins[3];
  __add_to_fanout ( yig, edge, node );
  edge = fanins[4];
  __add_to_fanout ( yig, edge, node );
  edge = fanins[5];
  __add_to_fanout ( yig, edge, node );
}

void update_fanins ( Yig &yig, Node &node, std::array<Edge, 6> &fanins )
{
  // need to remove the already connected drivers first.
  for ( auto &fanin : fanins )
  {
    auto driver_id = to_id ( fanin );
    auto edge_to_remove = to_edge ( node.id, fanin.complement );
    const auto it = yig.fanout_edge_list.find ( driver_id );
    assert ( it !=
             yig.fanout_edge_list.end () ); // should be there in the table
    auto edge_list = it->second;
    auto pos = std::find ( edge_list.begin (), edge_list.end (), edge_to_remove );
    assert ( pos != edge_list.end () ); // edge should be already entered
    edge_list.erase ( pos );
    yig.fanout_edge_list[driver_id] = edge_list; // not really required
  }

  add_fanins ( yig, node, fanins );
}


int add_yig ( Yig &yig, std::array<Edge, 6> &fanins )
{
  assert ( good ( yig ) );
  auto &a = fanins[0];
  auto &b = fanins[1];
  auto &c = fanins[2];
  auto &d = fanins[3];
  auto &e = fanins[4];
  auto &f = fanins[5];

  // reduction rules
  // TODO: find and apply new reduction rules
  // 1. consts - 0, 1 and unit clauses.
  // all the below cases will become Maj ( <x,x,a>  <x,x,b>  <x,x,c> )
  // same vals in middle, left, right or bottom
  if ( ( b == c ) && ( b == e ) ) return b.node_id;
  if ( ( a == b ) && ( b == d ) ) return a.node_id;
  if ( ( a == c ) && ( c == f ) ) return c.node_id;
  if ( ( d == e ) && ( e == f ) ) return d.node_id;

  // check if already strashed
  const auto it = yig.strash.find ( fanins );
  if ( it != yig.strash.end () ) return it->second;

  // reached here. create a new node, update the tables and return the id.
  Node node;
  node.id = yig.node_list.size (); // counting from 0
  node.type = NodeType::PLAIN;
  node.value = 0;
  node.trav_id = 0;
  // update order 1. strash,  2. node_list 3. add_fanins
  yig.strash[fanins] = node.id;
  yig.node_list.emplace_back ( std::move ( node ) );
  // the fanout edges should not be populated
  assert ( yig.fanout_edge_list.find ( node.id ) == yig.fanout_edge_list.end () );
  add_fanins ( yig, node, fanins );

  return node.id;
}


// TODO: Do we need to enter PIs to strash or not? Decide later
// currently not entering.
// We always provide a name to the PI
int add_pi ( Yig &yig, const std::string &name )
{
  assert ( good ( yig ) );
  std::string pi_name = "i" + std::to_string ( yig.pi_nodeid_list.size () );
  if ( name != "" ) pi_name = name;

  Node node;
  node.fanins = {-1, -1, -1, -1, -1, -1}; // fill with -1
  node.id = yig.node_list.size ();        // counting from 0
  node.type = NodeType::PI;
  node.value = 0;
  node.trav_id = 0;
  yig.node_list.emplace_back ( std::move ( node ) );
  yig.pi_nodeid_list.emplace_back ( node.id );
  yig.node_name_list[to_id ( node )] = pi_name;

  // the fanout edges should not be populated at this stage
  assert ( yig.fanout_edge_list.find ( node.id ) == yig.fanout_edge_list.end () );

  return node.id;
}

// We always provide a name to the PO
void tag_as_po ( Yig &yig, const Edge &edge, const std::string &name )
{
  assert ( good ( yig ) );
  std::string po_name;
  if ( name != "" )
    po_name = name;
  else
    po_name = "o" + std::to_string ( yig.po_list.size () );

  auto &node = to_node ( yig, edge ); // update the reference as PO type

  if ( node.type == NodeType::CONST0 )
    node.type = NodeType::CONST0_PO;
  else if ( node.type == NodeType::PI )
    node.type = NodeType::PI_PO;
  else if ( node.type == NodeType::PLAIN )
    node.type = NodeType::PO;

  yig.po_list.emplace_back ( po_info_type{edge, name} );

  // note: internal node name must start with "w", so cannot tag like below
  // yig.node_name_list[to_id ( edge )] = po_name;
}

void remove_all_po_tags_for_edge ( Yig &yig, const Edge &edge )
{
  assert ( good ( yig ) );
  for ( auto i = 0; i < po_count ( yig ); i++ )
  {
    if ( yig.po_list[i].first == edge )
      yig.po_list.erase ( yig.po_list.begin () + i );
  }
  auto &node = to_node ( yig, edge );
  auto type = node.type; // revert the node type
  if ( type == NodeType::PO )
    node.type = NodeType::PLAIN;
  else if ( type == NodeType::PI_PO )
    node.type = NodeType::PI;
  else if ( type == NodeType::CONST0_PO )
    node.type = NodeType::CONST0;
  else
    assert ( false && "NodeType must be PO/PI_PO/CONST0_PO" );
}

void remove_nth_po_tag ( Yig &yig, int n )
{
  assert ( good ( yig ) );
  assert ( n < yig.po_list.size () && "n out of bound" );
  auto &node = to_node ( yig, yig.po_list[n].first );
  yig.po_list.erase ( yig.po_list.begin () + n );
  auto type = node.type; // revert the node type
  if ( type == NodeType::PO )
    node.type = NodeType::PLAIN;
  else if ( type == NodeType::PI_PO )
    node.type = NodeType::PI;
  else if ( type == NodeType::CONST0_PO )
    node.type = NodeType::CONST0;
  else
    assert ( false && "NodeType must be PO/PI_PO/CONST0_PO" );
}

void remove_first_po_tag ( Yig &yig )
{
  remove_nth_po_tag ( yig, 0 );
}

int edge_count ( const Yig &yig )
{
  auto num_fanouts = 0;
  for ( auto &fanout : yig.fanout_edge_list )
  {
    num_fanouts = num_fanouts + fanout.second.size ();
  }
  auto num_fanins = yig.node_list.size () * 6;
  std::cout << "Fanin edges: " << num_fanins << " Fanout edges: " << num_fanouts << "\n";
  return num_fanouts;
}

// Just add the nodes, without any connection
int add_node ( Yig &yig )
{

  assert ( good ( yig ) );
  Node node;
  node.fanins = {-2, -2, -2, -2, -2, -2}; // to be connected later
  node.id = yig.node_list.size ();        // counting from 0
  node.type = NodeType::PLAIN;
  node.value = 0;
  node.trav_id = 0;
  yig.node_list.emplace_back ( std::move ( node ) );
  return node.id;
}


int level_rec ( const Yig &yig, std::vector<int> &levels, const int node_id )
{
  assert ( node_id < levels.size () );
  assert ( good ( yig ) );

  if ( is_const0 ( node_id ) ) return -2; // const, not really needed
  if ( levels[node_id] != -1 ) return levels[node_id]; // already computed
  const auto &fanins = get_fanin_edges ( yig, node_id );
  auto l0 = levels[to_id ( fanins[0] )];
  auto l1 = levels[to_id ( fanins[1] )];
  auto l2 = levels[to_id ( fanins[2] )];
  auto l3 = levels[to_id ( fanins[3] )];
  auto l4 = levels[to_id ( fanins[4] )];
  auto l5 = levels[to_id ( fanins[5] )];
  if ( l0 == -1 ) l0 = level_rec ( yig, levels, to_id ( fanins[0] ) );
  if ( l1 == -1 ) l1 = level_rec ( yig, levels, to_id ( fanins[1] ) );
  if ( l2 == -1 ) l2 = level_rec ( yig, levels, to_id ( fanins[2] ) );
  if ( l3 == -1 ) l3 = level_rec ( yig, levels, to_id ( fanins[3] ) );
  if ( l4 == -1 ) l4 = level_rec ( yig, levels, to_id ( fanins[4] ) );
  if ( l5 == -1 ) l5 = level_rec ( yig, levels, to_id ( fanins[5] ) );
  assert ( l0 != -1 );
  assert ( l1 != -1 );
  assert ( l2 != -1 );
  assert ( l3 != -1 );
  assert ( l4 != -1 );
  assert ( l5 != -1 );

  // how come all 6 inputs are constants?
  assert ( ( l0 + l1 + l2 + l3 + l4 + l5 ) != -12 ); 
  
  levels[node_id] = std::max ( {l0, l1, l2, l3, l4, l5} ) + 1;
  return levels[node_id];
}


// All PIs are level 0.
// const-0 is at level -1.
// Returns a map of node-id vs level.
std::vector<int> compute_levels ( const Yig &yig )
{
  std::vector<int> levels;
  levels.assign ( node_count ( yig ), -1 ); // includes const0
  levels[0] = -2; // const0 -> initial assign -2, in the end change back to -1.

  for ( auto &pi : all_pi_ids ( yig ) )
  {
    levels[pi] = 0;
  }

  for ( auto &po : all_pos ( yig ) )
  {
    level_rec ( yig, levels, to_id ( po ) );
  }

  levels[0] = -1; // put the correct the const0 level back
  return levels;
}

int get_max_level ( const Yig &yig, const std::vector<int> &levels )
{
  int max_level = 0;
  for ( auto &po : all_pos ( yig ) )
  {
    if ( levels[to_id ( po )] > max_level ) max_level = levels[to_id ( po )];
  }
  return max_level;
}



// ONLY for iwls competition
// IWLS competition does not start counting nodes from 0, rather from 1

// basic I/O
void write_yag2 ( const Yig &yig, std::ostream &ofs )
{
  assert ( good ( yig ) );
  ofs << "YAG \n";
  ofs << ".i " << pi_count ( yig ) << "\n";
  ofs << ".o " << po_count ( yig ) << "\n";

  const auto &pos = all_pos ( yig );

  // std::unordered_map<int, bool> po_map;
  // po_map.reserve ( po_count );

  for ( const auto &po : all_pos ( yig ) )
  {
    ofs << to_yig_string ( yig, po ) << "  // id:" << to_id ( po ) << "\n";
  }

  for ( const auto &n : all_nodes ( yig ) )
  {
    if ( is_const0 ( n ) ) continue;
    if ( is_pi ( n ) ) continue;
    ofs << to_yig_string ( yig, n ) << "  // id:" << to_id ( n ) << "\n";
  }

  ofs << ".e\n";
  ofs << "Written by aXc on :: " << curr_time_str () << "\n";
}


std::string to_yig_string ( const Yig &yig, const Node &node )
{
  assert ( good ( yig ) );
  assert ( !is_pi ( node ) && "PI cannot have drivers!" );
  assert ( !is_const0 ( node ) && "Const cannot have drivers!" );

  return to_name ( yig, node ) + " = Y2 (" + to_name ( yig, node.fanins[0] ) + ", " +
         to_name ( yig, node.fanins[1] ) + ", " + to_name ( yig, node.fanins[2] ) +
         ", " + to_name ( yig, node.fanins[3] ) + ", " +
         to_name ( yig, node.fanins[4] ) + ", " + to_name ( yig, node.fanins[5] ) + ")";
}

std::string to_yig_string ( const Yig &yig, const po_info_type &po )
{
  auto po_node = to_node ( yig, po.first );
  if ( is_node_pipo ( po_node ) )
  {
    const auto &pin = to_name ( yig, po_node );
    const auto pi_name = is_po_regular ( po ) ? pin : ( std::string ( "~" ) + pin );
    return to_name ( po ) + " = Y2 (" + pi_name + ", " + pi_name + ", 0, " + pi_name + ", 0, 0)";
  }
  if ( is_const0 ( po_node ) )
  {
    std::string pin = is_const1 ( po.first ) ? "1" : "0";
    return to_name ( po ) + " = Y2 (" + pin + ", " + pin + ", 0, " + pin +
           ", 0, 0)";
  }

  assert ( po_node.type == NodeType::PO );

  auto a = is_po_regular ( po ) ? po_node.fanins[0] : !po_node.fanins[0];
  auto b = is_po_regular ( po ) ? po_node.fanins[1] : !po_node.fanins[1];
  auto c = is_po_regular ( po ) ? po_node.fanins[2] : !po_node.fanins[2];
  auto d = is_po_regular ( po ) ? po_node.fanins[3] : !po_node.fanins[3];
  auto e = is_po_regular ( po ) ? po_node.fanins[4] : !po_node.fanins[4];
  auto f = is_po_regular ( po ) ? po_node.fanins[5] : !po_node.fanins[5];


  assert ( to_id ( a ) != -2 && " Driver:a uninitialized " );
  assert ( to_id ( b ) != -2 && " Driver:b uninitialized " );
  assert ( to_id ( c ) != -2 && " Driver:c uninitialized " );
  assert ( to_id ( d ) != -2 && " Driver:d uninitialized " );
  assert ( to_id ( e ) != -2 && " Driver:e uninitialized " );
  assert ( to_id ( f ) != -2 && " Driver:f uninitialized " );

  assert ( to_id ( a ) >= 0 );
  assert ( to_id ( b ) >= 0 );
  assert ( to_id ( c ) >= 0 );
  assert ( to_id ( d ) >= 0 );
  assert ( to_id ( e ) >= 0 );
  assert ( to_id ( f ) >= 0 );

  return to_name ( po ) + " = Y2 (" + to_name ( yig, a ) + ", " +
         to_name ( yig, b ) + ", " + to_name ( yig, c ) + ", " + to_name ( yig, d ) +
         ", " + to_name ( yig, e ) + ", " + to_name ( yig, f ) + ")";
}

/*
std::string to_yig_string2 ( const Yig &yig, const Node &node )
{
  assert ( false ); // complete, distinguish b/w Y2 and Y1
}
*/



namespace iwls
{
std::string to_iwls_name ( const Yig &yig, const Edge &edge )
{
  auto id = edge.node_id;
  auto node = to_node ( yig, edge );
  
  assert ( id < yig.node_list.size () );

  if ( is_const1 ( edge ) ) return "1";
  if ( is_const0 ( edge ) ) return "0";

  std::stringstream str;
  if ( edge.complement ) str << "~";
  
  if ( is_pi ( node ) )
  {
    str << "i" << id ; 
  }
  else if ( is_node_pipo ( node ) )
  {
    str << "i" << id ;
  }
  else if ( is_node_constpo ( node ) )
  {
    assert ( false );
  }
  else
  {
    str << "w" << ( id - pi_count ( yig ) );
  }

  auto name = str.str ();
  return name;
}

inline std::string to_iwls_name ( const po_info_type &po )
{
  assert ( false );
  std::stringstream str;
  str << "o" << ( to_id ( po ) + 1 );
  return str.str ();
}

inline std::string to_iwls_name ( const int &po_index )
{
  std::stringstream str;
  str << "o" << ( po_index + 1 );
  return str.str ();
}

inline std::string to_iwls_name ( const Yig &yig, const Node &node )
{
  Edge e = to_edge ( node, false );
  return to_iwls_name ( yig, e );
}

std::string to_iwls_yig_string ( const Yig &yig, const Node &node )
{
  assert ( good ( yig ) );
  assert ( !is_pi ( node ) && "PI cannot have drivers!" );
  assert ( !is_const0 ( node ) && "Const cannot have drivers!" );

  return to_iwls_name ( yig, node ) + " = Y2 ("
    + to_iwls_name ( yig, node.fanins[0] ) + ", "
    + to_iwls_name ( yig, node.fanins[1] ) + ", "
    + to_iwls_name ( yig, node.fanins[2] ) + ", "
    + to_iwls_name ( yig, node.fanins[3] ) + ", "
    + to_iwls_name ( yig, node.fanins[4] ) + ", "
    + to_iwls_name ( yig, node.fanins[5] ) + ")";
}


std::string to_iwls_yig_string (
  const Yig &yig, const po_info_type &po,
  const int po_index )
{
  auto po_node = to_node ( yig, po.first );
  if ( is_node_pipo ( po_node ) )
  {
    const auto &pin = to_iwls_name ( yig, po_node );
    const auto pi_name = is_po_regular ( po ) ? pin : ( std::string ( "~" ) + pin );
    return to_iwls_name ( po_index ) + " = Y2 (" + pi_name + ", " + pi_name + ", 0, " + pi_name + ", 0, 0)";
  }
  if ( is_const0 ( po_node ) )
  {
    std::string pin = is_const1 ( po.first ) ? "1" : "0";
    return to_iwls_name ( po_index ) + " = Y2 (" + pin + ", " + pin + ", 0, " + pin +
           ", 0, 0)";
  }

  assert ( po_node.type == NodeType::PO );

  auto a = is_po_regular ( po ) ? po_node.fanins[0] : !po_node.fanins[0];
  auto b = is_po_regular ( po ) ? po_node.fanins[1] : !po_node.fanins[1];
  auto c = is_po_regular ( po ) ? po_node.fanins[2] : !po_node.fanins[2];
  auto d = is_po_regular ( po ) ? po_node.fanins[3] : !po_node.fanins[3];
  auto e = is_po_regular ( po ) ? po_node.fanins[4] : !po_node.fanins[4];
  auto f = is_po_regular ( po ) ? po_node.fanins[5] : !po_node.fanins[5];


  assert ( to_id ( a ) != -2 && " Driver:a uninitialized " );
  assert ( to_id ( b ) != -2 && " Driver:b uninitialized " );
  assert ( to_id ( c ) != -2 && " Driver:c uninitialized " );
  assert ( to_id ( d ) != -2 && " Driver:d uninitialized " );
  assert ( to_id ( e ) != -2 && " Driver:e uninitialized " );
  assert ( to_id ( f ) != -2 && " Driver:f uninitialized " );

  assert ( to_id ( a ) >= 0 );
  assert ( to_id ( b ) >= 0 );
  assert ( to_id ( c ) >= 0 );
  assert ( to_id ( d ) >= 0 );
  assert ( to_id ( e ) >= 0 );
  assert ( to_id ( f ) >= 0 );

  return to_iwls_name ( po_index ) + " = Y2 ("
    + to_iwls_name ( yig, a ) + ", " + to_iwls_name ( yig, b ) + ", "
    + to_iwls_name ( yig, c ) + ", " + to_iwls_name ( yig, d ) + ", "
    + to_iwls_name ( yig, e ) + ", " + to_iwls_name ( yig, f ) + ")";
}

// basic I/O
void write_yag ( const Yig &yig, std::ostream &ofs )
{
  assert ( good ( yig ) );
  ofs << ".i " << pi_count ( yig ) << "\n";
  ofs << ".o " << po_count ( yig ) << "\n";
  ofs << ".w " << ( node_count ( yig ) - pi_count ( yig ) - 1 ) << "\n";
  const auto &pos = all_pos ( yig );

  int indx = 0;
  for ( const auto &po : all_pos ( yig ) )
  {
    ofs << to_iwls_yig_string ( yig, po, indx ) << "\n";
    indx++;
  }

  for ( const auto &n : all_nodes ( yig ) )
  {
    if ( is_const0 ( n ) ) continue;
    if ( is_pi ( n ) ) continue;
    ofs << to_iwls_yig_string ( yig, n ) << "\n";
  }

  ofs << ".e\n";
}

} // namespace iwls


inline bool is_majority_or_lesser ( Node &n )
{
  auto ff = n.fanins;
  std::vector<Edge> f = {ff[0], ff[1], ff[2], ff[3], ff[4], ff[5] };
  std::sort ( &f[0], &f[5] );
  auto last = std::unique ( f.begin(), f.end() );
  f.erase ( last, f.end () );
  const auto s = f.size ();
  
  if ( s == 1 )
  {
    return true;
  }
  else if ( s == 2 ) // only 2 inputs (2 input gate)
  {
    return true;
  }
  else if ( s == 3 ) // only a MAJ/AND2/OR2 gate can have this
  {
    return true;
  }

  return false;
}


void update_delay_rec ( Yig &yig, Node &n )
{
  if ( is_pi ( n ) )
  {
    n.value = 0;
    return;
  }
  if ( n.value != -1 ) return;
  
  auto &f = n.fanins;
  update_delay_rec ( yig, to_node ( yig, f[0] ) );
  update_delay_rec ( yig, to_node ( yig, f[1] ) );
  update_delay_rec ( yig, to_node ( yig, f[2] ) );
  update_delay_rec ( yig, to_node ( yig, f[3] ) );
  update_delay_rec ( yig, to_node ( yig, f[4] ) );
  update_delay_rec ( yig, to_node ( yig, f[5] ) );
  
  int v0 = to_node ( yig, f[0] ).value;
  int v1 = to_node ( yig, f[1] ).value;
  int v2 = to_node ( yig, f[2] ).value;
  int v3 = to_node ( yig, f[3] ).value;
  int v4 = to_node ( yig, f[4] ).value;
  int v5 = to_node ( yig, f[5] ).value;

  if ( is_complement ( f[0] ) ) v0++;
  if ( is_complement ( f[1] ) ) v1++;
  if ( is_complement ( f[2] ) ) v2++;
  if ( is_complement ( f[3] ) ) v3++;
  if ( is_complement ( f[4] ) ) v4++;
  if ( is_complement ( f[5] ) ) v5++;

  auto m = std::max ( {v0, v1, v2, v3, v4, v5} );
  if ( is_majority_or_lesser ( n ) )
    n.value = m + 1;
  else
    n.value = m + 2; // 2 levels of Maj in one y gate
}


int max_qca_delay ( Yig &yig )
{
  for ( auto &n : yig.node_list )
  {
    n.value = -1;
  }
  yig.node_list[0].value = 0;

  auto max_delay = 0;
  for ( auto &po : yig.po_list )
  {
    auto n = to_node ( yig, po.first );
    update_delay_rec ( yig, n );
    if ( n.value > max_delay ) max_delay = n.value;
  }

  return max_delay;
}

int count_qca_inversions ( Yig &yig )
{
  auto compl_edge_count = 0;
  for ( const auto &n : yig.node_list )
  {
    const auto &f = n.fanins;
    if ( f[0].complement )  compl_edge_count++;
    if ( f[1].complement )  compl_edge_count++;
    if ( f[2].complement )  compl_edge_count++;
    if ( f[3].complement )  compl_edge_count++;
    if ( f[4].complement )  compl_edge_count++;
    if ( f[5].complement )  compl_edge_count++;
  }
  return compl_edge_count;
}


int count_qca_maj ( Yig &yig )
{
  int count = 0;
  for ( auto &n : yig.node_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const0 ( n ) ) continue;
    if ( is_majority_or_lesser ( n ) )
      count++;
    else
      count = count + 4;
  }

  return count;
}

// Cost = (M^2 + I + C^2) * T
// M = #Maj, I = #Inv, C = #CrossOvers (not available), T = delay
long qca_cost_function ( Yig &yig )
{
  const long M = count_qca_maj ( yig );
  const long T = max_qca_delay ( yig );
  const long I = count_qca_inversions ( yig );

  const long cost_func = ( ( M * M ) + I ) * T;
  return cost_func;
  
}


//------------------------------------------------------------------------------
} // Yise


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
