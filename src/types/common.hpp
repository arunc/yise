/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : common.hpp
 * @brief  : interconversions among the types MIG, AIG etc
 */

/*-------------------------------------------------------------------------------*/


#ifndef COMMON_HPP
#define COMMON_HPP

#include "aig.hpp"
#include "mig.hpp"
#include "qca.hpp"

#include <iomanip>
#include <sstream>

namespace Yise
{

// from functions/io/aiger.cpp
namespace Mig
{
Aig::Aig to_aig ( const Mig &mig );
}

namespace Qca
{
Aig::Aig to_aig ( const Qca &qca );
Qca opt_maj ( Aig::Aig &orig_aig );
}

namespace Aig
{
Mig::Mig to_mig ( const Aig &aig );
}

inline std::string to_hex_str ( const unsigned long &tt )
{
  std::stringstream str;
  str << "0x";
  str << std::hex << std::setw ( 16 ) << std::setfill ( '0' ) << tt;
  return str.str ();
}

inline std::string to_hex_str ( const unsigned &tt )
{
  std::stringstream str;
  str << "0x";
  str << std::hex << std::setw ( 8 ) << std::setfill ( '0' ) << tt;
  return str.str ();
}

inline std::string to_cex_str ( const std::vector<int> &cex )
{
  std::stringstream ss;
  for ( const auto &el : cex )
  {
    ss << el << " ";
  }
  return ss.str ();
}


}
#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

