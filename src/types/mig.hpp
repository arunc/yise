/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : mig.hpp
 * @brief  : Majority Inverter Graph
 */

/*-------------------------------------------------------------------------------
      a
     / \
    /   \
   b ---- c

m = Maj (a, b, c) := <a, b, c>
m = ab + bc + ac

Reduction rules
Unit clauses and consts
1) <a, a, c>  =  a
2) <a, !a, c> =  c
3) <0, b, c>  =  b & c    (2 i/p AND)
4) <1, b, c>  =  b | c    (2 i/p OR)

Axiom:
Inversion of m = m of inversions, follows from the majority property

--------------------------------------------------------------------------------
Conventions in the MIG DAG::
Edge is a literal
id   is a variable
Node is a vertex
pi   is a primary input
po   is a primary output

literal: (variable and sign) : ( id << 1 ) | sign )
id0: *const-1*, id1 : pi0, id2: pi2 and so on
po is only a tag. Any literal ( i.e., edge ) can be tagged as a PO.
Note: 0 is the literal for const-1 and 1 is the literal for const-0 

pi_list is a list of primary input edges (**edges are taken as PI, not Nodes)
po_list is a list of primary output edges

--------------------------------------------------------------------------------
Basic construction:
Mig mig ( "myCircuit", 100 );         // 100: estimate of max node-count

Edge in0 = add_pi ( mig, "in0" );     // "in0":  name of the primary input
Edge in1 = add_pi ( mig, "in1" );     // "in1":  name of the primary input
Edge in2 = add_pi ( mig, "in2" );     // "in2":  name of the primary input

Edge a = add_and ( mig, in0, in1 );   // a = in0 & in1    (AND gate)
Edge b = add_or  ( mig, in0, in1 );   // b = in0 | in1    (OR gate)
Edge c = add_maj ( mig, a, b, in2 );  // c = <a, b, in2>  (Majority gate)
Edge d = add_xor ( mig, a, b );       // d = a ^ b        (XOR gate) 
Edge e = complement ( c );            // e = !c           (NOT gate)

tag_as_po ( mig, e, "out0" );         // "out0":  name of the primary output
tag_as_po ( mig, d, "out1" );         // "out1":  name of the primary output

mig = strash ( mig );                 // cleanup and remove stray nodes

-------------------------------------------------------------------------------*/
//------------------------------------------------------------------------------


#ifndef MIG_HPP
#define MIG_HPP

#include "platform.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// clang-format off
namespace Yise
{
namespace Mig
{

enum class NodeType
{
  CONST1,     // const-1
  PI,         // primary input
  PI_PO,      // primary input and output together
  PO,         // primary output
  PLAIN       // internal node; anything other than the above types
};

struct Mig;
struct Node;

// Edge is the index of the Node ( node-id) shifted by 1 bit to left
// Edge: = ( id ( node ) << 1 ) | negation
using Edge = unsigned;
static const Edge edge_undef = 0xfffffffe;
static const Edge edge1 = 0x0;
static const Edge edge0 = 0x1;

// type to hold po information: edge and name
// Note: edge can have a separate name, if its a PI or internal node
struct Po_t
{
  Edge edge;
  Mig *p_mig;
  std::string name;
  int indx = -1;
  
  Po_t () {}
  Po_t ( Edge e, const std::string &_name, Mig *mig_ptr )
  {
    edge = e;
    name = _name;
    p_mig = mig_ptr;
  }
  inline void update_index ( int index ) { indx = index ;}
  inline unsigned node_id   () const { return edge >> 1; }
  inline bool is_complement () const { return 1u == ( edge & 1u ); }
};
  
//------------------------------------------------------------------------------
// Note: Reduction rules must be applied before calling hash
inline void sort_three ( Edge &a, Edge &b, Edge &c)
{
  if ( a > b ) std::swap ( a, b );
  if ( a > c ) std::swap ( a, c );
  if ( b > c ) std::swap ( b, c );
}
  
struct EdgeHash
{
  size_t operator () ( const std::array<Edge, 3> &edges ) const
  {
    // 1. sort the majority gate first, <a, b, c>
    std::array<Edge, 3> m = {edges[0], edges[1], edges[2]};
    sort_three ( m[0], m[1], m[2] );

    // 2. compute the hash of individual elements and combine
    auto h0 = std::hash<Edge>() ( m[0] );
    auto h1 = std::hash<Edge>() ( m[1] );
    auto h2 = std::hash<Edge>() ( m[2] );

    auto mig_hash = (h0 ^ (h1 << 1) ) ^ (h2 << 1);
    return mig_hash;
  }
};

struct EdgeEqual
{
  size_t operator () ( const std::array<Edge, 3> &e1, const std::array<Edge, 3> &e2 ) const
  {
    std::array<Edge, 3> m1 = e1;
    std::array<Edge, 3> m2 = e2;
    sort_three ( m1[0], m1[1], m1[2] );
    sort_three ( m2[0], m2[1], m2[2] );
    return (m1[0] == m2[0]) && (m1[1] == m2[1]) && (m1[2] == m2[2]);
  }
};

using HashTable = std::unordered_map<std::array<Edge, 3>, Edge, EdgeHash, EdgeEqual>;
using NameList_t = std::unordered_map<Edge, std::string>;
using FanoutList_t = std::unordered_map<Edge, std::unordered_set<Edge>>;
//------------------------------------------------------------------------------
// Note: p_mig is ntk ptr. After any transfer in ownership this ptr to be updated.
struct Node
{
  unsigned id;                 // node id
  std::array<Edge, 3> fanins;  // fanin edges: a, b, c
  int value;                   // user defined value
  int trav_id;                 // traversal id
  int level;                   // level in the topological order
  NodeType type;               // NodeType type = NodeType::PLAIN;
  Mig *p_mig;                  // Network
  bool flagA;                  // flag1 for optimization
  bool flagB;                  // flag2 for optimization

  Node ( int _id, const std::array<Edge, 3> &_fanins, NodeType _type, Mig *_mig )
    : p_mig ( _mig )
  {
    id = _id;
    fanins = _fanins;
    type = _type;
    value = 0;
    trav_id = 0;
    level = -2;
  }
  inline bool operator== ( const Node &other ) const { return id == other.id; }
  inline bool operator<  ( const Node &other ) const { return id < other.id; }
  
  inline Edge edge ( bool negate = false ) const { return ( id << 1 ) | negate; }
};

//------------------------------------------------------------------------------
class Mig
{
public:
  std::string       name = "";        // circuit name
  std::string       file = "";        // source filename
  std::vector<Node> node_list;        // list of all nodes
  int               curr_trav_id = 0; // current traversal id
  std::vector<Edge> pi_list;          // list of primary inputs (only regular edges)
  std::vector<Po_t> po_list;          // list of primary outputs
  NameList_t        name_list;        // list of edge names, edges have polarity
  FanoutList_t      fanout_list;      // fanout-edges of each edge, only regular edges

  // Notes:
  // fanout_list is a map of Node to fanouts. pi_list is a list of PI nodes.
  // But instead of nodes, we store corresponding regular edges
  // name_list is a map of Edge to name, +ve/-ve edges can have separate names
  //
  //----------------------------------------------------------------------------
  Mig () {} // default ctor
  Mig ( const std::string &ckt, const unsigned nnodes ) { init ( ckt, nnodes ); }
  Mig ( const Mig& other )    { copy ( other ); } // copy ctor;
  Mig (Mig&& other) noexcept  { move ( other ); } // move ctor
  Mig& operator= (const Mig& other)     { copy ( other ); return *this; } // copy assign
  Mig& operator= (Mig&& other) noexcept { move ( other ); return *this; } // move assign

  // explicit initialization of MIG
  void init ( const std::string &ckt_name, const unsigned num_nodes )
  {
    // LSB0 for sign, and LSB1 used in hashing
    static const auto limit = 1ul << ( ( sizeof ( unsigned ) * 8 ) - 2  );
    assert ( num_nodes < limit );
    name = ckt_name;
    node_list.reserve ( num_nodes );
    node_list.emplace_back ( Node{0, {}, NodeType::CONST1, this} ); // const1
    strash.reserve ( num_nodes );
    fanout_list.reserve ( num_nodes );
  }

  inline bool good () const
  {
    if ( node_list.empty () ) return false;
    return ( node_list[0].id == 0 ) && ( node_list[0].p_mig == this );
  }

  inline unsigned num_pis   () const { return pi_list.size (); }
  inline unsigned num_pos   () const { return po_list.size (); }
  inline unsigned num_nodes () const { return node_list.size (); }

  friend Edge add_mig ( Mig &mig, const std::array<Edge, 3> &fanins, const std::string &name );
  friend bool lookup_mig_node ( Mig &mig, Edge a, Edge b, Edge c );
private:
  // only add_mig has access to hash-table
  HashTable strash; // reduction rules must be applied before calling hash

  // default ctors cannot update the p_mig in the node list.
  // hence need explicit copy, move ctors
  void copy ( const Mig &other )
  {
    name = other.name;
    file = other.file;
    node_list = other.node_list;
    curr_trav_id = other.curr_trav_id;
    pi_list = other.pi_list;
    po_list = other.po_list;
    name_list = other.name_list;
    fanout_list = other.fanout_list;
    for ( auto &n : node_list ) // default cannot do this
    {
      n.p_mig = this;
    }
    for ( auto &po : po_list )
    {
      po.p_mig = this;
    }
  }

  void move ( Mig &other ) noexcept
  {
    name = std::move ( other.name );
    file = std::move ( other.file );
    node_list = std::move ( other.node_list );
    curr_trav_id = std::move ( other.curr_trav_id );
    pi_list = std::move ( other.pi_list );
    po_list = std::move ( other.po_list );
    name_list = std::move ( other.name_list );
    fanout_list = std::move ( other.fanout_list );
    for ( auto &n : node_list ) // default cannot do this
    {
      n.p_mig = this;
    }
    for ( auto &po : po_list )
    {
      po.p_mig = this;
    }
  }
  
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// All utility functions

inline Edge complement    ( const Edge &e )  { return e ^ 1u; }
inline Edge regular       ( const Edge &e )  { return ( e >> 1 ) << 1; }
inline bool is_complement ( const Edge &e )  { return 1u == ( e & 1u ); }
inline bool is_regular    ( const Edge &e )  { return !is_complement ( e ); }

inline void set_network_name ( Mig &mig, const std::string &name ) { mig.name = name; }
inline std::string get_network_name ( const Mig &mig ) { return mig.name; }
inline void set_file ( Mig &mig, const std::string &name ) { mig.file = name; }
inline std::string get_file ( const Mig &mig ) { return mig.file; }
inline void set_trav_id ( Mig &mig, int trav_id ) { mig.curr_trav_id = trav_id; }
inline void inc_trav_id ( Mig &mig ) { mig.curr_trav_id = mig.curr_trav_id + 1; }
inline int get_trav_id       ( const Mig &mig ) { return mig.curr_trav_id; }
inline void set_curr_trav_id ( Node &node ) { node.trav_id = node.p_mig->curr_trav_id; }

//------------------------------------------------------------------------------
// Edge-node-id conversions
inline Edge to_edge ( int id, bool negate = false ) { return (id << 1) | negate; }
inline int  to_id   ( const Edge &e ) { return e >> 1; }
inline Node to_node ( Mig &mig, Edge &e )
{
  auto id = to_id ( e );
  assert ( mig.good () && id < mig.node_list.size () );
  return mig.node_list[id];
}
inline Node* to_node_p ( Mig &mig, Edge &e )
{
  auto id = to_id ( e );
  assert ( mig.good () && id < mig.node_list.size () );
  return &mig.node_list[id];
}
inline Node to_node ( const Mig &mig, const Edge &e )
{
  assert ( mig.good () && to_id ( e ) < mig.node_list.size () );
  return mig.node_list[to_id ( e )];
}
inline Node to_node ( Mig *p_mig, Edge &e )
{
  auto id = to_id ( e );
  assert ( p_mig->good () && id < p_mig->node_list.size () );
  return p_mig->node_list[id];
}
inline Node* to_node_p ( Mig *p_mig, Edge &e )
{
  auto id = to_id ( e );
  assert ( p_mig->good () && id < p_mig->node_list.size () );
  return &p_mig->node_list[id];
}
inline Node to_node ( const Mig *p_mig, const Edge &e )
{
  assert ( p_mig->good () && to_id ( e ) < p_mig->node_list.size () );
  return p_mig->node_list[to_id ( e )];
}

inline Node to_node ( const Po_t &po ) { 
  assert ( po.p_mig->good () );
  return po.p_mig->node_list[po.node_id ()];
}

// id and edge are same types. Hence prefix _id_ to distinguish
inline Node id_to_node ( Mig &mig, int id )
{
  assert ( mig.good () && id < mig.node_list.size () );
  return mig.node_list[id];
}
inline Node id_to_node ( const Mig &mig, int id )
{
  assert ( mig.good () && id < mig.node_list.size () );
  return mig.node_list[id];
}
inline Node id_to_node ( Mig *p_mig, int id )
{
  assert ( p_mig->good () && id < p_mig->node_list.size () );
  return p_mig->node_list[id];
}
inline Node id_to_node ( const Mig *p_mig, int id )
{
  assert ( p_mig->good () && id < p_mig->node_list.size () );
  return p_mig->node_list[id];
}

std::string to_name ( const Mig *p_mig, const Edge &e );
std::string to_name ( const Mig &mig, const Edge &e );
std::string to_name ( Mig &mig, const Edge &e );
std::string to_name ( Mig *p_mig, const Edge &e );
inline std::string to_name ( const Node &n ) { return to_name ( *n.p_mig , n.edge () ); }
inline std::string to_name ( Node &n ) { return to_name ( *n.p_mig , n.edge () ); }

inline Node const1_node ( const Mig &mig ) { assert ( mig.good () ); return mig.node_list[0]; }
inline int  const1_id   () { return 0; }

//------------------------------------------------------------------------------
inline Node to_node0  ( const Node &n ) { 
  assert ( n.p_mig->good () );
  assert ( to_id ( n.fanins[0] ) < n.p_mig->node_list.size () );
  return n.p_mig->node_list[ to_id ( n.fanins[0] )];
}
inline Node to_node1  ( const Node &n ) { 
  assert ( n.p_mig->good () );
  assert ( to_id ( n.fanins[1] ) < n.p_mig->node_list.size () );
  return n.p_mig->node_list[ to_id ( n.fanins[1] )];
}
inline Node to_node2  ( const Node &n ) { 
  assert ( n.p_mig->good () );
  assert ( to_id ( n.fanins[2] ) < n.p_mig->node_list.size () );
  return n.p_mig->node_list[ to_id ( n.fanins[2] )];
}
inline int to_id0 ( const Node &n ) { return to_node0 ( n ).id; }
inline int to_id1 ( const Node &n ) { return to_node1 ( n ).id; }
inline int to_id2 ( const Node &n ) { return to_node2 ( n ).id; }

//------------------------------------------------------------------------------
// boolean predicates
inline bool is_const ( const Node &n )  { return n.id == 0; }
inline bool is_const ( const Edge &e )  { return ( e == edge0 ) || ( e == edge1 ); } 
inline bool is_id_const ( const int id ){ return id == 0 ; } 
inline bool is_const ( const Po_t &po ) { return is_const ( po.edge ); }

inline bool is_const1 ( const Node &n )  { return n.id == 0; }
inline bool is_id_const1 ( const int id ){ return id == 0; } 
inline bool is_const1 ( const Edge &e )  { return e == edge1; }
inline bool is_const1 ( const Po_t &po ) { return is_const1 ( po.edge ); }

inline bool is_const0 ( const Edge &e )  { return e == edge0; }
inline bool is_const0 ( const Po_t &po ) { return is_const0 ( po.edge ); }

inline bool is_pi ( const Node &n ) { return n.type == NodeType::PI || n.type == NodeType::PI_PO; }
inline bool is_pi ( const Mig &mig, const Edge &e ) { return is_pi ( to_node ( mig, e ) ); }
inline bool is_pi ( Mig *p_mig, Edge &e ) { return is_pi ( to_node ( p_mig, e ) ); }
inline bool is_id_pi ( const Mig &mig, const int &id ) { return is_pi ( id_to_node ( mig, id ) ); }
inline bool is_id_pi ( Mig *p_mig, int id ) { return is_pi ( id_to_node ( p_mig, id ) ); }

inline bool is_po ( const Node &n ) { return n.type == NodeType::PO || n.type == NodeType::PI_PO; }
inline bool is_po ( Mig *p_mig, Edge e ) { return is_po ( to_node ( p_mig, e ) ); }
inline bool is_po ( const Mig &mig, const Edge &e ) { return is_po ( to_node ( mig, e ) ); }
inline bool is_id_po ( const Mig &mig, const int &id ) { return is_po ( id_to_node ( mig, id ) ); }
inline bool is_id_po ( Mig *p_mig, int id ) { return is_po ( id_to_node ( p_mig, id ) ); }

inline bool is_pipo ( const Node &n ) { return n.type == NodeType::PI_PO; }
inline bool is_pipo ( const Mig &mig, const Edge &e ) { return is_pipo ( to_node ( mig, e ) ); }
inline bool is_pipo ( Mig *p_mig, Edge e ) { return is_pipo ( to_node ( p_mig, e ) ); }
inline bool is_id_pipo ( const Mig &mig, const int &id ) { return is_pipo ( id_to_node ( mig, id ) ); }
inline bool is_id_pipo ( Mig *p_mig, int id ) { return is_pipo ( id_to_node ( p_mig, id ) ); }

inline bool is_plain ( const Node &n ) { return n.type == NodeType::PLAIN; }
inline bool is_plain ( const Mig &mig, const Edge &e ) { return is_plain ( to_node ( mig, e ) ); }
inline bool is_id_plain ( const Mig &mig, const int &id ) { return is_plain ( id_to_node ( mig, id ) ); }

inline bool has_fanouts ( const Mig &mig, const Edge &e ) { return mig.fanout_list.find ( e ) != mig.fanout_list.end (); }
inline bool has_fanouts ( Mig *p_mig, Edge e ) { return p_mig->fanout_list.find ( e ) != p_mig->fanout_list.end (); }
inline bool has_fanouts ( const Node &n ) { return has_fanouts ( *n.p_mig, n.edge () ); }
inline bool has_fanouts_for_id ( const Mig &mig, const int &id ) { return has_fanouts ( mig, to_edge ( id ) ); }

inline bool has_name ( const Mig &mig, const Edge &e ) { return mig.name_list.find ( e ) != mig.name_list.end (); }
inline bool has_name ( Mig *p_mig, Edge e ) { return p_mig->name_list.find ( e ) != p_mig->name_list.end (); }
inline bool has_name ( const Node &n ) { return has_name ( *n.p_mig, n.edge () ); }
inline bool has_name_for_id ( const Mig &mig, const int &id ) { return has_name ( mig, to_edge ( id ) ); }


//------------------------------------------------------------------------------
// for C++11 iterators
inline std::vector<Node> all_nodes   ( Mig &mig )       { assert ( mig.good () ); return mig.node_list; }
inline std::vector<Po_t> all_pos     ( Mig &mig )       { assert ( mig.good () ); return mig.po_list; }
inline std::vector<Edge> all_pis     ( Mig &mig )       { assert ( mig.good () ); return mig.pi_list; }
inline std::vector<Node> all_nodes   ( const Mig &mig ) { assert ( mig.good () ); return mig.node_list; }
inline std::vector<Po_t> all_pos     ( const Mig &mig ) { assert ( mig.good () ); return mig.po_list; }
inline std::vector<Edge> all_pis     ( const Mig &mig ) { assert ( mig.good () ); return mig.pi_list; }
inline std::unordered_map<Edge, std::string> name_map ( Mig &mig ) { assert ( mig.good () ); return mig.name_list; }

inline std::vector<Node> all_nodes   ( Mig *p_mig ) { assert ( p_mig->good () ); return p_mig->node_list; }
inline std::vector<Po_t> all_pos     ( Mig *p_mig ) { assert ( p_mig->good () ); return p_mig->po_list; }
inline std::vector<Edge> all_pis     ( Mig *p_mig ) { assert ( p_mig->good () ); return p_mig->pi_list; }
inline std::unordered_map<Edge, std::string> name_map ( Mig *p_mig ) { assert ( p_mig->good () ); return p_mig->name_list; }

inline std::unordered_set<Edge> all_fanouts ( const Mig &mig, const Edge &e )
{
  assert ( mig.good () );
  const auto itr = mig.fanout_list.find ( e );
  if ( itr == mig.fanout_list.end () )
    return  std::unordered_set<Edge> {};
  else
    return itr->second;
}

inline std::unordered_set<Edge> all_fanouts ( Mig *p_mig, const Edge &e ) { return all_fanouts ( *p_mig, e ); }
inline std::unordered_set<Edge> all_fanouts ( const Node &n ) { return all_fanouts ( n.p_mig, n.edge () ); }

inline std::array<Edge, 3> all_fanins ( const Mig &mig, const Edge &e )
{
  if ( e == edge_undef )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef}; 
  }
  else if ( is_pi ( mig, e ) || is_const ( e ) )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef};
  }
  else
  {
    return to_node ( mig, e ).fanins;
  }
}

inline std::array<Edge, 3> all_fanins ( Mig *p_mig, Edge e )
{
  if ( e == edge_undef )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef}; 
  }
  else if ( is_pi ( p_mig, e ) || is_const ( e ) )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef};
  }
  else
  {
    return to_node ( p_mig, e ).fanins;
  }
}

inline std::array<Edge, 3> all_fanins ( const Node &n ) { return all_fanins ( *n.p_mig, n.edge () ); }

//------------------------------------------------------------------------------
// PI and PO indices
inline int edge_to_pi_index ( const Mig &mig, const Edge &e )
{
  assert ( is_pi ( mig, e ) );
  assert ( mig.good () );
  auto itr = std::find ( mig.pi_list.begin (), mig.pi_list.end (), e );
  assert ( itr != mig.pi_list.end () );
  return ( itr - mig.pi_list.begin () );
}

inline int node_to_pi_index ( const Node &n )
{
  assert ( n.p_mig->good () );
  assert ( is_pi ( n ) );
  return ( edge_to_pi_index ( *n.p_mig, n.edge () ) );
}

inline int id_to_pi_index ( const Mig &mig, const int node_id )
{
  assert ( is_pi ( mig, node_id ) );
  return ( edge_to_pi_index ( mig, to_edge ( node_id ) ) );
}

inline Edge po_index_to_edge ( const Mig &mig, const int i ) { return mig.po_list[i].edge; }
inline int po_index_to_id    ( const Mig &mig, const int i ) { return mig.po_list[i].node_id (); }

//------------------------------------------------------------------------------
// Traversal ID manipulation
inline bool is_trav_id_curr ( const Node &n ) { return n.trav_id == ( n.p_mig )->curr_trav_id; }
inline bool is_trav_id_last ( const Node &n ) { return n.trav_id == ( ( n.p_mig )->curr_trav_id - 1 ); }

//------------------------------------------------------------------------------
// structural hashing ( similar to copying )
Mig strash ( const Mig &mig1 );
Mig copy ( const Mig &mig1 ); // take a blind copy

// check if a MAJ node already exists for a given fanin
inline bool lookup_mig_node ( Mig &mig, Edge a, Edge b, Edge c )
{
  assert ( mig.good () );
  assert ( a != edge_undef );
  assert ( b != edge_undef );
  assert ( c != edge_undef );
  
  // reduction rules
  if ( ( a == b ) || ( a == c ) || ( b == c ) || ( a == complement ( b ) )
       || ( a == complement ( c ) ) || ( b == complement ( c ) ) )
  {
    return true;
  }

  // 1. check if already strashed
  std::array<Edge, 3> fanins = {a, b, c};
  std::sort ( &fanins[0], &fanins[2] );
  const auto it = mig.strash.find ( fanins );
  if ( it != mig.strash.end () ) return true;

  // 2. <!a, !b, !c> = !<a, b, c> :: covers nand, nor, and, or
  std::array<Edge, 3> inv_copy = {
    complement ( fanins[0] ),
    complement ( fanins[1] ),
    complement ( fanins[2] )
  };
  const auto inv_it = mig.strash.find ( inv_copy );
  if ( inv_it != mig.strash.end () ) return true;
  
  return false;
}

// Adding/Updating new nodes, PI, POs
Edge add_mig ( Mig &mig, const std::array<Edge, 3> &fanins, const std::string &name = "" );
Edge add_pi    ( Mig &mig, const std::string &name = "" );
void tag_as_po ( Mig &mig, Edge &edge, const std::string &name = "" );

inline Edge add_and (
  Mig &mig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge0, a, b};
  return add_mig ( mig, fanins, name );
}

inline Edge add_nand (
  Mig &mig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge1, complement ( a ), complement ( b ) };
  return add_mig ( mig, fanins, name );
}

inline Edge add_or (
  Mig &mig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge1, a, b};
  return add_mig ( mig, fanins, name );
}

inline Edge add_nor (
  Mig &mig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge0, complement ( a ), complement ( b )};
  return add_mig ( mig, fanins, name );
}

inline Edge add_maj (
  Mig &mig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {a, b, c}  ;
  return add_mig ( mig, fanins, name );
}

inline Edge add_mux (
  Mig &mig, const Edge &in0, const Edge &in1, const Edge &c, const std::string &name = "" )
{
  auto x = add_and ( mig, complement ( c ), in0 );
  auto y = add_and ( mig,  c, in1 );
  return add_or ( mig, x, y, name );
}
  
inline Edge add_xor (
  Mig &mig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  auto x = add_and ( mig, complement ( a ), b );
  auto y = add_and ( mig, complement ( b ), a );
  return add_or ( mig, x, y, name );
}

// a ^ b ^ c = Maj ( !Maj ( a, b, c), c, Maj ( a, b, !c) )
inline Edge add_xor (
  Mig &mig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto m1 = add_maj ( mig, a, b, c );
  auto m2 = add_maj ( mig, a, b, complement ( c ) );
  return add_maj ( mig, m1, c, m2, name );
}

inline Edge add_xnor (
  Mig &mig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  auto x = add_and ( mig, complement ( a ), b );
  auto y = add_and ( mig, complement ( b ), a );
  auto zz = complement ( add_or ( mig, x, y ) );
  if ( name != "" ) mig.name_list[zz] = name;
  return zz;
}

// a ^ b ^ c = Maj ( !Maj ( a, b, c), c, Maj ( a, b, !c) )
inline Edge add_xnor (
  Mig &mig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto m1 = add_maj ( mig, a, b, c );
  auto m2 = add_maj ( mig, a, b, complement ( c ) );
  auto zz = complement ( add_maj ( mig, m1, c, m2, name ) );
  if ( name != "" ) mig.name_list[zz] = name;
  return zz;
}


bool check_mig ( const Mig &mig, bool debug = false );

// All PIs are level 0. const-1 is at level -1. returns a map of node-id vs level.
std::vector<int> compute_levels ( const Mig &mig );
void update_levels ( Mig &mig ); // annotate all the nodes with levels
int get_max_level ( const Mig &mig );

void write_dot ( const Mig &mig, const std::string &file, const bool pretty = false );

// I/O (from aiger.cpp, miger.cpp)
Mig read_aig ( const std::string &file );
Mig read_mig ( const std::string &file );
void write_aig ( const Mig &mig, const std::string &file, bool ascii = false );
void write_mig ( const Mig &mig, const std::string &file, bool ascii = false );

} // Mig namespace
//------------------------------------------------------------------------------
// clang-format on
//------------------------------------------------------------------------------
} // Yise namespace

#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
