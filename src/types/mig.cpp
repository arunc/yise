/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : mig.cpp
 * @brief  : Majority Inverter Graph
 */

/*-------------------------------------------------------------------------------*/

//clang-format off

#include "mig.hpp"
#include "aig.hpp"

#include <utils/common_utils.hpp>

#include <sstream>



namespace Yise
{
namespace Mig
{

std::string to__name ( const Mig *p_mig, const Edge &e )
{
  if ( is_const0 ( e ) ) return "0";
  if ( is_const1 ( e ) ) return "1";
  if ( e == edge_undef ) return "undef";

  std::stringstream str;
  const auto itr = p_mig->name_list.find ( e );
  const auto itr_inv = p_mig->name_list.find ( complement ( e ) );

  if ( itr != p_mig->name_list.end () ) // check for pi
  {
    str << itr->second;
  }
  else if ( itr_inv != p_mig->name_list.end () ) // check for ~pi
  {
    str << "~"<< itr_inv->second;
  }
  else // all unnamed
  {
    const auto sign = is_complement ( e ) ? "~n" : "n";
    str << sign << to_id ( e );
  }

  return str.str ();
}

std::string to_name ( const Mig &mig, const Edge &e )   { return to__name ( &mig, e ); }
std::string to_name ( const Mig *p_mig, const Edge &e ) { return to__name ( p_mig, e ); }
std::string to_name ( Mig *p_mig, const Edge &e )       { return to__name ( p_mig, e ); }
std::string to_name ( Mig &mig, const Edge &e )         { return to__name ( &mig, e ); }

inline void add_fanout ( Mig &mig, const Edge &ee, const Edge &fanout_edge )
{
  const auto e = regular ( ee );
  mig.fanout_list[e].emplace ( fanout_edge ); // Hashset, removes duplicates
}

  
Edge add_mig ( Mig &mig, const std::array<Edge, 3> &fanins, const std::string &name )
{
  assert ( mig.good () );
  const auto &a = fanins[0];
  const auto &b = fanins[1];
  const auto &c = fanins[2];

  assert ( a != edge_undef );
  assert ( b != edge_undef );
  assert ( c != edge_undef );
  
  // reduction rules
  if ( a == b ) return a;
  if ( a == c ) return a;
  if ( b == c ) return b;
  if ( a == complement ( b ) ) return c;
  if ( a == complement ( c ) ) return b;
  if ( b == complement ( c ) ) return a;

  // strash rules
  // 1. check if already strashed
  auto copy = fanins;
  std::sort ( &copy[0], &copy[2] );
  const auto it = mig.strash.find ( copy );
  if ( it != mig.strash.end () ) return it->second;

  // 2. <!a, !b, !c> = !<a, b, c> :: covers nand, nor, and, or
  std::array<Edge, 3> inv_copy = {
    complement ( copy[0] ),
    complement ( copy[1] ),
    complement ( copy[2] )
  };
  const auto inv_it = mig.strash.find ( inv_copy );
  if ( inv_it != mig.strash.end () )
  {
    return complement ( inv_it->second );
  }

  // create a new node, update the tables
  int id = mig.node_list.size ();
  Node node = {id, copy, NodeType::PLAIN, &mig};
  auto edge = node.edge ();
  mig.strash[fanins] = edge;
  mig.node_list.emplace_back ( std::move ( node ) );
  assert ( mig.fanout_list.find ( edge ) == mig.fanout_list.end () );
  if ( name != "" ) mig.name_list[edge] = name;
  
  add_fanout ( mig, fanins[0], edge );
  add_fanout ( mig, fanins[1], edge );
  add_fanout ( mig, fanins[2], edge );

  return edge;
}

Edge add_pi ( Mig &mig, const std::string &name )
{
  assert ( mig.good () );
  std::stringstream pi_str;
  if ( name == "" )
  {
    pi_str << "pi" << mig.pi_list.size ();
  }
  else
  {
    pi_str << name;
  }

  int id = mig.node_list.size ();
  Node node = {id, {edge_undef, edge_undef, edge_undef}, NodeType::PI, &mig};
  auto edge = node.edge ();
  mig.pi_list.emplace_back ( edge );
  assert ( mig.fanout_list.find ( edge ) == mig.fanout_list.end () );
  mig.name_list[edge] = pi_str.str ();
  mig.node_list.emplace_back ( std::move ( node ) );
  return edge;
}

void tag_as_po ( Mig &mig, Edge &edge, const std::string &name )
{
  assert ( mig.good () );
  std::stringstream po_str;
  if ( name == "" )
  {
    po_str << "po" << mig.po_list.size ();
  }
  else
  {
    po_str << name;
  }

  auto po = Po_t ( edge, po_str.str (), &mig );
  po.update_index ( mig.po_list.size () );
  mig.po_list.emplace_back ( po );
  auto node = to_node_p ( mig, edge );
  
  if ( is_const ( edge ) )
  {
    assert ( node->type == NodeType::CONST1 );
  }
  else if ( is_pi ( mig, edge ) )
  {
    node->type = NodeType::PI_PO;
  }
  else
  {
    node->type = NodeType::PO;
  }

}


Edge add_edges_rec (
  std::unordered_map<Edge, Edge> &mig_map,
  Mig &mig2, const Mig &mig1,
  const Edge &e1
  )
{
  auto itr = mig_map.find ( e1 );
  if ( itr != mig_map.end () ) return itr->second;
  
  const auto &fanins = all_fanins ( mig1, e1 ); // implicit -> regular (e1)
  Edge a2 = add_edges_rec ( mig_map, mig2, mig1, fanins[0] );
  Edge b2 = add_edges_rec ( mig_map, mig2, mig1, fanins[1] );
  Edge c2 = add_edges_rec ( mig_map, mig2, mig1, fanins[2] );

  const auto &fanins2 = std::array<Edge, 3> {a2, b2, c2};
  Edge e2;
  if ( has_name ( mig1, e1 ) ) // if there is an internal node name, preserve it
  {
    e2 = add_mig ( mig2, fanins2, to_name ( mig1, e1 ) );    
  }
  else
  {
    e2 = add_mig ( mig2, fanins2 );
  }

  // e1 and e2 should be of same polarity
  // all_fanins () returns the regular edge only.
  // i.e., e1 is converted to regular ( e1 ) before returning fanins.
  // Hence if the incoming e1 is complemented that has to be reflected.
  if ( is_complement ( e1 ) ) e2 = complement ( e2 );
  mig_map[e1] = e2;
  mig_map[ complement ( e1 )] = complement ( e2 );
  return e2;
}


Mig strash ( const Mig &mig1 )
{
  assert ( check_mig ( mig1, true ) );
  auto mig2 = Mig ( mig1.name, mig1.num_nodes () );
  
  std::unordered_map<Edge, Edge> mig_map;
  mig_map.reserve ( mig1.num_nodes () );
  mig_map[edge0] = edge0;
  mig_map[edge1] = edge1;
  mig_map[edge_undef] = edge_undef;

  for ( const auto &pi : all_pis ( mig1 ) )
  {
    auto e = add_pi ( mig2, to_name ( mig1, pi ) );
    mig_map[pi] = e;
    mig_map[complement ( pi )] = complement ( e );
  }

  for ( const auto &po1 : mig1.po_list )
  {
    auto po2 = add_edges_rec ( mig_map, mig2, mig1, po1.edge );
    // added po edge can be of opposite sign, but taken care internally
    tag_as_po ( mig2, po2, po1.name );
  }
  
  assert ( check_mig ( mig2, true ) );
  return mig2;
}

Mig copy ( const Mig &mig1 )
{
  Mig mig_cpy;
  const auto p_mig_cpy = &mig_cpy;
  mig_cpy.file = mig1.file;
  mig_cpy.name = mig1.name;
  mig_cpy.node_list = mig1.node_list;
  mig_cpy.curr_trav_id = mig1.curr_trav_id;
  mig_cpy.pi_list = mig1.pi_list;
  mig_cpy.po_list = mig1.po_list;
  mig_cpy.name_list = mig1.name_list;
  mig_cpy.fanout_list = mig1.fanout_list;

  for ( auto &po : mig_cpy.po_list ) { po.p_mig = p_mig_cpy; }
  for ( auto &n : mig_cpy.node_list ) { n.p_mig = p_mig_cpy; }
  
  return mig_cpy;
}


std::string to_fanin_po_str ( const Mig &mig, const Edge &e )
{
  std::stringstream str;
  if ( is_pi ( mig, e ) || is_const ( e ) )
  {
    str << "< 0, " << to_name ( mig, e ) << ", " << to_name ( mig, e ) << " >";
    return str.str ();
  }

  const auto &fanins = all_fanins ( to_node ( mig, e ) );
  const auto &f0 = fanins[0] ;
  const auto &f1 = fanins[1] ;
  const auto &f2 = fanins[2] ;

   str << "< "
      << to_name ( mig, f0 ) << ", " << to_name ( mig, f1 ) << ", " << to_name ( mig, f2 )
      << " >";
  return str.str ();
}

std::string to_fanin_str ( const Mig &mig, const Edge &e )
{
  std::stringstream str;
  if ( is_pi ( mig, e ) || is_const ( e ) )
  {
    str << "< 0, " << to_name ( mig, e ) << ", " << to_name ( mig, e ) << " >";
    return str.str ();
  }

  const auto &fanins = all_fanins ( to_node ( mig, e ) );
  const auto &f0 = is_complement ( e ) ? complement ( fanins[0] ) : fanins[0] ;
  const auto &f1 = is_complement ( e ) ? complement ( fanins[1] ) : fanins[1] ;
  const auto &f2 = is_complement ( e ) ? complement ( fanins[2] ) : fanins[2] ;

  str << "< "
      << to_name ( mig, f0 ) << ", " << to_name ( mig, f1 ) << ", " << to_name ( mig, f2 )
      << " >";
  return str.str ();
}

std::string to_str ( const Mig &mig, const Edge &e )
{
  std::stringstream str;
  assert ( !is_complement ( to_node ( mig, e ).edge () ) );
  str << to_name ( to_node ( mig, e ) ) << " = ";
  str << to_fanin_str ( mig, e );
  return str.str ();
}

void write_mig ( const Mig &mig, std::ofstream &ofs )
{
  assert ( false ); // May be buggy, dont use now.
  assert ( check_mig ( mig, true ) );
  ofs << "MIG: " << mig.name << " @" << curr_time_str () << "\n";
  ofs << "PI:  ";
  for ( const auto &pi : all_pis ( mig ) )
  {
    ofs << to_name ( mig, pi ) << " ";
  }
  ofs << "\n";
  ofs << "PO:  ";
  
  for ( const auto &po : mig.po_list )
  {
    ofs << po.name << " ";
  }
  ofs << "\n";

  for ( const auto &po : all_pos ( mig ) )
  {
    auto e = po.edge;
    ofs << po.name << " = " << to_fanin_po_str ( mig, e ) << "\n";
  }

  for ( const auto &n : all_nodes ( mig ) )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_po ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    assert ( !is_complement ( n.edge () ) );
    ofs << to_str ( mig, n.edge () ) << "\n";
  }
  
}

bool check_mig ( const Mig &mig, bool debug )
{
  for ( const auto &n : mig.node_list )
  {
    if ( n.p_mig != &mig )
    {
      if ( debug ) std::cout << "Node: " << n.id << " MIG is wrong\n";
      return false;
    }
  }
  if ( !mig.good () )
  {
    if ( debug ) std::cout << "Mig is not good\n";
    return false;
  }
  return true;
}


void level_rec ( std::vector<int> &levels, const Node &node )
{
  const auto &id = node.id;
  if ( levels[id] != -1 ) return; // already computed
  if ( id == 0 ) return; // const1
  const auto &id0 = to_id0 ( node );
  const auto &id1 = to_id1 ( node );
  const auto &id2 = to_id2 ( node );
  if ( levels [id0] == -1 ) level_rec ( levels, to_node0 ( node ) );
  if ( levels [id1] == -1 ) level_rec ( levels, to_node1 ( node ) );
  if ( levels [id2] == -1 ) level_rec ( levels, to_node2 ( node ) );

  auto my_max = [&] ( int a, int b, int c )
    {
      return a > b ? ( a > c ? a : c ) : ( b > c ? b : c );
    };
  
  levels[id] = my_max ( levels[id0], levels[id1], levels[id2] ) + 1;
}

// All PIs are level 0. const-1 is at level -1.
// Returns a map of node-id vs level.
std::vector<int> compute_levels ( const Mig &mig )
{
  assert ( mig.good () );
  std::vector<int> levels;
  levels.assign ( mig.node_list.size (), -1 );
  levels[0] = -2; // const1

  for ( const auto &pi : mig.pi_list )
  {
    levels[ to_id ( pi )] = 0;
  }
  for ( const auto &po : mig.po_list )
  {
    level_rec ( levels, to_node ( po ) );
  }
  levels[0] = -1; // const1
  return levels;
}

void update_levels ( Mig &mig )
{
  const auto &levels = compute_levels ( mig );
  for ( auto &n : mig.node_list )
  {
    n.level = levels[n.id];
  }
}

int get_max_level ( const Mig &mig )
{
  int max = -2;
  const auto &levels = compute_levels ( mig );
  for ( const auto &po : mig.po_list )
  {
    if (levels [po.node_id ()] > max )
    {
      max = levels [po.node_id ()];
    }
  }
  return max;
}

int get_max_level ( const Mig &mig, const std::vector<int> &levels )
{
  int max = -2;
  for ( const auto &po : mig.po_list )
  {
    if (levels [po.node_id ()] > max )
    {
      max = levels [po.node_id ()];
    }
  }
  return max;
}

void write_dot ( const Mig &mig, const std::string &file, const bool pretty )
{
  assert ( mig.good () );
  if ( mig.po_list.empty () )
  {
    std::cout << "[e] No Primary Output to writeout!\n";
    return;
  }
  if ( mig.pi_list.empty () ) // ToDo: remove this restriction
  {
    std::cout << "[e] No Primary Input. Plain const outputs are ignored!\n";
    return;
  }
  if ( mig.node_list.size () > 200 )
  {
    std::cout << "[e] Cannot write dot files for #nodes > 200!\n";
    return;
  }
  auto ofs = fopen ( (char *)file.c_str (), "w" );
  if ( ofs  == NULL )
  {
    std::cout << "[e] Cannot open " << file << " for writing!\n";
    return;
  }
  
  auto levels = compute_levels ( mig );
  levels[0] = 0;
  const auto min_level = 0;
  const auto max_level = get_max_level ( mig, levels ) + 1; // 1+ for POs
  const auto num_nodes = mig.num_nodes (); // including 0, and PIs
  bool const_po = false;
  bool const_edge = false;
  
  // 1. write the DOT header
  std::fprintf ( ofs, "# %s\n",  "GraphViz network by Yise" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "digraph network {\n" );
  if ( pretty )
  {
    std::fprintf ( ofs, "size = \"5,5\";\n" );
    std::fprintf ( ofs, "fontsize=26;\n" );
  }
  else
  {
    std::fprintf ( ofs, "size = \"7.5,10\";\n" );
  }
  std::fprintf ( ofs, "center = true;\n" );
  std::fprintf ( ofs, "edge [dir = back];\n" );
  std::fprintf ( ofs, "\n" );
  
  // labels on the left of the picture
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  node [shape = plaintext];\n" );
  std::fprintf ( ofs, "  edge [style = invis];\n" );
  std::fprintf ( ofs, "  LevelTitle1 [label=\"\"];\n" );
  
  // 2. generate node names with labels
  // labels are provided later with groups
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    // the visible node name
    std::fprintf ( ofs, "  Level%d", Level );
    std::fprintf ( ofs, " [label = " );
    // label name
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "];\n" );
  }

  // 2. generate the levels for nodes
   std::fprintf ( ofs, "  LevelTitle1 ->" );
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    std::fprintf ( ofs, "  Level%d",  Level );
    if ( Level != min_level )
      std::fprintf ( ofs, " ->" );
    else
      std::fprintf ( ofs, ";" );
  }
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

  
  // 3. generate title box on top
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  rank = same;\n" );
  std::fprintf ( ofs, "  LevelTitle1;\n" );
  std::fprintf ( ofs, "  title1 [shape=plaintext,\n" );
  if ( pretty )
  {
    std::fprintf ( ofs, "          fontsize=26,\n" );
  }
  //std::fprintf ( ofs, "          fontsize=18,\n" );
  //std::fprintf ( ofs, "          fontname = \"Times-Roman\",\n" );
  std::fprintf ( ofs, "          label=\"" );
  std::fprintf ( ofs, "MIG: %s ", (char *)mig.name.c_str () );
  if ( !pretty )
  {
    std::fprintf ( ofs, " by Yise @ %s ",  (char *)curr_time_str ().c_str () );
  }

  std::fprintf ( ofs, "\n(#nodes: %d #levels: %d #PI: %d #PO: %d)",
		 mig.num_nodes () - mig.num_pis () - 1, max_level - 1, mig.num_pis (), mig.num_pos () );
  std::fprintf ( ofs, "\"\n" );
  std::fprintf ( ofs, "         ];\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

   // 4. generate the POs, the topmost level.
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", max_level );
  for ( auto i = 0; i < mig.po_list.size (); i++ )
  {
    const auto &po = mig.po_list[i];
    if ( is_const ( po ) ) const_po = true;     // flag if there are const POs
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", 
		   num_nodes + i ,              // each PO is a taken as a Node
		   (char *)po.name.c_str () );  // name of the PO
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
      std::fprintf ( ofs, ", shape = hexagon, style = filled, color = gray22" );
    }
    else
    {
      std::fprintf ( ofs, ", shape = triangle, style = filled, color = gray22" );
    }
    std::fprintf ( ofs, ", fillcolor = bisque];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  

  // Generate different labels if pretty print is requested.
  std::unordered_map<int, int> labels; // Nobody cares about efficiency
  for ( auto i = 0; i < mig.node_list.size (); i++ )
  {
    const auto &n = id_to_node ( mig, i );
    if ( !pretty )
    {
      labels.emplace ( std::make_pair ( n.id, n.id ) );
    }
    else 
    {
      if ( is_pi ( n ) ) continue;
      if ( is_const ( n ) ) continue;
      const auto ss = labels.size () + 1; 
      labels[n.id] = ss;
    }
  }

  // 5. generate nodes of each rank other than PO, PI 
  for ( int Level = max_level - 1; Level >= min_level && Level > 0; Level-- )
  {
    std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", Level ); 
    for ( const auto &n : mig.node_list )
    {
      if ( levels[n.id] !=  Level ) continue;
      if ( is_pi ( n ) || is_const ( n ) ) continue;
      // If fanin is const-edge then we need to prinout the const
      if ( is_const ( n.fanins[0] ) ) const_edge = true;
      if ( is_const ( n.fanins[1] ) ) const_edge = true;
      if ( is_const ( n.fanins[2] ) ) const_edge = true;
      int num = n.id;
      if ( pretty )
      {
	assert ( labels.find ( n.id ) != labels.end () );
	num = labels.find ( n.id )->second;
      }
      std::fprintf ( ofs, "  Node%d [label = \"%d\"", n.id , num );  
      if ( pretty )
      {
	std::fprintf ( ofs, ", fontsize=26" );
      }
      std::fprintf ( ofs, ", shape = ellipse];\n" );
    }
    std::fprintf ( ofs, "}\n\n" );
  }

  // 6. generate the PI nodes, the lowest level
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", min_level );
  if ( const_po || const_edge ) // if there is a const in PI or internal nodes write it out
  {
    std::fprintf ( ofs, "  Node0 [label = \"1\"" );
    std::fprintf ( ofs, ", shape = hexagon, style = filled" );
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
    }
    std::fprintf ( ofs, ", color = gray22, fillcolor = gray];\n" );
  }
  
  for ( const auto &pi : mig.pi_list )
  {
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", to_id ( pi ), to_name ( mig, pi ).c_str () );
    std::fprintf ( ofs, ", shape = %s", "hexagon, style = filled" );
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
    }
    std::fprintf ( ofs, ", color = gray22, fillcolor = lightblue];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );

  // 7. Order the top 3 levels using invisible edges
   for ( auto i = 0; i < mig.po_list.size (); i++ )             // title2 -> POs
  {
     std::fprintf ( ofs, "title1 -> Node%d [style = invis];\n", num_nodes + i );
  }
  for ( auto i = 1; i < mig.po_list.size (); i++ )             // among all POs
  {
    auto j = i - 1;
    std::fprintf ( ofs, "Node%d -> Node%d [style = invis];\n",
		   num_nodes + j , num_nodes + i );
  }
  
  // 8. generate all edges using fanin details
  for ( const auto &n : mig.node_list )
  {
    if ( is_const ( n ) || is_pi ( n ) ) continue;
    for ( auto &e : n.fanins )
    {
      std::fprintf ( ofs, "Node%d",  n.id );
      std::fprintf ( ofs, " -> " );
      std::fprintf ( ofs, "Node%d",  to_id ( e ) );
      std::fprintf ( ofs, " [style = %s", is_complement ( e ) ? "dotted" : "bold" );
      std::fprintf ( ofs, "];\n" );
    }
  }
  
  // 9. Finally generate all the PO edges
  for ( auto i = 0; i < mig.po_list.size (); i++ )
  {
    const auto &po = mig.po_list[i];
    std::fprintf ( ofs, "Node%d", num_nodes + i );
    std::fprintf ( ofs, " -> " );
    std::fprintf ( ofs, "Node%d", po.node_id () );
    std::fprintf ( ofs, " [style = %s", po.is_complement () ? "dotted" : "bold" );
    std::fprintf ( ofs, "];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  fclose( ofs );
}

// For the purpose of my thesis :)
Mig get_full_adder ()
{
  Mig mig ( "full_adder", 20 );
  auto a = add_pi ( mig, "a" );
  auto b = add_pi ( mig, "b" );
  auto cin = add_pi ( mig, "cin" );
  auto cout = add_maj (mig,  a, b, cin );
  auto sum = add_xor ( mig, a, b, cin );
  
  tag_as_po ( mig, sum, "sum" );
  tag_as_po ( mig, cout, "cout" );

  mig = strash ( mig );

  return mig;
}


} // Mig namespace
//------------------------------------------------------------------------------
} // Yise namespace

//clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
