/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cnf.hpp
 * @brief  : Header for CNF manipulation (conjunctive normal form)
 */

/*-------------------------------------------------------------------------------*/

/*
  Notes: Internal meaning of literal is slightly different from the original DIMACS.
  I use the convention used in our DAGs (AIG, MIG) etc.
  A variable is the index ( i.e., ID corresponding to the Node in DAG )
  A literal is var + polarity ( i.e., Edge in the DAG )
*/

#ifndef CNF_TYPE_HPP
#define CNF_TYPE_HPP

#include <sat/sat_solver.hpp>

#include <vector>
#include <sstream>
#include <utility>

namespace Yise
{
namespace Cnf
{

using Clause = std::vector<int>;

inline int  complement    ( const int &lit )  { return lit ^ 1u; }
inline int  regular       ( const int &lit )  { return ( lit >> 1 ) << 1; }
inline bool is_complement ( const int &lit )  { return 1u == ( lit & 1u ); }
inline bool is_regular    ( const int &lit )  { return !is_complement ( lit ); }


// variable is the ID and the literal is the corresponding Edge
inline int var2lit ( const int var, bool polarity ) { return ( var << 1 ) | polarity; }
inline int lit2var ( const int lit ) { return lit >> 1; }
inline unsigned lit2edge ( int lit ) { return lit; }
inline unsigned var2id   ( int var ) { return var; }
inline unsigned var2edge ( int var, bool polarity ) { return var2lit ( var, polarity ); }
inline unsigned lit2id   ( int lit ) { return lit2var ( lit ); }


// CNF asserting outputs of AIG to be 1
// variables correspond to 1,2,3 etc... i.e. as given in DIMACS, w/o 0
// but clauses are from literals; i.e., lit = (var << 1 | polarity)
struct Cnf
{
  void *p_dag = nullptr;          // corresponding AIG/MIG/QCA manager
  int num_lits = 0; // total number of literals in CNF
  std::vector<Clause> clauses;    // CNF clauses
  std::vector<int> var_count;     // count of CNF variable for each node ID (-1 if unused)
  std::vector<int> obj_to_clause; // mapping of objects into clauses
  std::vector<int> obj_to_count;  // mapping of objects into clause number
  std::vector<unsigned char> clause_polarities;  // polarity of input literals in each clause
  std::vector<int>    node_map;    // mapping of internal nodes
  std::vector<int> var_list;       // unique list of all variables

  std::vector<int> cex; // Map of literal to cex-value
  
  inline int num_clauses () const { return clauses.size (); }
  inline int num_vars () const { return var_list.size (); }
};

//------------------------------------------------------------------------------
// CNF read/write 

// from  functions/io/cnf.cpp
Cnf read_cnf ( const std::string &filename );
void write_cnf ( const Cnf &cnf, const std::string &filename );


//------------------------------------------------------------------------------
// Sat solver ( Minisat )
// Returns true if successful
bool write_cnf_to_solver ( Sat::sat_solver *solver, Cnf &cnf );
inline bool write_cnf_to_solver ( Sat::sat_solver &solver, Cnf &cnf )
{
  return write_cnf_to_solver ( &solver, cnf );
}
inline Sat::sat_solver *to_solver ( Cnf &cnf )
{
  auto solver = Sat::sat_solver_new ();
  const auto ret = write_cnf_to_solver ( solver, cnf );
  if ( ret )
    return solver;
  else
    return nullptr;
}

std::vector<int> get_cex ( Cnf &cnf, Sat::sat_solver *solver );
inline void update_cex ( Cnf &cnf, Sat::sat_solver *solver )
{
  const auto &cex = get_cex ( cnf, solver );
  cnf.cex = std::move ( cex );
}
//------------------------------------------------------------------------------

// Aig conversions
Aig::Aig to_aig ( Cnf &orig_cnf, const std::string &name = "miter" );

// Naive conversion, using direct Tseitin, blows up the CNF
Cnf to_cnf ( Aig::Aig &miter ); // essentially DNF to CNF

//------------------------------------------------------------------------------
}
}
#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

