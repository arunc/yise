/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : aig.hpp
 * @brief  : And Inverter Graph
 */

/*-------------------------------------------------------------------------------

c = And (a, b) 
c = ab

Reduction rules
Unit clauses and consts
1) (a, a)  =  a
2) (a, !a) =  0
3) (0, b)  =  0
4) (1, b)  =  b

2-input OR   = !Aig (!a, !b)
2-input NOR  =  Aig (!a, !b)
2-input NAND = !Aig (a, b)

--------------------------------------------------------------------------------
Conventions in the AIG DAG::
Edge is a literal
id   is a variable
Node is a vertex
pi   is a primary input
po   is a primary output

literal: (variable and sign) : ( id << 1 ) | sign )
id0: *const-1*, id1 : pi0, id2: pi2 and so on
po is only a tag. Any literal ( i.e., edge ) can be tagged as a PO.
Note: 0 is the literal for const-1 and 1 is the literal for const-0 

pi_list is a list of primary input edges (**edges are taken as PI, not Nodes)
po_list is a list of primary output edges

--------------------------------------------------------------------------------
Basic construction:
Aig aig ( "myCircuit", 100 );         // 100: estimate of max node-count

Edge in0 = add_pi ( aig, "in0" );     // "in0":  name of the primary input
Edge in1 = add_pi ( aig, "in1" );     // "in1":  name of the primary input
Edge in2 = add_pi ( aig, "in2" );     // "in2":  name of the primary input

Edge a = add_and ( aig, in0, in1 );   // a = in0 & in1    (AND gate)
Edge b = add_or  ( aig, in0, in1 );   // b = in0 | in1    (OR gate)
Edge c = add_maj ( aig, a, b, in2 );  // c = <a, b, in2>  (Majority gate)
Edge d = add_xor ( aig, a, b );       // d = a ^ b        (XOR gate) 
Edge e = complement ( c );            // e = !c           (NOT gate)

tag_as_po ( aig, e, "out0" );         // "out0":  name of the primary output
tag_as_po ( aig, d, "out1" );         // "out1":  name of the primary output

aig = strash ( aig );                 // cleanup and remove stray nodes

-------------------------------------------------------------------------------*/
//------------------------------------------------------------------------------


#ifndef AIG_HPP
#define AIG_HPP

#include "platform.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// clang-format off
namespace Yise
{
namespace Aig
{

enum class NodeType
{
  CONST1,     // const-1
  PI,         // primary input
  PI_PO,      // primary input and output together
  PO,         // primary output
  PLAIN       // internal node; anything other than the above types
};

struct Aig;
struct Node;

// Edge is the index of the Node ( node-id) shifted by 1 bit to left
// Edge: = ( id ( node ) << 1 ) | negation
using Edge = unsigned;
static const Edge edge_undef = 0xfffffffe;
static const Edge edge1 = 0x0;
static const Edge edge0 = 0x1;

// type to hold po information: edge and name
// Note: edge can have a separate name, if its a PI or internal node
struct Po_t
{
  Edge edge;
  Aig *p_aig;
  std::string name;
  int indx = -1;
  
  Po_t () {}
  Po_t ( Edge e, const std::string &_name, Aig *aig_ptr )
  {
    edge = e;
    name = _name;
    p_aig = aig_ptr;
  }
  inline void update_index ( int index ) { indx = index ;}
  inline unsigned node_id   () const { return edge >> 1; }
  inline bool is_complement () const { return 1u == ( edge & 1u ); }
};
  
//------------------------------------------------------------------------------
// Note: Reduction rules must be applied before calling hash
struct EdgeHash
{
  size_t operator () ( const std::array<Edge, 2> &edges ) const
  {
    // 1. sort the and gate first, (a, b)
    std::array<Edge, 2> m = {edges[0], edges[1]};
    if ( m[0] > m[1] ) std::swap ( m[0], m[1] );

    // 2. compute the hash of individual elements and combine
    auto h0 = std::hash<Edge>() ( m[0] );
    auto h1 = std::hash<Edge>() ( m[1] );

    auto aig_hash = (h0 ^ (h1 << 1) );
    return aig_hash;
  }
};

struct EdgeEqual
{
  size_t operator () ( const std::array<Edge, 2> &e1, const std::array<Edge, 2> &e2 ) const
  {
    std::array<Edge, 2> m1 = e1;
    std::array<Edge, 2> m2 = e2;
    if ( m1[0] > m1[1] ) std::swap ( m1[0], m1[1] );
    if ( m2[0] > m2[1] ) std::swap ( m2[0], m2[1] );
    
    return (m1[0] == m2[0]) && (m1[1] == m2[1]);
  }
};

using HashTable = std::unordered_map<std::array<Edge, 2>, Edge, EdgeHash, EdgeEqual>;
using NameList_t = std::unordered_map<Edge, std::string>;
using FanoutList_t = std::unordered_map<Edge, std::unordered_set<Edge>>;
//------------------------------------------------------------------------------
// Note: p_aig is ntk ptr. After any transfer in ownership this ptr to be updated.
struct Node
{
  unsigned id;                 // node id
  std::array<Edge, 2> fanins;  // fanin edges: a, b
  int value;                   // user defined value
  int trav_id;                 // traversal id
  int level;                   // level in the topological order
  NodeType type;               // NodeType type = NodeType::PLAIN;
  Aig *p_aig;                  // Network
  bool flagA;                  // flag1 for optimization
  bool flagB;                  // flag2 for optimization
  char sim_value;              // simulation value (0:'0', 1:'1', 2:'X', -1:'U')
  
  Node ( int _id, const std::array<Edge, 2> &_fanins, NodeType _type, Aig *_aig )
    : p_aig ( _aig )
  {
    id = _id;
    fanins = _fanins;
    type = _type;
    value = 0;
    trav_id = 0;
    level = -2;
    flagA = false;
    flagB = false;
    sim_value = -1;
  }
  inline bool operator== ( const Node &other ) const { return id == other.id; }
  inline bool operator<  ( const Node &other ) const { return id < other.id; }
  
  inline Edge edge ( bool negate = false ) const { return ( id << 1 ) | negate; }
};

//------------------------------------------------------------------------------
class Aig
{
public:
  std::string       name = "";        // circuit name
  std::string       file = "";        // source filename
  std::vector<Node> node_list;        // list of all nodes
  int               curr_trav_id = 0; // current traversal id
  std::vector<Edge> pi_list;          // list of primary inputs (only regular edges)
  std::vector<Po_t> po_list;          // list of primary outputs
  NameList_t        name_list;        // list of edge names; edges have polarity
  FanoutList_t      fanout_list;      // fanout-edges of each edge; only regular edges

  // Notes:
  // fanout_list is a map of Node to fanouts. pi_list is a list of PI nodes.
  // But instead of nodes, we store corresponding regular edges
  // name_list is a map of Edge to name, +ve/-ve edges can have separate names
  //
  //----------------------------------------------------------------------------
  Aig () {} // default ctor
  Aig ( const std::string &ckt, const unsigned nnodes ) { init ( ckt, nnodes ); }
  Aig ( const Aig& other )    { copy ( other ); } // copy ctor;
  Aig (Aig&& other) noexcept  { move ( other ); } // move ctor
  Aig& operator= (const Aig& other)     { copy ( other ); return *this; } // copy assign
  Aig& operator= (Aig&& other) noexcept { move ( other ); return *this; } // move assign

  // explicit initialization of AIG
  void init ( const std::string &ckt_name, const unsigned num_nodes )
  {
    // LSB0 for sign, and LSB1 used in hashing
    static const auto limit = 1ul << ( ( sizeof ( unsigned ) * 8 ) - 2  );
    assert ( num_nodes < limit );
    name = ckt_name;
    node_list.reserve ( num_nodes );
    node_list.emplace_back ( Node{0, {}, NodeType::CONST1, this} ); // const1
    node_list[0].level = -1;
    strash.reserve ( num_nodes );
    fanout_list.reserve ( num_nodes );
  }

  inline bool good () const
  {
    if ( node_list.empty () ) return false;
    return ( node_list[0].id == 0 ) && ( node_list[0].p_aig == this );
  }

  inline unsigned num_pis   () const { return pi_list.size (); }
  inline unsigned num_pos   () const { return po_list.size (); }
  inline unsigned num_nodes () const { return node_list.size (); }
  inline unsigned num_regular_nodes () const { return num_nodes () - num_pis () - 1; }

  friend Edge add_aig ( Aig &aig, const std::array<Edge, 2> &fanins, const std::string &name );
  friend bool lookup_aig_node ( Aig &aig, Edge a, Edge b );
private:
  // only add_aig has access to hash-table
  HashTable strash; // reduction rules must be applied before calling hash

  // default ctors cannot update the p_aig in the node list.
  // hence need explicit copy, move ctors
  void copy ( const Aig &other )
  {
    name = other.name;
    file = other.file;
    node_list = other.node_list;
    curr_trav_id = other.curr_trav_id;
    pi_list = other.pi_list;
    po_list = other.po_list;
    name_list = other.name_list;
    fanout_list = other.fanout_list;
    strash = other.strash;
    for ( auto &n : node_list ) // default cannot do this
    {
      n.p_aig = this;
    }
    for ( auto &po : po_list )
    {
      po.p_aig = this;
    }
  }

  void move ( Aig &other ) noexcept
  {
    name = std::move ( other.name );
    file = std::move ( other.file );
    node_list = std::move ( other.node_list );
    curr_trav_id = std::move ( other.curr_trav_id );
    pi_list = std::move ( other.pi_list );
    po_list = std::move ( other.po_list );
    name_list = std::move ( other.name_list );
    fanout_list = std::move ( other.fanout_list );
    strash = std::move ( other.strash );
    for ( auto &n : node_list ) // default cannot do this
    {
      n.p_aig = this;
    }
    for ( auto &po : po_list )
    {
      po.p_aig = this;
    }
  }
  
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// All utility functions

inline Edge complement    ( const Edge &e )  { return e ^ 1u; }
inline Edge regular       ( const Edge &e )  { return ( e >> 1 ) << 1; }
inline bool is_complement ( const Edge &e )  { return 1u == ( e & 1u ); }
inline bool is_regular    ( const Edge &e )  { return !is_complement ( e ); }

inline void set_network_name ( Aig &aig, const std::string &name ) { aig.name = name; }
inline std::string get_network_name ( const Aig &aig ) { return aig.name; }
inline void set_file ( Aig &aig, const std::string &name ) { aig.file = name; }
inline std::string get_file ( const Aig &aig ) { return aig.file; }
inline void set_trav_id ( Aig &aig, int trav_id ) { aig.curr_trav_id = trav_id; }
inline void inc_trav_id ( Aig &aig ) { aig.curr_trav_id = aig.curr_trav_id + 1; }
inline int get_trav_id       ( const Aig &aig ) { return aig.curr_trav_id; }
inline void set_curr_trav_id ( Node &node ) { node.trav_id = node.p_aig->curr_trav_id; }
inline void set_curr_trav_id ( Node *node ) { node->trav_id = node->p_aig->curr_trav_id; }

//------------------------------------------------------------------------------
// Edge-node-id conversions
inline Edge to_edge ( int id, bool negate = false ) { return (id << 1) | negate; }
inline int  to_id   ( const Edge &e ) { return e >> 1; }
inline Node to_node ( Aig &aig, Edge &e )
{
  auto id = to_id ( e );
  assert ( aig.good () && id < aig.node_list.size () );
  return aig.node_list[id];
}
inline Node* to_node_p ( Aig &aig, Edge &e )
{
  auto id = to_id ( e );
  assert ( aig.good () && id < aig.node_list.size () );
  return &aig.node_list[id];
}
inline Node to_node ( const Aig &aig, const Edge &e )
{
  assert ( aig.good () && to_id ( e ) < aig.node_list.size () );
  return aig.node_list[to_id ( e )];
}

inline Node to_node ( Aig *p_aig, Edge &e )
{
  auto id = to_id ( e );
  assert ( p_aig->good () && id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}
inline Node* to_node_p ( Aig *p_aig, Edge &e )
{
  auto id = to_id ( e );
  assert ( p_aig->good () && id < p_aig->node_list.size () );
  return &p_aig->node_list[id];
}
inline Node to_node ( const Aig *p_aig, const Edge &e )
{
  assert ( p_aig->good () && to_id ( e ) < p_aig->node_list.size () );
  return p_aig->node_list[to_id ( e )];
}

inline Node to_node ( const Po_t &po ) { 
  assert ( po.p_aig->good () );
  return po.p_aig->node_list[po.node_id ()];
}

// id and edge are same types. Hence prefix _id_ to distinguish
inline Node id_to_node ( Aig &aig, int id )
{
  assert ( aig.good () && id < aig.node_list.size () );
  return aig.node_list[id];
}
inline Node id_to_node ( const Aig &aig, int id )
{
  assert ( aig.good () && id < aig.node_list.size () );
  return aig.node_list[id];
}
inline Node id_to_node ( Aig *p_aig, int id )
{
  assert ( p_aig->good () && id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}
inline Node id_to_node ( const Aig *p_aig, int id )
{
  assert ( p_aig->good () && id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}

std::string to_name ( const Aig *p_aig, const Edge &e );
std::string to_name ( const Aig &aig, const Edge &e );
std::string to_name ( Aig &aig, const Edge &e );
std::string to_name ( Aig *p_aig, const Edge &e );
inline std::string to_name ( const Node &n ) { return to_name ( *n.p_aig , n.edge () ); }
inline std::string to_name ( Node &n ) { return to_name ( *n.p_aig , n.edge () ); }

inline Node const1_node ( const Aig &aig ) { assert ( aig.good () ); return aig.node_list[0]; }
inline int  const1_id   () { return 0; }

//------------------------------------------------------------------------------
inline Node to_node0  ( const Node &n ) { 
  assert ( n.p_aig->good () );
  assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[0] )];
}
inline Node to_node1  ( const Node &n ) { 
  assert ( n.p_aig->good () );
  assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[1] )];
}
inline int to_id0 ( const Node &n ) { return to_node0 ( n ).id; }
inline int to_id1 ( const Node &n ) { return to_node1 ( n ).id; }
//------------------------------------------------------------------------------
// boolean predicates
inline bool is_const ( const Node &n )  { return n.id == 0; }
inline bool is_const ( const Edge &e )  { return ( e == edge0 ) || ( e == edge1 ); } 
inline bool is_id_const ( const int id ){ return id == 0 ; } 
inline bool is_const ( const Po_t &po ) { return is_const ( po.edge ); }

inline bool is_const1 ( const Node &n )  { return n.id == 0; }
inline bool is_id_const1 ( const int id ){ return id == 0; } 
inline bool is_const1 ( const Edge &e )  { return e == edge1; }
inline bool is_const1 ( const Po_t &po ) { return is_const1 ( po.edge ); }

inline bool is_const0 ( const Edge &e )  { return e == edge0; }
inline bool is_const0 ( const Po_t &po ) { return is_const0 ( po.edge ); }

inline bool is_pi ( const Node &n ) { return n.type == NodeType::PI || n.type == NodeType::PI_PO; }
inline bool is_pi ( const Aig &aig, const Edge &e ) { return is_pi ( to_node ( aig, e ) ); }
inline bool is_pi ( Aig *p_aig, Edge &e ) { return is_pi ( to_node ( p_aig, e ) ); }
inline bool is_id_pi ( const Aig &aig, const int &id ) { return is_pi ( id_to_node ( aig, id ) ); }
inline bool is_id_pi ( Aig *p_aig, int id ) { return is_pi ( id_to_node ( p_aig, id ) ); }

inline bool is_po ( const Node &n ) { return n.type == NodeType::PO || n.type == NodeType::PI_PO; }
inline bool is_po ( const Aig &aig, const Edge &e ) { return is_po ( to_node ( aig, e ) ); }
inline bool is_po ( Aig *p_aig, Edge e ) { return is_po ( to_node ( p_aig, e ) ); }
inline bool is_id_po ( const Aig &aig, const int &id ) { return is_po ( id_to_node ( aig, id ) ); }
inline bool is_id_po ( Aig *p_aig, int id ) { return is_po ( id_to_node ( p_aig, id ) ); }

inline bool is_pipo ( const Node &n ) { return n.type == NodeType::PI_PO; }
inline bool is_pipo ( const Aig &aig, const Edge &e ) { return is_pipo ( to_node ( aig, e ) ); }
inline bool is_pipo ( Aig *p_aig, Edge e ) { return is_pipo ( to_node ( p_aig, e ) ); }
inline bool is_id_pipo ( const Aig &aig, const int &id ) { return is_pipo ( id_to_node ( aig, id ) ); }
inline bool is_id_pipo ( Aig *p_aig, int id ) { return is_pipo ( id_to_node ( p_aig, id ) ); }

inline bool is_plain ( const Node &n ) { return n.type == NodeType::PLAIN; }
inline bool is_plain ( const Aig &aig, const Edge &e ) { return is_plain ( to_node ( aig, e ) ); }
inline bool is_id_plain ( const Aig &aig, const int &id ) { return is_plain ( id_to_node ( aig, id ) ); }

inline bool has_fanouts ( const Aig &aig, const Edge &e ) { return aig.fanout_list.find ( e ) != aig.fanout_list.end (); }
inline bool has_fanouts ( Aig *p_aig, Edge e ) { return p_aig->fanout_list.find ( e ) != p_aig->fanout_list.end (); }
inline bool has_fanouts ( const Node &n ) { return has_fanouts ( *n.p_aig, n.edge () ); }
inline bool has_fanouts_for_id ( const Aig &aig, const int &id ) { return has_fanouts ( aig, to_edge ( id ) ); }

inline bool has_name ( const Aig &aig, const Edge &e ) { return aig.name_list.find ( e ) != aig.name_list.end (); }
inline bool has_name ( Aig *p_aig, Edge e ) { return p_aig->name_list.find ( e ) != p_aig->name_list.end (); }
inline bool has_name ( const Node &n ) { return has_name ( *n.p_aig, n.edge () ); }
inline bool has_name_for_id ( const Aig &aig, const int &id ) { return has_name ( aig, to_edge ( id ) ); }


//------------------------------------------------------------------------------
// for C++11 iterators
inline std::vector<Node> all_nodes   ( Aig &aig )       { assert ( aig.good () ); return aig.node_list; }
inline std::vector<Po_t> all_pos     ( Aig &aig )       { assert ( aig.good () ); return aig.po_list; }
inline std::vector<Edge> all_pis     ( Aig &aig )       { assert ( aig.good () ); return aig.pi_list; }
inline std::vector<Node> all_nodes   ( const Aig &aig ) { assert ( aig.good () ); return aig.node_list; }
inline std::vector<Po_t> all_pos     ( const Aig &aig ) { assert ( aig.good () ); return aig.po_list; }
inline std::vector<Edge> all_pis     ( const Aig &aig ) { assert ( aig.good () ); return aig.pi_list; }
inline std::unordered_map<Edge, std::string> name_map ( Aig &aig ) { assert ( aig.good () ); return aig.name_list; }

inline std::vector<Node> all_nodes   ( Aig *p_aig ) { assert ( p_aig->good () ); return p_aig->node_list; }
inline std::vector<Po_t> all_pos     ( Aig *p_aig ) { assert ( p_aig->good () ); return p_aig->po_list; }
inline std::vector<Edge> all_pis     ( Aig *p_aig ) { assert ( p_aig->good () ); return p_aig->pi_list; }
inline std::unordered_map<Edge, std::string> name_map ( Aig *p_aig ) { assert ( p_aig->good () ); return p_aig->name_list; }

inline std::unordered_set<Edge> all_fanouts ( const Aig &aig, const Edge &e )
{
  assert ( aig.good () );
  const auto itr = aig.fanout_list.find ( e );
  if ( itr == aig.fanout_list.end () )
    return  std::unordered_set<Edge> {};
  else
    return itr->second;
}

inline std::unordered_set<Edge> all_fanouts ( Aig *p_aig, const Edge &e ) { return all_fanouts ( *p_aig, e ); }
inline std::unordered_set<Edge> all_fanouts ( const Node &n ) { return all_fanouts ( n.p_aig, n.edge () ); }

inline std::array<Edge, 2> all_fanins ( const Aig &aig, const Edge &e )
{
  if ( e == edge_undef )
  {
    return std::array<Edge, 2> {edge_undef, edge_undef}; 
  }
  else if ( is_pi ( aig, e ) || is_const ( e ) )
  {
    return std::array<Edge, 2> {edge_undef, edge_undef};
  }
  else
  {
    return to_node ( aig, e ).fanins;
  }
}

inline std::array<Edge, 2> all_fanins ( Aig *p_aig, Edge e )
{
  if ( e == edge_undef )
  {
    return std::array<Edge, 2> {edge_undef, edge_undef}; 
  }
  else if ( is_pi ( p_aig, e ) || is_const ( e ) )
  {
    return std::array<Edge, 2> {edge_undef, edge_undef};
  }
  else
  {
    return to_node ( p_aig, e ).fanins;
  }
}

inline std::array<Edge, 2> all_fanins ( const Node &n ) { return all_fanins ( *n.p_aig, n.edge () ); }

//------------------------------------------------------------------------------
// PI and PO indices
inline int edge_to_pi_index ( const Aig &aig, const Edge &e )
{
  assert ( is_pi ( aig, e ) );
  assert ( aig.good () );
  auto itr = std::find ( aig.pi_list.begin (), aig.pi_list.end (), e );
  assert ( itr != aig.pi_list.end () );
  return ( itr - aig.pi_list.begin () );
}

inline int node_to_pi_index ( const Node &n )
{
  assert ( n.p_aig->good () );
  assert ( is_pi ( n ) );
  return ( edge_to_pi_index ( *n.p_aig, n.edge () ) );
}

inline int id_to_pi_index ( const Aig &aig, const int node_id )
{
  assert ( is_pi ( aig, node_id ) );
  return ( edge_to_pi_index ( aig, to_edge ( node_id ) ) );
}

inline Edge po_index_to_edge ( const Aig &aig, const int i ) { return aig.po_list[i].edge; }
inline int po_index_to_id    ( const Aig &aig, const int i ) { return aig.po_list[i].node_id (); }

//------------------------------------------------------------------------------
// Traversal ID manipulation
inline bool is_trav_id_curr ( const Node &n ) { return n.trav_id == ( n.p_aig )->curr_trav_id; }
inline bool is_trav_id_last ( const Node &n ) { return n.trav_id == ( ( n.p_aig )->curr_trav_id - 1 ); }

//------------------------------------------------------------------------------
// structural hashing ( similar to copying )
Aig strash ( const Aig &aig1 );
Aig copy ( const Aig &aig1 ); // take a blind copy

// check if an AND node already exists for a given fanin
inline bool lookup_aig_node ( Aig &aig, Edge a, Edge b )
{
  assert ( aig.good () );
  assert ( a != edge_undef );
  assert ( b != edge_undef );
  
  // reduction rules
  if ( ( a == b ) || ( a == edge0 || b == edge0 )
       || ( a == edge1 ) || ( b == edge1 )
       || ( a == complement ( b ) ) ) return true;

  // 1. check if already strashed
  std::array<Edge, 2> fanins = {a, b};
  if ( fanins[0] > fanins[1] ) std::swap ( fanins[0], fanins[1] );
  const auto it = aig.strash.find ( fanins );
  if ( it != aig.strash.end () ) return true;
  return false;
}

// Adding/Updating new nodes, PI, POs
Edge add_aig ( Aig &aig, const std::array<Edge, 2> &fanins, const std::string &name = "" );
Edge add_pi    ( Aig &aig, const std::string &name = "" );
void tag_as_po ( Aig &aig, Edge &edge, const std::string &name = "" );

inline Edge add_and ( // 2-input AND
  Aig &aig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 2> &fanins = {a, b};
  return add_aig ( aig, fanins, name );
}

inline Edge add_and ( // 3-input AND
  Aig &aig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  if ( lookup_aig_node ( aig, a, c ) ) // see if there is a shared node already
  {
    const auto &ac = add_and ( aig, a, c );
    return add_and ( aig, ac, b );
    
  }
  else
  {
    const auto &ab = add_and ( aig, a, b );
    return add_and ( aig, ab, c );
  }
}

inline Edge add_nand (
  Aig &aig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 2> &fanins = {a, b};
  return complement ( add_aig ( aig, fanins, name ) );
}

inline Edge add_or ( // 2-input OR
  Aig &aig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 2> &fanins = {complement ( a ), complement ( b )};
  return complement ( add_aig ( aig, fanins, name ) );
}

inline Edge add_or ( // 3-input OR
  Aig &aig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  if ( lookup_aig_node ( aig, a, c ) ) // see if there is a shared node already
  {
    const auto &ac = add_or ( aig, a, c );
    return add_or ( aig, ac, b );
    
  }
  else
  {
    const auto &ab = add_or ( aig, a, b );
    return add_or ( aig, ab, c );
  }
}

inline Edge add_nor (
  Aig &aig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 2> &fanins = {complement ( a ), complement ( b )};
  return add_aig ( aig, fanins, name );
}

inline Edge add_maj (
  Aig &aig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto ab = add_and ( aig, a, b );
  auto bc = add_and ( aig, b, c );
  auto ca = add_and ( aig, c, a );
  return add_or ( aig, ab, bc, ca );
}

inline Edge add_mux (
  Aig &aig, const Edge &in0, const Edge &in1, const Edge &c, const std::string &name = "" )
{
  auto x = add_and ( aig, complement ( c ), in0 );
  auto y = add_and ( aig,  c, in1 );
  return add_or ( aig, x, y, name );
}
  
inline Edge add_xor (
  Aig &aig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  auto x = add_and ( aig, complement ( a ), b );
  auto y = add_and ( aig, complement ( b ), a );
  return add_or ( aig, x, y, name );
}

// a ^ b ^ c = Maj ( !Maj ( a, b, c), c, Maj ( a, b, !c) )
inline Edge add_xor (
  Aig &aig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto m1 = add_maj ( aig, a, b, c );
  auto m2 = add_maj ( aig, a, b, complement ( c ) );
  return add_maj ( aig, m1, c, m2, name );
}

inline Edge add_xnor (
  Aig &aig, const Edge &a, const Edge &b, const std::string &name = "" )
{
  auto x = add_and ( aig, complement ( a ), b );
  auto y = add_and ( aig, complement ( b ), a );
  auto zz = complement ( add_or ( aig, x, y ) );
  if ( name != "" ) aig.name_list[zz] = name;
  return zz;
}

// a ^ b ^ c = Maj ( !Maj ( a, b, c), c, Maj ( a, b, !c) )
inline Edge add_xnor (
  Aig &aig, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto m1 = add_maj ( aig, a, b, c );
  auto m2 = add_maj ( aig, a, b, complement ( c ) );
  auto zz = complement ( add_maj ( aig, m1, c, m2, name ) );
  if ( name != "" ) aig.name_list[zz] = name;
  return zz;
}


bool check_aig ( const Aig &aig, bool debug = false );

// All PIs are level 0. const1 is level -1. returns a map of node-id vs level.
std::vector<int> compute_levels ( const Aig &aig ); // levels[node_id] = level_of_node
void update_levels ( Aig &aig ); // annotate all nodes with levels (only if required)
int get_max_level ( Aig &aig );  // Note: updates the levels if need.
bool check_level ( const Aig &aig ); // check if levels are correct

void write_dot ( const Aig &aig, const std::string &file, const bool pretty = false );

// I/O (from aiger.cpp)
Aig read_aig ( const std::string &file );
void write_aig ( const Aig &aig, const std::string &file, bool ascii = false ); 

} // Aig namespace
//------------------------------------------------------------------------------
// clang-format on
//------------------------------------------------------------------------------
} // Yise namespace

#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
