/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : gate.hpp
 * @brief  : Gate type for structural representation
 */

/*-------------------------------------------------------------------------------
  Note: All gates have single fanouts, except the FANOUT gate.
-------------------------------------------------------------------------------*/
//------------------------------------------------------------------------------


#ifndef GATE_HPP
#define GATE_HPP

#include "platform.hpp"
#include <types/aig.hpp>

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// clang-format off
namespace Yise
{
namespace Gate
{

enum class Type
{
  CONST1,     // const-1
  PI,         // primary input
  BUF,        // Buffer
  AND,        // AND
  OR,         // OR
  XOR,        // XOR
  FANOUT,     // FANOUT Only this gate is allowed to have multiple fanouts
  PO          // primary output
};

inline std::string to_str ( const Type &typ )
{
  if ( typ == Type::CONST1 ) return "k1";   
  if ( typ == Type::PI )     return "pi";
  if ( typ == Type::PO )     return "po";
  if ( typ == Type::BUF )    return "buf";  
  if ( typ == Type::AND )    return "and";  
  if ( typ == Type::OR )     return "or";   
  if ( typ == Type::XOR )    return "xor";  
  if ( typ == Type::FANOUT ) return "fanout";
  return "unknown";
}

struct Netlist;

using NameList_t = std::unordered_map<int, std::string>;
//------------------------------------------------------------------------------
// Note: p_netlist is ntk ptr. After any transfer in ownership this ptr to be updated.
struct Gate
{
  int id;                      // Gate id
  std::vector<int> fanins;     // fanin gates
  std::vector<int> fanouts;    // fanout gates
  int trav_id;                 // traversal id
  Type type;                   // type of the gate
  bool flag;                   // flag
  int value;                   // user defined value
  int level;                   // topologically sorted level
  Netlist *p_netlist;          // Network pointer      
  char sim_value;              // simulation value
  Aig::Edge aig_edge;          // corresponding AIG edge
  bool inv;                    // inversion at the output
  bool virt;                   // virtual gate is an imaginary gate
  
  Gate ( int _id, const std::vector<int> &_fanins, Type _type, Netlist *_gl, bool _inv )
    : p_netlist ( _gl )
  {
    id = _id;
    fanins = _fanins;
    type = _type;
    value = 0;
    trav_id = 0;
    level = -2;
    flag = false;
    sim_value = -1;
    aig_edge = Aig::edge_undef;
    inv = _inv;
    virt = false;
  }
  inline bool operator== ( const Gate &other ) const { return id == other.id; }
  inline bool operator<  ( const Gate &other ) const { return id < other.id; }

  inline bool good () const 
  {
    if ( type != Type::FANOUT && type != Type::PO && fanouts.size () != 1 ) return false;
    if ( type == Type::PI && !fanins.empty () ) return false;
    if ( type == Type::PO && !fanouts.empty () ) return false;
    if ( id < 0 ) return false;
    static const auto ten_million = 10000000;
    if ( id > ten_million ) return false;
    if ( sim_value < -1 || sim_value > 2 ) return false;
    if ( p_netlist == nullptr ) return false;
    if ( ( type == Type::AND || type == Type::OR || type == Type::XOR ) && ( fanins.size () != 2 ) ) return false;
    if ( ( type == Type::BUF ) && ( fanins.size () != 1 ) ) return false;
    if ( type == Type::CONST1 && !fanins.empty () ) return false;
    
    return true;
  }
  
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


class Netlist
{
public:
  std::string       name = "";        // circuit name
  std::string       file = "";        // source filename
  std::vector<Gate> gate_list;        // list of all gates
  int               curr_trav_id = 0; // current traversal id
  std::vector<int>  pi_list;          // list of primary input ID
  std::vector<int>  po_list;          // list of primary output ID
  NameList_t        name_list;        // list of gate names

  //----------------------------------------------------------------------------
  Netlist () {} // default ctor
  Netlist ( const std::string &name, const unsigned ngates ) { init ( name, ngates ); }
  Netlist ( const Netlist& other )    { copy ( other ); } // copy ctor;
  Netlist (Netlist&& other) noexcept  { move ( other ); } // move ctor
  Netlist& operator= (const Netlist& other)     { copy ( other ); return *this; } // copy assign
  Netlist& operator= (Netlist&& other) noexcept { move ( other ); return *this; } // move assign

  // explicit initialization of GATELIST
  void init ( const std::string &ckt_name, const unsigned num_gates )
  {
    name = ckt_name;
    gate_list.reserve ( num_gates );
  }

  inline unsigned num_pis   () const { return pi_list.size (); }
  inline unsigned num_pos   () const { return po_list.size (); }
  inline unsigned num_gates () const { return gate_list.size (); }
  inline unsigned num_regular_gates () const { return num_gates () - num_pis () - num_pos (); }

private:
  // default ctors cannot update the p_gatelist in the gate list.
  // hence need explicit copy, move ctors
  void copy ( const Netlist &other )
  {
    name = other.name;
    file = other.file;
    gate_list = other.gate_list;
    curr_trav_id = other.curr_trav_id;
    pi_list = other.pi_list;
    po_list = other.po_list;
    name_list = other.name_list;
    for ( auto &n : gate_list ) // default cannot do this
    {
      n.p_netlist = this;
    }
  }

  void move ( Netlist &other ) noexcept
  {
    name = std::move ( other.name );
    file = std::move ( other.file );
    gate_list = std::move ( other.gate_list );
    curr_trav_id = std::move ( other.curr_trav_id );
    pi_list = std::move ( other.pi_list );
    po_list = std::move ( other.po_list );
    name_list = std::move ( other.name_list );
    for ( auto &n : gate_list ) // default cannot do this
    {
      n.p_netlist = this;
    }
  }

  
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
bool check ( const Netlist &ckt ); // before using, prune atleat once
Netlist prune ( const Netlist &ckt_orig ); // Remove all the floating gates

inline bool is_virtual ( const Gate &g ) { return g.virt; }
inline bool is_virt    ( const Gate &g ) { return g.virt; }

inline bool is_pi      ( const Gate &g ) { return g.type == Type::PI; }
inline bool is_po      ( const Gate &g ) { return g.type == Type::PO; }
inline bool is_buf     ( const Gate &g ) { return g.type == Type::BUF; }
inline bool is_inv     ( const Gate &g ) { return g.type == Type::BUF && g.inv; }
inline bool is_and     ( const Gate &g ) { return g.type == Type::AND; }
inline bool is_nand    ( const Gate &g ) { return g.type == Type::AND && g.inv; }
inline bool is_or      ( const Gate &g ) { return g.type == Type::OR; }
inline bool is_nor     ( const Gate &g ) { return g.type == Type::OR && g.inv; }
inline bool is_xor     ( const Gate &g ) { return g.type == Type::XOR; }
inline bool is_xnor    ( const Gate &g ) { return g.type == Type::XOR && g.inv; }
inline bool is_const0  ( const Gate &g ) { return g.type == Type::CONST1 && g.inv; }
inline bool is_const1  ( const Gate &g ) { return g.type == Type::CONST1; }
inline bool is_fanout  ( const Gate &g ) { return g.type == Type::FANOUT; }
inline bool is_regular ( const Gate &g ) { return is_and(g) || is_or(g) || is_xor(g) || is_buf (g); }

inline Gate fanin0 ( const Gate &g )
{
  assert ( !g.fanins.empty () );
  auto ckt = g.p_netlist;
  return ckt->gate_list[g.fanins[0]];
}
inline Gate &fanin0 ( Gate *g )
{
  assert ( !g->fanins.empty () );
  auto ckt = g->p_netlist;
  return ckt->gate_list[g->fanins[0]];
}
inline Gate fanin1 ( const Gate &g )
{
  assert ( g.fanins.size () > 1 );
  auto ckt = g.p_netlist;
  return ckt->gate_list[g.fanins[1]];
}
inline Gate &fanin1 ( Gate *g )
{
  assert ( g->fanins.size () > 1 );
  auto ckt = g->p_netlist;
  return ckt->gate_list[g->fanins[1]];
}
inline Gate to_gate ( const Netlist &ckt, int id )
{
  assert ( id < ckt.gate_list.size () );
  return ckt.gate_list[id];
}
inline Gate &to_gate ( Netlist *ckt, int id )
{
  assert ( id < ckt->gate_list.size () );
  return ckt->gate_list[id];
}

int add_gate (
  Netlist &ckt, const Type &type, bool inv,
  const std::vector<int> &fanins, const std::string &name = ""
  );

inline int add_and ( Netlist &ckt, int a, int b ) { return add_gate ( ckt, Type::AND, false, std::vector<int>{a, b} ); }
inline int add_nand ( Netlist &ckt, int a, int b ) { return add_gate ( ckt, Type::AND, true, std::vector<int>{a, b} ); }
inline int add_or ( Netlist &ckt, int a, int b ) { return add_gate ( ckt, Type::OR, false, std::vector<int>{a, b} ); }
inline int add_nor ( Netlist &ckt, int a, int b ) { return add_gate ( ckt, Type::OR, true, std::vector<int>{a, b} ); }
inline int add_xor ( Netlist &ckt, int a, int b ) { return add_gate ( ckt, Type::XOR, false, std::vector<int>{a, b} ); }
inline int add_xnor ( Netlist &ckt, int a, int b ) { return add_gate ( ckt, Type::XOR, true, std::vector<int>{a, b} ); }
inline int add_buf ( Netlist &ckt, int a ) { return add_gate ( ckt, Type::BUF, false, std::vector<int>{a} ); }
inline int add_inv ( Netlist &ckt, int a ) { return add_gate ( ckt, Type::BUF, true, std::vector<int>{a} ); }


inline int add_pi ( Netlist &ckt, const std::string &name = "" )
{
  std::vector<int> fanins;
  std::stringstream pi_str;
  if ( name == "" )
  {
    pi_str << "pi" << ckt.pi_list.size ();
  }
  else
  {
    pi_str << name;
  }
  return add_gate ( ckt, Type::PI, false, fanins, pi_str.str () );
}

inline int add_po (
  Netlist &ckt,
  const std::vector<int> &fanins, const std::string &name = ""
  )
{
  std::stringstream po_str;
  if ( name == "" )
  {
    po_str << "po" << ckt.po_list.size ();
  }
  else
  {
    po_str << name;
  }
  return add_gate ( ckt, Type::PO, false, fanins, po_str.str () );
}

inline std::string to_type_str ( const Gate &g ) { return to_str ( g.type ); }
inline std::string to_type_str ( Gate *g ) { return to_str ( g->type ); }
inline bool has_name ( const Gate &g )
{
  const auto &itr = g.p_netlist->name_list.find ( g.id );
  return itr != g.p_netlist->name_list.end ();
  
}
inline std::string to_name ( const Gate &g )
{
  const auto &itr = g.p_netlist->name_list.find ( g.id );
  if ( itr != g.p_netlist->name_list.end () ) return itr->second;
  
  if ( is_const0 ( g ) ) return "k0_"  + std::to_string ( g.id );
  if ( is_const1 ( g ) ) return "k1_"  + std::to_string ( g.id );
  if ( is_inv ( g ) )    return "inv"  + std::to_string ( g.id );
  if ( is_nand ( g ) )   return "nand" + std::to_string ( g.id );
  if ( is_nor ( g ) )    return "nor"  + std::to_string ( g.id );
  if ( is_xnor ( g ) )   return "xnor" + std::to_string ( g.id );
  
  return to_type_str ( g ) + std::to_string ( g.id );
}

inline std::string to_name ( const Netlist &ckt, int id )
{
  const auto &g = to_gate ( ckt, id );
  return to_name ( g );
}

Aig::Aig to_aig ( Netlist &ckt ); // convert to a corresponding AIG
bool check_level ( const Netlist &ckt ); // check if topological levels are correct

void write_dot ( const Netlist &ckt, const std::string &file, bool pretty = false );

} // Gate namespace
//------------------------------------------------------------------------------
// clang-format on
//------------------------------------------------------------------------------
} // Yise namespace

#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:



