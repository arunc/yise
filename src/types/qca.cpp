/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : qca.cpp
 * @brief  : Quantum Dot Cellular Automata (QCA) Graph
 */

/*-------------------------------------------------------------------------------*/

//clang-format off

#include "qca.hpp"
#include "aig.hpp"

#include <utils/common_utils.hpp>

#include <sstream>



namespace Yise
{
namespace Qca
{

std::string to__name ( const Qca *p_qca, const Edge &e )
{
  if ( is_const0 ( e ) ) return "0";
  if ( is_const1 ( e ) ) return "1";
  if ( e == edge_undef ) return "undef";

  std::stringstream str;
  const auto itr = p_qca->name_list.find ( e );
  const auto itr_inv = p_qca->name_list.find ( complement ( e ) );

  if ( itr != p_qca->name_list.end () ) // check for pi
  {
    str << itr->second;
  }
  else if ( itr_inv != p_qca->name_list.end () ) // check for ~pi
  {
    str << "~"<< itr_inv->second;
  }
  else // all unnamed
  {
    const auto sign = is_complement ( e ) ? "~n" : "n";
    str << sign << to_id ( e );
  }

  return str.str ();
}

std::string to_name ( const Qca &qca, const Edge &e )   { return to__name ( &qca, e ); }
std::string to_name ( const Qca *p_qca, const Edge &e ) { return to__name ( p_qca, e ); }
std::string to_name ( Qca *p_qca, const Edge &e )       { return to__name ( p_qca, e ); }
std::string to_name ( Qca &qca, const Edge &e )         { return to__name ( &qca, e ); }

inline void add_fanout ( Qca &qca, const Edge &ee, const Edge &fanout_edge )
{
  const auto e = regular ( ee );
  qca.fanout_list[e].emplace ( fanout_edge ); // Hashset, removes duplicates
}

inline bool to_invert ( const std::array<Edge, 3> &fanins )
{
  return false;
  auto inv = is_complement (fanins[0]) + is_complement (fanins[1]) + is_complement (fanins[2]);
  return inv > 2;
}

Edge add_qca ( Qca &qca, const std::array<Edge, 3> &fanins, const std::string &name )
{
  assert ( qca.good () );
  const auto &a = fanins[0];
  const auto &b = fanins[1];
  const auto &c = fanins[2];

  assert ( a != edge_undef );
  assert ( b != edge_undef );
  assert ( c != edge_undef );
  
  // reduction rules
  if ( a == b ) return a;
  if ( a == c ) return a;
  if ( b == c ) return b;
  if ( a == complement ( b ) ) return c;
  if ( a == complement ( c ) ) return b;
  if ( b == complement ( c ) ) return a;

  // strash rules
  // 1. check if already strashed
  auto copy = fanins;
  std::sort ( &copy[0], &copy[2] );
  const auto it = qca.strash.find ( copy );
  if ( it != qca.strash.end () ) return it->second;

  // 2. <!a, !b, !c> = !<a, b, c> :: covers nand, nor, and, or
  std::array<Edge, 3> inv_copy = {
    complement ( copy[0] ),
    complement ( copy[1] ),
    complement ( copy[2] )
  };
  const auto inv_it = qca.strash.find ( inv_copy );
  if ( inv_it != qca.strash.end () )
  {
    return complement ( inv_it->second );
  }

  // create a new node, update the tables
  // balance the side that has lesser inversions
  if ( to_invert ( fanins ) )
  {
    int id = qca.node_list.size ();
    Node node = {id, copy, NodeType::PLAIN, &qca};
    auto edge = node.edge ( true );
    qca.strash[inv_copy] = edge;
    qca.node_list.emplace_back ( std::move ( node ) );
    assert ( qca.fanout_list.find ( edge ) == qca.fanout_list.end () );

    if ( name != "" ) qca.name_list[edge] = name;
    add_fanout ( qca, inv_copy[0], edge );
    add_fanout ( qca, inv_copy[1], edge );
    add_fanout ( qca, inv_copy[2], edge );
    
    return edge;
  }
  else
  {
    int id = qca.node_list.size ();
    Node node = {id, copy, NodeType::PLAIN, &qca};
    auto edge = node.edge ();
    qca.strash[fanins] = edge;
    qca.node_list.emplace_back ( std::move ( node ) );
    assert ( qca.fanout_list.find ( edge ) == qca.fanout_list.end () );
    if ( name != "" ) qca.name_list[edge] = name;
    
    add_fanout ( qca, fanins[0], edge );
    add_fanout ( qca, fanins[1], edge );
    add_fanout ( qca, fanins[2], edge );
    
    return edge;
  }
}

Edge add_pi ( Qca &qca, const std::string &name )
{
  assert ( qca.good () );
  std::stringstream pi_str;
  if ( name == "" )
  {
    pi_str << "pi" << qca.pi_list.size ();
  }
  else
  {
    pi_str << name;
  }

  int id = qca.node_list.size ();
  Node node = {id, {edge_undef, edge_undef, edge_undef}, NodeType::PI, &qca};
  auto edge = node.edge ();
  qca.pi_list.emplace_back ( edge );
  assert ( qca.fanout_list.find ( edge ) == qca.fanout_list.end () );
  qca.name_list[edge] = pi_str.str ();
  qca.node_list.emplace_back ( std::move ( node ) );
  return edge;
}

void tag_as_po ( Qca &qca, Edge &edge, const std::string &name )
{
  assert ( qca.good () );
  std::stringstream po_str;
  if ( name == "" )
  {
    po_str << "po" << qca.po_list.size ();
  }
  else
  {
    po_str << name;
  }

  // add_fanout ( qca, edge, edge ); // A PO is a fanout
  auto po = Po_t ( edge, po_str.str (), &qca );
  po.update_index ( qca.po_list.size () );
  qca.po_list.emplace_back ( po );
  auto node = to_node_p ( qca, edge );
  
  if ( is_const ( edge ) )
  {
    assert ( node->type == NodeType::CONST1 );
  }
  else if ( is_pi ( qca, edge ) )
  {
    node->type = NodeType::PI_PO;
  }
  else
  {
    node->type = NodeType::PO;
  }

}


Edge add_edges_rec (
  std::unordered_map<Edge, Edge> &qca_map,
  Qca &qca2, const Qca &qca1,
  const Edge &e1
  )
{
  auto itr = qca_map.find ( e1 );
  if ( itr != qca_map.end () ) return itr->second;
  
  const auto &fanins = all_fanins ( qca1, e1 ); // implicit -> regular (e1)
  Edge a2 = add_edges_rec ( qca_map, qca2, qca1, fanins[0] );
  Edge b2 = add_edges_rec ( qca_map, qca2, qca1, fanins[1] );
  Edge c2 = add_edges_rec ( qca_map, qca2, qca1, fanins[2] );

  const auto &fanins2 = std::array<Edge, 3> {a2, b2, c2};
  Edge e2;
  if ( has_name ( qca1, e1 ) ) // if there is an internal node name, preserve it
  {
    e2 = add_qca ( qca2, fanins2, to_name ( qca1, e1 ) );    
  }
  else
  {
    e2 = add_qca ( qca2, fanins2 );
  }

  // e1 and e2 should be of same polarity
  // all_fanins () returns the regular edge only.
  // i.e., e1 is converted to regular ( e1 ) before returning fanins.
  // Hence if the incoming e1 is complemented that has to be reflected.
  if ( is_complement ( e1 ) ) e2 = complement ( e2 );
  qca_map[e1] = e2;
  qca_map[ complement ( e1 )] = complement ( e2 );
  return e2;
}


Qca strash ( const Qca &qca1 )
{
  assert ( check_qca ( qca1, true ) );
  auto qca2 = Qca ( qca1.name, qca1.num_nodes () );
  
  std::unordered_map<Edge, Edge> qca_map;
  qca_map.reserve ( qca1.num_nodes () );
  qca_map[edge0] = edge0;
  qca_map[edge1] = edge1;
  qca_map[edge_undef] = edge_undef;

  for ( const auto &pi : all_pis ( qca1 ) )
  {
    auto e = add_pi ( qca2, to_name ( qca1, pi ) );
    qca_map[pi] = e;
    qca_map[complement ( pi )] = complement ( e );
  }

  for ( const auto &po1 : qca1.po_list )
  {
    auto po2 = add_edges_rec ( qca_map, qca2, qca1, po1.edge );
    // added po edge can be of opposite sign, but taken care internally
    tag_as_po ( qca2, po2, po1.name );
  }
  
  assert ( check_qca ( qca2, true ) );
  return qca2;
}

Qca copy ( const Qca &qca1 )
{
  Qca qca_cpy;
  const auto p_qca_cpy = &qca_cpy;
  qca_cpy.name = qca1.name;
  qca_cpy.file = qca1.file;
  qca_cpy.node_list = qca1.node_list;
  qca_cpy.curr_trav_id = qca1.curr_trav_id;
  qca_cpy.pi_list = qca1.pi_list;
  qca_cpy.po_list = qca1.po_list;
  qca_cpy.name_list = qca1.name_list;
  qca_cpy.fanout_list = qca1.fanout_list;

  for ( auto &po : qca_cpy.po_list ) { po.p_qca = p_qca_cpy; }
  for ( auto &n : qca_cpy.node_list ) { n.p_qca = p_qca_cpy; }
  
  return qca_cpy;
}


std::string to_fanin_po_str ( const Qca &qca, const Edge &e )
{
  std::stringstream str;
  if ( is_pi ( qca, e ) || is_const ( e ) )
  {
    str << "< 0, " << to_name ( qca, e ) << ", " << to_name ( qca, e ) << " >";
    return str.str ();
  }

  const auto &fanins = all_fanins ( to_node ( qca, e ) );
  const auto &f0 = fanins[0] ;
  const auto &f1 = fanins[1] ;
  const auto &f2 = fanins[2] ;

   str << "< "
      << to_name ( qca, f0 ) << ", " << to_name ( qca, f1 ) << ", " << to_name ( qca, f2 )
      << " >";
  return str.str ();
}

std::string to_fanin_str ( const Qca &qca, const Edge &e )
{
  std::stringstream str;
  if ( is_pi ( qca, e ) || is_const ( e ) )
  {
    str << "< 0, " << to_name ( qca, e ) << ", " << to_name ( qca, e ) << " >";
    return str.str ();
  }

  const auto &fanins = all_fanins ( to_node ( qca, e ) );
  const auto &f0 = is_complement ( e ) ? complement ( fanins[0] ) : fanins[0] ;
  const auto &f1 = is_complement ( e ) ? complement ( fanins[1] ) : fanins[1] ;
  const auto &f2 = is_complement ( e ) ? complement ( fanins[2] ) : fanins[2] ;

  str << "< "
      << to_name ( qca, f0 ) << ", " << to_name ( qca, f1 ) << ", " << to_name ( qca, f2 )
      << " >";
  return str.str ();
}

std::string to_str ( const Qca &qca, const Edge &e )
{
  std::stringstream str;
  assert ( !is_complement ( to_node ( qca, e ).edge () ) );
  str << to_name ( to_node ( qca, e ) ) << " = ";
  str << to_fanin_str ( qca, e );
  return str.str ();
}

void write_qca ( const Qca &qca, std::ofstream &ofs )
{
  assert ( false ); // May be buggy, dont use now.
  assert ( check_qca ( qca, true ) );
  ofs << "QCA: " << qca.name << " @" << curr_time_str () << "\n";
  ofs << "PI:  ";
  for ( const auto &pi : all_pis ( qca ) )
  {
    ofs << to_name ( qca, pi ) << " ";
  }
  ofs << "\n";
  ofs << "PO:  ";
  
  for ( const auto &po : qca.po_list )
  {
    ofs << po.name << " ";
  }
  ofs << "\n";

  for ( const auto &po : all_pos ( qca ) )
  {
    auto e = po.edge;
    ofs << po.name << " = " << to_fanin_po_str ( qca, e ) << "\n";
  }

  for ( const auto &n : all_nodes ( qca ) )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_po ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    assert ( !is_complement ( n.edge () ) );
    ofs << to_str ( qca, n.edge () ) << "\n";
  }
  
}

bool check_qca ( const Qca &qca, bool debug )
{
  for ( const auto &n : qca.node_list )
  {
    if ( n.p_qca != &qca )
    {
      if ( debug ) std::cout << "Node: " << n.id << " QCA is wrong\n";
      return false;
    }
  }
  if ( !qca.good () )
  {
    if ( debug ) std::cout << "Qca is not good\n";
    return false;
  }
  return true;
}


void level_rec ( std::vector<int> &levels, const Node &node )
{
  const auto &id = node.id;
  if ( levels[id] != -1 ) return; // already computed
  if ( id == 0 ) return; // const1
  const auto &id0 = to_id0 ( node );
  const auto &id1 = to_id1 ( node );
  const auto &id2 = to_id2 ( node );
  if ( levels [id0] == -1 ) level_rec ( levels, to_node0 ( node ) );
  if ( levels [id1] == -1 ) level_rec ( levels, to_node1 ( node ) );
  if ( levels [id2] == -1 ) level_rec ( levels, to_node2 ( node ) );

  auto my_max = [&] ( int a, int b, int c )
    {
      return a > b ? ( a > c ? a : c ) : ( b > c ? b : c );
    };

  auto l0 = levels[id0];
  auto l1 = levels[id1];
  auto l2 = levels[id2];

  if ( is_complement ( node.fanins[0] ) ) l0++;
  if ( is_complement ( node.fanins[1] ) ) l1++;
  if ( is_complement ( node.fanins[2] ) ) l2++;
  
  levels[id] = my_max ( l0, l1, l2 ) + 1;
}

// For QCA, inverters are an extra level.
// All PIs are level 0. const-1 is at level -1.
// Returns a map of node-id vs level.
std::vector<int> compute_levels ( const Qca &qca )
{
  assert ( qca.good () );
  std::vector<int> levels;
  levels.assign ( qca.node_list.size (), -1 );
  levels[0] = -2; // const1

  for ( const auto &pi : qca.pi_list )
  {
    levels[ to_id ( pi )] = 0;
  }
  for ( const auto &po : qca.po_list )
  {
    level_rec ( levels, to_node ( po ) );
  }
  levels[0] = -1; // const1
  return levels;
}

void update_levels ( Qca &qca )
{
  const auto &levels = compute_levels ( qca );
  for ( auto &n : qca.node_list )
  {
    n.level = levels[n.id];
  }
}

int get_max_level ( const Qca &qca )
{
  int max = -2;
  const auto &levels = compute_levels ( qca );
  for ( const auto &po : qca.po_list )
  {
    if (levels [po.node_id ()] >= max )
    {
      max = levels [po.node_id ()];
      if ( po.is_complement ( ) ) max++;
    }
  }
  return max;
}

int get_max_level ( const Qca &qca, const std::vector<int> &levels )
{
  int max = -2;
  for ( const auto &po : qca.po_list )
  {
    if (levels [po.node_id ()] >= max )
    {
      max = levels [po.node_id ()];
      if ( po.is_complement ( ) ) max++;
    }
  }
  return max;
}

int count_inverters ( const Qca &qca )
{
  int count = 0;
  for ( const auto &n : qca.node_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    if ( is_pipo ( n ) ) continue;
    if ( is_complement ( n.fanins[0] ) )  count++;
    if ( is_complement ( n.fanins[1] ) )  count++;
    if ( is_complement ( n.fanins[2] ) )  count++;
  }

  for ( auto &po : qca.po_list )
  {
    if ( po.is_complement () ) count++;
  }
  
  return count;
}

void write_dot ( const Qca &qca, const std::string &file )
{
  assert ( qca.good () );
  if ( qca.po_list.empty () )
  {
    std::cout << "[e] No Primary Output to writeout!\n";
    return;
  }
  if ( qca.pi_list.empty () ) // ToDo: remove this restriction
  {
    std::cout << "[e] No Primary Input. Plain const outputs are ignored!\n";
    return;
  }
  if ( qca.node_list.size () > 200 )
  {
    std::cout << "[e] Cannot write dot files for #nodes > 200!\n";
    return;
  }
  auto ofs = fopen ( (char *)file.c_str (), "w" );
  if ( ofs  == NULL )
  {
    std::cout << "[e] Cannot open " << file << " for writing!\n";
    return;
  }
  
  auto levels = compute_levels ( qca );
  levels[0] = 0;
  const auto min_level = 0;
  const auto max_level = get_max_level ( qca, levels ) + 1; // 1+ for POs
  const auto num_nodes = qca.num_nodes (); // including 0, and PIs
  const auto num_inv = count_inverters ( qca );
  bool const_po = false;
  bool const_edge = false;
  
  // 1. write the DOT header
  std::fprintf ( ofs, "# %s\n",  "GraphViz network by Yise" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "digraph network {\n" );
  std::fprintf ( ofs, "size = \"7.5,10\";\n" );
  std::fprintf ( ofs, "center = true;\n" );
  std::fprintf ( ofs, "edge [dir = back];\n" );
  std::fprintf ( ofs, "\n" );
  
  // labels on the left of the picture
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  node [shape = plaintext];\n" );
  std::fprintf ( ofs, "  edge [style = invis];\n" );
  std::fprintf ( ofs, "  LevelTitle1 [label=\"\"];\n" );
  
  // 2. generate node names with labels
  // labels are provided later with groups
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    // the visible node name
    std::fprintf ( ofs, "  Level%d", Level );
    std::fprintf ( ofs, " [label = " );
    // label name
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "];\n" );
  }

  // 2. generate the levels for nodes
   std::fprintf ( ofs, "  LevelTitle1 ->" );
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    std::fprintf ( ofs, "  Level%d",  Level );
    if ( Level != min_level )
      std::fprintf ( ofs, " ->" );
    else
      std::fprintf ( ofs, ";" );
  }
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

  
  // 3. generate title box on top
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  rank = same;\n" );
  std::fprintf ( ofs, "  LevelTitle1;\n" );
  std::fprintf ( ofs, "  title1 [shape=plaintext,\n" );
  std::fprintf ( ofs, "          fontsize=18,\n" );
  std::fprintf ( ofs, "          fontname = \"Times-Roman\",\n" );
  std::fprintf ( ofs, "          label=\"" );
  std::fprintf ( ofs, "QCA \\\"%s\\\" ", (char *)qca.name.c_str () );
  std::fprintf ( ofs, " by Yise @ %s ",  (char *)curr_time_str ().c_str () );
  std::fprintf ( ofs, "\n(#maj: %d, #inv: %d, #PI: %d, #PO: %d)",
		 qca.num_nodes () - qca.num_pis () - 1,
		 num_inv, qca.num_pis (), qca.num_pos () );
  std::fprintf ( ofs, "\"\n" );
  std::fprintf ( ofs, "         ];\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

   // 4. generate the POs, the topmost level.
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", max_level );
  for ( auto i = 0; i < qca.po_list.size (); i++ )
  {
    const auto &po = qca.po_list[i];
    if ( is_const ( po ) ) const_po = true;     // flag if there are const POs
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", 
		   num_nodes + i ,              // each PO is a taken as a Node
		   (char *)po.name.c_str () );  // name of the PO
    std::fprintf ( ofs, ", shape = triangle, style = filled, color = gray22" );
    std::fprintf ( ofs, ", fillcolor = bisque];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  

  // 5. generate nodes of each rank other than PO, PI 
  for ( int Level = max_level - 1; Level >= min_level && Level > 0; Level-- )
  {
    std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", Level ); 
    for ( const auto &n : qca.node_list )
    {
      if ( levels[n.id] !=  Level ) continue;
      if ( is_pi ( n ) || is_const ( n ) ) continue;
      // If fanin is const-edge then we need to prinout the const
      if ( is_const ( n.fanins[0] ) ) const_edge = true;
      if ( is_const ( n.fanins[1] ) ) const_edge = true;
      if ( is_const ( n.fanins[2] ) ) const_edge = true;
      std::fprintf ( ofs, "  Node%d [label = \"%d\\n%s\"", n.id , n.id, "" );  
      std::fprintf ( ofs, ", shape = ellipse];\n" );
    }
    std::fprintf ( ofs, "}\n\n" );
  }

  // 6. generate the PI nodes, the lowest level
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", min_level );
  if ( const_po || const_edge ) // if there is a const in PI or internal nodes write it out
  {
    std::fprintf ( ofs, "  Node0 [label = \"1\"" );
    std::fprintf ( ofs, ", shape = hexagon, style = filled" );
    std::fprintf ( ofs, ", color = gray22, fillcolor = gray];\n" );
  }
  
  for ( const auto &pi : qca.pi_list )
  {
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", to_id ( pi ), to_name ( qca, pi ).c_str () );
    std::fprintf ( ofs, ", shape = %s", "hexagon, style = filled" );
    std::fprintf ( ofs, ", color = gray22, fillcolor = lightblue];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );

  // 7. Order the top 3 levels using invisible edges
   for ( auto i = 0; i < qca.po_list.size (); i++ )             // title2 -> POs
  {
     std::fprintf ( ofs, "title1 -> Node%d [style = invis];\n", num_nodes + i );
  }
  for ( auto i = 1; i < qca.po_list.size (); i++ )             // among all POs
  {
    auto j = i - 1;
    std::fprintf ( ofs, "Node%d -> Node%d [style = invis];\n",
		   num_nodes + j , num_nodes + i );
  }
  
  // 8. generate all edges using fanin details
  for ( const auto &n : qca.node_list )
  {
    if ( is_const ( n ) || is_pi ( n ) ) continue;
    for ( auto &e : n.fanins )
    {
      std::fprintf ( ofs, "Node%d",  n.id );
      std::fprintf ( ofs, " -> " );
      std::fprintf ( ofs, "Node%d",  to_id ( e ) );
      std::fprintf ( ofs, " [style = %s", is_complement ( e ) ? "dotted" : "bold" );
      std::fprintf ( ofs, "];\n" );
    }
  }
  
  // 9. Finally generate all the PO edges
  for ( auto i = 0; i < qca.po_list.size (); i++ )
  {
    const auto &po = qca.po_list[i];
    std::fprintf ( ofs, "Node%d", num_nodes + i );
    std::fprintf ( ofs, " -> " );
    std::fprintf ( ofs, "Node%d", po.node_id () );
    std::fprintf ( ofs, " [style = %s", po.is_complement () ? "dotted" : "bold" );
    std::fprintf ( ofs, "];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  fclose( ofs );
}


} // Qca namespace
//------------------------------------------------------------------------------
} // Yise namespace

//clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
