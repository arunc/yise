/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : aig.cpp
 * @brief  : And Inverter Graph
 */

/*-------------------------------------------------------------------------------*/

//clang-format off

#include "aig.hpp"
#include "mig.hpp"

#include <utils/common_utils.hpp>

#include <sstream>



namespace Yise
{
namespace Aig
{

std::string to__name ( const Aig *p_aig, const Edge &e )
{
  if ( is_const0 ( e ) ) return "0";
  if ( is_const1 ( e ) ) return "1";
  if ( e == edge_undef ) return "undef";

  std::stringstream str;
  const auto itr = p_aig->name_list.find ( e );
  const auto itr_inv = p_aig->name_list.find ( complement ( e ) );

  if ( itr != p_aig->name_list.end () ) // check for pi
  {
    str << itr->second;
  }
  else if ( itr_inv != p_aig->name_list.end () ) // check for ~pi
  {
    str << "~"<< itr_inv->second;
  }
  else // all unnamed
  {
    const auto sign = is_complement ( e ) ? "~n" : "n";
    str << sign << to_id ( e );
  }

  return str.str ();
}

std::string to_name ( const Aig &aig, const Edge &e )   { return to__name ( &aig, e ); }
std::string to_name ( const Aig *p_aig, const Edge &e ) { return to__name ( p_aig, e ); }
std::string to_name ( Aig *p_aig, const Edge &e )       { return to__name ( p_aig, e ); }
std::string to_name ( Aig &aig, const Edge &e )         { return to__name ( &aig, e ); }

inline void add_fanout ( Aig &aig, const Edge &ee, const Edge &fanout_edge )
{
  const auto e = regular ( ee );
  aig.fanout_list[e].emplace ( fanout_edge ); // Hashset, removes duplicates
}

  
Edge add_aig ( Aig &aig, const std::array<Edge, 2> &fanins, const std::string &name )
{
  assert ( aig.good () );
  const auto &a = fanins[0];
  const auto &b = fanins[1];

  assert ( a != edge_undef );
  assert ( b != edge_undef );
  
  // reduction rules
  if ( a == b ) return a;
  if ( a == edge0 || b == edge0 ) return edge0;
  if ( a == edge1 ) return b;
  if ( b == edge1 ) return a;
  if ( a == complement ( b ) ) return edge0;

  // strash rules
  // 1. check if already strashed
  std::array<Edge, 2> cpy = fanins;
  if ( cpy[0] > cpy[1] ) std::swap ( cpy[0], cpy[1] );
  const auto &it = aig.strash.find ( cpy );
  if ( it != aig.strash.end () ) return it->second;
  assert ( cpy[0] < cpy[1] );
  
  // create a new node, update the tables
  int id = aig.node_list.size ();
  Node node = {id, cpy, NodeType::PLAIN, &aig};
  auto edge = node.edge ();
  aig.strash[fanins] = edge;

  assert ( to_id ( node.fanins[0] ) < node.p_aig->node_list.size () );
  assert ( to_id ( node.fanins[1] ) < node.p_aig->node_list.size () );
  const auto &node0 = node.p_aig->node_list[ to_id ( node.fanins[0] )];
  const auto &node1 = node.p_aig->node_list[ to_id ( node.fanins[1] )];
  node.level = std::max ( node0.level, node1.level ) + 1;
  
  aig.node_list.emplace_back ( std::move ( node ) );
  assert ( aig.fanout_list.find ( edge ) == aig.fanout_list.end () );
  if ( name != "" ) aig.name_list[edge] = name;
  
  add_fanout ( aig, fanins[0], edge );
  add_fanout ( aig, fanins[1], edge );

  return edge;
}

Edge add_pi ( Aig &aig, const std::string &name )
{
  assert ( aig.good () );
  std::stringstream pi_str;
  if ( name == "" )
  {
    pi_str << "pi" << aig.pi_list.size ();
  }
  else
  {
    pi_str << name;
  }

  int id = aig.node_list.size ();
  Node node = {id, {edge_undef, edge_undef}, NodeType::PI, &aig};
  node.level = 0;
  auto edge = node.edge ();
  aig.pi_list.emplace_back ( edge );
  assert ( aig.fanout_list.find ( edge ) == aig.fanout_list.end () );
  aig.name_list[edge] = pi_str.str ();
  aig.node_list.emplace_back ( std::move ( node ) );
  return edge;
}

void tag_as_po ( Aig &aig, Edge &edge, const std::string &name )
{
  assert ( aig.good () );
  std::stringstream po_str;
  if ( name == "" )
  {
    po_str << "po" << aig.po_list.size ();
  }
  else
  {
    po_str << name;
  }

  auto po = Po_t ( edge, po_str.str (), &aig );
  po.update_index ( aig.po_list.size () );
  aig.po_list.emplace_back ( po );
  auto node = to_node_p ( aig, edge );
  
  if ( is_const ( edge ) )
  {
    assert ( node->type == NodeType::CONST1 );
  }
  else if ( is_pi ( aig, edge ) )
  {
    node->type = NodeType::PI_PO;
  }
  else
  {
    node->type = NodeType::PO;
  }

}


Edge add_edges_rec (
  std::unordered_map<Edge, Edge> &aig_map,
  Aig &aig2, const Aig &aig1,
  const Edge &e1
  )
{
  auto itr = aig_map.find ( e1 );
  if ( itr != aig_map.end () ) return itr->second;
  
  const auto &fanins = all_fanins ( aig1, e1 ); // implicit -> regular (e1)
  Edge a2 = add_edges_rec ( aig_map, aig2, aig1, fanins[0] );
  Edge b2 = add_edges_rec ( aig_map, aig2, aig1, fanins[1] );

  const auto &fanins2 = std::array<Edge, 2> {a2, b2};
  Edge e2;
  if ( has_name ( aig1, e1 ) ) // if there is an internal node name, preserve it
  {
    e2 = add_aig ( aig2, fanins2, to_name ( aig1, e1 ) );    
  }
  else
  {
    e2 = add_aig ( aig2, fanins2 );
  }

  // e1 and e2 should be of same polarity
  // all_fanins () returns the regular edge only.
  // i.e., e1 is converted to regular ( e1 ) before returning fanins.
  // Hence if the incoming e1 is complemented that has to be reflected.
  if ( is_complement ( e1 ) ) e2 = complement ( e2 );
  aig_map[e1] = e2;
  aig_map[ complement ( e1 )] = complement ( e2 );
  return e2;
}


Aig strash ( const Aig &aig1 )
{
  assert ( check_aig ( aig1, true ) );
  auto aig2 = Aig ( aig1.name, aig1.num_nodes () );
  
  std::unordered_map<Edge, Edge> aig_map;
  aig_map.reserve ( aig1.num_nodes () );
  aig_map[edge0] = edge0;
  aig_map[edge1] = edge1;
  aig_map[edge_undef] = edge_undef;

  for ( const auto &pi : all_pis ( aig1 ) )
  {
    auto e = add_pi ( aig2, to_name ( aig1, pi ) );
    aig_map[pi] = e;
    aig_map[complement ( pi )] = complement ( e );
  }

  for ( const auto &po1 : aig1.po_list )
  {
    auto po2 = add_edges_rec ( aig_map, aig2, aig1, po1.edge );
    // added po edge can be of opposite sign, but taken care internally
    tag_as_po ( aig2, po2, po1.name );
  }
  
  assert ( check_aig ( aig2, true ) );
  return aig2;
}

Aig copy ( const Aig &aig1 )
{
  Aig aig_cpy;
  const auto p_aig_cpy = &aig_cpy;
  aig_cpy.file = aig1.file;
  aig_cpy.name = aig1.name;
  aig_cpy.node_list = aig1.node_list;
  aig_cpy.curr_trav_id = aig1.curr_trav_id;
  aig_cpy.pi_list = aig1.pi_list;
  aig_cpy.po_list = aig1.po_list;
  aig_cpy.name_list = aig1.name_list;
  aig_cpy.fanout_list = aig1.fanout_list;

  for ( auto &po : aig_cpy.po_list ) { po.p_aig = p_aig_cpy; }
  for ( auto &n : aig_cpy.node_list ) { n.p_aig = p_aig_cpy; }
  
  return aig_cpy;
}


std::string to_fanin_po_str ( const Aig &aig, const Edge &e )
{
  std::stringstream str;
  if ( is_pi ( aig, e ) || is_const ( e ) )
  {
    str << "< 0, " << to_name ( aig, e ) << ", " << to_name ( aig, e ) << " >";
    return str.str ();
  }

  const auto &fanins = all_fanins ( to_node ( aig, e ) );
  const auto &f0 = fanins[0] ;
  const auto &f1 = fanins[1] ;
  const auto &f2 = fanins[2] ;

   str << "< "
      << to_name ( aig, f0 ) << ", " << to_name ( aig, f1 ) << ", " << to_name ( aig, f2 )
      << " >";
  return str.str ();
}

std::string to_fanin_str ( const Aig &aig, const Edge &e )
{
  std::stringstream str;
  if ( is_pi ( aig, e ) || is_const ( e ) )
  {
    str << "< 0, " << to_name ( aig, e ) << ", " << to_name ( aig, e ) << " >";
    return str.str ();
  }

  const auto &fanins = all_fanins ( to_node ( aig, e ) );
  const auto &f0 = is_complement ( e ) ? complement ( fanins[0] ) : fanins[0] ;
  const auto &f1 = is_complement ( e ) ? complement ( fanins[1] ) : fanins[1] ;
  const auto &f2 = is_complement ( e ) ? complement ( fanins[2] ) : fanins[2] ;

  str << "< "
      << to_name ( aig, f0 ) << ", " << to_name ( aig, f1 ) << ", " << to_name ( aig, f2 )
      << " >";
  return str.str ();
}

std::string to_str ( const Aig &aig, const Edge &e )
{
  std::stringstream str;
  assert ( !is_complement ( to_node ( aig, e ).edge () ) );
  str << to_name ( to_node ( aig, e ) ) << " = ";
  str << to_fanin_str ( aig, e );
  return str.str ();
}

bool check_aig ( const Aig &aig, bool debug )
{
  for ( const auto &n : aig.node_list )
  {
    if ( n.p_aig != &aig )
    {
      if ( debug ) std::cout << "Node: " << n.id << " AIG is wrong\n";
      return false;
    }
  }
  if ( !aig.good () )
  {
    if ( debug ) std::cout << "Aig is not good\n";
    return false;
  }
  return true;
}


void level_rec ( std::vector<int> &levels, const Node &node )
{
  const auto &id = node.id;
  if ( levels[id] != -1 ) return; // already computed
  if ( id == 0 ) return; // const1
  const auto &id0 = to_id0 ( node );
  const auto &id1 = to_id1 ( node );
  if ( levels [id0] == -1 ) level_rec ( levels, to_node0 ( node ) );
  if ( levels [id1] == -1 ) level_rec ( levels, to_node1 ( node ) );
  
  levels[id] = std::max ( levels[id0], levels[id1] ) + 1;
}

// All PIs are level 0. const-1 is at level -1.
// Returns a map of node-id vs level.
std::vector<int> compute_levels ( const Aig &aig )
{
  assert ( aig.good () );
  std::vector<int> levels;
  levels.assign ( aig.node_list.size (), -1 );
  levels[0] = -2; // const1

  for ( const auto &pi : aig.pi_list )
  {
    levels[ to_id ( pi )] = 0;
  }
  for ( const auto &po : aig.po_list )
  {
    level_rec ( levels, to_node ( po ) );
  }
  levels[0] = -1; // const1
  return levels;
}

void update_levels ( Aig &aig )
{
  if ( check_level ( aig ) ) return; // nothing to update
  const auto &levels = compute_levels ( aig );
  for ( auto &n : aig.node_list )
  {
    n.level = levels[n.id];
  }
}

bool check_level ( const Aig &aig )
{
  for ( const auto &n : aig.node_list )
  {
    if ( is_const ( n ) && n.level != -1 )  return false;
    if ( is_pi ( n ) && n.level != 0 ) return false;
    if ( is_pipo ( n ) && n.level != 0 ) return false;
    assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
    assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
    const auto &n0 =  n.p_aig->node_list[ to_id ( n.fanins[0] )];
    const auto &n1 =  n.p_aig->node_list[ to_id ( n.fanins[1] )];
    if ( ( n0.level >= n.level ) || ( n1.level >= n.level ) )  return false;
  }
  return true;
}

int get_max_level ( Aig &aig )
{
  int max = -2;
  update_levels ( aig );
  for ( const auto &po : aig.po_list )
  {
    const auto &id = po.node_id ();
    assert ( id < aig.node_list.size () );
    const auto &n =  aig.node_list[id];
    if ( n.level > max ) max = n.level;
  }
  return max;
}


int get_max_level ( const Aig &aig, const std::vector<int> &levels )
{
  int max = -2;
  for ( const auto &po : aig.po_list )
  {
    if (levels [po.node_id ()] > max )
    {
      max = levels [po.node_id ()];
    }
  }
  return max;
}

void write_dot ( const Aig &aig, const std::string &file, const bool pretty )
{
  assert ( aig.good () );
  if ( aig.po_list.empty () )
  {
    std::cout << "[e] No Primary Output to writeout!\n";
    return;
  }
  if ( aig.pi_list.empty () ) // ToDo: remove this restriction
  {
    std::cout << "[e] No Primary Input. Plain const outputs are ignored!\n";
    return;
  }
  if ( aig.node_list.size () > 200 )
  {
    std::cout << "[e] Cannot write dot files for #nodes > 200!\n";
    return;
  }
  auto ofs = fopen ( (char *)file.c_str (), "w" );
  if ( ofs  == NULL )
  {
    std::cout << "[e] Cannot open " << file << " for writing!\n";
    return;
  }
  
  auto levels = compute_levels ( aig );
  levels[0] = 0;
  const auto min_level = 0;
  const auto max_level = get_max_level ( aig, levels ) + 1; // 1+ for POs
  const auto num_nodes = aig.num_nodes (); // including 0, and PIs
  bool const_po = false;
  
  // 1. write the DOT header
  std::fprintf ( ofs, "# %s\n",  "GraphViz network by Yise" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "digraph network {\n" );
  if ( pretty )
  {
    std::fprintf ( ofs, "size = \"5,5\";\n" );
    std::fprintf ( ofs, "fontsize=26;\n" );
  }
  else
  {
    std::fprintf ( ofs, "size = \"7.5,10\";\n" );
  }
  std::fprintf ( ofs, "center = true;\n" );
  std::fprintf ( ofs, "edge [dir = back];\n" );
  std::fprintf ( ofs, "\n" );
  
  // labels on the left of the picture
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  node [shape = plaintext];\n" );
  std::fprintf ( ofs, "  edge [style = invis];\n" );
  std::fprintf ( ofs, "  LevelTitle1 [label=\"\"];\n" );
  
  // 2. generate node names with labels
  // labels are provided later with groups
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    // the visible node name
    std::fprintf ( ofs, "  Level%d", Level );
    std::fprintf ( ofs, " [label = " );
    // label name
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "\"" );
    std::fprintf ( ofs, "];\n" );
  }

  // 2. generate the levels for nodes
   std::fprintf ( ofs, "  LevelTitle1 ->" );
  for ( int Level = max_level; Level >= min_level; Level-- )
  {
    std::fprintf ( ofs, "  Level%d",  Level );
    if ( Level != min_level )
      std::fprintf ( ofs, " ->" );
    else
      std::fprintf ( ofs, ";" );
  }
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

  
  // 3. generate title box on top
  std::fprintf ( ofs, "{\n" );
  std::fprintf ( ofs, "  rank = same;\n" );
  std::fprintf ( ofs, "  LevelTitle1;\n" );
  std::fprintf ( ofs, "  title1 [shape=plaintext,\n" );
  if ( pretty )
  {
    std::fprintf ( ofs, "          fontsize=26,\n" );
  }
  //std::fprintf ( ofs, "          fontsize=18,\n" );
  //std::fprintf ( ofs, "          fontname = \"Times-Roman\",\n" );
  std::fprintf ( ofs, "          label=\"" );
  std::fprintf ( ofs, "AIG: %s ", (char *)aig.name.c_str () );
  if ( !pretty )
  {
    std::fprintf ( ofs, " by Yise @ %s ",  (char *)curr_time_str ().c_str () );
  }

  std::fprintf ( ofs, "\n(#nodes: %d #levels: %d #PI: %d #PO: %d)",
		 aig.num_nodes () - aig.num_pis () - 1, max_level - 1, aig.num_pis (), aig.num_pos () );
  std::fprintf ( ofs, "\"\n" );
  std::fprintf ( ofs, "         ];\n" );
  std::fprintf ( ofs, "}" );
  std::fprintf ( ofs, "\n" );
  std::fprintf ( ofs, "\n" );

   // 4. generate the POs, the topmost level.
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", max_level );
  for ( auto i = 0; i < aig.po_list.size (); i++ )
  {
    const auto &po = aig.po_list[i];
    if ( is_const ( po ) ) const_po = true;     // flag if there are const POs
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", 
		   num_nodes + i ,              // each PO is a taken as a Node
		   (char *)po.name.c_str () );  // name of the PO
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
      std::fprintf ( ofs, ", shape = hexagon, style = filled, color = gray22" );
    }
    else
    {
      std::fprintf ( ofs, ", shape = triangle, style = filled, color = gray22" );
    }
    std::fprintf ( ofs, ", fillcolor = bisque];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  
  // Generate different labels if pretty print is requested.
  std::unordered_map<int, int> labels; // Nobody cares about efficiency
  for ( auto i = 0; i < aig.node_list.size (); i++ )
  {
    const auto &n = id_to_node ( aig, i );
    if ( !pretty )
    {
      labels.emplace ( std::make_pair ( n.id, n.id ) );
    }
    else 
    {
      if ( is_pi ( n ) ) continue;
      if ( is_const ( n ) ) continue;
      const auto ss = labels.size () + 1; 
      labels[n.id] = ss;
    }
  }
  
  // 5. generate nodes of each rank other than PO, PI 
  for ( int Level = max_level - 1; Level >= min_level && Level > 0; Level-- )
  {
    std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", Level ); 
    for ( const auto &n : aig.node_list )
    {
      if ( levels[n.id] !=  Level ) continue;
      if ( is_pi ( n ) || is_const ( n ) ) continue;
      int num = n.id;
      if ( pretty )
      {
	assert ( labels.find ( n.id ) != labels.end () );
	num = labels.find ( n.id )->second;
      }
      std::fprintf ( ofs, "  Node%d [label = \"%d\"", n.id , num );
      if ( pretty )
      {
	std::fprintf ( ofs, ", fontsize=26" );
      }
      std::fprintf ( ofs, ", shape = ellipse];\n" );
    }
    std::fprintf ( ofs, "}\n\n" );
  }

  // 6. generate the PI nodes, the lowest level
  std::fprintf ( ofs, "{\n  rank = same;\n  Level%d;\n", min_level );
  if ( const_po ) // if there is a const in PI write it out
  {
    std::fprintf ( ofs, "  Node0 [label = \"1\"" );
    std::fprintf ( ofs, ", shape = hexagon, style = filled" );
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
    }
    std::fprintf ( ofs, ", color = gray22, fillcolor = gray];\n" );
  }
  
  for ( const auto &pi : aig.pi_list )
  {
    std::fprintf ( ofs, "  Node%d [label = \"%s\"", to_id ( pi ), to_name ( aig, pi ).c_str () );
    std::fprintf ( ofs, ", shape = %s", "hexagon, style = filled" );
    if ( pretty )
    {
      std::fprintf ( ofs, ", fontsize=26" );
    }
    std::fprintf ( ofs, ", color = gray22, fillcolor = lightblue];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );

  // 7. Order the top 3 levels using invisible edges
   for ( auto i = 0; i < aig.po_list.size (); i++ )             // title2 -> POs
  {
     std::fprintf ( ofs, "title1 -> Node%d [style = invis];\n", num_nodes + i );
  }
  for ( auto i = 1; i < aig.po_list.size (); i++ )             // among all POs
  {
    auto j = i - 1;
    std::fprintf ( ofs, "Node%d -> Node%d [style = invis];\n",
		   num_nodes + j , num_nodes + i );
  }
  
  // 8. generate all edges using fanin details
  for ( const auto &n : aig.node_list )
  {
    if ( is_const ( n ) || is_pi ( n ) ) continue;
    for ( auto &e : n.fanins )
    {
      std::fprintf ( ofs, "Node%d",  n.id );
      std::fprintf ( ofs, " -> " );
      std::fprintf ( ofs, "Node%d",  to_id ( e ) );
      std::fprintf ( ofs, " [style = %s", is_complement ( e ) ? "dotted" : "bold" );
      std::fprintf ( ofs, "];\n" );
    }
  }
  
  // 9. Finally generate all the PO edges
  for ( auto i = 0; i < aig.po_list.size (); i++ )
  {
    const auto &po = aig.po_list[i];
    std::fprintf ( ofs, "Node%d", num_nodes + i );
    std::fprintf ( ofs, " -> " );
    std::fprintf ( ofs, "Node%d", po.node_id () );
    std::fprintf ( ofs, " [style = %s", po.is_complement () ? "dotted" : "bold" );
    std::fprintf ( ofs, "];\n" );
  }
  std::fprintf ( ofs, "}\n\n" );
  fclose( ofs );
}


// For the purpose of my thesis :)
Aig get_full_adder ()
{
  Aig aig ( "full_adder", 20 );
  auto a = add_pi ( aig, "a" );
  auto b = add_pi ( aig, "b" );
  auto cin = add_pi ( aig, "cin" );
  auto xor1 = add_xor ( aig, a, b );
  auto xor2 = add_xor ( aig, cin, xor1 );
  tag_as_po ( aig, xor2, "sum" );
  auto and1 = add_and ( aig, cin, xor1 );
  auto and2 = add_and ( aig, a, b );
  auto or1 = add_or ( aig, and1, and2 );
  tag_as_po ( aig, or1, "cout" );

  aig = strash ( aig );

  return aig;
}


} // Aig namespace
//------------------------------------------------------------------------------
} // Yise namespace

//clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
