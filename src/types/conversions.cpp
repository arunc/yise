/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : conversions.cpp
 * @brief  : Interconversion among gia<->abc::ntk, gia<->abc::gia
 */
//-------------------------------------------------------------------------------


#include "experimental.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iterator>
#include <regex>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iomanip>

#include <ext-libs/abc/abc_api.hpp>
#include <utils/common_utils.hpp>

#include <functions/truth/yig_truth.hpp>

namespace abc
{


typedef struct Abc_ManRef_t_ Abc_ManRef_t;
struct Abc_ManRef_t_
{
  // user specified parameters
  int nNodeSizeMax; // the limit on the size of the supernode
  int nConeSizeMax; // the limit on the size of the containing cone
  int fVerbose;     // the verbosity flag
  // internal data structures
  Vec_Ptr_t *vVars;    // truth tables
  Vec_Ptr_t *vFuncs;   // functions
  Vec_Int_t *vMemory;  // memory
  Vec_Str_t *vCube;    // temporary
  Vec_Int_t *vForm;    // temporary
  Vec_Ptr_t *vVisited; // temporary
  Vec_Ptr_t *vLeaves;  // temporary
  // node statistics
  int nLastGain;
  int nNodesConsidered;
  int nNodesRefactored;
  int nNodesGained;
  int nNodesBeg;
  int nNodesEnd;
  // runtime statistics
  abctime timeCut;
  abctime timeTru;
  abctime timeDcs;
  abctime timeSop;
  abctime timeFact;
  abctime timeEval;
  abctime timeRes;
  abctime timeNtk;
  abctime timeTotal;
};


Abc_ManRef_t *Abc_NtkManRefStart ( int nNodeSizeMax, int nConeSizeMax,
                                   int fUseDcs, int fVerbose );

void Abc_NtkManRefStop ( Abc_ManRef_t *p );

word *Abc_NodeConeTruth ( Vec_Ptr_t *vVars, Vec_Ptr_t *vFuncs, int nWordsMax,
                          Abc_Obj_t *pRoot, Vec_Ptr_t *vLeaves, Vec_Ptr_t *vVisited );

int Abc_NodeConeIsConst0 ( word *pTruth, int nVars );

int Abc_NodeConeIsConst1 ( word *pTruth, int nVars );


inline word *get_cut_tt ( Abc_ManRef_t *p, Abc_Obj_t *pNode, Vec_Ptr_t *vFanins )
{
  int nWordsMax = Abc_Truth6WordNum ( p->nNodeSizeMax );
  p->nNodesConsidered++;

  auto pTruth = Abc_NodeConeTruth ( p->vVars, p->vFuncs, nWordsMax, pNode,
                                    vFanins, p->vVisited ); // tt of the cut
  assert ( pTruth != nullptr );
  assert ( !Abc_NodeConeIsConst0 ( pTruth, Vec_PtrSize ( vFanins ) ) );
  assert ( !Abc_NodeConeIsConst1 ( pTruth, Vec_PtrSize ( vFanins ) ) );

  return pTruth;
}

inline word get_cut_tt ( Abc_ManRef_t *p, Abc_Obj_t *pNode,
                         const std::vector<Abc_Obj_t *> &v_fanins )
{

  auto vFanins = Vec_PtrStart ( v_fanins.size () );
  for ( auto i = 0; i < v_fanins.size (); i++ )
  {
    Vec_PtrWriteEntry ( vFanins, i, v_fanins[i] );
  }

  int nWordsMax = Abc_Truth6WordNum ( p->nNodeSizeMax );
  p->nNodesConsidered++;

  auto pTruth = Abc_NodeConeTruth ( p->vVars, p->vFuncs, nWordsMax, pNode,
                                    vFanins, p->vVisited ); // tt of the cut
  assert ( pTruth != nullptr );
  //assert ( !Abc_NodeConeIsConst0 ( pTruth, Vec_PtrSize ( vFanins ) ) );
  //assert ( !Abc_NodeConeIsConst1 ( pTruth, Vec_PtrSize ( vFanins ) ) );

  Vec_PtrFree ( vFanins ); // just free after computing
  return ( *pTruth );      // some form integer, pass by value.
}

std::vector<Abc_Obj_t *> find_cut ( Abc_ManCut_t *pManCut, Abc_Obj_t *pNode )
{
  Vec_Ptr_t *vFanins;
  // Abc_NodeFindCut ( pManCut, pNode, fUseDcs );
  vFanins = Abc_NodeFindCut ( pManCut, pNode, 0 );
  std::vector<Abc_Obj_t *> v_fanins;
  for ( auto i = 0; i < vFanins->nSize; i++ )
  {
    Abc_Obj_t *obj = static_cast<Abc_Obj_t *> ( Vec_PtrEntry ( vFanins, i ) );
    v_fanins.emplace_back ( obj );
  }
  // Vec_PtrFree ( vFanins ); // just free after computing
  return v_fanins;
}

void mark_internal_rec ( Abc_Obj_t *pRoot, Vec_Ptr_t *vFanins )
{ // TODO: rewrite
  auto fanin0 = Abc_ObjFanin0 ( pRoot );
  auto fanin1 = Abc_ObjFanin1 ( pRoot );

  bool f0_rec = true;
  bool f1_rec = true;

  for ( auto i = 0; i < vFanins->nSize; i++ )
  {
    Abc_Obj_t *obj = static_cast<Abc_Obj_t *> ( Vec_PtrEntry ( vFanins, i ) );
    int obj_id = Abc_ObjId ( obj );
    if ( obj_id == Abc_ObjId ( fanin0 ) ) // reached leaf, stop recursion
      f0_rec = false;
    if ( obj_id == Abc_ObjId ( fanin1 ) ) // reached leaf, stop recursion
      f1_rec = false;
  }

  if ( f0_rec )
  {
    Abc_NodeSetTravIdCurrent ( fanin0 );
    mark_internal_rec ( fanin0, vFanins );
  }
  if ( f1_rec )
  {
    Abc_NodeSetTravIdCurrent ( fanin1 );
    mark_internal_rec ( fanin1, vFanins );
  }
}

void mark_internal_nodes ( Abc_Obj_t *pRoot, Vec_Ptr_t *vFanins )
{
  // make sure there are no trivial cuts; else infinite loop
  for ( auto i = 0; i < vFanins->nSize; i++ ) // no leaves shd be internal
  {
    Abc_Obj_t *obj = static_cast<Abc_Obj_t *> ( Vec_PtrEntry ( vFanins, i ) );
    assert ( Abc_ObjId ( obj ) != Abc_ObjId ( pRoot ) );
  }
  mark_internal_rec ( pRoot, vFanins );
}


void mark_internal_rec ( Abc_Obj_t *pRoot, const std::vector<Abc_Obj_t *> &vFanins )
{ // TODO: rewrite
  auto fanin0 = Abc_ObjFanin0 ( pRoot );
  auto fanin1 = Abc_ObjFanin1 ( pRoot );

  bool f0_rec = true;
  bool f1_rec = true;

  for ( auto &obj : vFanins )
  {
    int obj_id = Abc_ObjId ( obj );
    if ( obj_id == Abc_ObjId ( fanin0 ) ) // reached leaf, stop recursion
      f0_rec = false;
    if ( obj_id == Abc_ObjId ( fanin1 ) ) // reached leaf, stop recursion
      f1_rec = false;
  }

  if ( f0_rec )
  {
    Abc_NodeSetTravIdCurrent ( fanin0 );
    mark_internal_rec ( fanin0, vFanins );
  }
  if ( f1_rec )
  {
    Abc_NodeSetTravIdCurrent ( fanin1 );
    mark_internal_rec ( fanin1, vFanins );
  }
}

void mark_internal_nodes ( Abc_Obj_t *pRoot, const std::vector<Abc_Obj_t *> &vFanins )
{
  // make sure there are no trivial cuts; else infinite loop
  for ( const auto &obj : vFanins ) // no leaves shd be internal
  {
    assert ( Abc_ObjId ( obj ) != Abc_ObjId ( pRoot ) );
  }
  mark_internal_rec ( pRoot, vFanins );
}


} // abc namespace


namespace Yise
{

using namespace abc;


namespace
{
int tt_hit_six = 0;
int tt_hit_five = 0;
int tt_hit_four = 0;
int tt_hit_three = 0;
int tt_hit_two = 0;
int tt_hit_one = 0;
int and_hit = 0;

std::ofstream tt_ofs;
bool first_time = false;
}

// clang-format off

// for the direct toplogical order of this routine check commit: 6aaad7cae30
void process_node_rev ( Yig &yig, Abc_Obj_t *pNode,
			std::unordered_map<int, int> &abc_map,
			std::unordered_map<abc::word, int> &yig_tt_map,
			Abc_ManRef_t *pManRef,
			Abc_ManCut_t *pManCut )
{
  // Anything in abc_map should have a driver. Hence this will be multiple drivers
  assert ( abc_map.find ( Abc_ObjId ( pNode ) ) == abc_map.end () );
  assert ( Abc_ObjFanoutNum ( pNode ) <= 1000 );
  assert ( !Abc_ObjIsPo ( pNode ) ); 
  assert ( !Abc_ObjIsPi ( pNode ) );
  assert ( !Abc_ObjIsComplement ( pNode ) && "Invert fanins if we ever hit this" );


  // 1. get past constants. Here add all the cut internal nodes if needed.
  // skip the constant node. Abc_NodeIsConst ( pNode ) has problems;
  if ( Abc_NodeIsPersistant ( pNode ) ) return;

  // 3. compute a reconvergence-driven cut in ntk for pNode
  const auto &vFanins = find_cut ( pManCut, pNode );
  
  // 4. find the TT of the cut, and match with our yig library.
  auto pTruth = get_cut_tt ( pManRef, pNode, vFanins );

  // In the IWLS benchmark y2.aig function has an internal cut that evaluates to 0.
  // deal with the miscellaneous stuff
  if ( abc::Abc_NodeConeIsConst0 ( &pTruth, vFanins.size () ) )
  {
    abc_map [Abc_ObjId ( pNode )] = 0; // const-0 id
    return;
  }
  if ( abc::Abc_NodeConeIsConst1 ( &pTruth, vFanins.size () ) )
  {
    abc_map [Abc_ObjId ( pNode )] = 0; // const-0 id
    return;
  }
  
  auto tt_itr = yig_tt_map.find ( pTruth );

  
  auto ntk = Abc_ObjNtk ( pNode );
  // Lambda to find or update the AbcObj id
  // a cut can have fanins in  PI
  auto find_in_abc_map = [&]( Abc_Obj_t *fanin0 ) {
    auto konst =
    ( fanin0 == Abc_AigConst1 ( ntk ) ) ||
    ( fanin0 == Abc_ObjNot ( Abc_AigConst1 ( ntk ) ) ) ;
    assert ( !konst ); // paranoid!
    
    auto f0_itr = abc_map.find ( Abc_ObjId ( fanin0 ) );
    if ( f0_itr != abc_map.end () ) return f0_itr->second;
    process_node_rev ( yig, fanin0, abc_map, yig_tt_map, pManRef, pManCut );
    f0_itr = abc_map.find ( Abc_ObjId ( fanin0 ) );
    assert ( f0_itr != abc_map.end () );
    return f0_itr->second;
  };
  int root__id = -1; // Only for debugging at the end of this function
  
  //tt_itr = yig_tt_map.end ();
  if ( tt_itr == yig_tt_map.end () ) // no match in truth-table yig_tt_map
  {  // take as a normal AND gate and connect
    //tt_ofs << axekit::get_hex_str ( pTruth ) << " " << vFanins.size () << "\n";
    
    auto f0 = find_in_abc_map ( Abc_ObjFanin0 ( pNode ) ); // recurse until found
    auto f1 = find_in_abc_map ( Abc_ObjFanin1 ( pNode ) ); // recurse until found
    assert ( !Abc_ObjIsComplement ( pNode ) );

    // 2. create the yig node.
    const auto root_id = add_node ( yig );
    abc_map [Abc_ObjId ( pNode )] = root_id;
    root__id = root_id;
    
    Edge e0 = Abc_ObjFaninC0 ( pNode ) ? to_complement_edge ( f0 )
                                       : to_regular_edge ( f0 );
    Edge e1 = Abc_ObjFaninC1 ( pNode ) ? to_complement_edge ( f1 )
                                       : to_regular_edge ( f1 );
    auto z = edge_zero ();
    std::array<Edge, 6> leaves = {e0, e1, z, z, e0, e1};  // And (a, b)
    add_fanins ( yig, to_node ( yig, root_id ), leaves ); // connect the leaves
    and_hit++;
  }
  else // we have a match in truth-table yig_tt_map
  {
    assert ( vFanins.size () <= 6 );
    std::array<Edge, 6> leaves; // a, b, c, d, e, and f

    if ( vFanins.size () == 6 ) tt_hit_six++;
    if ( vFanins.size () == 5 ) tt_hit_five++;
    if ( vFanins.size () == 4 ) tt_hit_four++;
    if ( vFanins.size () == 3 ) tt_hit_three++;
    if ( vFanins.size () == 2 ) tt_hit_two++;
    if ( vFanins.size () == 1 ) tt_hit_one++;


    int v = 0;
    for (auto k = 0; k < 6; k++)
    {
      const auto perm = leaf_permutation ( tt_itr->second, k );
      const auto flip = leaf_negation ( tt_itr->second, k );

      const int MY_TT_ENC_CONST1 = 6;
      const int MY_TT_ENC_DONTCARE = 7;
      
      if ( perm == MY_TT_ENC_CONST1 )
      {
	leaves[k] = flip ? edge_zero () : edge_one ();
      }
      else if ( perm == MY_TT_ENC_DONTCARE )
      {
	assert ( false );
      }
      else
      {
	assert ( v < vFanins.size () );
	auto obj = vFanins[v];
	v++;
	
	auto leaf_id = find_in_abc_map ( obj ); // recurse if we cant find it
	assert ( !Abc_ObjIsComplement ( obj ) );
	assert ( !Abc_ObjIsComplement ( pNode ) );
	leaves[k] = flip ? to_complement_edge ( leaf_id ) : to_regular_edge ( leaf_id );
      }
    }
    mark_internal_nodes ( pNode, vFanins );
    // 2. create the yig node.
    const auto root_id = add_node ( yig );
    abc_map [Abc_ObjId ( pNode )] = root_id;
    root__id = root_id;
    add_fanins ( yig, to_node ( yig, root_id ), leaves ); // connect the leaves

  }

  //std::cout << to_yig_string ( yig, to_node ( yig, root__id ) ) << "  ";
  //std::cout << "abc_map[" << Abc_ObjId ( pNode ) << "] = "<< root__id << "\n";
}


// This will add nodes in the reverse topological order.
// better chances of compact representation, but has recursion
Yig ntk_to_yig_rev_topo ( Abc_Ntk_t *ntk )
{
  //tt_ofs.open ( "tt_var.dump" );
  
  Yig yig;
  init ( yig, Abc_NtkName ( ntk ), Abc_NtkObjNum ( ntk ) + 10 );
  std::unordered_map<int, int> abc_map; // abcObjId to YigId
  abc_map.reserve ( Abc_NtkObjNum ( ntk ) + 10 );

  // note: ABC maps const1 where as Yig maps const0
  //abc_map[0] = 0; // abc_const0 to yig_const1
  //abc_map[0] = Abc_ObjId ( Abc_ObjNot ( Abc_AigConst1 ( ntk ) ) );
  abc_map[Abc_ObjId ( Abc_ObjNot ( Abc_AigConst1 ( ntk ) ) )] = 0;
  for ( auto i = 0; i < Abc_NtkPiNum ( ntk ); i++ ) // add all the inputs
  {
    auto abc_id = Abc_ObjId ( Abc_NtkPi ( ntk, i ) );
    auto id = add_pi ( yig, "i" + std::to_string ( i ) );
    abc_map[abc_id] = id;
    assert ( !Abc_ObjIsComplement ( Abc_NtkPi ( ntk, i ) ) );
  }
  
  const auto max_cut_size = 6;
  auto nConeSizeMax = 16;
  auto fUseDcs = 0;
  ntk = Abc_NtkStrash ( ntk, 1, 1, 0 );
  assert ( !Abc_NtkGetChoiceNum ( ntk ) ); // requirement from Abc_refactor

  Abc_ManRef_t *pManRef;
  Abc_ManCut_t *pManCut;

  Abc_AigCleanup ( (Abc_Aig_t *)ntk->pManFunc );
  // start the managers
  // Abc_NtkManCutStart( max_cut_size, 100000, 100000, 100000 ); ??
  pManCut = Abc_NtkManCutStart ( max_cut_size, nConeSizeMax, 2, 1000 );
  pManRef = Abc_NtkManRefStart ( max_cut_size, nConeSizeMax, fUseDcs, 0 );
  pManRef->vLeaves = Abc_NtkManCutReadCutLarge ( pManCut );
  pManRef->nNodesBeg = Abc_NtkNodeNum ( ntk );

  abc::Abc_NtkIncrementTravId ( ntk ); // travid is used to mark internal nodes

  auto yig_tt_map = generate_yig_tt (); // truth-table map for yig

  Abc_Obj_t *pNode;

  // from Abc_ObjForEachNode abc macro
  auto limit = Vec_PtrSize( ( ntk )->vObjs ) - 1;
  if ( limit >= Abc_NtkObjNumMax ( ntk ) ) limit = Abc_NtkObjNumMax ( ntk ) - 1;
  for ( ; limit > 0 && (((pNode) = Abc_NtkObj(ntk, limit )), 1); limit-- )
  { 
    if ( pNode == Abc_AigConst1 ( ntk ) ) continue; // consts are always headache
    if ( pNode == Abc_ObjNot ( Abc_AigConst1 ( ntk ) ) ) continue;
    
    // skip already added as part of recursion in process_node_rev
    if ( abc_map.find ( Abc_ObjId ( pNode ) ) != abc_map.end () ) continue; 
    // no need to add internal nodes, i.e. nodes inside a cut here
    if ( Abc_NodeIsTravIdCurrent ( pNode ) ) continue; 
    if ( ( pNode ) == nullptr || !Abc_ObjIsNode ( pNode ) ) continue;
    if ( Abc_ObjIsPi ( pNode ) ) continue; 
    if ( Abc_ObjIsPo ( pNode ) ) continue;
    process_node_rev ( yig, pNode, abc_map, yig_tt_map, pManRef, pManCut );
  }


  // Finally connect and tag the POs
  for ( auto i = 0; i < Abc_NtkPoNum ( ntk ); i++ )
  {
    Edge po;
    auto pNode = Abc_NtkPo ( ntk, i ) ;
    if ( pNode == Abc_AigConst1 ( ntk ) ||
	 Abc_ObjFanin0 ( pNode ) == Abc_AigConst1 ( ntk ) )
    {
      po = Abc_ObjFaninC0 ( pNode ) ? get_edge_const0 ( yig ) : get_edge_const1 ( yig ) ;
    }
    else if ( pNode == Abc_ObjNot ( Abc_AigConst1 ( ntk ) ) || 
	      Abc_ObjFanin0 ( pNode ) == Abc_ObjNot ( Abc_AigConst1 ( ntk ) ) )
    {
      po = Abc_ObjFaninC0 ( pNode ) ? get_edge_const1 ( yig ) : get_edge_const0 ( yig ) ;
    }
    else
    {
      auto abc_id = Abc_ObjFaninId0 ( Abc_NtkPo ( ntk, i ) );
      auto po_itr = abc_map.find ( abc_id );
      // TODO: here is a chance that a node marked as internal directly drives PO
      if ( po_itr ==  abc_map.end () )
      {
	auto obj =  Abc_ObjFanin0 ( Abc_NtkPo ( ntk, i ) );
	assert ( Abc_NodeIsTravIdCurrent ( obj ) );
	process_node_rev ( yig, obj, abc_map, yig_tt_map, pManRef, pManCut );
	po_itr = abc_map.find ( abc_id );
      }
      
      assert ( po_itr != abc_map.end () ); // now there should be a driver
      auto id = po_itr->second;
      
      // Abc_ObjIsComplement ( Abc_NtkPo ( ntk, i ) ) crashes
      po = Abc_ObjFaninC0 ( Abc_NtkPo ( ntk, i ) ) ?
	to_complement_edge ( id ) : to_regular_edge ( id );
    }
    
    tag_as_po ( yig, po, "o" + std::to_string ( i ) );
  }


  pManRef->nNodesEnd = Abc_NtkNodeNum ( ntk );
  // delete the managers
  Abc_NtkManCutStop ( pManCut );
  Abc_NtkManRefStop ( pManRef );
  // std::exit ( 0 );
  //tt_ofs.close ();

  /*
  std::cout << "Six Input TT hit   = " << tt_hit_six << "\n";
  std::cout << "Five Input TT hit  = " << tt_hit_five << "\n";
  std::cout << "Four Input TT hit  = " << tt_hit_four << "\n";
  std::cout << "Three Input TT hit = " << tt_hit_three << "\n";
  std::cout << "Two Input TT hit   = " << tt_hit_two << "\n";
  std::cout << "One Input TT hit   = " << tt_hit_one << "\n";
  std::cout << "Two Input AND hit  = " << and_hit << "\n";
  std::cout << "Total = " << ( tt_hit_six + tt_hit_five + tt_hit_four + tt_hit_three
  + tt_hit_two + tt_hit_one + and_hit ) << "\n";
  */  
  return yig;
}




//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

inline Abc_Obj_t *abc_add_maj ( Abc_Ntk_t *pNtk, std::array<Abc_Obj_t *, 3> &maj )
{
  auto pMan = (Abc_Aig_t *)pNtk->pManFunc;

  auto ab = Abc_AigAnd ( pMan, maj[0], maj[1] );
  auto bc = Abc_AigAnd ( pMan, maj[1], maj[2] );
  auto ca = Abc_AigAnd ( pMan, maj[2], maj[0] );

  auto m1 = Abc_AigOr ( pMan, ab, bc ); // ab | bc
  auto m2 = Abc_AigOr ( pMan, m1, ca ); // ab | bc | ca

  //std::cout << "Adding maj \n";
  return m2;
}


Abc_Obj_t *add_abc_node ( const Node &node, const Yig &yig,
			  std::unordered_map<unsigned, int> &abc_map,
			  Abc_Ntk_t *pNtk )
{

  // Some useful lamdas
  auto abc_id_to_obj = [&] ( const int abc_id )
  {
    if ( abc_id == abc_map[0] ) 
      return Abc_ObjNot ( Abc_AigConst1 ( pNtk ) ); // const0 ID is -0x7F000000
    if ( abc_id == Abc_ObjId ( Abc_AigConst1 ( pNtk ) ) )
      return Abc_AigConst1 ( pNtk ); // This ID is 0. But lets be on safe side
    if ( abc_id < 0 )
      return Abc_ObjNot ( Abc_NtkObj ( pNtk, -abc_id ) );
    
    return Abc_NtkObj ( pNtk, abc_id );
  };

  // for a const, we can't do Abc_NtkObj
  auto assign_obj = [&] ( const Edge &a, int abc_id )
  {
    if ( is_const1 ( a ) ) return Abc_AigConst1 ( pNtk );
    if ( is_const0 ( a ) ) return Abc_ObjNot ( Abc_AigConst1 ( pNtk ) );
    if ( is_regular ( a ) ) return abc_id_to_obj ( abc_id );
    //std::cout << "negative\n";
    return Abc_ObjNot ( abc_id_to_obj ( abc_id ) );
  };

  // abc_map should be complete, do only after all recursions
  auto yig_id_to_abc_id = [&] ( const Edge &e )
  {
    auto itr = abc_map.find ( to_id ( e ) );
    assert ( itr != abc_map.end () );
    return itr->second;
  };

  //----------------------------------------------------------------------------
  // start program
  auto n_itr = abc_map.find ( to_id ( node ) );
  if ( n_itr != abc_map.end () ) return abc_id_to_obj ( n_itr->second );

  auto fanins = get_fanin_edges ( node );
  auto a = fanins[0];
  auto b = fanins[1];
  auto c = fanins[2];
  auto d = fanins[3];
  auto e = fanins[4];
  auto f = fanins[5];

  if ( abc_map.find ( to_id ( a ) ) == abc_map.end () )
    add_abc_node ( to_node ( yig, a ), yig, abc_map, pNtk );

  if ( abc_map.find ( to_id ( b ) ) == abc_map.end () )
    add_abc_node ( to_node ( yig, b ), yig, abc_map, pNtk );

  if ( abc_map.find ( to_id ( c ) ) == abc_map.end () )
    add_abc_node ( to_node ( yig, c ), yig, abc_map, pNtk );

  if ( abc_map.find ( to_id ( d ) ) == abc_map.end () )
    add_abc_node ( to_node ( yig, d ), yig, abc_map, pNtk );

  if ( abc_map.find ( to_id ( e ) ) == abc_map.end () )
    add_abc_node ( to_node ( yig, e ), yig, abc_map, pNtk );

  if ( abc_map.find ( to_id ( f ) ) == abc_map.end () )
    add_abc_node ( to_node ( yig, f ), yig, abc_map, pNtk );


  auto pa = assign_obj ( a, yig_id_to_abc_id ( a ) );
  auto pb = assign_obj ( b, yig_id_to_abc_id ( b ) );
  auto pc = assign_obj ( c, yig_id_to_abc_id ( c ) );
  auto pd = assign_obj ( d, yig_id_to_abc_id ( d ) );
  auto pe = assign_obj ( e, yig_id_to_abc_id ( e ) );
  auto pf = assign_obj ( f, yig_id_to_abc_id ( f ) );

  if ( is_complement ( a ) ) pa = Abc_ObjNot ( Abc_ObjRegular ( pa ) );
  if ( is_complement ( b ) ) pb = Abc_ObjNot ( Abc_ObjRegular ( pb ) );
  if ( is_complement ( c ) ) pc = Abc_ObjNot ( Abc_ObjRegular ( pc ) );
  if ( is_complement ( d ) ) pd = Abc_ObjNot ( Abc_ObjRegular ( pd ) );
  if ( is_complement ( e ) ) pe = Abc_ObjNot ( Abc_ObjRegular ( pe ) );
  if ( is_complement ( f ) ) pf = Abc_ObjNot ( Abc_ObjRegular ( pf ) );

  auto maj1 = std::array<Abc_Obj_t *, 3>{pa, pb, pc};
  auto maj2 = std::array<Abc_Obj_t *, 3>{pb, pd, pe};
  auto maj3 = std::array<Abc_Obj_t *, 3>{pc, pe, pf};

  auto m1 = abc_add_maj ( pNtk, maj1 );
  auto m2 = abc_add_maj ( pNtk, maj2 );
  auto m3 = abc_add_maj ( pNtk, maj3 );

  auto maj_final = std::array<Abc_Obj_t *, 3>{m1, m2, m3};
  auto final = abc_add_maj ( pNtk, maj_final );

  // there is a case, when the added node in ABC itself has a negation
  // In very few cases, node object can be complemented in ABC, not just edges.
  // this was our long standing bug.
  // assert ( !Abc_ObjIsComplement ( final ) );
  // abc_map[to_id ( node )] = Abc_ObjId ( Abc_ObjRegular ( final ) );
  if ( Abc_ObjIsComplement ( final ) )
  {
    //std::cout <<"what?\n";
    abc_map[to_id ( node )] = -Abc_ObjId ( Abc_ObjRegular ( final ) );
  }
  else
  {
    abc_map[to_id ( node )] = Abc_ObjId ( final );
  }

  return final;
}


struct EHash
{
  size_t operator () ( const Edge &e ) const
  {
    return ( e.node_id << 1 ) + e.complement;
  }
};
struct EEqual
{
  size_t operator () ( const Edge &e1, const Edge &e2 ) const
  {
    return e1 == e2;
  }
};


Abc_Obj_t *make_abc_node ( const Edge &edge, const Yig &yig, Abc_Ntk_t *pNtk,
			   std::unordered_map<Edge, Abc_Obj_t*, EHash, EEqual> &obj_map
  )
{
  
  auto itr = obj_map.find ( edge );
  if ( itr != obj_map.end () ) return itr->second;
  itr = obj_map.find ( !edge );
  if ( itr != obj_map.end () ) return Abc_ObjNot ( itr->second );


  auto get_edge = [&] ( const Edge &e )
  {
    auto itr = obj_map.find ( e );
    if ( itr != obj_map.end () ) return itr->second;
    itr = obj_map.find ( !e );
    if ( itr != obj_map.end () ) return Abc_ObjNot ( itr->second );
    auto obj = make_abc_node ( e, yig, pNtk, obj_map );
    return obj;
  };

  
  auto fanins = get_fanin_edges ( to_node ( yig, edge) );
  auto aObj = get_edge ( fanins[0] );
  auto bObj = get_edge ( fanins[1] );
  auto cObj = get_edge ( fanins[2] );
  auto dObj = get_edge ( fanins[3] );
  auto eObj = get_edge ( fanins[4] );
  auto fObj = get_edge ( fanins[5] );
  auto a = fanins[0];
  auto b = fanins[1];
  auto c = fanins[2];
  auto d = fanins[3];
  auto e = fanins[4];
  auto f = fanins[5];

  /*
  if ( is_complement ( a ) ) aObj = Abc_ObjNot ( Abc_ObjRegular ( aObj ) );
  if ( is_complement ( b ) ) bObj = Abc_ObjNot ( Abc_ObjRegular ( bObj ) );
  if ( is_complement ( c ) ) cObj = Abc_ObjNot ( Abc_ObjRegular ( cObj ) );
  if ( is_complement ( d ) ) dObj = Abc_ObjNot ( Abc_ObjRegular ( dObj ) );
  if ( is_complement ( e ) ) eObj = Abc_ObjNot ( Abc_ObjRegular ( eObj ) );
  if ( is_complement ( f ) ) fObj = Abc_ObjNot ( Abc_ObjRegular ( fObj ) );
  */

  obj_map[a] = aObj;
  obj_map[b] = bObj;
  obj_map[c] = cObj;
  obj_map[d] = dObj;
  obj_map[e] = eObj;
  obj_map[f] = fObj;

  auto maj1 = std::array<Abc_Obj_t *, 3>{aObj, bObj, cObj};
  auto maj2 = std::array<Abc_Obj_t *, 3>{bObj, dObj, eObj};
  auto maj3 = std::array<Abc_Obj_t *, 3>{cObj, eObj, fObj};

  auto m1 = abc_add_maj ( pNtk, maj1 );
  auto m2 = abc_add_maj ( pNtk, maj2 );
  auto m3 = abc_add_maj ( pNtk, maj3 );

  auto maj_final = std::array<Abc_Obj_t *, 3>{m1, m2, m3};
  auto final = abc_add_maj ( pNtk, maj_final );

  //if ( is_complement ( edge ) ) final = Abc_ObjNot ( final );
  obj_map[edge] = final;
  obj_map[!edge] = Abc_ObjNot ( final );
  return final;
}



Abc_Ntk_t *yig_to_ntk ( Yig &yig, bool dummy )
{

  assert ( good ( yig ) );
  
  // allocate the empty AIG
  auto pNtkNew = Abc_NtkAlloc( ABC_NTK_STRASH, ABC_FUNC_AIG, 1 );
  pNtkNew->pName = Extra_UtilStrsav ( (char *) get_network_name ( yig ).c_str () );
  pNtkNew->pSpec = Extra_UtilStrsav ( (char *) get_file ( yig ).c_str () );
  pNtkNew->nConstrs = 0;

  assert ( node_count ( yig ) <= (1 << 29 ) );  // 2**31 is the max objects possible

  std::unordered_map<Edge, Abc_Obj_t*, EHash, EEqual> obj_map;
  obj_map.reserve ( node_count ( yig ) + 10 );
  obj_map[get_edge_const1 ( yig )] = Abc_AigConst1 ( pNtkNew );
  obj_map[get_edge_const0 ( yig )] = Abc_ObjNot ( Abc_AigConst1 ( pNtkNew ) );

  // create the PIs
  for ( const auto &pi_id : all_pi_ids ( yig ) )
  {
    auto pObj = Abc_NtkCreatePi(pNtkNew);
    assert ( !Abc_ObjIsComplement ( pObj ) ); // PIs cannot be complemented.
    obj_map[to_regular_edge ( pi_id )] = pObj;
    obj_map[to_complement_edge ( pi_id )] = Abc_ObjNot ( pObj );
  }

  // create the POs recursively
  for ( const auto &po : all_pos ( yig ) )
  {
    const auto &po_node = to_node ( yig, po );
    if ( is_node_pipo ( po_node ) ) 
    {
      auto itr = obj_map.find ( to_edge ( po ) );
      if ( itr == obj_map.end () )
      {
	itr = obj_map.find ( !to_edge ( po ) );
	assert ( itr != obj_map.end () ); // all PIs are already added
	auto poObj = Abc_NtkCreatePo ( pNtkNew );
	Abc_ObjAddFanin ( poObj, itr->second );
	poObj->fCompl0 = is_po_complement ( po ) ? 0 : 1; // double negation
      }
      else
      {
	auto poObj = Abc_NtkCreatePo ( pNtkNew );
	Abc_ObjAddFanin ( poObj, itr->second );
	poObj->fCompl0 = is_po_complement ( po ) ? 1 : 0; // normal negation
      }
    }
    else if ( is_node_constpo ( po_node ) )
    {
      auto poObj = Abc_NtkCreatePo(pNtkNew);
      Abc_ObjAddFanin ( poObj, Abc_AigConst1 ( pNtkNew ) );
      poObj->fCompl0 = is_po_complement ( po ) ? 0 : 1; // adding const1, not 0.
    }
    else
    {
      assert ( po_node.type == NodeType::PO );
      auto poObj = Abc_NtkCreatePo(pNtkNew);
      auto pObj = make_abc_node ( to_edge ( po ), yig, pNtkNew, obj_map );
      Abc_ObjAddFanin ( poObj, pObj );
      poObj->fCompl0 = is_po_complement ( po ) ? 1 : 0;
    }

  }

  Abc_NtkShortNames( pNtkNew );
  // remove the extra nodes
  Abc_AigCleanup( (Abc_Aig_t *)pNtkNew->pManFunc );
  return pNtkNew;
  // Note: Only IsStrash and HasAig true for this network

}

// Not a competition question. Lets use recursion.
Abc_Ntk_t *yig_to_ntk ( const Yig &yig )
{
  assert ( good ( yig ) );
  
  // allocate the empty AIG
  auto pNtkNew = Abc_NtkAlloc( ABC_NTK_STRASH, ABC_FUNC_AIG, 1 );
  pNtkNew->pName = Extra_UtilStrsav ( (char *) get_network_name ( yig ).c_str () );
  pNtkNew->pSpec = Extra_UtilStrsav ( (char *) get_file ( yig ).c_str () );
  pNtkNew->nConstrs = 0;

  // if the abc_obj_id is -ve, the node object itself needs to be negated.
  // ABC can store both node and edge concepts in same object.
  // Howver in Yig, the node cannot be complemented, ONLY edges can be complemented.
  // A case in ABC, where the object corresponding to a node being negated is rare
  // Usually input objects (edges) are changed instead of output object (node) negation.
  // But there are benchmarks, e.g., voter in EPFL, where this happens for some nodes.
  
  std::unordered_map<unsigned, int> abc_map; // yigId to AbcId
  assert ( node_count ( yig ) <= (1 << 30 ) );  // 2**31 is the max objects possible
  abc_map.reserve ( node_count ( yig ) );
  abc_map[0] = Abc_ObjId ( Abc_ObjNot ( Abc_AigConst1 ( pNtkNew ) ) ); // this is -ve
  
  // create the PIs
  for ( const auto &pi_id : all_pi_ids ( yig ) )
  {
    auto pObj = Abc_NtkCreatePi(pNtkNew);
    assert ( !Abc_ObjIsComplement ( pObj ) ); // PIs cannot be complemented.
    abc_map[pi_id] = Abc_ObjId ( pObj ) ;
  }

  // create all POs immediately after PIs, needed to maintain the correct node order.
  std::vector <Abc_Obj_t*> abc_pos;
  for ( const auto &po : all_pos ( yig ) )
  {
    auto po_id = to_id ( po );
    auto pObj = Abc_NtkCreatePo(pNtkNew);
    abc_pos.emplace_back ( pObj );
    //abc_map[po_id] = Abc_ObjId ( pObj ) ;
  }

  const auto &pos = all_pos ( yig );
  for ( auto k = 0; k < pos.size (); k++ )
  {
    const auto &po = pos[k];
    const auto &po_node = to_node ( yig, po );
    if ( is_node_pipo ( po_node ) ) // TODO: think about these first two cases
    {
      auto pObj = Abc_NtkPi ( pNtkNew, node_to_pi_num ( yig, po_node ) );
      Abc_ObjAddFanin( abc_pos[k], pObj );
      abc_pos[k]->fCompl0 = is_po_complement ( po ) ? 1 : 0;
    }
    else if ( is_node_constpo ( po_node ) )
    {
      auto k1 = Abc_AigConst1 ( pNtkNew );
      auto k0 = Abc_ObjNot ( k1 ); 
      auto pObj = is_po_complement ( po ) ? k0 : k1;
      Abc_ObjAddFanin( abc_pos[k], pObj );
    }
    else
    {
      assert ( po_node.type == NodeType::PO );
      auto pObj = add_abc_node ( po_node, yig, abc_map, pNtkNew );
      Abc_ObjAddFanin( abc_pos[k], pObj );
      abc_pos[k]->fCompl0 = is_po_complement ( po ) ? 1 : 0;
    }
  }

  Abc_NtkShortNames( pNtkNew );
  // remove the extra nodes
  Abc_AigCleanup( (Abc_Aig_t *)pNtkNew->pManFunc );
  return pNtkNew;
  // Note: Only IsStrash and HasAig true for this network
}



Gia_Man_t *yig_to_gia ( const Yig &yig )
{
  auto ntk = yig_to_ntk ( yig );
  std::string file = "/tmp/yig.aig";
  Io_WriteAiger( ntk, (char *)file.c_str(), 1, 0, 0 );
  auto gia = Gia_AigerRead ( (char *)file.c_str(), 1, 1, 1 );
  return gia;
}

Yig gia_to_yig ( Gia_Man_t *gia )
{
  std::string file = "/tmp/gia.aig";
  Gia_AigerWrite ( gia, (char *)file.c_str(), 1, 0 );
  auto ntk = Io_ReadAiger ( (char *)file.c_str(), 1 );

  return ntk_to_yig_rev_topo ( ntk );
}


void yig_to_verilog ( const Yig &yig, std::ofstream &ofs )
{
  void write_ygate ( std::ofstream &ofs );
  assert ( good ( yig ) );
  write_ygate ( ofs );
  ofs << "\n\n";

  const auto spc = "   ";
  const auto &pis  = all_pi_ids ( yig );
  const auto &pos  = all_pos ( yig );
  
  ofs << "module " << get_network_name ( yig ) << " (\n";
  ofs << spc;
  for ( const auto &pi : pis )
  {
    ofs << "input " << to_name ( yig, to_node ( yig, pi ) ) << ", ";
  }
  ofs << "\n";
  ofs << spc;
  for ( auto i = 0; i < pos.size () - 1; i++ )
  {
    ofs << "output " << to_name ( pos[i] ) << ", ";
  }
  ofs << "output " << to_name ( pos[pos.size () - 1] ) << "\n";
  ofs << spc << ");\n\n";
  

  const auto &nodes = all_nodes ( yig );
  
  ofs << spc << "wire ";
  for ( auto i = 0; i < nodes.size () - 1; i++ )
  {
    auto n = nodes[i];
    if ( is_pi ( n ) ) continue;
    if ( is_node_po ( n ) ) continue;
    if ( is_const0 ( n ) ) continue;

    ofs << to_name ( yig, n ) << ", ";
  }
  ofs << to_name ( yig, nodes[nodes.size () - 1] ) << ";\n\n";
  
    
  for ( const auto &n : nodes )
  {
    if ( is_pi ( n ) ) continue;
    //if ( is_node_po ( n ) ) continue;
    if ( is_const0 ( n ) ) continue;

    ofs << spc;

    ofs << "ygate yinst_" << std::to_string ( to_id ( n ) ) << " ( "
	<< ".y(" << to_name ( yig, n ) << "), "
	<< ".a(" << to_name ( yig, n.fanins[0] ) << "), "
	<< ".b(" << to_name ( yig, n.fanins[1] ) << "), "
	<< ".c(" << to_name ( yig, n.fanins[2] ) << "), "
	<< ".d(" << to_name ( yig, n.fanins[3] ) << "), "
	<< ".e(" << to_name ( yig, n.fanins[4] ) << "), "
	<< ".f(" << to_name ( yig, n.fanins[5] ) << ") );";
      
    ofs << "\n";
  }

  ofs << "\n";

  for ( auto &po : pos )
  {
    ofs << spc << "assign " << to_name ( po ) << " = "
	<< to_name ( yig, to_edge ( po ) ) << ";\n";
  }

  ofs << "\n\n";
  ofs << "endmodule\n\n";
  
}


void write_ygate ( std::ofstream &ofs )
{
  const auto spc = "   ";
  ofs << "\n";
  ofs << "module ygate ( y, a, b, c, d, e, f );\n";
  ofs << spc << "output y;\n";
  ofs << spc << "input  a, b, c, d, e, f;\n";
  ofs << spc << "\n";
  ofs << spc << "wire   m1, m2, m3;\n";

  ofs << spc << "assign m1 = ( a & b ) | ( b & c ) | ( c & a );\n";
  ofs << spc << "assign m2 = ( b & d ) | ( d & e ) | ( e & b );\n";
  ofs << spc << "assign m3 = ( c & e ) | ( e & f ) | ( f & c );\n";
  ofs << "\n";
  ofs << spc << "assign y = ( m1 & m2 ) | ( m2 & m3 ) | ( m3 & m1 );\n";
  ofs << "\n";
  ofs << "endmodule // ygate\n";
}




bool check_abc ( const Yig & yig, const Edge &a, const Edge &b, const Edge &c )
{
  if ( b == c // first condition
       // && !is_const ( b ) // b and c not constants, TODO: remove this
       //&& !is_const ( a ) // a is not a constant already
       //&& b != a // a = b = c
       && !is_pi ( to_node ( yig, b ) ) // if any of these is a PI
    )
  {
    std::cout << "FSAFAFAFA\n";
    return true;
  }
  return false;
}

void print_x ( const Yig &yig )
{
  assert ( good ( yig ) );
  for ( auto &n : all_nodes ( yig ) )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_node_po ( n ) ) continue;
    if ( is_const0 ( n ) ) continue;

    auto a = n.fanins[0];
    auto b = n.fanins[1];
    auto c = n.fanins[2];
    auto d = n.fanins[3];
    auto e = n.fanins[4];
    auto f = n.fanins[5];

    if ( check_abc ( yig, a, b, c ) ) std::cout << "found\n";
    if ( check_abc ( yig, d, b, e ) ) std::cout << "found\n";
    if ( check_abc ( yig, f, c, e ) ) std::cout << "found\n";
    
  }
}

void write_dot ( const Yig &yig, const std::string &file )
{
  std::ofstream ofs (  file );
  write_dot ( yig, ofs );
}

void write_dot ( const Yig &yig, std::ofstream &ofs )
{
  assert ( node_count ( yig ) < 50 ) ;
  assert ( good ( yig ) );
  ofs << "digraph " << get_network_name ( yig ) << " {\n";


  for ( auto &pi_id : all_pi_ids ( yig ) )
  {
    auto name =  to_name ( yig, to_node ( yig, pi_id ) );
    ofs << name 
	<< " [label=\"" << name  << "\""
	<< "]; \n";
  }


  for ( auto &po : all_pos ( yig ) )
  {
    auto name = to_name ( po );
    ofs << name 
	<< " [label=\"" << name  << "\""
	<< "]; \n";
  }
  
  
  for ( auto &n : all_nodes ( yig ) )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const0 ( n ) ) continue;

    for ( auto &f : n.fanins )
    {
      if ( is_complement ( f ) )
      {
	ofs << to_name ( yig, to_node ( yig, f ) ) << " -> "
	    << to_name ( yig, n ) << " [style=dotted]"
	    << ";\n";
	
      }
    }
    
    auto a = n.fanins[0];
    auto b = n.fanins[1];
    auto c = n.fanins[2];
    auto d = n.fanins[3];
    auto e = n.fanins[4];
    auto f = n.fanins[5];
    
    
    ofs << "";
  }


  ofs << "}";
}

} // Yise

// clang-format on

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
