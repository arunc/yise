/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : bdd.hpp
 * @brief  : Binary decision diagram
 */

/*
  BDDs are unique for a given low-high BDD relationship and a variable.
  Variables are the free variables     

  BDDs are identified either of the two ways -> using Bdd index and the BDD Manager
  or the BDD object as such. 
  Use id_to_bdd ( BddManager, BddId ) to convert from id to Bdd object
  For reverse conversion use the id member of the Bdd object
  
  This package follows the ROBDD tutorial from Henrik Reik Anderson, TUD

  Only for basic Bdd manipulations, does not support dynamic variable ordering.
  Efficiency and speed is not top priority.

 */
/*-------------------------------------------------------------------------------*/

#ifndef BDD_HPP
#define BDD_HPP

#include "platform.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// clang-format off
namespace Yise
{
namespace Bdd
{

using Var = int;      // The variable 
using BddId = int;    // the index in the bdd_list

inline BddId id0 () { return 0; }; // zero-bdd id
inline BddId id1 () { return 1; }; // one-bdd id

//------------------------------------------------------------------------------
// Hashtable maps (v, L, H)->Node
struct BddHash
{
  size_t operator () ( const std::array<int, 3> &input ) const
  {
    // compute the hash of individual elements and combine
    auto h0 = std::hash<Var>()   ( input[0] );
    auto h1 = std::hash<BddId>() ( input[1] );
    auto h2 = std::hash<BddId>() ( input[2] );

    auto bdd_hash = (h0 ^ (h1 << 1) ^ ( h2 << 2) );
    return bdd_hash;
  }
};

struct BddEqual
{
  size_t operator () ( const std::array<int, 3> &b1, const std::array<int, 3> &b2 ) const
  {
    return ( b1[0] == b2[0] && b1[1] == b2[1]) && (b1[2] == b2[2]);
  }
};

using HashTable = std::unordered_map<std::array<int, 3>, int, BddHash, BddEqual>;
using NameList_t = std::unordered_map<BddId, std::string>;
//------------------------------------------------------------------------------

class BddManager;
class Bdd
{
public:
  Var         var;          // Variable (PI) associated with the BDD
  BddManager  *p_manager;   // Network pointer
  BddId       id;           // index of the BDD in BDDManager
  int         trav_id = 0;  // Traversal ID
  

  Bdd ( Var _var, BddManager *_p_manager, BddId _id, BddId _low, BddId _high )
    : var ( _var ), p_manager ( _p_manager ), id ( _id ), l ( _low ), h ( _high )
  {
  }
  
  inline BddId low  ( bool complement = false ) { return complement ? h : l; } // get the low child
  inline BddId high ( bool complement = false ) { return complement ? l : h; } // get the high child
private:
  BddId      l;         // index of low BDD
  BddId      h;         // index of high BDD
};

// BddManager is for the whole circuit
class BddManager
{
public:
  std::string       name = "";        // circuit name
  std::string       file = "";        // source filename
  std::vector<Bdd>  bdd_list;         // list of all Bdds
  int               curr_trav_id = 0; // current traversal id
  int               num_vars;         // number of variables
  
  using BddSet = std::unordered_set<BddId>;
  std::unordered_map<Var, BddSet> var_to_bdds;  // set of nodes for each var
  std::unordered_map<BddId, BddSet> fanout_list; // set of fanouts for each Bdd
  std::vector<BddId> po_list;         // list of all PO BDDs
  NameList_t         name_list;       // list of BddId names
  NameList_t         pi_name_list;    // list of Var/PI names

  HashTable lookup; 
  
  //----------------------------------------------------------------------------
  Bdd bdd0 () { return bdd_list[0]; };
  Bdd bdd1 () { return bdd_list[1]; };
  BddManager () {} // default ctor
  BddManager ( const std::string &ckt, unsigned nvars ) { init ( ckt, num_vars ); }
  BddManager ( const BddManager& other )    { copy ( other ); } // copy ctor;
  BddManager (BddManager&& other) noexcept  { move ( other ); } // move ctor
  BddManager& operator= (const BddManager& other)     { copy ( other ); return *this; } // copy assign
  BddManager& operator= (BddManager&& other) noexcept { move ( other ); return *this; } // move assign

  // explicit initialization of BddManager
  void init ( const std::string &ckt_name, const unsigned estimate_nvars )
  {
    num_vars = 0;
    assert ( estimate_nvars <= 128 ); // should be sufficient for our plans
    // no clue how many BDDs per var. Assuming 64 BDDs per level. So max = 8192 Bdds only
    const auto num_bdds = estimate_nvars << 6; 
    name = ckt_name;
    bdd_list.reserve ( num_bdds );
    lookup.reserve ( num_bdds );
    var_to_bdds.reserve ( num_bdds );
    fanout_list.reserve ( num_bdds );
    name_list.reserve ( estimate_nvars * 3 );  // estimate of PIs + POs
    bdd_list.emplace_back ( Bdd (0, this, this->bdd_list.size(), -1, -1) ); // zero bdd
    bdd_list.emplace_back ( Bdd (1, this, this->bdd_list.size(), -1, -1) ); // one bdd
  }

  inline bool good () const
  {
    if ( bdd_list.empty () ) return false;
    return ( bdd_list[0].var == 0 ) && ( bdd_list[0].p_manager == this )
      && ( bdd_list[1].var == 1 ) && ( bdd_list[1].p_manager == this ) && num_vars > 0;
  }

  inline unsigned num_pis   () const { return num_vars; }
  inline unsigned num_pos   () const { return po_list.size (); }
  inline unsigned num_bdds  () const { return bdd_list.size (); }
  inline unsigned num_regular_nodes () const { return num_bdds () - 2; }


private:

  // default ctors cannot update the p_manager in the node list.
  // hence need explicit copy, move ctors
  void copy ( const BddManager &other )
  {
    name = other.name;
    file = other.file;
    bdd_list = other.bdd_list;
    curr_trav_id = other.curr_trav_id;
    num_vars = other.num_vars;
    var_to_bdds = other.var_to_bdds;
    fanout_list = other.fanout_list;
    po_list = other.po_list;
    name_list = other.name_list;
    lookup = other.lookup;
    for ( auto &n : bdd_list ) // default cannot do this
    {
      n.p_manager = this;
    }
  }

  void move ( BddManager &other ) noexcept
  {
    name = std::move ( other.name );
    file = std::move ( other.file );
    bdd_list = std::move ( other.bdd_list );
    curr_trav_id = std::move ( other.curr_trav_id );
    num_vars = std::move ( other.num_vars );
    var_to_bdds = std::move ( other.var_to_bdds );
    fanout_list = std::move ( other.fanout_list );
    po_list = std::move ( other.po_list );
    name_list = std::move ( other.name_list );
    lookup = std::move ( other.lookup );
    for ( auto &n : bdd_list ) // default cannot do this
    {
      n.p_manager = this;
    }
  }

  
};


inline bool lookup_bdd ( const BddManager &mgr, Var var, BddId low, BddId high )
{
  assert ( mgr.good () );
  const auto &itr = mgr.lookup.find ( std::array<int, 3>{var, low, high});
  return itr != mgr.lookup.end ();
}

inline bool lookup_bdd ( BddManager *mgr, Var var, BddId low, BddId high )
{
  assert ( mgr->good () );
  const auto &itr = mgr->lookup.find ( std::array<int, 3>{var, low, high});
  return itr != mgr->lookup.end ();
}

inline BddId get_bdd_id ( BddManager *mgr, Var var, BddId low, BddId high )
{
  assert ( lookup_bdd ( mgr, var, high, low ) );
  const auto &itr = mgr->lookup.find ( std::array<int, 3>{var, low, high});
  return itr->second;
}

inline Bdd get_bdd ( BddManager *mgr, Var var, BddId low, BddId high )
{
  return mgr->bdd_list[get_bdd_id ( mgr, var, low, high )];
}

inline BddId get_bdd_id ( const BddManager &mgr, Var var, BddId low, BddId high )
{
  assert ( lookup_bdd ( mgr, var, high, low ) );
  const auto &itr = mgr.lookup.find ( std::array<int, 3>{var, low, high});
  return itr->second;
}

inline Bdd get_bdd ( const BddManager &mgr, Var var, BddId low, BddId high )
{
  return mgr.bdd_list[get_bdd_id ( mgr, var, low, high )];
}

inline Bdd &low_bdd ( Bdd &bdd, bool complement = false )
{
  assert ( bdd.p_manager != nullptr );
  assert ( bdd.low ( complement ) < bdd.p_manager->bdd_list.size () );
  return bdd.p_manager->bdd_list[ bdd.low ( complement ) ];
}

inline Bdd &high_bdd ( Bdd &bdd, bool complement = false )
{
  assert ( bdd.p_manager != nullptr );
  assert ( bdd.high ( complement ) < bdd.p_manager->bdd_list.size () );
  return bdd.p_manager->bdd_list[ bdd.high ( complement ) ];
}
  
void add_var ( BddManager &mgr, const std::string &name = "" );
void add_pi  ( BddManager &mgr, const std::string &name = "" );


} // Bdd namespace
//------------------------------------------------------------------------------
// clang-format on
//------------------------------------------------------------------------------
} // Yise namespace

#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
