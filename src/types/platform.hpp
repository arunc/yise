/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : platform.hpp
 * @brief  : Sets some platform dependent macros
 */

//------------------------------------------------------------------------------

#ifndef PLATFORM_INCLUDE_HPP
#define PLATFORM_INCLUDE_HPP


#if 0

// SO:591996
#ifdef _WIN32
   //define something for Windows (32-bit and 64-bit, this part is common)
   #error "Windows is NOT supported for now"
   #ifdef _WIN64
   //define something for Windows (64-bit only)
   #else
   //define something for Windows (32-bit only)
   #endif
#elif __APPLE__
    #error "I have not used a Mac. Donno the types :("
    #include "TargetConditionals.h"
    #if TARGET_IPHONE_SIMULATOR
         // iOS Simulator
    #elif TARGET_OS_IPHONE
        // iOS device
    #elif TARGET_OS_MAC
        // Other kinds of Mac OS
    #else
    #   error "Unknown Apple platform"
    #endif
#elif __linux__
    // linux
    #define MIG_EDGE_EQUIV_TYPE unsigned long
    #if __x86_64__

    #else

    #endif

#else
    #error "Unknown platform/compiler"
#endif



#endif
#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

