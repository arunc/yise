/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : qca.hpp
 * @brief  : Quantum Dot Cellular Automata (QCA Graph
 */

/*-------------------------------------------------------------------------------
QCA is a special kind of Majority gate.
Main difference is in the inverter handling, delay, levels and area of QCA

      a
     / \
    /   \
   b ---- c

m = Maj (a, b, c) := <a, b, c>
m = ab + bc + ac

Reduction rules
Unit clauses and consts
1) <a, a, c>  =  a
2) <a, !a, c> =  c
3) <0, b, c>  =  b & c    (2 i/p AND)
4) <1, b, c>  =  b | c    (2 i/p OR)

Axiom:
Inversion of m = m of inversions, follows from the majority property

--------------------------------------------------------------------------------
Conventions in the QCA DAG::
Edge is a literal
id   is a variable
Node is a vertex
pi   is a primary input
po   is a primary output

literal: (variable and sign) : ( id << 1 ) | sign )
id0: *const-1*, id1 : pi0, id2: pi2 and so on
po is only a tag. Any literal ( i.e., edge ) can be tagged as a PO.
Note: 0 is the literal for const-1 and 1 is the literal for const-0 

pi_list is a list of primary input edges (**edges are taken as PI, not Nodes)
po_list is a list of primary output edges

--------------------------------------------------------------------------------
Basic construction:
Qca qca ( "myCircuit", 100 );         // 100: estimate of max node-count

Edge in0 = add_pi ( qca, "in0" );     // "in0":  name of the primary input
Edge in1 = add_pi ( qca, "in1" );     // "in1":  name of the primary input
Edge in2 = add_pi ( qca, "in2" );     // "in2":  name of the primary input

Edge a = add_and ( qca, in0, in1 );   // a = in0 & in1    (AND gate)
Edge b = add_or  ( qca, in0, in1 );   // b = in0 | in1    (OR gate)
Edge c = add_maj ( qca, a, b, in2 );  // c = <a, b, in2>  (Majority gate)
Edge d = add_xor ( qca, a, b );       // d = a ^ b        (XOR gate) 
Edge e = complement ( c );            // e = !c           (NOT gate)

tag_as_po ( qca, e, "out0" );         // "out0":  name of the primary output
tag_as_po ( qca, d, "out1" );         // "out1":  name of the primary output

qca = strash ( qca );                 // cleanup and remove stray nodes

-------------------------------------------------------------------------------*/
//------------------------------------------------------------------------------


#ifndef QCA_HPP
#define QCA_HPP

#include "platform.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

// clang-format off
namespace Yise
{
namespace Qca
{

enum class NodeType
{
  CONST1,     // const-1
  PI,         // primary input
  PI_PO,      // primary input and output together
  PO,         // primary output
  PLAIN       // internal node; anything other than the above types
};

struct Qca;
struct Node;

// Edge is the index of the Node ( node-id) shifted by 1 bit to left
// Edge: = ( id ( node ) << 1 ) | negation
using Edge = unsigned;
static const Edge edge_undef = 0xfffffffe;
static const Edge edge1 = 0x0;
static const Edge edge0 = 0x1;

// type to hold po information: edge and name
// Note: edge can have a separate name, if its a PI or internal node
struct Po_t
{
  Edge edge;
  Qca *p_qca;
  std::string name;
  int indx = -1;
  
  Po_t () {}
  Po_t ( Edge e, const std::string &_name, Qca *qca_ptr )
  {
    edge = e;
    name = _name;
    p_qca = qca_ptr;
  }
  inline void update_index ( int index ) { indx = index ;}
  inline unsigned node_id   () const { return edge >> 1; }
  inline bool is_complement () const { return 1u == ( edge & 1u ); }
};
  
//------------------------------------------------------------------------------
// Note: Reduction rules must be applied before calling hash
inline void sort_three ( Edge &a, Edge &b, Edge &c)
{
  if ( a > b ) std::swap ( a, b );
  if ( a > c ) std::swap ( a, c );
  if ( b > c ) std::swap ( b, c );
}

struct EdgeHash
{
  size_t operator () ( const std::array<Edge, 3> &edges ) const
  {
    // 1. sort the majority gate first, <a, b, c>
    std::array<Edge, 3> m = {edges[0], edges[1], edges[2]};
    sort_three ( m[0], m[1], m[2] );

    // 2. compute the hash of individual elements and combine
    auto h0 = std::hash<Edge>() ( m[0] );
    auto h1 = std::hash<Edge>() ( m[1] );
    auto h2 = std::hash<Edge>() ( m[2] );

    auto qca_hash = (h0 ^ (h1 << 1) ) ^ (h2 << 1);
    return qca_hash;
  }
};

struct EdgeEqual
{
  size_t operator () ( const std::array<Edge, 3> &e1, const std::array<Edge, 3> &e2 ) const
  {
    std::array<Edge, 3> m1 = e1;
    std::array<Edge, 3> m2 = e2;
    sort_three ( m1[0], m1[1], m1[2] );
    sort_three ( m2[0], m2[1], m2[2] );
    return (m1[0] == m2[0]) && (m1[1] == m2[1]) && (m1[2] == m2[2]);
  }
};

using HashTable = std::unordered_map<std::array<Edge, 3>, Edge, EdgeHash, EdgeEqual>;
using NameList_t = std::unordered_map<Edge, std::string>;
using FanoutList_t = std::unordered_map<Edge, std::unordered_set<Edge>>;
//------------------------------------------------------------------------------
// Note: p_qca is ntk ptr. After any transfer in ownership this ptr to be updated.
struct Node
{
  unsigned id;                 // node id
  std::array<Edge, 3> fanins;  // fanin edges: a, b, c
  int value;                   // user defined value
  int trav_id;                 // traversal id
  int level;                   // level in the topological order
  NodeType type;               // NodeType type = NodeType::PLAIN;
  Qca *p_qca;                  // Network
  bool flagA;                  // flag1 for optimization
  bool flagB;                  // flag2 for optimization

  Node ( int _id, const std::array<Edge, 3> &_fanins, NodeType _type, Qca *_qca )
    : p_qca ( _qca )
  {
    id = _id;
    fanins = _fanins;
    type = _type;
    value = 0;
    trav_id = 0;
    level = -2;
  }
  inline bool operator== ( const Node &other ) const { return id == other.id; }
  inline bool operator<  ( const Node &other ) const { return id < other.id; }
  
  inline Edge edge ( bool negate = false ) const { return ( id << 1 ) | negate; }
};

//------------------------------------------------------------------------------
class Qca
{
public:
  std::string       name = "";        // circuit name
  std::string       file = "";        // source filename
  std::vector<Node> node_list;        // list of all nodes
  int               curr_trav_id = 0; // current traversal id
  std::vector<Edge> pi_list;          // list of primary inputs (only regular edges)
  std::vector<Po_t> po_list;          // list of primary outputs
  NameList_t        name_list;        // list of edge names, edges have polarity
  FanoutList_t      fanout_list;      // fanout-edges of each edge, only regular edges

  // Notes:
  // fanout_list is a map of Node to fanouts. pi_list is a list of PI nodes.
  // But instead of nodes, we store corresponding regular edges
  // name_list is a map of Edge to name, +ve/-ve edges can have separate names
  //
  //----------------------------------------------------------------------------
  Qca () {} // default ctor
  Qca ( const std::string &ckt, const unsigned nnodes ) { init ( ckt, nnodes ); }
  Qca ( const Qca& other )    { copy ( other ); } // copy ctor;
  Qca (Qca&& other) noexcept  { move ( other ); } // move ctor
  Qca& operator= (const Qca& other)     { copy ( other ); return *this; } // copy assign
  Qca& operator= (Qca&& other) noexcept { move ( other ); return *this; } // move assign

  // explicit initialization of QCA
  void init ( const std::string &ckt_name, const unsigned num_nodes )
  {
    // LSB0 for sign, and LSB1 used in hashing
    static const auto limit = 1ul << ( ( sizeof ( unsigned ) * 8 ) - 2  );
    assert ( num_nodes < limit );
    name = ckt_name;
    node_list.reserve ( num_nodes );
    node_list.emplace_back ( Node{0, {}, NodeType::CONST1, this} ); // const1
    strash.reserve ( num_nodes );
    fanout_list.reserve ( num_nodes );
  }

  inline bool good () const
  {
    if ( node_list.empty () ) return false;
    return ( node_list[0].id == 0 ) && ( node_list[0].p_qca == this );
  }

  inline unsigned num_pis   () const { return pi_list.size (); }
  inline unsigned num_pos   () const { return po_list.size (); }
  inline unsigned num_nodes () const { return node_list.size (); }

  friend Edge add_qca ( Qca &qca, const std::array<Edge, 3> &fanins, const std::string &name );
  friend bool lookup_qca_node ( Qca &qca, Edge a, Edge b, Edge c );
private:
  // only add_qca has access to hash-table
  HashTable strash; // reduction rules must be applied before calling hash

  // default ctors cannot update the p_qca in the node list.
  // hence need explicit copy, move ctors
  void copy ( const Qca &other )
  {
    name = other.name;
    file = other.file;
    node_list = other.node_list;
    curr_trav_id = other.curr_trav_id;
    pi_list = other.pi_list;
    po_list = other.po_list;
    name_list = other.name_list;
    fanout_list = other.fanout_list;
    for ( auto &n : node_list ) // default cannot do this
    {
      n.p_qca = this;
    }
    for ( auto &po : po_list )
    {
      po.p_qca = this;
    }
  }

  void move ( Qca &other ) noexcept
  {
    name = std::move ( other.name );
    file = std::move ( other.file );
    node_list = std::move ( other.node_list );
    curr_trav_id = std::move ( other.curr_trav_id );
    pi_list = std::move ( other.pi_list );
    po_list = std::move ( other.po_list );
    name_list = std::move ( other.name_list );
    fanout_list = std::move ( other.fanout_list );
    for ( auto &n : node_list ) // default cannot do this
    {
      n.p_qca = this;
    }
    for ( auto &po : po_list )
    {
      po.p_qca = this;
    }
  }
  
};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// All utility functions

inline Edge complement    ( const Edge &e )  { return e ^ 1u; }
inline Edge regular       ( const Edge &e )  { return ( e >> 1 ) << 1; }
inline bool is_complement ( const Edge &e )  { return 1u == ( e & 1u ); }
inline bool is_regular    ( const Edge &e )  { return !is_complement ( e ); }

inline void set_network_name ( Qca &qca, const std::string &name ) { qca.name = name; }
inline std::string get_network_name ( const Qca &qca ) { return qca.name; }
inline void set_file ( Qca &qca, const std::string &name ) { qca.file = name; }
inline std::string get_file ( const Qca &qca ) { return qca.file; }
inline void set_trav_id ( Qca &qca, int trav_id ) { qca.curr_trav_id = trav_id; }
inline void inc_trav_id ( Qca &qca ) { qca.curr_trav_id = qca.curr_trav_id + 1; }
inline int get_trav_id       ( const Qca &qca ) { return qca.curr_trav_id; }
inline void set_curr_trav_id ( Node &node ) { node.trav_id = node.p_qca->curr_trav_id; }

//------------------------------------------------------------------------------
// Edge-node-id conversions
inline Edge to_edge ( int id, bool negate = false ) { return (id << 1) | negate; }
inline int  to_id   ( const Edge &e ) { return e >> 1; }
inline Node to_node ( Qca &qca, Edge &e )
{
  auto id = to_id ( e );
  assert ( qca.good () && id < qca.node_list.size () );
  return qca.node_list[id];
}
inline Node* to_node_p ( Qca &qca, Edge &e )
{
  auto id = to_id ( e );
  assert ( qca.good () && id < qca.node_list.size () );
  return &qca.node_list[id];
}
inline Node to_node ( const Qca &qca, const Edge &e )
{
  assert ( qca.good () && to_id ( e ) < qca.node_list.size () );
  return qca.node_list[to_id ( e )];
}
inline Node to_node ( Qca *p_qca, Edge &e )
{
  auto id = to_id ( e );
  assert ( p_qca->good () && id < p_qca->node_list.size () );
  return p_qca->node_list[id];
}
inline Node* to_node_p ( Qca *p_qca, Edge &e )
{
  auto id = to_id ( e );
  assert ( p_qca->good () && id < p_qca->node_list.size () );
  return &p_qca->node_list[id];
}
inline Node to_node ( const Qca *p_qca, const Edge &e )
{
  assert ( p_qca->good () && to_id ( e ) < p_qca->node_list.size () );
  return p_qca->node_list[to_id ( e )];
}

inline Node to_node ( const Po_t &po ) { 
  assert ( po.p_qca->good () );
  return po.p_qca->node_list[po.node_id ()];
}

// id and edge are same types. Hence prefix _id_ to distinguish
inline Node id_to_node ( Qca &qca, int id )
{
  assert ( qca.good () && id < qca.node_list.size () );
  return qca.node_list[id];
}
inline Node id_to_node ( const Qca &qca, int id )
{
  assert ( qca.good () && id < qca.node_list.size () );
  return qca.node_list[id];
}
inline Node id_to_node ( Qca *p_qca, int id )
{
  assert ( p_qca->good () && id < p_qca->node_list.size () );
  return p_qca->node_list[id];
}
inline Node id_to_node ( const Qca *p_qca, int id )
{
  assert ( p_qca->good () && id < p_qca->node_list.size () );
  return p_qca->node_list[id];
}

std::string to_name ( const Qca *p_qca, const Edge &e );
std::string to_name ( const Qca &qca, const Edge &e );
std::string to_name ( Qca &qca, const Edge &e );
std::string to_name ( Qca *p_qca, const Edge &e );
inline std::string to_name ( const Node &n ) { return to_name ( *n.p_qca , n.edge () ); }
inline std::string to_name ( Node &n ) { return to_name ( *n.p_qca , n.edge () ); }

inline Node const1_node ( const Qca &qca ) { assert ( qca.good () ); return qca.node_list[0]; }
inline int  const1_id   () { return 0; }

//------------------------------------------------------------------------------
inline Node to_node0  ( const Node &n ) { 
  assert ( n.p_qca->good () );
  assert ( to_id ( n.fanins[0] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[0] )];
}
inline Node to_node1  ( const Node &n ) { 
  assert ( n.p_qca->good () );
  assert ( to_id ( n.fanins[1] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[1] )];
}
inline Node to_node2  ( const Node &n ) { 
  assert ( n.p_qca->good () );
  assert ( to_id ( n.fanins[2] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[2] )];
}
inline int to_id0 ( const Node &n ) { return to_node0 ( n ).id; }
inline int to_id1 ( const Node &n ) { return to_node1 ( n ).id; }
inline int to_id2 ( const Node &n ) { return to_node2 ( n ).id; }

//------------------------------------------------------------------------------
// boolean predicates
inline bool is_const ( const Node &n )  { return n.id == 0; }
inline bool is_const ( const Edge &e )  { return ( e == edge0 ) || ( e == edge1 ); } 
inline bool is_id_const ( const int id ){ return id == 0 ; } 
inline bool is_const ( const Po_t &po ) { return is_const ( po.edge ); }

inline bool is_const1 ( const Node &n )  { return n.id == 0; }
inline bool is_id_const1 ( const int id ){ return id == 0; } 
inline bool is_const1 ( const Edge &e )  { return e == edge1; }
inline bool is_const1 ( const Po_t &po ) { return is_const1 ( po.edge ); }

inline bool is_const0 ( const Edge &e )  { return e == edge0; }
inline bool is_const0 ( const Po_t &po ) { return is_const0 ( po.edge ); }

inline bool is_pi ( const Node &n ) { return n.type == NodeType::PI || n.type == NodeType::PI_PO; }
inline bool is_pi ( const Qca &qca, const Edge &e ) { return is_pi ( to_node ( qca, e ) ); }
inline bool is_pi ( Qca *p_qca, Edge &e ) { return is_pi ( to_node ( p_qca, e ) ); }
inline bool is_id_pi ( const Qca &qca, const int &id ) { return is_pi ( id_to_node ( qca, id ) ); }
inline bool is_id_pi ( Qca *p_qca, int id ) { return is_pi ( id_to_node ( p_qca, id ) ); }

inline bool is_po ( const Node &n ) { return n.type == NodeType::PO || n.type == NodeType::PI_PO; }
inline bool is_po ( Qca *p_qca, Edge e ) { return is_po ( to_node ( p_qca, e ) ); }
inline bool is_po ( const Qca &qca, const Edge &e ) { return is_po ( to_node ( qca, e ) ); }
inline bool is_id_po ( const Qca &qca, const int &id ) { return is_po ( id_to_node ( qca, id ) ); }
inline bool is_id_po ( Qca *p_qca, int id ) { return is_po ( id_to_node ( p_qca, id ) ); }

inline bool is_pipo ( const Node &n ) { return n.type == NodeType::PI_PO; }
inline bool is_pipo ( const Qca &qca, const Edge &e ) { return is_pipo ( to_node ( qca, e ) ); }
inline bool is_pipo ( Qca *p_qca, Edge e ) { return is_pipo ( to_node ( p_qca, e ) ); }
inline bool is_id_pipo ( const Qca &qca, const int &id ) { return is_pipo ( id_to_node ( qca, id ) ); }
inline bool is_id_pipo ( Qca *p_qca, int id ) { return is_pipo ( id_to_node ( p_qca, id ) ); }

inline bool is_plain ( const Node &n ) { return n.type == NodeType::PLAIN; }
inline bool is_plain ( const Qca &qca, const Edge &e ) { return is_plain ( to_node ( qca, e ) ); }
inline bool is_id_plain ( const Qca &qca, const int &id ) { return is_plain ( id_to_node ( qca, id ) ); }

inline bool has_fanouts ( const Qca &qca, const Edge &e ) { return qca.fanout_list.find ( e ) != qca.fanout_list.end (); }
inline bool has_fanouts ( Qca *p_qca, Edge e ) { return p_qca->fanout_list.find ( e ) != p_qca->fanout_list.end (); }
inline bool has_fanouts ( const Node &n ) { return has_fanouts ( *n.p_qca, n.edge () ); }
inline bool has_fanouts_for_id ( const Qca &qca, const int &id ) { return has_fanouts ( qca, to_edge ( id ) ); }

inline bool has_name ( const Qca &qca, const Edge &e ) { return qca.name_list.find ( e ) != qca.name_list.end (); }
inline bool has_name ( Qca *p_qca, Edge e ) { return p_qca->name_list.find ( e ) != p_qca->name_list.end (); }
inline bool has_name ( const Node &n ) { return has_name ( *n.p_qca, n.edge () ); }
inline bool has_name_for_id ( const Qca &qca, const int &id ) { return has_name ( qca, to_edge ( id ) ); }


//------------------------------------------------------------------------------
// for C++11 iterators
inline std::vector<Node> all_nodes   ( Qca &qca )       { assert ( qca.good () ); return qca.node_list; }
inline std::vector<Po_t> all_pos     ( Qca &qca )       { assert ( qca.good () ); return qca.po_list; }
inline std::vector<Edge> all_pis     ( Qca &qca )       { assert ( qca.good () ); return qca.pi_list; }
inline std::vector<Node> all_nodes   ( const Qca &qca ) { assert ( qca.good () ); return qca.node_list; }
inline std::vector<Po_t> all_pos     ( const Qca &qca ) { assert ( qca.good () ); return qca.po_list; }
inline std::vector<Edge> all_pis     ( const Qca &qca ) { assert ( qca.good () ); return qca.pi_list; }
inline std::unordered_map<Edge, std::string> name_map ( Qca &qca ) { assert ( qca.good () ); return qca.name_list; }

inline std::vector<Node> all_nodes   ( Qca *p_qca ) { assert ( p_qca->good () ); return p_qca->node_list; }
inline std::vector<Po_t> all_pos     ( Qca *p_qca ) { assert ( p_qca->good () ); return p_qca->po_list; }
inline std::vector<Edge> all_pis     ( Qca *p_qca ) { assert ( p_qca->good () ); return p_qca->pi_list; }
inline std::unordered_map<Edge, std::string> name_map ( Qca *p_qca ) { assert ( p_qca->good () ); return p_qca->name_list; }

inline std::unordered_set<Edge> all_fanouts ( const Qca &qca, const Edge &e )
{
  assert ( qca.good () );
  const auto itr = qca.fanout_list.find ( e );
  if ( itr == qca.fanout_list.end () )
    return  std::unordered_set<Edge> {};
  else
    return itr->second;
}

inline std::unordered_set<Edge> all_fanouts ( Qca *p_qca, const Edge &e ) { return all_fanouts ( *p_qca, e ); }
inline std::unordered_set<Edge> all_fanouts ( const Node &n ) { return all_fanouts ( n.p_qca, n.edge () ); }

inline std::array<Edge, 3> all_fanins ( const Qca &qca, const Edge &e )
{
  if ( e == edge_undef )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef}; 
  }
  else if ( is_pi ( qca, e ) || is_const ( e ) )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef};
  }
  else
  {
    return to_node ( qca, e ).fanins;
  }
}

inline std::array<Edge, 3> all_fanins ( Qca *p_qca, Edge e )
{
  if ( e == edge_undef )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef}; 
  }
  else if ( is_pi ( p_qca, e ) || is_const ( e ) )
  {
    return std::array<Edge, 3> {edge_undef, edge_undef, edge_undef};
  }
  else
  {
    return to_node ( p_qca, e ).fanins;
  }
}

inline std::array<Edge, 3> all_fanins ( const Node &n ) { return all_fanins ( *n.p_qca, n.edge () ); }

//------------------------------------------------------------------------------
// PI and PO indices
inline int edge_to_pi_index ( const Qca &qca, const Edge &e )
{
  assert ( is_pi ( qca, e ) );
  assert ( qca.good () );
  auto itr = std::find ( qca.pi_list.begin (), qca.pi_list.end (), e );
  assert ( itr != qca.pi_list.end () );
  return ( itr - qca.pi_list.begin () );
}

inline int node_to_pi_index ( const Node &n )
{
  assert ( n.p_qca->good () );
  assert ( is_pi ( n ) );
  return ( edge_to_pi_index ( *n.p_qca, n.edge () ) );
}

inline int id_to_pi_index ( const Qca &qca, const int node_id )
{
  assert ( is_pi ( qca, node_id ) );
  return ( edge_to_pi_index ( qca, to_edge ( node_id ) ) );
}

inline Edge po_index_to_edge ( const Qca &qca, const int i ) { return qca.po_list[i].edge; }
inline int po_index_to_id    ( const Qca &qca, const int i ) { return qca.po_list[i].node_id (); }

//------------------------------------------------------------------------------
// Traversal ID manipulation
inline bool is_trav_id_curr ( const Node &n ) { return n.trav_id == ( n.p_qca )->curr_trav_id; }
inline bool is_trav_id_last ( const Node &n ) { return n.trav_id == ( ( n.p_qca )->curr_trav_id - 1 ); }

//------------------------------------------------------------------------------
// structural hashing ( similar to copying )
Qca strash ( const Qca &qca1 );
Qca copy ( const Qca &qca1 ); // take a blind copy

// check if a MAJ node already exists for a given fanin
inline bool lookup_qca_node ( Qca &qca, Edge a, Edge b, Edge c )
{
  assert ( qca.good () );
  assert ( a != edge_undef );
  assert ( b != edge_undef );
  assert ( c != edge_undef );
  
  // reduction rules
  if ( ( a == b ) || ( a == c ) || ( b == c ) || ( a == complement ( b ) )
       || ( a == complement ( c ) ) || ( b == complement ( c ) ) )
  {
    return true;
  }

  // 1. check if already strashed
  std::array<Edge, 3> fanins = {a, b, c};
  std::sort ( &fanins[0], &fanins[2] );
  const auto it = qca.strash.find ( fanins );
  if ( it != qca.strash.end () ) return true;

  // 2. <!a, !b, !c> = !<a, b, c> :: covers nand, nor, and, or
  std::array<Edge, 3> inv_copy = {
    complement ( fanins[0] ),
    complement ( fanins[1] ),
    complement ( fanins[2] )
  };
  const auto inv_it = qca.strash.find ( inv_copy );
  if ( inv_it != qca.strash.end () ) return true;
  
  return false;
}

// Adding/Updating new nodes, PI, POs
Edge add_qca ( Qca &qca, const std::array<Edge, 3> &fanins, const std::string &name = "" );
Edge add_pi    ( Qca &qca, const std::string &name = "" );
void tag_as_po ( Qca &qca, Edge &edge, const std::string &name = "" );

inline Edge add_and (
  Qca &qca, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge0, a, b};
  return add_qca ( qca, fanins, name );
}

inline Edge add_nand (
  Qca &qca, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge1, complement ( a ), complement ( b ) };
  return add_qca ( qca, fanins, name );
}

inline Edge add_or (
  Qca &qca, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge1, a, b};
  return add_qca ( qca, fanins, name );
}

inline Edge add_nor (
  Qca &qca, const Edge &a, const Edge &b, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {edge0, complement ( a ), complement ( b )};
  return add_qca ( qca, fanins, name );
}

inline Edge add_maj (
  Qca &qca, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  const std::array<Edge, 3> &fanins = {a, b, c}  ;
  return add_qca ( qca, fanins, name );
}

inline Edge add_mux (
  Qca &qca, const Edge &in0, const Edge &in1, const Edge &c, const std::string &name = "" )
{
  auto x = add_and ( qca, complement ( c ), in0 );
  auto y = add_and ( qca,  c, in1 );
  return add_or ( qca, x, y, name );
}
  
inline Edge add_xor (
  Qca &qca, const Edge &a, const Edge &b, const std::string &name = "" )
{
  auto x = add_and ( qca, complement ( a ), b );
  auto y = add_and ( qca, complement ( b ), a );
  return add_or ( qca, x, y, name );
}

// a ^ b ^ c = Maj ( !Maj ( a, b, c), c, Maj ( a, b, !c) )
inline Edge add_xor (
  Qca &qca, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto m1 = add_maj ( qca, a, b, c );
  auto m2 = add_maj ( qca, a, b, complement ( c ) );
  return add_maj ( qca, m1, c, m2, name );
}

inline Edge add_xnor (
  Qca &qca, const Edge &a, const Edge &b, const std::string &name = "" )
{
  auto x = add_and ( qca, complement ( a ), b );
  auto y = add_and ( qca, complement ( b ), a );
  auto zz = complement ( add_or ( qca, x, y ) );
  if ( name != "" ) qca.name_list[zz] = name;
  return zz;
}

// a ^ b ^ c = Maj ( !Maj ( a, b, c), c, Maj ( a, b, !c) )
inline Edge add_xnor (
  Qca &qca, const Edge &a, const Edge &b, const Edge &c, const std::string &name = "" )
{
  auto m1 = add_maj ( qca, a, b, c );
  auto m2 = add_maj ( qca, a, b, complement ( c ) );
  auto zz = complement ( add_maj ( qca, m1, c, m2, name ) );
  if ( name != "" ) qca.name_list[zz] = name;
  return zz;
}


bool check_qca ( const Qca &qca, bool debug = false );

// All PIs are level 0. const-1 is at level -1. returns a map of node-id vs level.
std::vector<int> compute_levels ( const Qca &qca );
void update_levels ( Qca &qca ); // annotate all the nodes with levels
int get_max_level ( const Qca &qca );
int count_inverters ( const Qca &qca ); // count the number of inverters
void write_dot ( const Qca &qca, const std::string &file );

inline long long compute_cost ( const Qca &qca )
{
  long long cc = ( long long )( ( long long ) qca.num_nodes () * ( long long ) qca.num_nodes () )
    + ( long long ) count_inverters ( qca ); 
  return cc * ( long long ) get_max_level ( qca );
}
inline int get_max_delay ( const Qca &qca ) { return get_max_level ( qca ); }

// I/O (from aiger.cpp, qcaer.cpp)
Qca read_aig ( const std::string &file );
Qca read_qca ( const std::string &file );
void write_aig ( const Qca &qca, const std::string &file, bool ascii = false );
void write_qca ( const Qca &qca, const std::string &file, bool ascii = false );

} // Qca namespace
//------------------------------------------------------------------------------
// clang-format on
//------------------------------------------------------------------------------
} // Yise namespace

#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
