/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : bdd.cpp
 * @brief  : Binary decision diagram 
 */

/*-------------------------------------------------------------------------------*/

//clang-format off

#include "bdd.hpp"

#include <utils/common_utils.hpp>

#include <sstream>



namespace Yise
{
namespace Bdd
{

BddId add_bdd ( BddManager &mgr, Var var, BddId low, BddId high )
{
  if ( low == high ) return low;
  if ( lookup_bdd ( mgr, var, low, high ) ) return get_bdd_id ( mgr, var, low, high );

  const auto id = mgr.bdd_list.size ();
  mgr.bdd_list.emplace_back ( Bdd ( var, &mgr, id, low, high ) );

  return id;
  // TODO: update all other tables
}

void add_var ( BddManager &mgr, const std::string &name )
{
  assert ( mgr.num_vars == mgr.pi_name_list.size () );
  std::stringstream ss;
  if ( name == "" ) ss << "pi" << mgr.num_vars;
  else ss << name;
  mgr.pi_name_list[mgr.pi_name_list.size()] = ss.str ();
  mgr.num_vars++;
}

void add_pi ( BddManager &mgr, const std::string &name )
{
  add_var ( mgr, name );
}


BddId ite ( BddManager &mgr, BddId F, BddId G, BddId H )
{
  assert ( false );
  // TODO: complete
}

  
} // Bdd namespace
//------------------------------------------------------------------------------
} // Yise namespace

//clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


