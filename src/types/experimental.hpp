/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : experimental.hpp
 * @brief  : Some more functions
 */
//------------------------------------------------------------------------------

#ifndef EXPERIMENTAL_INCLUDE_HPP
#define EXPERIMENTAL_INCLUDE_HPP

#include "yig.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iterator>
#include <regex>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <iomanip>

#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/abc/abc_truth.hpp>
#include <utils/common_utils.hpp>

#include <functions/truth/yig_truth.hpp>

namespace Yise
{
void print_x ( const Yig &yig ); // count the don't cares
void write_dot ( const Yig &yig, std::ofstream &ofs );
void write_dot ( const Yig &yig, const std::string &file );
}



#endif


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
