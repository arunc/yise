/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cut.hpp
 * @brief  : Cut enumeration for QCA
 */
//------------------------------------------------------------------------------

#ifndef QCA_CUT_HPP
#define QCA_CUT_HPP

#include <types/qca.hpp>

#include <unordered_set>

namespace Yise
{
namespace Qca
{


// A cut is a unique set of leaf node ids
struct Cut
{
  std::unordered_set<int> leaves;
  int volume = 0;
  int delay = 0;
  float area_flow = 0.0;
};

//------------------------------------------------------------------------------
// Initialize the cut system
inline void init_cuts ( Qca &qca )
{
  update_levels ( qca ); // annotate the levels
  for ( auto &n : qca.node_list ) { n.value = 0; } // reset all node-values
}

inline void init_mffc ( Qca &qca )
{
  init_cuts ( qca );
}

// Note: must call init_cuts () before get_cuts ()
Cut get_cut ( Node &node, int k = 6 );
// Note: must call init_mffc () before get_mffc ()
Cut get_mffc ( Node &node, int k = 6 );

//------------------------------------------------------------------------------

}
}


#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
