/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cut.hpp
 * @brief  : Cut enumeration for QCA
 */
//------------------------------------------------------------------------------

#include "cut.hpp"

#include <sstream>
namespace Yise
{
namespace Qca
{

std::string cut_str ( const Qca &qca, const Cut &cut )
{
  std::stringstream ss;
  for ( auto &l : cut.leaves ) { ss << to_name ( qca, to_edge ( l ) ) << " "; }
  return ss.str ();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node2  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[2] )];
}
inline Node &node_from_id ( Qca *p_qca, int id )
{
  assert ( id < p_qca->node_list.size () );
  return p_qca->node_list[id];
}

inline void reset_node_values ( Qca *p_qca, std::vector<unsigned> &visited )
{
  for ( auto &id : visited )
  {
    assert ( id < p_qca->node_list.size () );
    auto &node = p_qca->node_list[id];
    node.value = 0;
  }
}

// cost: number of nodes that will be on the cut-leaves if this node is removed
inline int get_leaf_cost ( Node &leaf_node )
{
  assert ( leaf_node.value == 1 );
  if ( is_pi ( leaf_node ) ) return 999;
  assert ( !is_const ( leaf_node ) ); // think about this

  auto &f0 = node0 ( leaf_node );
  auto &f1 = node1 ( leaf_node );
  auto &f2 = node2 ( leaf_node );

  auto cost = ( !f0.value ) + ( !f1.value ) + ( !f2.value );
  // always accept if the number of leaves does not increase
  if ( cost < 3 ) return cost;
  // return the number of nodes that will be on the leaves if this node is removed
  return cost;
}

bool expand_cut ( Cut &cut, Qca *p_qca, std::vector<unsigned> &visited, int k )
{
  auto best_fanin = 0;
  auto best_cost = 100;
  
  // 1. find the best leaf node to expand
  for ( auto &leaf : cut.leaves )
  {
    auto &leaf_node = node_from_id ( p_qca, leaf );
    const auto &curr_cost = get_leaf_cost ( leaf_node );

    bool update =  best_cost > curr_cost ||
      ( best_cost == curr_cost && leaf_node.level > node_from_id ( p_qca, best_fanin ).level );
    
    if ( update )
    {
      best_cost   = curr_cost;
      best_fanin = leaf;
    }
    if ( best_cost == 0 ) break;
  }

  if ( best_fanin == 0 ) return false; // can't improve
  assert ( best_cost < 4 );
  if ( cut.leaves.size () - 1 + best_cost > k ) return false; // no more room for leaves
  cut.leaves.erase ( best_fanin ); // remove that node and add new set of leaves
  auto &f0 = node0 ( node_from_id ( p_qca, best_fanin ) );
  auto &f1 = node1 ( node_from_id ( p_qca, best_fanin ) );
  // 2. check for reconvergence and add the leaves
  if ( !f0.value )
  {
    f0.value = 1;
    visited.emplace_back ( f0.id );
    cut.leaves.emplace ( f0.id );
  }
  if ( !f1.value )
  {
    f1.value = 1;
    visited.emplace_back ( f1.id );
    cut.leaves.emplace ( f1.id );
  }
  assert ( cut.leaves.size () <= k );
  return true; // continue with the last updated leaves
}

// Note: must call init_cuts () before get_cuts ()
Cut get_cut ( Node &node, int k )
{
  assert ( false ); 
  assert ( k > 1 );
  assert ( node.p_qca->good () );
  assert ( !is_pi ( node ) );
  assert ( !is_const ( node ) );
  assert ( !is_pipo ( node ) );

  node.value = 1;
  auto &f0 = node0 ( node );
  auto &f1 = node1 ( node );
  auto &f2 = node2 ( node );
  f0.value = 1;
  f1.value = 1;
  f2.value = 1;
  Cut cut;
  std::vector<unsigned> visited = {node.id, f0.id, f1.id, f2.id };
  cut.leaves.emplace ( f0.id );
  cut.leaves.emplace ( f1.id );
  while ( expand_cut ( cut, node.p_qca, visited, k ) );
  assert ( cut.leaves.size () <= k );
  cut.volume = visited.size () - cut.leaves.size () - 1;
  reset_node_values ( node.p_qca, visited );
  return cut;
}

bool expand_mffc ( Cut &cut, Qca *p_qca, std::vector<unsigned> &visited, int k )
{
  auto best_fanin = 0;
  auto best_cost = 100;
  
  // 1. find the best leaf node to expand
  for ( auto &leaf : cut.leaves )
  {
    auto &leaf_node = node_from_id ( p_qca, leaf );
    const auto &curr_cost = get_leaf_cost ( leaf_node );
    const auto &fanouts = all_fanouts ( leaf_node );
    const auto num_fanouts = fanouts.size ();
    bool update =  best_cost > curr_cost ||
      ( best_cost == curr_cost && leaf_node.level > node_from_id ( p_qca, best_fanin ).level );
    update = update && (num_fanouts < 2 );
    if ( update )
    {
      best_cost   = curr_cost;
      best_fanin = leaf;
    }
    if ( best_cost == 0 ) break;
  }

  if ( best_fanin == 0 ) return false; // can't improve
  assert ( best_cost < 4 );
  if ( cut.leaves.size () - 1 + best_cost > k ) return false; // no more room for leaves
  cut.leaves.erase ( best_fanin ); // remove that node and add new set of leaves
  auto &f0 = node0 ( node_from_id ( p_qca, best_fanin ) );
  auto &f1 = node1 ( node_from_id ( p_qca, best_fanin ) );
  auto &f2 = node2 ( node_from_id ( p_qca, best_fanin ) );
  // 2. check for reconvergence and add the leaves
  if ( !f0.value )
  {
    f0.value = 1;
    visited.emplace_back ( f0.id );
    cut.leaves.emplace ( f0.id );
  }
  if ( !f1.value )
  {
    f1.value = 1;
    visited.emplace_back ( f1.id );
    cut.leaves.emplace ( f1.id );
  }
  if ( !f2.value )
  {
    f1.value = 2;
    visited.emplace_back ( f2.id );
    cut.leaves.emplace ( f2.id );
  }
  assert ( cut.leaves.size () <= k );
  return true; // continue with the last updated leaves
}

// Note: must call init_mffc () before get_mffc ()
Cut get_mffc ( Node &node, int k )
{
  assert ( k > 1 );
  assert ( node.p_qca->good () );
  assert ( !is_pi ( node ) );
  assert ( !is_const ( node ) );
  assert ( !is_pipo ( node ) );

  node.value = 1;
  auto &f0 = node0 ( node );
  auto &f1 = node1 ( node );
  auto &f2 = node2 ( node );

  Cut cut;
  if ( all_fanouts (f0).size () > 1 || all_fanouts (f1).size () > 1
       || all_fanouts (f2).size () > 1 )
  {
    return cut; // return an empty cut if cant expand
  }
  f0.value = 1;
  f1.value = 1;
  f2.value = 1;
  std::vector<unsigned> visited = {node.id, f0.id, f1.id, f2.id};
  cut.leaves.emplace ( f0.id );
  cut.leaves.emplace ( f1.id );
  cut.leaves.emplace ( f2.id );
  while ( expand_mffc ( cut, node.p_qca, visited, k ) );
  assert ( cut.leaves.size () <= k );
  cut.volume = visited.size () - cut.leaves.size () - 1;
  reset_node_values ( node.p_qca, visited );
  return cut;
}


//------------------------------------------------------------------------------
}
}


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
 
