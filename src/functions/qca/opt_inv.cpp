/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : opt_inv.cpp
 * @brief  : Optimize the inverters
 */

//------------------------------------------------------------------------------

#include "opt.hpp"

#include <functions/qca/cut.hpp>
#include <types/common.hpp>

namespace Yise
{
namespace Qca
{

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
inline int count_compl_fanins ( Node &n )
{
  auto count = 0;
  if ( is_complement ( n.fanins[0] ) ) count++;
  if ( is_complement ( n.fanins[1] ) ) count++;
  if ( is_complement ( n.fanins[2] ) ) count++;
  return count;
}

inline int count_compl_fanouts ( Node &n )
{
  auto count = 0;
  const auto &edge = to_edge ( n.id );
  const auto &neg_edge = to_edge ( n.id, true );
  auto itr = n.p_qca->fanout_list.find ( edge );
  if ( itr != n.p_qca->fanout_list.end () )
  {
    auto &fanouts = itr->second;
    
    for ( auto &xx : fanouts )
    {
      auto &node = node_from_id ( n.p_qca, to_id ( xx ) );
      if ( node.fanins[0] == neg_edge ) count++;
      if ( node.fanins[1] == neg_edge ) count++;
      if ( node.fanins[2] == neg_edge ) count++;
    }
  }
  return count;
}

inline int count_all_fanouts ( Node &n )
{
  const auto &edge = to_edge ( n.id );
  const auto &itr = n.p_qca->fanout_list.find ( edge );
  if ( itr != n.p_qca->fanout_list.end () ) return itr->second.size ();
  return 0;
}

inline int count_compl_pos (
  Node &n,
  const std::unordered_map<Edge, std::vector<int>> &po_map
  )
{
  auto count = 0;
  const auto &edge = to_edge ( n.id );
  const auto &itr = po_map.find ( edge );
  if ( itr != po_map.end () )
  {
    auto &indices = itr->second;
    for ( auto &i : indices )
    {
      assert ( i < n.p_qca->po_list.size () );
      auto &po = n.p_qca->po_list[i];
      if ( po.is_complement () ) count++;
    }
  }
  return count;
}

inline int count_all_pos (
  Node &n,
  const std::unordered_map<Edge, std::vector<int>> &po_map
  )
{
  const auto &edge = to_edge ( n.id );
  const auto &itr = po_map.find ( edge );
  if ( itr != po_map.end () ) return itr->second.size ();
  return 0;
}

inline void complement_pos (
  Node &n,
  const std::unordered_map<Edge, std::vector<int>> &po_map
  )
{
  const auto &edge = to_edge ( n.id );
  const auto &itr = po_map.find ( edge );
  if ( itr != po_map.end () )
  {
    auto &indices = itr->second;
    for ( auto &i : indices )
    {
      assert ( i < n.p_qca->po_list.size () );
      auto &po = n.p_qca->po_list[i];
      po.edge = complement ( po.edge );
    }
  }
}

inline void complement_all ( Node &n )
{
  n.fanins[0] = complement ( n.fanins[0] ); // complement fanins
  n.fanins[1] = complement ( n.fanins[1] );
  n.fanins[2] = complement ( n.fanins[2] );

  const auto &edge = to_edge ( n.id );

  auto itr = n.p_qca->fanout_list.find ( edge ); // complement fanouts
  if ( itr != n.p_qca->fanout_list.end () )
  {
    auto &fanouts = itr->second;

    for ( auto &xx : fanouts )
    {
      auto &node = node_from_id ( n.p_qca, to_id ( xx ) );
      if (regular (node.fanins[0]) == edge) node.fanins[0] = complement (node.fanins[0]);
      if (regular (node.fanins[1]) == edge) node.fanins[1] = complement (node.fanins[1]);
      if (regular (node.fanins[2]) == edge) node.fanins[2] = complement (node.fanins[2]);
    }
  }
}

inline std::unordered_map<Edge, std::vector<int>> make_po_map ( Qca &qca )
{
  std::unordered_map<Edge, std::vector<int>> po_map;
  po_map.reserve ( qca.po_list.size () );
  for ( auto i = 0; i < qca.po_list.size (); i++ )
  {
    const auto &po = qca.po_list[i];
    const auto &e = regular ( po.edge );
    const auto &itr = po_map.find ( e );
    if ( itr == po_map.end () ) // deal with multiple POs
    {
      po_map[e] = {i};
    }
    else 
    {
      auto &pos = itr->second;
      pos.emplace_back ( i );
    }
  }
  return po_map;
}

void opt_inv_forward ( Qca &qca )
{
  // map of a regular edge to POs
  const auto &po_map = make_po_map ( qca );
  
  for ( auto &n : qca.node_list )
  {
    if ( is_const ( n ) || is_pi ( n ) || is_pipo ( n ) ) continue;
    const auto total_edges = 3 + count_all_fanouts ( n ) + count_all_pos ( n, po_map );
    const auto total_inv = count_compl_fanins ( n ) +
      count_compl_fanouts ( n ) +  count_compl_pos ( n, po_map );
    
    //if ( n.id != 12 ) continue;
    
    if ( total_inv > ( total_edges >> 1 ) ) // complement fanins, fanouts, POs
    {
      complement_all ( n );
      complement_pos ( n, po_map );
    }
  }
}


void opt_inv_backward_rec (
  Node &n,
  const std::unordered_map<Edge, std::vector<int>> &po_map
  )
{
  if ( is_trav_id_curr ( n ) ) return;
  if ( is_const ( n ) || is_pi ( n ) || is_pipo ( n ) ) return;

  set_curr_trav_id ( n );
  const auto total_edges = 3 + count_all_fanouts ( n ) + count_all_pos ( n, po_map );
  const auto total_inv = count_compl_fanins ( n ) +
    count_compl_fanouts ( n ) +  count_compl_pos ( n, po_map );
  
  if ( total_inv > ( total_edges >> 1 ) ) // complement fanins, fanouts, POs
  {
    complement_all ( n );
    complement_pos ( n, po_map );
  }
  auto &n0 = node0 ( n );
  auto &n1 = node1 ( n );
  auto &n2 = node2 ( n );
  opt_inv_backward_rec ( n0, po_map );
  opt_inv_backward_rec ( n1, po_map );
  opt_inv_backward_rec ( n2, po_map );
}

void opt_inv_backward ( Qca &qca ) // reverese topological
{
  // map of a regular edge to POs
  const auto &po_map = make_po_map ( qca );
  inc_trav_id ( qca );

  for ( auto &pi : qca.pi_list ) // mark the boundary
  {
    auto &n = node_from_id ( &qca, to_id ( pi ) );
    set_curr_trav_id ( n );
  }
  
  for ( auto &po : qca.po_list )
  {
    assert ( to_id ( po.edge ) < qca.node_list.size () );
    auto &node = qca.node_list[ to_id ( po.edge )];
    opt_inv_backward_rec ( node, po_map );
  }
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

} // namespace Qca
} // namespace Yise

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
