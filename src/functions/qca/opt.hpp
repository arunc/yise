/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : opt.hpp
 * @brief  : Header for all QCA optimizations
 */

//------------------------------------------------------------------------------
#ifndef OPT_QCA_HPP
#define OPT_QCA_HPP

#include <types/qca.hpp>


namespace Yise
{

namespace Qca
{

//------------------------------------------------------------------------------
// Optimize inverters
// Below two are inplace optimizations
void opt_inv_backward ( Qca &qca ); // reverese topological optimization
void opt_inv_forward ( Qca &qca );  // topological optimization

inline Qca opt_inv ( Qca &orig_qca )
{
  auto qca = strash ( orig_qca ); // work on a strashed copy
  opt_inv_forward ( qca );
  opt_inv_backward ( qca );
  return qca;
}

inline void opt_inv_inplace ( Qca &qca )
{
  void opt_inv_int (Qca &);
  opt_inv_forward ( qca );
  opt_inv_backward ( qca );
}
//------------------------------------------------------------------------------



//------------------------------------------------------------------------------
// useful inliners
inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node2  ( Node &n ) { 
  assert ( to_id ( n.fanins[2] ) < n.p_qca->node_list.size () );
  return n.p_qca->node_list[ to_id ( n.fanins[2] )];
}
inline Node &node_from_id ( Qca *p_qca, int id )
{
  assert ( id < p_qca->node_list.size () );
  return p_qca->node_list[id];
}
//------------------------------------------------------------------------------

}
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
