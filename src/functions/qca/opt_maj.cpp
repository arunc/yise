/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : opt_maj.cpp
 * @brief  : Cut+TT based optimization for majority function
 */

//------------------------------------------------------------------------------
// corresponding header is common.hpp

#include "opt.hpp"

#include <functions/aig/cut.hpp>
#include <functions/aig/npn.hpp>
#include <functions/aig/truth.hpp>

#include <types/common.hpp>

namespace Yise
{
namespace Qca
{

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Goal is to instantiate maximum majority nodes as possible
namespace opt_maj_space
{

inline Aig::Node &node0  ( Aig::Node &n ) { 
  assert ( Aig::to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ Aig::to_id ( n.fanins[0] )];
}
inline Aig::Node &node1  ( Aig::Node &n ) { 
  assert ( Aig::to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ Aig::to_id ( n.fanins[1] )];
}
inline Aig::Node &node_from_id ( Aig::Aig *p_aig, int id )
{
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}


static const std::unordered_map <Aig::Truth_t, unsigned> sum_tt =
{
  //----------------------------------------------------------------------------
  /*
    3-input sum encoding ( XOR gate )
    p = Maj ( in[0], in[1], in[2] );
    q = Maj ( in[0], in[1], complement ( in[2] ) );
    out = Maj ( complement ( p ), in[2], q )
  */
  
  { 0x9696969696969696 , 0x00eee420 },    // enc: 7 7 7 2 1 0     stage: 0
  { 0x6969696969696969 , 0x00eee421 }     // enc: 7 7 7 2 1 ~0     stage: 0
 //----------------------------------------------------------------------------
};

/*
  3-input carry encoding ( simple Majority )
  q = Maj ( in[0], in[1], in[2] );
 */
static const std::unordered_map <Aig::Truth_t, unsigned> maj_tt =
{

 { 0xb2b2b2b2b2b2b2b2 , 0x00eee430 },    // enc: 7 7 7 2 ~1 0     stage: 0
 { 0x7171717171717171 , 0x00eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 0
 { 0xd4d4d4d4d4d4d4d4 , 0x00eee421 },    // enc: 7 7 7 2 1 ~0     stage: 0
 { 0xe8e8e8e8e8e8e8e8 , 0x00eee420 },    // enc: 7 7 7 2 1 0     stage: 0
 { 0x1717171717171717 , 0x00eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 0
 { 0x2b2b2b2b2b2b2b2b , 0x00eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 0
 { 0x8e8e8e8e8e8e8e8e , 0x00eee520 },    // enc: 7 7 7 ~2 1 0     stage: 0
 { 0x4d4d4d4d4d4d4d4d , 0x00eee521 }     // enc: 7 7 7 ~2 1 ~0     stage: 0
 
};


inline unsigned cut_to_enc ( const Aig::Cut &cut, const Aig::Truth_t &tt )
{
  if ( cut.leaves.size () != 3 ) return 0u;
  
  const auto &itr1 = sum_tt.find ( tt );
  if ( itr1 != sum_tt.end () ) { return itr1->second; }

  const auto &itr2 = maj_tt.find ( tt );
  if ( itr2 != maj_tt.end () ) { return itr2->second; }

  return 0u;
}

inline bool is_sum_encoded ( const Aig::Truth_t &tt )
{
  if ( sum_tt.find ( tt ) == sum_tt.end () ) return false;
  return true;
}

inline bool is_maj_encoded ( const Aig::Truth_t &tt )
{
  if ( maj_tt.find ( tt ) == maj_tt.end () ) return false;
  return true;
}


Edge add_enc_node (
  Qca &qca, const std::vector<Edge> &fanins,
  unsigned enc, bool sum_encoded
  )
{

  // 2. encode the inputs, fanins to enc_fanins
  const auto xxx = edge_undef;
  std::vector<Edge> enc_fanins = {xxx, xxx, xxx, xxx, xxx, xxx};
  for ( auto i = 0; i < fanins.size (); i++ )
  {
    auto edge = fanins [ Aig::leaf_permutation ( enc, i ) ];
    enc_fanins[i] = Aig::leaf_negation ( enc, i ) ? complement ( edge ) : edge;
  }

  // 3. form the QCA
  Edge qca_e;
  if ( sum_encoded )
  {
    auto p = add_maj ( qca, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto q = add_maj ( qca, enc_fanins[0], enc_fanins[1], complement ( enc_fanins[2] ) );
    qca_e = add_maj ( qca, complement ( p ), enc_fanins[2], q );
  }
  else
  {
    qca_e = add_maj ( qca, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
  }
  return qca_e;
}

Edge add_opt_node (
  Aig::Node &orig_node,
  Qca &qca,
  std::unordered_map<int, int> &aig_to_qca
  )
{
  auto itr = aig_to_qca.find ( to_edge ( orig_node.id ) );
  if ( itr != aig_to_qca.end () ) return itr->second;

  bool sum_encoded = false;
  // 1. find if a matching cut encoding is available
  const auto &cut = Aig::get_cut ( orig_node, 6 ); // first try 6 input cuts
  const auto &tt = Aig::compute_cut_tt ( orig_node, cut );
  unsigned enc = cut_to_enc ( cut, tt );
  sum_encoded = is_sum_encoded ( tt );
  
  if ( enc == 0u ) // now try 3 input cuts
  {
    const auto &cut = Aig::get_cut ( orig_node, 3 ); 
    const auto &tt = Aig::compute_cut_tt ( orig_node, cut );
    enc = cut_to_enc ( cut, tt );
    sum_encoded = is_sum_encoded ( tt );
  }
  Edge qca_e;
  
  if ( enc == 0u ) // no matching encoding
  {
    auto f0 = add_opt_node ( node0 ( orig_node ), qca, aig_to_qca );
    auto f1 = add_opt_node ( node1 ( orig_node ), qca, aig_to_qca );
    if ( is_complement ( orig_node.fanins[0] ) ) f0 = complement ( f0 );
    if ( is_complement ( orig_node.fanins[1] ) ) f1 = complement ( f1 );
    
    qca_e = add_and ( qca, f0, f1 );
  }
  else
  {
    std::vector<int> leaves ( cut.leaves.begin (), cut.leaves.end () );
    std::sort ( leaves.begin (), leaves.end () ); // fix an order
    std::vector<Edge> fanins;
    // 1. justify all the leaves, leaves to fanins
    for ( const auto &l : leaves )
    {
      auto &leaf_node = node_from_id ( orig_node.p_aig, l ); // leaves are node ids
      fanins.emplace_back ( add_opt_node ( leaf_node, qca, aig_to_qca ) );
    }
    qca_e = add_enc_node ( qca, fanins, enc, sum_encoded );
  }

  aig_to_qca [ to_edge ( orig_node.id )] = qca_e;
  aig_to_qca [ Aig::complement ( Aig::to_edge ( orig_node.id ) )] = complement ( qca_e );
  return qca_e;
}

}  // namespace opt_maj_space

Qca opt_maj ( Aig::Aig &orig_aig )
{
  assert ( orig_aig.good () );
  Qca qca ( orig_aig.name, orig_aig.num_nodes () );
  std::unordered_map<int, int> aig_to_qca;
  aig_to_qca.reserve ( ( qca.num_nodes () << 1 ) + 100 );
  aig_to_qca[Aig::edge0] = edge0;
  aig_to_qca[Aig::edge1] = edge1;
  aig_to_qca[Aig::edge_undef] = edge_undef;

  for ( const auto &pi : orig_aig.pi_list )
  {
    auto aig_pi = add_pi ( qca, Aig::to_name ( orig_aig, pi ) );
    aig_to_qca[pi] = aig_pi;
    aig_to_qca[complement ( pi )] = Aig::complement ( aig_pi );
  }

  for ( const auto &po : orig_aig.po_list )
  {
    auto &n = orig_aig.node_list[ po.node_id () ] ;
    auto qca_po = opt_maj_space::add_opt_node ( n, qca, aig_to_qca );
    if ( po.is_complement () ) qca_po = complement ( qca_po );
    tag_as_po ( qca, qca_po, po.name );
  }

  qca = strash ( qca );
  return qca;
}






//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

} // namespace Qca
} // namespace Yise

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
