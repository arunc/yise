/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : truth.cpp
 * @brief  : Truth table manipulations for cut
 */

//------------------------------------------------------------------------------
#ifndef TRUTH_MIG_HPP
#define TRUTH_MIG_HPP

#include <types/mig.hpp>

#include "cut.hpp"

namespace Yise
{
namespace Mig
{
using Truth_t = unsigned long;

const Truth_t TtVarConst0 = 0x0000000000000000;
const Truth_t TtVarConst1 = 0xFFFFFFFFFFFFFFFF;
const std::array<Truth_t, 6> TtVariables = { // word: {b5 b4 b3 b2 b1 b0}
  0xAAAAAAAAAAAAAAAA, // b0
  0xCCCCCCCCCCCCCCCC, // b1
  0xF0F0F0F0F0F0F0F0, // b2
  0xFF00FF00FF00FF00, // b3
  0xFFFF0000FFFF0000, // b4
  0xFFFFFFFF00000000  // b5 
};

Truth_t compute_cut_tt ( const Node &node, const Cut &cut );

}
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
