/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cut.cpp
 * @brief  : Cut enumeration for MIG
 */
//------------------------------------------------------------------------------

#include "cut.hpp"

namespace Yise
{
namespace Mig
{

Cut make_cut (
  const Node &n, std::unordered_map<int, Cut> &cut_list, const int k )
{
  assert ( !is_pi ( n ) );
  assert ( !is_const ( n ) );
  assert ( !is_pipo ( n ) );

  auto itr = cut_list.find ( n.id );
  if ( itr != cut_list.end () ) return itr->second;
  
  const auto &leaves0 = make_cut ( to_node0 ( n ), cut_list, k );
  const auto &leaves1 = make_cut ( to_node1 ( n ), cut_list, k );
  assert ( !leaves0.empty () );
  assert ( !leaves1.empty () );
  auto leaves = leaves0;
  leaves.insert ( leaves1.begin (), leaves1.end () );

  if ( leaves.size () > k ) // can't merge these. take nodes from 1 level down
  {
    Cut cut;
    cut.reserve ( k );
    cut.emplace ( to_id0 ( n ) );
    cut.emplace ( to_id1 ( n ) );
    cut_list[n.id] = std::move ( cut );
  }
  else
  {
    cut_list[n.id] = std::move ( leaves );
  }
  return cut_list[n.id];
}

// Note: PI and const nodes do not have cuts ( only trivial cuts )
std::unordered_map<int, Cut> enumerate_cuts ( Mig &mig, const int k )
{
  assert ( k > 1 );
  assert ( mig.good () );
  std::unordered_map<int, Cut> cut_list;
  cut_list.reserve ( mig.node_list.size () );

  // Initialize level-1 cuts
  for ( const auto &pi : mig.pi_list )
  {
    // assert ( has_fanouts ( mig, pi ) ); // EPFL i2c benchmark fails!!
    const auto &fanouts = all_fanouts ( mig, pi );
    for ( const auto &e : fanouts )
    {
      if ( cut_list.find ( to_id ( e ) ) != cut_list.end () ) continue;
      Cut cut;
      cut.reserve ( k );
      const auto &n = to_node ( mig, e );
      cut.emplace ( to_id0 ( n ) );
      cut.emplace ( to_id1 ( n ) );
      cut_list[to_id ( e )] = std::move ( cut );
    }
  }

  for ( const auto &po : mig.po_list )
  {
    const auto &n = to_node ( po );
    if ( is_const ( n ) )  continue;
    if ( is_pipo ( n ) )  continue;
    make_cut ( n, cut_list, k );
  }

  return cut_list;
}

}
}

//------------------------------------------------------------------------------
// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
