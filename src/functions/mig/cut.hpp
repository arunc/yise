/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cut.hpp
 * @brief  : Cut enumeration for MIG
 */
//------------------------------------------------------------------------------

#ifndef MIG_CUT_HPP
#define MIG_CUT_HPP

#include <types/mig.hpp>

namespace Yise
{
namespace Mig
{

// A cut is a unique set of leaf node ids
using Cut = std::unordered_set<int>;

// Note: PI and const nodes do not have cuts ( only trivial cuts )
std::unordered_map<int, Cut> enumerate_cuts ( Mig &mig, const int k = 6 );

}
}


#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
