/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : truth.cpp
 * @brief  : truth table for local cut.
 */

//------------------------------------------------------------------------------

#include "truth.hpp"

namespace Yise
{
namespace Mig
{

Truth_t compute_tt_rec ( const Node &node, std::unordered_map<int, Truth_t> &tt_map )
{
  auto itr = tt_map.find ( node.id );
  if ( itr != tt_map.end () ) return itr->second;
  assert ( !is_pi ( node ) ); // either should be a leaf or not part of the cut
  assert ( !is_const ( node ) );
  
  auto tt0 = compute_tt_rec ( to_node0 ( node ), tt_map );
  auto tt1 = compute_tt_rec ( to_node1 ( node ), tt_map );
  auto tt2 = compute_tt_rec ( to_node2 ( node ), tt_map );
  const auto &fanins = node.fanins;
  tt0 = is_complement ( fanins[0] ) ? (~tt0) : tt0;
  tt1 = is_complement ( fanins[1] ) ? (~tt1) : tt1;
  tt2 = is_complement ( fanins[2] ) ? (~tt2) : tt2;
  auto tt = ( ( tt0 & tt1 ) | ( tt1 & tt2 ) | ( tt2 & tt0 ) );
  tt_map[node.id] = std::move ( tt );
  return tt_map[node.id];
}

Truth_t compute_cut_tt ( const Node &node, const Cut &cut )
{
  assert ( cut.size () < 7 );
  assert ( !cut.empty () );
  std::vector<int> leaves ( cut.begin (), cut.end () );
  std::sort ( leaves.begin (), leaves.end () );
  std::unordered_map<int, Truth_t> tt_map;
  tt_map.reserve ( 20 );
  tt_map[0] = TtVarConst0;
  for ( auto i = 0; i < leaves.size (); i++ )
  {
    tt_map[leaves[i]] =  TtVariables[i];
  }
  compute_tt_rec  ( node, tt_map );
  auto itr = tt_map.find ( node.id );
  assert ( itr != tt_map.end () );
  return itr->second;
}

}
} // namespace Yise

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
