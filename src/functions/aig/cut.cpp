/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cut.hpp
 * @brief  : Cut enumeration for AIG
 */
//------------------------------------------------------------------------------

#include "cut.hpp"

#include <sstream>
namespace Yise
{
namespace Aig
{

std::string cut_manager_str ( const Aig &aig, const CutManager &cut_manager )
{
  const auto &cut_list = cut_manager.cut_list;
  std::stringstream ss;
  for ( const auto &elem : cut_list )
  {
    if ( elem.second.leaves.empty () ) continue;
    ss << elem.first << " : {";
    for ( const auto &l : elem.second.leaves ) 
    {
      ss << to_name ( aig, to_edge ( l ) ) << " ";
    }
    ss << "}\n";
  }
  return ss.str ();
}

std::string cut_str ( const Aig &aig, const Cut &cut )
{
  std::stringstream ss;
  for ( auto &l : cut.leaves ) { ss << to_name ( aig, to_edge ( l ) ) << " "; }
  return ss.str ();
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
Cut make_cut ( const Node &n, CutManager &cut_manager)
{
  //assert ( !is_pi ( n ) );
  assert ( !is_const ( n ) );
  //assert ( !is_pipo ( n ) );
  assert ( cut_manager.p_aig == n.p_aig );
  
  auto &cut_list = cut_manager.cut_list;
  const int k = cut_manager.k;
  
  auto itr = cut_list.find ( n.id );
  if ( itr != cut_list.end () ) return itr->second;
  
  const auto &cut0 = make_cut ( to_node0 ( n ), cut_manager );
  const auto &cut1 = make_cut ( to_node1 ( n ), cut_manager );
  assert ( !cut0.leaves.empty () );
  assert ( !cut1.leaves.empty () );
  auto leaves = cut0.leaves;
  leaves.insert ( cut1.leaves.begin (), cut1.leaves.end () );

  if ( leaves.size () > k ) // can't merge these. take nodes from 1 level down
  {
    Cut cut;
    cut.leaves.reserve ( k );
    cut.leaves.emplace ( to_id0 ( n ) );
    cut.leaves.emplace ( to_id1 ( n ) );
    cut_list[n.id] = std::move ( cut );
  }
  else
  {
    Cut cut;
    cut.leaves = leaves;
    cut_list[n.id] = std::move ( cut );
  }
  return cut_list[n.id];
}

// Note: PI and const nodes do not have cuts ( only trivial cuts )
CutManager enumerate_cuts ( Aig &aig, const int k )
{
  assert ( k > 1 );
  assert ( aig.good () );
  std::unordered_map<int, Cut> cut_list;
  cut_list.reserve ( aig.node_list.size () );

  // Initialize trivial cuts for all PIs
  for ( const auto &pi : aig.pi_list )
  {
    Cut cut;
    cut.leaves.emplace ( to_id ( pi ) );
    cut_list[ to_id ( pi ) ] = std::move ( cut );
    continue;
    
    //assert ( has_fanouts ( aig, pi ) ); // EPFL i2c benchmark this fails. why?
    // this is because of an unconnected PI :-)
    
    const auto &fanouts = all_fanouts ( aig, pi );
    for ( const auto &e : fanouts )
    {
      if ( cut_list.find ( to_id ( e ) ) != cut_list.end () ) continue;
      Cut cut;
      cut.leaves.reserve ( k );
      const auto &n = to_node ( aig, e );
      cut.leaves.emplace ( to_id0 ( n ) );
      cut.leaves.emplace ( to_id1 ( n ) );
      cut_list[to_id ( e )] = std::move ( cut );
    }
  }

  CutManager cut_manager;
  cut_manager.p_aig = &aig;
  cut_manager.k = k;
  cut_manager.cut_list = std::move ( cut_list );
  for ( const auto &po : aig.po_list )
  {
    const auto &n = to_node ( po );
    if ( is_const ( n ) )  continue;
    if ( is_pipo ( n ) )  continue;
    make_cut ( n, cut_manager );
  }

  return cut_manager;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node_from_id ( Aig *p_aig, int id )
{
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}

inline void reset_node_values ( Aig *p_aig, std::vector<unsigned> &visited )
{
  for ( auto &id : visited )
  {
    assert ( id < p_aig->node_list.size () );
    auto &node = p_aig->node_list[id];
    node.value = 0;
  }
}

// cost: number of nodes that will be on the cut-leaves if this node is removed
inline int get_leaf_cost ( Node &leaf_node )
{
  assert ( leaf_node.value == 1 );
  if ( is_pi ( leaf_node ) ) return 999;
  assert ( !is_const ( leaf_node ) ); // think about this

  auto &f0 = node0 ( leaf_node );
  auto &f1 = node1 ( leaf_node );

  auto cost = ( !f0.value ) + ( !f1.value );
  // always accept if the number of leaves does not increase
  if ( cost < 2 ) return cost;
  // return the number of nodes that will be on the leaves if this node is removed
  return cost;
}

bool expand_cut ( Cut &cut, Aig *p_aig, std::vector<unsigned> &visited, int k )
{
  auto best_fanin = 0;
  auto best_cost = 100;
  
  // 1. find the best leaf node to expand
  for ( auto &leaf : cut.leaves )
  {
    auto &leaf_node = node_from_id ( p_aig, leaf );
    const auto &curr_cost = get_leaf_cost ( leaf_node );

    bool update =  best_cost > curr_cost ||
      ( best_cost == curr_cost && leaf_node.level > node_from_id ( p_aig, best_fanin ).level );
    
    if ( update )
    {
      best_cost   = curr_cost;
      best_fanin = leaf;
    }
    if ( best_cost == 0 ) break;
  }

  if ( best_fanin == 0 ) return false; // can't improve
  assert ( best_cost < 3 );
  if ( cut.leaves.size () - 1 + best_cost > k ) return false; // no more room for leaves
  cut.leaves.erase ( best_fanin ); // remove that node and add new set of leaves
  auto &f0 = node0 ( node_from_id ( p_aig, best_fanin ) );
  auto &f1 = node1 ( node_from_id ( p_aig, best_fanin ) );
  // 2. check for reconvergence and add the leaves
  if ( !f0.value )
  {
    f0.value = 1;
    visited.emplace_back ( f0.id );
    cut.leaves.emplace ( f0.id );
  }
  if ( !f1.value )
  {
    f1.value = 1;
    visited.emplace_back ( f1.id );
    cut.leaves.emplace ( f1.id );
  }
  assert ( cut.leaves.size () <= k );
  return true; // continue with the last updated leaves
}

// Note: must call init_cuts () before get_cuts ()
Cut get_cut ( Node &node, int k )
{
  assert ( k > 1 );
  assert ( node.p_aig->good () );
  assert ( !is_pi ( node ) );
  assert ( !is_const ( node ) );
  assert ( !is_pipo ( node ) );

  node.value = 1;
  auto &f0 = node0 ( node );
  auto &f1 = node1 ( node );
  f0.value = 1;
  f1.value = 1;
  Cut cut;
  std::vector<unsigned> visited = {node.id, f0.id, f1.id};
  cut.leaves.emplace ( f0.id );
  cut.leaves.emplace ( f1.id );
  while ( expand_cut ( cut, node.p_aig, visited, k ) );
  assert ( cut.leaves.size () <= k );
  cut.volume = visited.size () - cut.leaves.size () - 1;
  reset_node_values ( node.p_aig, visited );
  return cut;
}

//------------------------------------------------------------------------------
}
}


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
 
