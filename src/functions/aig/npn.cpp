/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : npn.cpp
 * @brief  : NPN computations
 */

//------------------------------------------------------------------------------

#include "npn.hpp"

#include <types/common.hpp>

#include <cstdlib>
#include <fstream>

namespace Yise
{
namespace Aig
{

std::string to_str ( const std::vector<unsigned> &_perm )
{
  std::stringstream str;
  auto perm = _perm;
  std::reverse ( perm.begin (), perm.end () );
  for ( const auto &e : perm )
  {
    std::string sign = is_complement ( e ) ? "~" : "";
    str << sign << to_id ( e ) << " ";  //position
  }
  return str.str ();
}


// all based on corresponding ABC routines
// swaps nth var with (n+1)th var
Truth_t swap_adjacent_variables ( const Truth_t &tt, const int num_vars, const int n )
{
  static Truth_t tt_masks[5][3] = {
      {0x9999999999999999, 0x2222222222222222, 0x4444444444444444},
      {0xC3C3C3C3C3C3C3C3, 0x0C0C0C0C0C0C0C0C, 0x3030303030303030},
      {0xF00FF00FF00FF00F, 0x00F000F000F000F0, 0x0F000F000F000F00},
      {0xFF0000FFFF0000FF, 0x0000FF000000FF00, 0x00FF000000FF0000},
      {0xFFFF00000000FFFF, 0x00000000FFFF0000, 0x0000FFFF00000000}};

  assert ( num_vars < 7 );
  assert ( n < 5 ); // last swap is 4 w.r.t 5

  const auto shift = 1 << n;
  const Truth_t p1 = tt & tt_masks[n][0];
  const Truth_t p2 = ( tt & tt_masks[n][1] ) << shift;
  const Truth_t p3 = ( tt & tt_masks[n][2] ) >> shift;
  return p1 | p2 | p3;
}


// flips the nth var value
Truth_t flip_variable ( const Truth_t &tt, const int num_vars, const int n )
{
  assert ( num_vars < 7 );
  assert ( n <= 5 );

  static Truth_t mask0[6] = {0x5555555555555555, 0x3333333333333333,
                             0x0F0F0F0F0F0F0F0F, 0x00FF00FF00FF00FF,
                             0x0000FFFF0000FFFF, 0x00000000FFFFFFFF};
  const auto shift = 1 << n;
  const Truth_t p1 = ( tt & mask0[n] ) << shift;
  const Truth_t p2 = ( tt & ~mask0[n] ) >> shift;
  return p1 | p2;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
namespace abc_npn // Dont touch :)
{

struct varInfo
{
  int direction;
  int position;
};


struct swapInfo
{
  varInfo *posArray;
  int *realArray;
  int varN;
  int positionToSwap1;
  int positionToSwap2;
};


swapInfo *setSwapInfoPtr ( int varsN )
{
  int i;
  swapInfo *x = (swapInfo *)malloc ( sizeof ( swapInfo ) );
  x->posArray = (varInfo *)malloc ( sizeof ( varInfo ) * ( varsN + 2 ) );
  x->realArray = (int *)malloc ( sizeof ( int ) * ( varsN + 2 ) );
  x->varN = varsN;
  x->realArray[0] = varsN + 100;
  for ( i = 1; i <= varsN; i++ )
  {
    x->posArray[i].position = i;
    x->posArray[i].direction = -1;
    x->realArray[i] = i;
  }
  x->realArray[varsN + 1] = varsN + 10;
  return x;
}

void freeSwapInfoPtr ( swapInfo *x )
{
  free ( x->posArray );
  free ( x->realArray );
  free ( x );
}

int nextSwap ( swapInfo *x )
{
  int i, j, temp;
  for ( i = x->varN; i > 1; i-- )
  {
    if ( i > x->realArray[x->posArray[i].position + x->posArray[i].direction] )
    {
      x->posArray[i].position = x->posArray[i].position + x->posArray[i].direction;
      temp = x->realArray[x->posArray[i].position];
      x->realArray[x->posArray[i].position] = i;
      x->realArray[x->posArray[i].position - x->posArray[i].direction] = temp;
      x->posArray[temp].position = x->posArray[i].position - x->posArray[i].direction;
      for ( j = x->varN; j > i; j-- )
      {
        x->posArray[j].direction = x->posArray[j].direction * -1;
      }
      x->positionToSwap1 = x->posArray[temp].position - 1;
      x->positionToSwap2 = x->posArray[i].position - 1;
      return 1;
    }
  }
  return 0;
}


inline int oneBitPosition ( int x, int size )
{
  int i;
  for ( i = 0; i < size; i++ )
    if ( ( x >> i ) & 1 ) return i;
  return -1;
}

class PermInfo
{
public:
  PermInfo ( int nVars )
  {
    varN = nVars;
    flipCtr = 0;
    totalFlips = ( 1 << varN ) - 1;
    swapCtr = 0;
    totalSwaps = fact ( nVars ) - 1;
    flipArray = new int[totalFlips];
    swapArray = new int[totalSwaps];
    fillInSwapArray ();
    fillInFlipArray ();
  }
  ~PermInfo ()
  {
    delete[] flipArray;
    delete[] swapArray;
  }

  int varN;
  int *swapArray;
  int swapCtr;
  int totalSwaps;
  int *flipArray;
  int flipCtr;
  int totalFlips;

private:
  inline int fact ( int n )
  {
    return ( n == 1 || n == 0 ) ? 1 : fact ( n - 1 ) * n;
  }
  void fillInSwapArray ()
  {
    int counter = totalSwaps - 1;
    auto x = setSwapInfoPtr ( varN );
    while ( nextSwap ( x ) == 1 )
    {
      if ( x->positionToSwap1 < x->positionToSwap2 )
        swapArray[counter--] = x->positionToSwap1;
      else
        swapArray[counter--] = x->positionToSwap2;
    }
    freeSwapInfoPtr ( x );
  }

  void fillInFlipArray ()
  {
    int i, temp = 0, grayNumber;
    for ( i = 1; i <= totalFlips; i++ )
    {
      grayNumber = i ^ ( i >> 1 );
      flipArray[totalFlips - i] = oneBitPosition ( temp ^ grayNumber, varN );
      temp = grayNumber;
    }
  }
};
} // namespace abc_npn
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// general lambdas
std::vector<int> identity_permutation ( const int n )
{
  std::vector<int> perm;
  for ( auto i = 0; i < n; i++ ) // initial permutation positions
  {
    perm.emplace_back ( to_edge ( i ) ); // initially no negation
  }
  return perm;
}
	 
// iterates through every permutation
template<typename F>
void foreach_permutation (
  const Truth_t &tt,
  const abc_npn::PermInfo &pi,
  const std::vector<int> &in_perm, 
  F f
  )
{
  auto perm = in_perm;
  auto x = tt;
  auto updatePermPos = [&] ( std::vector<int> &p, int pos )
  {
    assert ( ( pos + 1 ) < p.size () );
    auto p1 = p[pos];
    auto p2 = p[pos + 1];
    p[pos] = p2;
    p[pos + 1] = p1;
  };

  f ( perm, x ); // identity

  for ( auto i = pi.totalSwaps - 1; i >= 0; i-- ) // all initial permutations
  {
    x = swap_adjacent_variables ( x, perm.size (), pi.swapArray[i] );
    updatePermPos ( perm, pi.swapArray[i] );
    f ( perm, x  );
  }

  updatePermPos ( perm, 0 );
}

// iterates through every negation
// something wrong with the negation scheme. Test thoroughly.
template<typename F>
void foreach_negation (
  const Truth_t &tt,
  const abc_npn::PermInfo &pi,
  const std::vector<int> &in_perm,
  F f
  )
{

  auto perm = in_perm;
  auto x = tt;
  auto x_bar = ~tt;

  auto flipNegPos = [&] ( std::vector<int> &p, int pos )
  {
    assert ( pos < p.size () );
    p[pos] = complement ( p[pos] );
  };

  f ( perm, false, x  );
  f ( perm, true, x_bar  );
  
  for ( auto j = pi.totalFlips - 1; j >= 0; j-- )
  {
    x = flip_variable ( x, perm.size (), pi.flipArray[j] );
    x_bar = flip_variable ( x_bar, perm.size (), pi.flipArray[j] );
    flipNegPos ( perm, pi.flipArray[j] );
    f ( perm, false, x  );
    f ( perm, true, x_bar  );

  }

  flipNegPos ( perm, pi.varN - 1 );
}


// iterates through every permutation and combination
template<typename F>
void foreach_npn (
  const Truth_t &tt,
  const int n, // total vars
  const int k, // permutation vars
  F f
  )
{
  // nothing to permute is special, depends on individual case, deal in callee
  assert ( k != 0 ); 
  assert ( n < 7 );
  assert ( k < 7 );
  assert ( k <= n );
  
  abc_npn::PermInfo pi ( k );
  auto perm = identity_permutation ( n );

  foreach_permutation (
    tt, pi, perm,
    [&] ( std::vector<int> &perm,  const Truth_t &ptt )
    {
      foreach_negation (
	ptt, pi, perm, 
	[&] ( std::vector<int> &perm, bool po_negation, const Truth_t &nptt )
	{
	  f ( perm, po_negation, nptt );
	}
	);
    }
    );

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

Truth_t compute_npn ( const Truth_t &tt, const int num_vars )
{
  assert ( num_vars < 7 );
  auto small = tt;
  foreach_npn (
    tt, num_vars, num_vars,
    [&] ( const std::vector<int> &perm, bool po_negation, const Truth_t &npn_tt )
    {
      if ( small < npn_tt ) small = npn_tt;
    }
    );
    return small;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
/*
 Generate all the permuations and store as static offline Map.
 Use these routines with a verilog input file
 
 int main ( int argc, char *argv[] )
 {
   int stage = std::stoi ( std::string ( argv[2] ) );
   Yise::Aig::generate_permut ( std::string ( argv[1] ), stage );
   return 0;
 }

 Further append the generated encodings to tt_maps.hpp
*/


// output negation is not taken into account in compute_tt()
// edge_permutations: append all newly found truth-tables and edge encoding
void edge_permutations (
  std::unordered_map<Truth_t, unsigned> &tts, // existing TT map
  const Truth_t &tt, const int num_vars
  )
{
  assert ( num_vars < 7 );
  assert ( num_vars != 0 ); 
  const auto n  = num_vars;
  const auto k  = num_vars;
  abc_npn::PermInfo pi ( k );
  auto perm = identity_permutation ( n );

  foreach_negation (
    tt, pi, perm, 
    [&] ( std::vector<int> &_perm, bool po_negation, const Truth_t &ptt )
    {
      if ( po_negation ) return;
      std::vector<unsigned> perm;
      for ( auto &p : _perm )
      {
	assert ( p >= 0 );
	perm.emplace_back ( (unsigned) p );
      }
      const auto &enc = encode_edges ( perm );
      if ( tts.find ( ptt ) == tts.end () ) tts[ptt] = enc;
    }
    );

  
  /*
    TODO: Our permutation has some problem/misunderstanding. Test throughly and fix later.
  foreach_npn (
    tt, num_vars, num_vars,
    [&] ( const std::vector<int> &_perm, bool po_negation, const Truth_t &npn_tt )
    {
      if ( po_negation ) return;
      std::vector<unsigned> perm;
      for ( auto &p : _perm )
      {
	assert ( p >= 0 );
	perm.emplace_back ( (unsigned) p );
      }
      const auto &enc = encode_edges ( perm );
      if ( tts.find ( enc ) == tts.end () ) tts[npn_tt] = enc;
    }
    );
  */
}

unsigned re_encode_with_const ( unsigned enc, int pos, unsigned const_val )
{
  assert ( const_val < 2 );
  assert ( pos < 6 );
  auto edges = decode_edges ( enc );
  const auto val = const_val == 0 ? 0xD : 0xC; // const0 and const1 encoding
  edges[pos] = val;
  return encode_edges ( edges );
}

void print_enc ( const std::unordered_map<Truth_t, unsigned> &tts )
{
  std::ofstream ofs ( "truth.map", std::ios::out | std::ios::app );
  
  for ( auto &el : tts )
  {
    std::cout << "  { " << to_hex_str ( el.first ) << " , "
	      << to_hex_str ( el.second ) << " },"
	      << "    // enc: " <<  to_str ( decode_edges ( el.second ) )
	      << "    stage: " <<  decode_stage ( el.second )
	      << "\n";
    ofs << "  { " << to_hex_str ( el.first ) << " , "
	<< to_hex_str ( el.second ) << " },"
	<< "    // enc: " <<  to_str ( decode_edges ( el.second ) )
	<< "    stage: " <<  decode_stage ( el.second )
	<< "\n";
  }
}

// for 2 input gates
//enc = re_encode_with_const (enc, 2, 0); 
//enc = re_encode_with_const (enc, 2, 0); 
//enc = re_encode_with_const (enc, 2, 0); 
//enc = re_encode_with_const (enc, 2, 1); 
//enc = re_encode_with_const (enc, 2, 1); 
//enc = re_encode_with_const (enc, 2, 1); 

unsigned apply_input_negation ( const int input_pos, unsigned enc )
{
  auto perm_in = decode_edges ( enc );
  std::vector<unsigned> perm;
  if ( input_pos >= 100 )
  {
    for ( auto &i : perm_in )
    {
      if ( to_id ( i ) <= 6 )
	perm.emplace_back ( complement ( i ) );
    }
  }
  else
  {
    for ( auto &i : perm_in )
    {
      if ( input_pos == to_id ( i ) ) perm.emplace_back ( complement ( i ) );
      else perm.emplace_back ( i );
    }
  }
  return encode_edges ( perm );
}

unsigned apply_stage ( const int stage, unsigned enc )
{
  if (stage == 0) { return Yise::Aig::encode_stage ( enc, 0 ); }
  if (stage == 1) { return encode_stage (enc, 1); }     
  if (stage == 2) { return encode_stage (enc, 2); }
  if (stage == 3) { return encode_stage (enc, 3); }
  if (stage == 4) { return encode_stage (enc, 4); }
  if (stage == 5) { return encode_stage (enc, 5); }
  if (stage == 6) { return encode_stage (enc, 6); }
  if ( stage == 13) { return encode_stage ( enc, 13 ); }
  if ( stage == 14) { return encode_stage ( enc, 14 ); }
  if ( stage == 15) { return encode_stage ( enc, 15 ); }

  assert ( false );
}

void generate_permut ( const std::string &file, const int stage )
{
  std::cout << "stage:" << stage << "\n";
  Truth_t compute_edge_tt ( const Node &node, const std::vector<int> &edges );
  auto aig1 = read_aig ( file );
  std::vector<int> leaves;
  for ( auto &pi : aig1.pi_list ) { leaves.push_back ( to_id ( pi ) ); }
  std::sort ( leaves.begin (), leaves.end () ); // doesnt have any effect

  std::unordered_map<Truth_t, unsigned> tts;
  assert ( aig1.po_list.size () == 1 );
  const auto &po = aig1.po_list[0];
  auto tt = compute_tt ( to_node ( po ), leaves ); // discards sign at PO
  if ( po.is_complement () ) tt = ~tt;
  //std::cout << "tt = " << to_hex_str ( tt ) << "\n";
  //std::cout << "~tt = " << to_hex_str ( ~tt ) << "\n";
  edge_permutations ( tts, tt, leaves.size () );

  auto tts_cpy = tts; 
  tts.clear ();
  for ( auto &el : tts_cpy )
  {
    unsigned enc = apply_stage ( stage, el.second );
    Truth_t tt = el.first;
    tts[tt] = enc;
  }
  print_enc ( tts );
}


void generate_adder_permut ( const std::string &file )
{
  Truth_t compute_edge_tt ( const Node &node, const std::vector<int> &edges );
  auto aig1 = read_aig ( file );
  std::vector<int> leaves;
  for ( auto &pi : aig1.pi_list ) { leaves.push_back ( to_id ( pi ) ); }
  std::sort ( leaves.begin (), leaves.end () ); // doesnt have any effect

  std::unordered_map<Truth_t, unsigned> tts;
  assert ( aig1.po_list.size () == 1 );
  const auto &po = aig1.po_list[0];
  auto tt = compute_tt ( to_node ( po ), leaves ); // discards sign at PO
  if ( po.is_complement () ) tt = ~tt;
  //std::cout << "tt = " << to_hex_str ( tt ) << "\n";
  //std::cout << "~tt = " << to_hex_str ( ~tt ) << "\n";
  edge_permutations ( tts, tt, leaves.size () );

  auto tts_cpy = tts; 
  tts.clear ();
  for ( auto &el : tts_cpy )
  {
    Truth_t tt = el.first;
    auto enc = el.second;
    tts[tt] = enc;
  }
  print_enc ( tts );
}


} // namespace Aig
} // namespace Yise

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
