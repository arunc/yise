/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : opt.hpp
 * @brief  : Header for all AIG optimizations
 */

//------------------------------------------------------------------------------
#ifndef OPT_AIG_HPP
#define OPT_AIG_HPP

#include <types/aig.hpp>


namespace Yise
{
namespace Aig
{

// Truthtable based optimization
Aig opt_tt ( Aig &orig_aig );

// Truthtable based adder chain optimization
Aig opt_adder ( Aig &orig_aig );

// balancing
Aig balance ( Aig &orig_aig );


}
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
