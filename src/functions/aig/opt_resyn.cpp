/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : opt_resyn.cpp
 * @brief  : All optimizations for resynthesis - balance, rewrite
 */

//------------------------------------------------------------------------------

#include "opt.hpp"

#include <functions/aig/tt_maps.hpp>
#include <functions/aig/cut.hpp>
#include <functions/aig/npn.hpp>
#include <types/common.hpp>

#include <sstream>

namespace Yise
{
namespace Aig
{

inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node_from_id ( Aig *p_aig, int id )
{
  if ( id >= p_aig->node_list.size () )
  {
    std::cout << "Failed ID: " << id << " node_list.size(): "
	      << p_aig->node_list.size () << "\n";
    std::exit ( 0 );
  }
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}

inline int num_fanouts ( const Node &n )
{
  auto itr = n.p_aig->fanout_list.find ( to_edge ( n.id ) );
  if ( itr !=  n.p_aig->fanout_list.end () )
    return itr->second.size ();
  return 0; // no fanouts so-far
}

void update_best_sharing ( Aig &aig_new, std::vector<Edge> &edges )
{
  if ( edges.size () < 3 ) return;
  const auto &last_indx = edges.size () - 1;
  int curr_best_fanout = -10;
  for ( int i = edges.size () - 1; i >= 1; i-- )
  {
    for ( int j = i - 1; j >= 0; j-- )
    {
      if ( lookup_aig_node ( aig_new, edges[i], edges[j] ) )
      {
	
	std::swap ( edges[i], edges[last_indx] );
	std::swap ( edges[j], edges[last_indx - 1] );
	return;
	
	/*
	// swap with candidates with maximum fanouts; but inferior results
	const auto &nnodes = aig_new.num_nodes ();
	const auto &old_node = node_from_id (
	  &aig_new, to_id ( add_and ( aig_new, edges[i], edges[j] ) ) );
	assert ( nnodes == aig_new.num_nodes () );
	const auto &nfanouts = num_fanouts ( old_node );
	if ( nfanouts > curr_best_fanout ) // swap only best fanout candidates
	{
	  curr_best_fanout = nfanouts;
	  std::swap ( edges[i], edges[last_indx] );
	  std::swap ( edges[j], edges[last_indx - 1] );
	}
	*/
      }
    }
    if ( (edges.size () - i) > 100 ) return; // iterate only on first 100 elements
  }
}


inline int super_gate_edges_rec (
  Aig &aig_orig, const Edge edge_orig, std::unordered_set<Edge> &edges )
{
  // edge repeated in same polarity
  if ( edges.find ( edge_orig ) != edges.end () ) return 0;
  // edge repeated in opposite polarity
  if ( edges.find ( complement ( edge_orig ) ) != edges.end () ) return -1;
  auto &n = node_from_id ( &aig_orig, to_id ( edge_orig ) );
  if ( is_complement ( edge_orig ) || is_pi ( n ) || edges.size () > 10000 )
  {
    edges.emplace ( edge_orig );
    return 0;
  }
  auto ret0 = super_gate_edges_rec ( aig_orig, n.fanins[0], edges );
  auto ret1 = super_gate_edges_rec ( aig_orig, n.fanins[1], edges );
  if ( ret0 == -1 || ret1 == -1 ) return -1;
  return ret0 || ret1;
}


inline std::unordered_set<Edge> super_gate_edges ( Aig &aig_orig, const Edge edge_orig )
{
  assert ( !is_complement ( edge_orig ) );
  std::unordered_set<Edge> edges;

  auto &n = node_from_id ( &aig_orig, to_id (edge_orig ) );
  auto ret0 = super_gate_edges_rec ( aig_orig, n.fanins[0], edges );
  auto ret1 = super_gate_edges_rec ( aig_orig, n.fanins[1], edges );
  if ( ret0 == -1 || ret1 == -1 )
  {
    edges.clear ();
  }
  return edges;
}


Edge balance_rec (
  Aig &aig_orig, Aig &aig_new, const Edge edge_orig,
  std::unordered_map<Edge, Edge> &balanced_map
  )
{
  assert ( !is_complement ( edge_orig ) );
  const auto &itr = balanced_map.find ( edge_orig );
  if ( itr != balanced_map.end () ) return itr->second;
  assert ( !is_pi ( aig_orig, edge_orig ) );

  const auto &ss = super_gate_edges ( aig_orig, edge_orig );
  if ( ss.empty () )
  {
    balanced_map[edge_orig] = edge0;
    return edge0;
  }

  std::vector<Edge> super_edges; // justify all the inputs to balanced ones
  for ( const auto &e : ss )
  {
    auto e_new = balance_rec ( aig_orig, aig_new, regular (e), balanced_map );
    e_new = is_complement ( e ) ? complement ( e_new ) : e_new;
    super_edges.emplace_back ( e_new );
  }

  auto sort_by_levels = [&] () { // TODO: use incremental sort
    std::sort ( // sort in the descending order
      super_edges.begin (), super_edges.end (),
      [&] ( const Edge &e1, const Edge &e2 ) {
	assert ( e1 != edge_undef );
	assert ( e2 != edge_undef );
	auto &n1 = node_from_id ( &aig_new, to_id ( e1 ) );
	auto &n2 = node_from_id ( &aig_new, to_id ( e2 ) );
	if ( n1.level < n2.level ) return false; // levels updated by construction
	if ( n1.level > n2.level ) return true;
	if ( n1.id < n2.id ) return false;
	if ( n1.id > n2.id ) return true;
	return false;
      }
      );
  };

  assert ( super_edges.size () > 1 );
  
  while ( super_edges.size () > 1 )
  {
    sort_by_levels ();
    update_best_sharing ( aig_new, super_edges );
    const auto &e1 = super_edges.back (); super_edges.pop_back ();
    const auto &e2 = super_edges.back (); super_edges.pop_back ();
    const auto &and_e = add_and ( aig_new, e1, e2 );
    const auto &itf = std::find ( super_edges.begin (), super_edges.end (), and_e );
    if ( itf == super_edges.end () ) super_edges.emplace_back ( and_e );
  }

  const auto &itt = balanced_map.find ( edge_orig );
  assert ( itt == balanced_map.end () );
  auto eee = super_edges[0];
  balanced_map[edge_orig] = eee;
  return eee;
}


Aig balance ( Aig &aig_orig )
{
  auto aig_new = Aig ( aig_orig.name, aig_orig.num_nodes () + 100 );
  update_levels ( aig_orig );
  
  std::unordered_map<Edge, Edge> balanced_map;
  for ( auto &pi : aig_orig.pi_list )
  {
    auto new_pi = add_pi ( aig_new, to_name ( aig_orig, pi ) );
    balanced_map[pi] = new_pi;
  }
  balanced_map[edge0] = edge0;
  balanced_map[edge1] = edge1;
  
  for ( auto &po : aig_orig.po_list )
  {
    auto new_po = balance_rec (
      aig_orig, aig_new, regular ( po.edge ), balanced_map 
      );
    if ( po.is_complement () ) new_po  = complement ( new_po );
    tag_as_po ( aig_new, new_po, po.name );
    balanced_map[po.edge] = new_po;
  }

  // return only the best
  const auto &max_level_orig = get_max_level ( aig_orig );
  const auto &max_level_new = get_max_level ( aig_new );
  const auto &num_nodes_orig = aig_orig.num_nodes ();
  const auto &num_nodes_new = aig_new.num_nodes ();

  if ( max_level_new == max_level_orig - 1 ) // just one level decreased
  {
    if ( num_nodes_new <= num_nodes_orig ) return aig_new;
    else return aig_orig; // else not worth for one point reduction
  }
  if ( max_level_new < max_level_orig ) return aig_new;
  if ( max_level_new == max_level_orig )
  {
    if ( num_nodes_new <= num_nodes_orig ) return aig_new;
    else return aig_orig;
  }

  // Note: if ever we hit the below one, return the one with minimal node count
  // i.e., uncomment the below line
  // if ( num_nodes_new < (num_nodes_orig + 20 ) ) return aig_new;
  assert ( num_nodes_orig >= num_nodes_new ); 
  return aig_orig;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

} // namespace Aig
} // namespace Yise

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
