/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cut.hpp
 * @brief  : Cut enumeration for AIG
 */
//------------------------------------------------------------------------------

#ifndef AIG_CUT_HPP
#define AIG_CUT_HPP

#include <types/aig.hpp>

#include <unordered_set>

namespace Yise
{
namespace Aig
{


// A cut is a unique set of leaf node ids
struct Cut
{
  std::unordered_set<int> leaves;
  int volume = 0;
  int delay = 0;
  float area_flow = 0.0;
};

//------------------------------------------------------------------------------
// Initialize the cut system
inline void init_cuts ( Aig &aig )
{
  update_levels ( aig ); // annotate the levels
  for ( auto &n : aig.node_list ) { n.value = 0; } // reset all node-values
}

// Note: must call init_cuts () before get_cuts ()
Cut get_cut ( Node &node, int k = 6 );

//------------------------------------------------------------------------------
// naive experimental cut manager
struct CutManager
{
  // map of node-id to cut
  std::unordered_map<int, Cut> cut_list;
  Aig *p_aig;
  int k; // k-feasible cut, i.e., max-size
  inline bool has_cut ( int node_id )
  {
    return cut_list.find ( node_id ) != cut_list.end ();
  }
  inline Cut get_cut ( int node_id )
  {
    assert ( has_cut ( node_id ) );
    return cut_list.find ( node_id )->second;
  }
};

// naive simple cut_enumeration
// Note: PI and const nodes do not have cuts ( only trivial cuts )
CutManager enumerate_cuts ( Aig &aig, const int k = 6 );
//------------------------------------------------------------------------------

}
}


#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
