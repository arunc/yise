/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : tt_maps.hpp
 * @brief  : Truth table maps for cut optimization
 */

//------------------------------------------------------------------------------
#ifndef TT_MAPS_AIG_HPP
#define TT_MAPS_AIG_HPP

#include "truth.hpp"

namespace Yise
{
namespace Aig
{

namespace TtMap
{

//------------------------------------------------------------------------------
static const std::unordered_map <Truth_t, unsigned> cut_size_6 =
{

  { 0x0000002b002b2b2b , 0xd0b97530 },    // enc: ~5 ~4 ~3 ~2 ~1 0     stage: 13
  { 0x2b2b002b002b0000 , 0xd0a87530 },    // enc: 5 4 ~3 ~2 ~1 0     stage: 13
  { 0x002b00002b2b002b , 0xd0b87530 },    // enc: ~5 4 ~3 ~2 ~1 0     stage: 13
  { 0x0000001700171717 , 0xd0b97531 },    // enc: ~5 ~4 ~3 ~2 ~1 ~0     stage: 13
  { 0x1717001700170000 , 0xd0a87531 },    // enc: 5 4 ~3 ~2 ~1 ~0     stage: 13
  { 0x0017000017170017 , 0xd0b87531 },    // enc: ~5 4 ~3 ~2 ~1 ~0     stage: 13
  { 0x0000008e008e8e8e , 0xd0b97520 },    // enc: ~5 ~4 ~3 ~2 1 0     stage: 13
  { 0x8e8e008e008e0000 , 0xd0a87520 },    // enc: 5 4 ~3 ~2 1 0     stage: 13
  { 0x008e00008e8e008e , 0xd0b87520 },    // enc: ~5 4 ~3 ~2 1 0     stage: 13
  { 0x00004d004d004d4d , 0xd0b96521 },    // enc: ~5 ~4 3 ~2 1 ~0     stage: 13
  { 0x4d004d4d00004d00 , 0xd0a96521 },    // enc: 5 ~4 3 ~2 1 ~0     stage: 13
  { 0x4d4d4d004d000000 , 0xd0a86521 },    // enc: 5 4 3 ~2 1 ~0     stage: 13
  { 0x0000170017001717 , 0xd0b96531 },    // enc: ~5 ~4 3 ~2 ~1 ~0     stage: 13
  { 0x1717170017000000 , 0xd0a86531 },    // enc: 5 4 3 ~2 ~1 ~0     stage: 13
  { 0x1700171700001700 , 0xd0a96531 },    // enc: 5 ~4 3 ~2 ~1 ~0     stage: 13
  { 0x0000b200b200b2b2 , 0xd0b96430 },    // enc: ~5 ~4 3 2 ~1 0     stage: 13
  { 0xb200b2b20000b200 , 0xd0a96430 },    // enc: 5 ~4 3 2 ~1 0     stage: 13
  { 0xb2b2b200b2000000 , 0xd0a86430 },    // enc: 5 4 3 2 ~1 0     stage: 13
  { 0x0000710071007171 , 0xd0b96431 },    // enc: ~5 ~4 3 2 ~1 ~0     stage: 13
  { 0x7100717100007100 , 0xd0a96431 },    // enc: 5 ~4 3 2 ~1 ~0     stage: 13
  { 0x7171710071000000 , 0xd0a86431 },    // enc: 5 4 3 2 ~1 ~0     stage: 13
  { 0x0000d400d400d4d4 , 0xd0b96421 },    // enc: ~5 ~4 3 2 1 ~0     stage: 13
  { 0xd400d4d40000d400 , 0xd0a96421 },    // enc: 5 ~4 3 2 1 ~0     stage: 13
  { 0xd4d4d400d4000000 , 0xd0a86421 },    // enc: 5 4 3 2 1 ~0     stage: 13
  { 0x0000e800e800e8e8 , 0xd0b96420 },    // enc: ~5 ~4 3 2 1 0     stage: 13
  { 0xe800e8e80000e800 , 0xd0a96420 },    // enc: 5 ~4 3 2 1 0     stage: 13
  { 0xe8e8e800e8000000 , 0xd0a86420 },    // enc: 5 4 3 2 1 0     stage: 13
  { 0x00002b002b002b2b , 0xd0b96530 },    // enc: ~5 ~4 3 ~2 ~1 0     stage: 13
  { 0x2b2b2b002b000000 , 0xd0a86530 },    // enc: 5 4 3 ~2 ~1 0     stage: 13
  { 0x2b002b2b00002b00 , 0xd0a96530 },    // enc: 5 ~4 3 ~2 ~1 0     stage: 13
  { 0x8e008e8e00008e00 , 0xd0a96520 },    // enc: 5 ~4 3 ~2 1 0     stage: 13
  { 0x8e8e8e008e000000 , 0xd0a86520 },    // enc: 5 4 3 ~2 1 0     stage: 13
  { 0x00008e008e008e8e , 0xd0b96520 },    // enc: ~5 ~4 3 ~2 1 0     stage: 13
  { 0x008e8e8e0000008e , 0xd0a97520 },    // enc: 5 ~4 ~3 ~2 1 0     stage: 13
  { 0x004d4d4d0000004d , 0xd0a97521 },    // enc: 5 ~4 ~3 ~2 1 ~0     stage: 13
  { 0x0000007100717171 , 0xd0b97431 },    // enc: ~5 ~4 ~3 2 ~1 ~0     stage: 13
  { 0x7171007100710000 , 0xd0a87431 },    // enc: 5 4 ~3 2 ~1 ~0     stage: 13
  { 0x0071000071710071 , 0xd0b87431 },    // enc: ~5 4 ~3 2 ~1 ~0     stage: 13
  { 0x4d4d004d004d0000 , 0xd0a87521 },    // enc: 5 4 ~3 ~2 1 ~0     stage: 13
  { 0x0000004d004d4d4d , 0xd0b97521 },    // enc: ~5 ~4 ~3 ~2 1 ~0     stage: 13
  { 0x004d00004d4d004d , 0xd0b87521 },    // enc: ~5 4 ~3 ~2 1 ~0     stage: 13
  { 0x0017171700000017 , 0xd0a97531 },    // enc: 5 ~4 ~3 ~2 ~1 ~0     stage: 13
  { 0x00b2b2b2000000b2 , 0xd0a97430 },    // enc: 5 ~4 ~3 2 ~1 0     stage: 13
  { 0xb2b200b200b20000 , 0xd0a87430 },    // enc: 5 4 ~3 2 ~1 0     stage: 13
  { 0x000000b200b2b2b2 , 0xd0b97430 },    // enc: ~5 ~4 ~3 2 ~1 0     stage: 13
  { 0x00b20000b2b200b2 , 0xd0b87430 },    // enc: ~5 4 ~3 2 ~1 0     stage: 13
  { 0xd4d400d400d40000 , 0xd0a87421 },    // enc: 5 4 ~3 2 1 ~0     stage: 13
  { 0x000000d400d4d4d4 , 0xd0b97421 },    // enc: ~5 ~4 ~3 2 1 ~0     stage: 13
  { 0x00d40000d4d400d4 , 0xd0b87421 },    // enc: ~5 4 ~3 2 1 ~0     stage: 13
  { 0x00e8e8e8000000e8 , 0xd0a97420 },    // enc: 5 ~4 ~3 2 1 0     stage: 13
  { 0x00d4d4d4000000d4 , 0xd0a97421 },    // enc: 5 ~4 ~3 2 1 ~0     stage: 13
  { 0xe8e800e800e80000 , 0xd0a87420 },    // enc: 5 4 ~3 2 1 0     stage: 13
  { 0x000000e800e8e8e8 , 0xd0b97420 },    // enc: ~5 ~4 ~3 2 1 0     stage: 13
  { 0x00e80000e8e800e8 , 0xd0b87420 },    // enc: ~5 4 ~3 2 1 0     stage: 13
  { 0x0071717100000071 , 0xd0a97431 },    // enc: 5 ~4 ~3 2 ~1 ~0     stage: 13
  { 0x002b2b2b0000002b , 0xd0a97530 },    // enc: 5 ~4 ~3 ~2 ~1 0     stage: 13
  { 0x8e0000008e8e8e00 , 0xd0b86520 },    // enc: ~5 4 3 ~2 1 0     stage: 13
  { 0x4d0000004d4d4d00 , 0xd0b86521 },    // enc: ~5 4 3 ~2 1 ~0     stage: 13
  { 0x1700000017171700 , 0xd0b86531 },    // enc: ~5 4 3 ~2 ~1 ~0     stage: 13
  { 0x2b0000002b2b2b00 , 0xd0b86530 },    // enc: ~5 4 3 ~2 ~1 0     stage: 13
  { 0xb2000000b2b2b200 , 0xd0b86430 },    // enc: ~5 4 3 2 ~1 0     stage: 13
  { 0x7100000071717100 , 0xd0b86431 },    // enc: ~5 4 3 2 ~1 ~0     stage: 13
  { 0xd4000000d4d4d400 , 0xd0b86421 },    // enc: ~5 4 3 2 1 ~0     stage: 13
  { 0xe8000000e8e8e800 , 0xd0b86420 },    // enc: ~5 4 3 2 1 0     stage: 13
  { 0x4d4d4dff4dffffff , 0xe0b97521 },    // enc: ~5 ~4 ~3 ~2 1 ~0     stage: 14
  { 0xffff4dff4dff4d4d , 0xe0a87521 },    // enc: 5 4 ~3 ~2 1 ~0     stage: 14
  { 0x4dff4d4dffff4dff , 0xe0b87521 },    // enc: ~5 4 ~3 ~2 1 ~0     stage: 14
  { 0x8e8e8eff8effffff , 0xe0b97520 },    // enc: ~5 ~4 ~3 ~2 1 0     stage: 14
  { 0xffff8eff8eff8e8e , 0xe0a87520 },    // enc: 5 4 ~3 ~2 1 0     stage: 14
  { 0x8eff8e8effff8eff , 0xe0b87520 },    // enc: ~5 4 ~3 ~2 1 0     stage: 14
  { 0x4d4dff4dff4dffff , 0xe0b96521 },    // enc: ~5 ~4 3 ~2 1 ~0     stage: 14
  { 0xff4dffff4d4dff4d , 0xe0a96521 },    // enc: 5 ~4 3 ~2 1 ~0     stage: 14
  { 0xffffff4dff4d4d4d , 0xe0a86521 },    // enc: 5 4 3 ~2 1 ~0     stage: 14
  { 0x2b2bff2bff2bffff , 0xe0b96530 },    // enc: ~5 ~4 3 ~2 ~1 0     stage: 14
  { 0xff2bffff2b2bff2b , 0xe0a96530 },    // enc: 5 ~4 3 ~2 ~1 0     stage: 14
  { 0xffffff2bff2b2b2b , 0xe0a86530 },    // enc: 5 4 3 ~2 ~1 0     stage: 14
  { 0xb2b2ffb2ffb2ffff , 0xe0b96430 },    // enc: ~5 ~4 3 2 ~1 0     stage: 14
  { 0xffb2ffffb2b2ffb2 , 0xe0a96430 },    // enc: 5 ~4 3 2 ~1 0     stage: 14
  { 0xffffffb2ffb2b2b2 , 0xe0a86430 },    // enc: 5 4 3 2 ~1 0     stage: 14
  { 0xb2b2b2ffb2ffffff , 0xe0b97430 },    // enc: ~5 ~4 ~3 2 ~1 0     stage: 14
  { 0xffffb2ffb2ffb2b2 , 0xe0a87430 },    // enc: 5 4 ~3 2 ~1 0     stage: 14
  { 0xb2ffb2b2ffffb2ff , 0xe0b87430 },    // enc: ~5 4 ~3 2 ~1 0     stage: 14
  { 0xe8e8ffe8ffe8ffff , 0xe0b96420 },    // enc: ~5 ~4 3 2 1 0     stage: 14
  { 0xffe8ffffe8e8ffe8 , 0xe0a96420 },    // enc: 5 ~4 3 2 1 0     stage: 14
  { 0xffffffe8ffe8e8e8 , 0xe0a86420 },    // enc: 5 4 3 2 1 0     stage: 14
  { 0x171717ff17ffffff , 0xe0b97531 },    // enc: ~5 ~4 ~3 ~2 ~1 ~0     stage: 14
  { 0xffff17ff17ff1717 , 0xe0a87531 },    // enc: 5 4 ~3 ~2 ~1 ~0     stage: 14
  { 0x17ff1717ffff17ff , 0xe0b87531 },    // enc: ~5 4 ~3 ~2 ~1 ~0     stage: 14
  { 0x717171ff71ffffff , 0xe0b97431 },    // enc: ~5 ~4 ~3 2 ~1 ~0     stage: 14
  { 0xffff71ff71ff7171 , 0xe0a87431 },    // enc: 5 4 ~3 2 ~1 ~0     stage: 14
  { 0x71ff7171ffff71ff , 0xe0b87431 },    // enc: ~5 4 ~3 2 ~1 ~0     stage: 14
  { 0xff8effff8e8eff8e , 0xe0a96520 },    // enc: 5 ~4 3 ~2 1 0     stage: 14
  { 0xffffff8eff8e8e8e , 0xe0a86520 },    // enc: 5 4 3 ~2 1 0     stage: 14
  { 0x8e8eff8eff8effff , 0xe0b96520 },    // enc: ~5 ~4 3 ~2 1 0     stage: 14
  { 0xff71ffff7171ff71 , 0xe0a96431 },    // enc: 5 ~4 3 2 ~1 ~0     stage: 14
  { 0x7171ff71ff71ffff , 0xe0b96431 },    // enc: ~5 ~4 3 2 ~1 ~0     stage: 14
  { 0xffffff71ff717171 , 0xe0a86431 },    // enc: 5 4 3 2 ~1 ~0     stage: 14
  { 0x8effffff8e8e8eff , 0xe0a97520 },    // enc: 5 ~4 ~3 ~2 1 0     stage: 14
  { 0xd4d4ffd4ffd4ffff , 0xe0b96421 },    // enc: ~5 ~4 3 2 1 ~0     stage: 14
  { 0xffd4ffffd4d4ffd4 , 0xe0a96421 },    // enc: 5 ~4 3 2 1 ~0     stage: 14
  { 0xffffffd4ffd4d4d4 , 0xe0a86421 },    // enc: 5 4 3 2 1 ~0     stage: 14
  { 0xff17ffff1717ff17 , 0xe0a96531 },    // enc: 5 ~4 3 ~2 ~1 ~0     stage: 14
  { 0xffffff17ff171717 , 0xe0a86531 },    // enc: 5 4 3 ~2 ~1 ~0     stage: 14
  { 0x1717ff17ff17ffff , 0xe0b96531 },    // enc: ~5 ~4 3 ~2 ~1 ~0     stage: 14
  { 0x4dffffff4d4d4dff , 0xe0a97521 },    // enc: 5 ~4 ~3 ~2 1 ~0     stage: 14
  { 0xe8ffffffe8e8e8ff , 0xe0a97420 },    // enc: 5 ~4 ~3 2 1 0     stage: 14
  { 0x2b2b2bff2bffffff , 0xe0b97530 },    // enc: ~5 ~4 ~3 ~2 ~1 0     stage: 14
  { 0xffff2bff2bff2b2b , 0xe0a87530 },    // enc: 5 4 ~3 ~2 ~1 0     stage: 14
  { 0x2bff2b2bffff2bff , 0xe0b87530 },    // enc: ~5 4 ~3 ~2 ~1 0     stage: 14
  { 0x17ffffff171717ff , 0xe0a97531 },    // enc: 5 ~4 ~3 ~2 ~1 ~0     stage: 14
  { 0xffffe8ffe8ffe8e8 , 0xe0a87420 },    // enc: 5 4 ~3 2 1 0     stage: 14
  { 0xe8e8e8ffe8ffffff , 0xe0b97420 },    // enc: ~5 ~4 ~3 2 1 0     stage: 14
  { 0xe8ffe8e8ffffe8ff , 0xe0b87420 },    // enc: ~5 4 ~3 2 1 0     stage: 14
  { 0xd4ffffffd4d4d4ff , 0xe0a97421 },    // enc: 5 ~4 ~3 2 1 ~0     stage: 14
  { 0xffffd4ffd4ffd4d4 , 0xe0a87421 },    // enc: 5 4 ~3 2 1 ~0     stage: 14
  { 0xd4d4d4ffd4ffffff , 0xe0b97421 },    // enc: ~5 ~4 ~3 2 1 ~0     stage: 14
  { 0xd4ffd4d4ffffd4ff , 0xe0b87421 },    // enc: ~5 4 ~3 2 1 ~0     stage: 14
  { 0xb2ffffffb2b2b2ff , 0xe0a97430 },    // enc: 5 ~4 ~3 2 ~1 0     stage: 14
  { 0x71ffffff717171ff , 0xe0a97431 },    // enc: 5 ~4 ~3 2 ~1 ~0     stage: 14
  { 0xffd4d4d4ffffffd4 , 0xe0b86421 },    // enc: ~5 4 3 2 1 ~0     stage: 14
  { 0x2bffffff2b2b2bff , 0xe0a97530 },    // enc: 5 ~4 ~3 ~2 ~1 0     stage: 14
  { 0xff8e8e8effffff8e , 0xe0b86520 },    // enc: ~5 4 3 ~2 1 0     stage: 14
  { 0xff4d4d4dffffff4d , 0xe0b86521 },    // enc: ~5 4 3 ~2 1 ~0     stage: 14
  { 0xff171717ffffff17 , 0xe0b86531 },    // enc: ~5 4 3 ~2 ~1 ~0     stage: 14
  { 0xff2b2b2bffffff2b , 0xe0b86530 },    // enc: ~5 4 3 ~2 ~1 0     stage: 14
  { 0xffb2b2b2ffffffb2 , 0xe0b86430 },    // enc: ~5 4 3 2 ~1 0     stage: 14
  { 0xff717171ffffff71 , 0xe0b86431 },    // enc: ~5 4 3 2 ~1 ~0     stage: 14
  { 0xffe8e8e8ffffffe8 , 0xe0b86420 },    // enc: ~5 4 3 2 1 0     stage: 14
  { 0x2bd4d4d42b2b2bd4 , 0xf0b86421 },    // enc: ~5 4 3 2 1 ~0     stage: 15
  { 0xd42b2b2bd4d4d42b , 0xf0b86530 },    // enc: ~5 4 3 ~2 ~1 0     stage: 15
  { 0x1717e817e817e8e8 , 0xf0b96531 },    // enc: ~5 ~4 3 ~2 ~1 ~0     stage: 15
  { 0x2b2bd42bd42bd4d4 , 0xf0b96530 },    // enc: ~5 ~4 3 ~2 ~1 0     stage: 15
  { 0x8e8e718e718e7171 , 0xf0b96520 },    // enc: ~5 ~4 3 ~2 1 0     stage: 15
  { 0x4d4db24db24db2b2 , 0xf0b96521 },    // enc: ~5 ~4 3 ~2 1 ~0     stage: 15
  { 0x718e8e8e7171718e , 0xf0b86520 },    // enc: ~5 4 3 ~2 1 0     stage: 15
  { 0xd4d42bd42bd42b2b , 0xf0b96421 },    // enc: ~5 ~4 3 2 1 ~0     stage: 15
  { 0xe8e817e817e81717 , 0xf0b96420 },    // enc: ~5 ~4 3 2 1 0     stage: 15
  { 0x17e8e8e8171717e8 , 0xf0b86420 },    // enc: ~5 4 3 2 1 0     stage: 15
  { 0x4d4d4db24db2b2b2 , 0xf0b97521 },    // enc: ~5 ~4 ~3 ~2 1 ~0     stage: 15
  { 0xb24d4d4db2b2b24d , 0xf0b86521 },    // enc: ~5 4 3 ~2 1 ~0     stage: 15
  { 0x8e8e8e718e717171 , 0xf0b97520 },    // enc: ~5 ~4 ~3 ~2 1 0     stage: 15
  { 0x2b2b2bd42bd4d4d4 , 0xf0b97530 },    // enc: ~5 ~4 ~3 ~2 ~1 0     stage: 15
  { 0x171717e817e8e8e8 , 0xf0b97531 },    // enc: ~5 ~4 ~3 ~2 ~1 ~0     stage: 15
  { 0xe8e8e817e8171717 , 0xf0b97420 },    // enc: ~5 ~4 ~3 2 1 0     stage: 15
  { 0xd4d4d42bd42b2b2b , 0xf0b97421 },    // enc: ~5 ~4 ~3 2 1 ~0     stage: 15
  { 0x7171718e718e8e8e , 0xf0b97431 },    // enc: ~5 ~4 ~3 2 ~1 ~0     stage: 15
  { 0xe8171717e8e8e817 , 0xf0b86531 },    // enc: ~5 4 3 ~2 ~1 ~0     stage: 15
  { 0xb2b2b24db24d4d4d , 0xf0b97430 },    // enc: ~5 ~4 ~3 2 ~1 0     stage: 15
  { 0x71718e718e718e8e , 0xf0b96431 },    // enc: ~5 ~4 3 2 ~1 ~0     stage: 15
  { 0xb2b24db24db24d4d , 0xf0b96430 },    // enc: ~5 ~4 3 2 ~1 0     stage: 15
  { 0x4db2b2b24d4d4db2 , 0xf0b86430 },    // enc: ~5 4 3 2 ~1 0     stage: 15
  { 0x8e7171718e8e8e71 , 0xf0b86431 },    // enc: ~5 4 3 2 ~1 ~0     stage: 15
  { 0x718e71718e8e718e , 0xf0b87431 },    // enc: ~5 4 ~3 2 ~1 ~0     stage: 15
  { 0xb24db2b24d4db24d , 0xf0b87430 },    // enc: ~5 4 ~3 2 ~1 0     stage: 15
  { 0xe817e8e81717e817 , 0xf0b87420 },    // enc: ~5 4 ~3 2 1 0     stage: 15
  { 0xd42bd4d42b2bd42b , 0xf0b87421 },    // enc: ~5 4 ~3 2 1 ~0     stage: 15
  { 0x4db24d4db2b24db2 , 0xf0b87521 },    // enc: ~5 4 ~3 ~2 1 ~0     stage: 15
  { 0x8e718e8e71718e71 , 0xf0b87520 },    // enc: ~5 4 ~3 ~2 1 0     stage: 15
  { 0x2bd42b2bd4d42bd4 , 0xf0b87530 },    // enc: ~5 4 ~3 ~2 ~1 0     stage: 15
  { 0x17e81717e8e817e8 , 0xf0b87531 }     // enc: ~5 4 ~3 ~2 ~1 ~0     stage: 15

  
};
//------------------------------------------------------------------------------
static const std::unordered_map <Truth_t, unsigned> cut_size_3 =
{

  // a | ( c & b )
  { 0xbabababababababa , 0x20eee034 },    // enc: 7 7 7   0   ~1   2    stage: 2
  { 0x5757575757575757 , 0x20eee135 },    // enc: 7 7 7  ~0   ~1  ~2    stage: 2
  { 0x7575757575757575 , 0x20eee134 },    // enc: 7 7 7  ~0   ~1   2    stage: 2
  { 0xd5d5d5d5d5d5d5d5 , 0x20eee124 },    // enc: 7 7 7  ~0    1   2    stage: 2
  { 0xeaeaeaeaeaeaeaea , 0x20eee024 },    // enc: 7 7 7   0    1   2    stage: 2
  { 0xabababababababab , 0x20eee035 },    // enc: 7 7 7   0   ~1  ~2    stage: 2
  { 0x5d5d5d5d5d5d5d5d , 0x20eee125 },    // enc: 7 7 7  ~0    1  ~2    stage: 2
  { 0xaeaeaeaeaeaeaeae , 0x20eee025 },    // enc: 7 7 7   0    1  ~2    stage: 2

  // b | ( c & a )
  { 0xb3b3b3b3b3b3b3b3 , 0x20eee340 },    // enc: 7 7 7 ~1  2  0     stage: 2
  { 0x7373737373737373 , 0x20eee341 },    // enc: 7 7 7 ~1  2 ~0     stage: 2
  { 0xecececececececec , 0x20eee240 },    // enc: 7 7 7  1  2  0     stage: 2
  { 0xdcdcdcdcdcdcdcdc , 0x20eee241 },    // enc: 7 7 7  1  2 ~0     stage: 2
  { 0x3737373737373737 , 0x20eee351 },    // enc: 7 7 7 ~1 ~2 ~0     stage: 2
  { 0x3b3b3b3b3b3b3b3b , 0x20eee350 },    // enc: 7 7 7 ~1 ~2  0     stage: 2
  { 0xcdcdcdcdcdcdcdcd , 0x20eee251 },    // enc: 7 7 7  1 ~2 ~0     stage: 2
  { 0xcececececececece , 0x20eee250 },    // enc: 7 7 7  1 ~2  0     stage: 2

  // a ^ ( c & b )
  { 0x9a9a9a9a9a9a9a9a , 0x30eee034 },    // enc: 7 7 7  0 ~1  2     stage: 3
  { 0x9595959595959595 , 0x30eee124 },    // enc: 7 7 7 ~0  1  2     stage: 3
  { 0x6565656565656565 , 0x30eee134 },    // enc: 7 7 7 ~0 ~1  2     stage: 3
  { 0x6a6a6a6a6a6a6a6a , 0x30eee024 },    // enc: 7 7 7  0  1  2     stage: 3
  { 0xa9a9a9a9a9a9a9a9 , 0x30eee035 },    // enc: 7 7 7  0 ~1 ~2     stage: 3
  { 0x5656565656565656 , 0x30eee135 },    // enc: 7 7 7 ~0 ~1 ~2     stage: 3
  { 0x5959595959595959 , 0x30eee125 },    // enc: 7 7 7 ~0  1 ~2     stage: 3
  { 0xa6a6a6a6a6a6a6a6 , 0x30eee025 },    // enc: 7 7 7  0  1 ~2     stage: 3

  // b ^ ( c & a )
  { 0x9393939393939393 , 0x30eee340 },    // enc: 7 7 7 ~1  2  0     stage: 3
  { 0x6363636363636363 , 0x30eee341 },    // enc: 7 7 7 ~1  2 ~0     stage: 3
  { 0xc6c6c6c6c6c6c6c6 , 0x30eee250 },    // enc: 7 7 7  1 ~2  0     stage: 3
  { 0x9c9c9c9c9c9c9c9c , 0x30eee241 },    // enc: 7 7 7  1  2 ~0     stage: 3
  { 0x3939393939393939 , 0x30eee350 },    // enc: 7 7 7 ~1 ~2  0     stage: 3
  { 0x6c6c6c6c6c6c6c6c , 0x30eee240 },    // enc: 7 7 7  1  2  0     stage: 3
  { 0x3636363636363636 , 0x30eee351 },    // enc: 7 7 7 ~1 ~2 ~0     stage: 3
  { 0xc9c9c9c9c9c9c9c9 , 0x30eee251 },    // enc: 7 7 7  1 ~2 ~0     stage: 3
  
  // a & ( c | b )
  { 0xa2a2a2a2a2a2a2a2 , 0x40eee034 },    // enc: 7 7 7   0  ~1   2    stage: 4
  { 0x5151515151515151 , 0x40eee134 },    // enc: 7 7 7  ~0  ~1   2    stage: 4
  { 0x5454545454545454 , 0x40eee124 },    // enc: 7 7 7  ~0   1   2    stage: 4
  { 0xa8a8a8a8a8a8a8a8 , 0x40eee024 },    // enc: 7 7 7   0   1   2    stage: 4
  { 0x4545454545454545 , 0x40eee125 },    // enc: 7 7 7  ~0   1  ~2    stage: 4
  { 0x2a2a2a2a2a2a2a2a , 0x40eee035 },    // enc: 7 7 7   0  ~1  ~2    stage: 4
  { 0x1515151515151515 , 0x40eee135 },    // enc: 7 7 7  ~0  ~1  ~2    stage: 4
  { 0x8a8a8a8a8a8a8a8a , 0x40eee025 },    // enc: 7 7 7   0   1  ~2    stage: 4

  // b & ( c | a )
  { 0x3131313131313131 , 0x40eee341 },    // enc: 7 7 7 ~1  2 ~0     stage: 4
  { 0xc4c4c4c4c4c4c4c4 , 0x40eee241 },    // enc: 7 7 7  1  2 ~0     stage: 4
  { 0x3232323232323232 , 0x40eee340 },    // enc: 7 7 7 ~1  2  0     stage: 4
  { 0xc8c8c8c8c8c8c8c8 , 0x40eee240 },    // enc: 7 7 7  1  2  0     stage: 4
  { 0x2323232323232323 , 0x40eee350 },    // enc: 7 7 7 ~1 ~2  0     stage: 4
  { 0x1313131313131313 , 0x40eee351 },    // enc: 7 7 7 ~1 ~2 ~0     stage: 4
  { 0x8c8c8c8c8c8c8c8c , 0x40eee250 },    // enc: 7 7 7  1 ~2  0     stage: 4
  { 0x4c4c4c4c4c4c4c4c , 0x40eee251 },    // enc: 7 7 7  1 ~2 ~0     stage: 4

  
  // a ^ ( c | b ) = repetition of ( a ^ ( b & c ) )
  // b ^ ( c | a ) = repetition of ( b ^ ( b & a ) )
  
  { 0xb2b2b2b2b2b2b2b2 , 0x00eee430 },    // enc: 7 7 7 2 ~1 0     stage: 0
  { 0x7171717171717171 , 0x00eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 0
  { 0xd4d4d4d4d4d4d4d4 , 0x00eee421 },    // enc: 7 7 7 2 1 ~0     stage: 0
  { 0xe8e8e8e8e8e8e8e8 , 0x00eee420 },    // enc: 7 7 7 2 1 0     stage: 0
  { 0x1717171717171717 , 0x00eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 0
  { 0x2b2b2b2b2b2b2b2b , 0x00eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 0
  { 0x8e8e8e8e8e8e8e8e , 0x00eee520 },    // enc: 7 7 7 ~2 1 0     stage: 0
  { 0x4d4d4d4d4d4d4d4d , 0x00eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 0
  { 0x2020202020202020 , 0x10eee430 },    // enc: 7 7 7 2 ~1 0     stage: 1
  { 0x1010101010101010 , 0x10eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 1
  { 0x4040404040404040 , 0x10eee421 },    // enc: 7 7 7 2 1 ~0     stage: 1
  { 0x8080808080808080 , 0x10eee420 },    // enc: 7 7 7 2 1 0     stage: 1
  { 0x0202020202020202 , 0x10eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 1
  { 0x0101010101010101 , 0x10eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 1
  { 0x0404040404040404 , 0x10eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 1
  { 0x0808080808080808 , 0x10eee520 },    // enc: 7 7 7 ~2 1 0     stage: 1
  { 0xf2f2f2f2f2f2f2f2 , 0x20eee430 },    // enc: 7 7 7 2 ~1 0     stage: 2
  { 0x8f8f8f8f8f8f8f8f , 0x20eee520 },    // enc: 7 7 7 ~2 1 0     stage: 2
  { 0xf1f1f1f1f1f1f1f1 , 0x20eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 2
  { 0xf4f4f4f4f4f4f4f4 , 0x20eee421 },    // enc: 7 7 7 2 1 ~0     stage: 2
  { 0x4f4f4f4f4f4f4f4f , 0x20eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 2
  { 0xf8f8f8f8f8f8f8f8 , 0x20eee420 },    // enc: 7 7 7 2 1 0     stage: 2
  { 0x2f2f2f2f2f2f2f2f , 0x20eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 2
  { 0x1f1f1f1f1f1f1f1f , 0x20eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 2
  { 0xd2d2d2d2d2d2d2d2 , 0x30eee430 },    // enc: 7 7 7 2 ~1 0     stage: 3
  { 0x2d2d2d2d2d2d2d2d , 0x30eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 3
  { 0xe1e1e1e1e1e1e1e1 , 0x30eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 3
  { 0xb4b4b4b4b4b4b4b4 , 0x30eee421 },    // enc: 7 7 7 2 1 ~0     stage: 3
  { 0x7878787878787878 , 0x30eee420 },    // enc: 7 7 7 2 1 0     stage: 3
  { 0x1e1e1e1e1e1e1e1e , 0x30eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 3
  { 0x4b4b4b4b4b4b4b4b , 0x30eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 3
  { 0x8787878787878787 , 0x30eee520 },    // enc: 7 7 7 ~2 1 0     stage: 3
  { 0xb0b0b0b0b0b0b0b0 , 0x40eee430 },    // enc: 7 7 7 2 ~1 0     stage: 4
  { 0x0b0b0b0b0b0b0b0b , 0x40eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 4
  { 0x7070707070707070 , 0x40eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 4
  { 0x0d0d0d0d0d0d0d0d , 0x40eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 4
  { 0xd0d0d0d0d0d0d0d0 , 0x40eee421 },    // enc: 7 7 7 2 1 ~0     stage: 4
  { 0xe0e0e0e0e0e0e0e0 , 0x40eee420 },    // enc: 7 7 7 2 1 0     stage: 4
  { 0x0707070707070707 , 0x40eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 4
  { 0x0e0e0e0e0e0e0e0e , 0x40eee520 },    // enc: 7 7 7 ~2 1 0     stage: 4
  { 0xfbfbfbfbfbfbfbfb , 0x50eee430 },    // enc: 7 7 7 2 ~1 0     stage: 5
  { 0xf7f7f7f7f7f7f7f7 , 0x50eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 5
  { 0xfdfdfdfdfdfdfdfd , 0x50eee421 },    // enc: 7 7 7 2 1 ~0     stage: 5
  { 0xfefefefefefefefe , 0x50eee420 },    // enc: 7 7 7 2 1 0     stage: 5
  { 0xbfbfbfbfbfbfbfbf , 0x50eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 5
  { 0x7f7f7f7f7f7f7f7f , 0x50eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 5
  { 0xdfdfdfdfdfdfdfdf , 0x50eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 5
  { 0xefefefefefefefef , 0x50eee520 },    // enc: 7 7 7 ~2 1 0     stage: 5
  { 0x4b4b4b4b4b4b4b4b , 0x60eee430 },    // enc: 7 7 7 2 ~1 0     stage: 6
  { 0x8787878787878787 , 0x60eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 6
  { 0x2d2d2d2d2d2d2d2d , 0x60eee421 },    // enc: 7 7 7 2 1 ~0     stage: 6
  { 0xd2d2d2d2d2d2d2d2 , 0x60eee521 },    // enc: 7 7 7 ~2 1 ~0     stage: 6
  { 0x1e1e1e1e1e1e1e1e , 0x60eee420 },    // enc: 7 7 7 2 1 0     stage: 6
  { 0xb4b4b4b4b4b4b4b4 , 0x60eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 6
  { 0x7878787878787878 , 0x60eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 6
  { 0xe1e1e1e1e1e1e1e1 , 0x60eee520 }     // enc: 7 7 7 ~2 1 0     stage: 6

  
};


// for the adder, stage information is for bit size
// stage0 = 1-bit adder (a, b, cin)
static const std::unordered_map <Truth_t, unsigned> sum_tt =
{

  //----------------------------------------------------------------------------
  /*
    3-input sum encoding ( XOR gate )
    p = Maj ( in[0], in[1], in[2] );
    q = Maj ( in[0], in[1], complement ( in[2] ) );
    out = Maj ( complement ( p ), in[2], q )
  */
  
 { 0x9696969696969696 , 0x00eee420 },    // enc: 7 7 7 2 1 0     stage: 0
 { 0x6969696969696969 , 0x00eee421 }     // enc: 7 7 7 2 1 ~0     stage: 0
 //----------------------------------------------------------------------------
};

/*
  3-input carry encoding ( simple Majority )
  q = Maj ( in[0], in[1], in[2] );
 */
static const std::unordered_map <Truth_t, unsigned> carry_tt =
{

 { 0xb2b2b2b2b2b2b2b2 , 0x00eee430 },    // enc: 7 7 7 2 ~1 0     stage: 0
 { 0x7171717171717171 , 0x00eee431 },    // enc: 7 7 7 2 ~1 ~0     stage: 0
 { 0xd4d4d4d4d4d4d4d4 , 0x00eee421 },    // enc: 7 7 7 2 1 ~0     stage: 0
 { 0xe8e8e8e8e8e8e8e8 , 0x00eee420 },    // enc: 7 7 7 2 1 0     stage: 0
 { 0x1717171717171717 , 0x00eee531 },    // enc: 7 7 7 ~2 ~1 ~0     stage: 0
 { 0x2b2b2b2b2b2b2b2b , 0x00eee530 },    // enc: 7 7 7 ~2 ~1 0     stage: 0
 { 0x8e8e8e8e8e8e8e8e , 0x00eee520 },    // enc: 7 7 7 ~2 1 0     stage: 0
 { 0x4d4d4d4d4d4d4d4d , 0x00eee521 }     // enc: 7 7 7 ~2 1 ~0     stage: 0
 
};


//------------------------------------------------------------------------------
}
}
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
