/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : truth.cpp
 * @brief  : Truth table manipulations for cut
 */

//------------------------------------------------------------------------------
#ifndef TRUTH_AIG_HPP
#define TRUTH_AIG_HPP

#include <types/aig.hpp>

#include "cut.hpp"

namespace Yise
{
namespace Aig
{
using Truth_t = unsigned long;

const Truth_t TtVarConst0 = 0x0000000000000000;
const Truth_t TtVarConst1 = 0xFFFFFFFFFFFFFFFF;
const std::array<Truth_t, 6> TtVariables = { // word: {b5 b4 b3 b2 b1 b0}
  0xAAAAAAAAAAAAAAAA, // b0
  0xCCCCCCCCCCCCCCCC, // b1
  0xF0F0F0F0F0F0F0F0, // b2
  0xFF00FF00FF00FF00, // b3
  0xFFFF0000FFFF0000, // b4
  0xFFFFFFFF00000000  // b5 
};

Truth_t compute_cut_tt ( const Node &node, const Cut &cut );

// Notes:
// leaves are the node-ids, does not compute using edges
// compute_tt is for output Node, in terms of input Node leaves
// negation in both output edge (for e.g., a PO ) and input edges are ignored.
Truth_t compute_tt ( const Node &node, const std::vector<int> &leaves );

inline Truth_t compute_tt ( // takes care of the edge negation
  const Aig &aig, Edge edge, const std::vector<int> &leaves
  )
{
  auto tt = compute_tt ( to_node ( aig, edge ), leaves );
  if ( is_complement ( edge ) ) tt = ~tt;
  return tt;
}

// additionally takes a list of leaf-nodes whose values are consts
Truth_t compute_const_tt (
  const Node &node, const std::vector<int> &leaves,
  const std::unordered_map<int, int> &const_leaves
  );


}
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
