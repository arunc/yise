/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : truth.cpp
 * @brief  : truth table for local cut.
 */

//------------------------------------------------------------------------------

#include "truth.hpp"

#include <types/common.hpp>

#include <sstream>

namespace Yise
{
namespace Aig
{
std::string to_id_str ( const std::vector<int> &leaves )
{
  std::stringstream str;
  auto perm = leaves;
  std::reverse ( perm.begin (), perm.end () );
  for ( const auto &e : perm )
  {
    str << e << " ";  
  }
  return str.str ();

}

Truth_t compute_tt_rec ( const Node &node, std::unordered_map<int, Truth_t> &tt_map )
{
  auto itr = tt_map.find ( node.id );
  if ( itr != tt_map.end () ) return itr->second;
  assert ( !is_pi ( node ) ); // either should be a leaf or not part of the cut
  assert ( !is_const ( node ) );
  
  auto tt0 = compute_tt_rec ( to_node0 ( node ), tt_map );
  auto tt1 = compute_tt_rec ( to_node1 ( node ), tt_map );
  const auto &fanins = node.fanins;
  tt0 = is_complement ( fanins[0] ) ? (~tt0) : tt0;
  tt1 = is_complement ( fanins[1] ) ? (~tt1) : tt1;
  auto tt = tt0 & tt1;

  tt_map[node.id] = std::move ( tt );
  return tt_map[node.id];
}

// compute_tt is for output Node, in terms of input Node leaves
// negation in both output edge (for e.g., a PO ) and input edges are ignored.
Truth_t compute_tt ( const Node &node, const std::vector<int> &leaves )
{
  assert ( leaves.size () < 7 );
  assert ( !leaves.empty () );
  std::unordered_map<int, Truth_t> tt_map;
  tt_map.reserve ( 20 );
  tt_map[0] = TtVarConst1;
  for ( auto i = 0; i < leaves.size (); i++ )
  {
    tt_map[leaves[i]] =  TtVariables[i];
  }
  compute_tt_rec  ( node, tt_map );
  auto itr = tt_map.find ( node.id );
  assert ( itr != tt_map.end () );
  return itr->second;
}

Truth_t compute_cut_tt ( const Node &node, const Cut &cut )
{
  assert ( !is_pi ( node ) );
  assert ( !is_const ( node ) );
  assert ( cut.leaves.size () < 7 );
  assert ( !cut.leaves.empty () );
  std::vector<int> leaves ( cut.leaves.begin (), cut.leaves.end () );
  std::sort ( leaves.begin (), leaves.end () );
  return compute_tt ( node, leaves );
}

// additionally takes a list of leaf-nodes whose values are consts
Truth_t compute_const_tt (
  const Node &node, const std::vector<int> &leaves,
  const std::unordered_map<int, int> &const_leaves
  )
{
  assert ( leaves.size () < 7 );
  assert ( !leaves.empty () );
  std::unordered_map<int, Truth_t> tt_map;
  tt_map.reserve ( 20 );
  tt_map[0] = TtVarConst1;
  for ( auto i = 0; i < leaves.size (); i++ )
  {
    auto itr = const_leaves.find ( leaves[i] );
    if ( itr != const_leaves.end () )
    {
      assert ( itr->second == 0 || itr->second == 1 );
      const auto &val = itr->second == 0 ? TtVarConst0 : TtVarConst1;
      tt_map[leaves[i]] = val;
    }
    else
    {
      tt_map[leaves[i]] =  TtVariables[i];
    }
  }
  compute_tt_rec  ( node, tt_map );
  auto itr = tt_map.find ( node.id );
  assert ( itr != tt_map.end () );
  return itr->second;
}



}
} // namespace Yise

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
