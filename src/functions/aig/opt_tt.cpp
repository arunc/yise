/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : opt_tt.cpp
 * @brief  : TT based optimization using cuts
 */

//------------------------------------------------------------------------------

#include "opt.hpp"

#include <functions/aig/tt_maps.hpp>
#include <functions/aig/cut.hpp>
#include <functions/aig/npn.hpp>
#include <types/common.hpp>

namespace Yise
{
namespace Aig
{

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node_from_id ( Aig *p_aig, int id )
{
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}

namespace
{
int __tt__6 = 0;
int __tt__3 = 0;
}

inline unsigned cut_to_enc ( const Cut &cut, const Truth_t &tt )
{
  if ( cut.leaves.size () == 6 )
  {
    auto itr = TtMap::cut_size_6.find ( tt );
    if ( itr != TtMap::cut_size_6.end () ) {__tt__6++; return itr->second;}
  }
  else if ( cut.leaves.size () == 3 )
  {
    auto itr = TtMap::cut_size_3.find ( tt );
    if ( itr != TtMap::cut_size_3.end () ) {__tt__3++; return itr->second;}
  }
  return 0u;
}




Edge add_enc_node (
  Aig &new_aig, const std::vector<Edge> &fanins,
  unsigned enc
  )
{

  // 2. encode the inputs, fanins to enc_fanins
  const auto xxx = edge_undef;
  std::vector<Edge> enc_fanins = {xxx, xxx, xxx, xxx, xxx, xxx};
  for ( auto i = 0; i < fanins.size (); i++ )
  {
    auto edge = fanins [ leaf_permutation ( enc, i ) ];
    enc_fanins[i] = leaf_negation ( enc, i ) ? complement ( edge ) : edge;
  }

  // 3. encode the stages and form the AIG
  Edge aig_e;
  const auto &stage = decode_stage ( enc );
  if ( stage < 7 ) assert ( fanins.size () == 3 ); // check all schemes in sync
  if ( stage > 12 ) assert ( fanins.size () == 6 ); // check all schemes in sync
  assert ( stage < 7 || stage > 12 );

  if ( stage == 0 )
  {
    aig_e = add_maj ( new_aig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
  }
  else if ( stage == 1 )
  {
    auto and_e = add_and ( new_aig, enc_fanins[0], enc_fanins[1] );
    aig_e = add_and ( new_aig, and_e, enc_fanins[2] );
  }
  else if ( stage == 2 )
  {
    auto and_e = add_and ( new_aig, enc_fanins[0], enc_fanins[1] );
    aig_e = add_or ( new_aig, and_e, enc_fanins[2] );
  }
  else if ( stage == 3 )
  {
    auto and_e = add_and ( new_aig, enc_fanins[0], enc_fanins[1] );
    aig_e = add_xor ( new_aig, and_e, enc_fanins[2] );
  }
  else if ( stage == 4 )
  {
    auto and_e = add_or ( new_aig, enc_fanins[0], enc_fanins[1] );
    aig_e = add_and ( new_aig, and_e, enc_fanins[2] );
  }
  else if ( stage == 5 )
  {
    auto and_e = add_or ( new_aig, enc_fanins[0], enc_fanins[1] );
    aig_e = add_or ( new_aig, and_e, enc_fanins[2] );
  }
  else if ( stage == 6 )
  {
    auto and_e = add_or ( new_aig, enc_fanins[0], enc_fanins[1] );
    aig_e = add_xor ( new_aig, and_e, enc_fanins[2] );
  }
  else if ( stage == 13 )
  {
    auto m1 = add_maj ( new_aig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto m2 = add_maj ( new_aig, enc_fanins[3], enc_fanins[4], enc_fanins[5] );
    aig_e = add_and ( new_aig, m1, m2 );
  }
  else if ( stage == 14 )
  {
    auto m1 = add_maj ( new_aig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto m2 = add_maj ( new_aig, enc_fanins[3], enc_fanins[4], enc_fanins[5] );
    aig_e = add_or ( new_aig, m1, m2 );
  }
  else if ( stage == 15 )
  {
    auto m1 = add_maj ( new_aig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto m2 = add_maj ( new_aig, enc_fanins[3], enc_fanins[4], enc_fanins[5] );
    aig_e = add_xor ( new_aig, m1, m2 );
  }
  else
  {
    assert ( false && "Unknown stage " );
  }

  return aig_e;
}


Edge add_opt_node (
  Node &orig_node,
  Aig &new_aig,
  std::unordered_map<int, int> &aig_to_aig
  )
{
  auto itr = aig_to_aig.find ( to_edge ( orig_node.id ) );
  if ( itr != aig_to_aig.end () ) return itr->second;

  // 1. find if a matching cut encoding is available
  const auto &cut = get_cut ( orig_node, 6 );
  const auto &tt = compute_cut_tt ( orig_node, cut );
  unsigned enc = cut_to_enc ( cut, tt );
  Edge aig_e;
  
  if ( enc == 0 ) // no matching encoding
  {
    auto f0 = add_opt_node ( node0 ( orig_node ), new_aig, aig_to_aig );
    auto f1 = add_opt_node ( node1 ( orig_node ), new_aig, aig_to_aig );
    if ( is_complement ( orig_node.fanins[0] ) ) f0 = complement ( f0 );
    if ( is_complement ( orig_node.fanins[1] ) ) f1 = complement ( f1 );
    
    aig_e = add_and ( new_aig, f0, f1 );
  }
  else
  {
    std::vector<int> leaves ( cut.leaves.begin (), cut.leaves.end () );
    std::sort ( leaves.begin (), leaves.end () ); // fix an order
    std::vector<Edge> fanins;
    // 1. justify all the leaves, leaves to fanins
    for ( const auto &l : leaves )
    {
      auto &leaf_node = node_from_id ( orig_node.p_aig, l ); // leaves are node ids
      fanins.emplace_back ( add_opt_node ( leaf_node, new_aig, aig_to_aig ) );
    }
    aig_e = add_enc_node ( new_aig, fanins, enc );
  }

  aig_to_aig [ to_edge ( orig_node.id )] = aig_e;
  aig_to_aig [ complement ( to_edge ( orig_node.id ) )] = complement ( aig_e );
  return aig_e;
}


Aig opt_tt ( Aig &orig_aig )
{
  assert ( orig_aig.good () );
  Aig new_aig ( orig_aig.name, orig_aig.num_nodes () );
  std::unordered_map<int, int> aig_to_aig;
  aig_to_aig.reserve ( ( new_aig.num_nodes () << 1 ) + 100 );
  aig_to_aig[edge0] = edge0;
  aig_to_aig[edge1] = edge1;
  aig_to_aig[edge_undef] = edge_undef;

  for ( const auto &pi : orig_aig.pi_list )
  {
    auto aig_pi = add_pi ( new_aig, to_name ( orig_aig, pi ) );
    aig_to_aig[pi] = aig_pi;
    aig_to_aig[complement ( pi )] = complement ( aig_pi );
  }

  for ( const auto &po : orig_aig.po_list )
  {
    auto &n = orig_aig.node_list[ po.node_id () ] ;
    auto aig_po = add_opt_node ( n, new_aig, aig_to_aig );
    if ( po.is_complement () ) aig_po = complement ( aig_po );
    tag_as_po ( new_aig, aig_po, po.name );
  }

  //std::cout << "6TT hit: " << __tt__6 << " 3TT hit:" << __tt__3 << "\n";
  new_aig = strash ( new_aig );
  return new_aig;
}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

} // namespace Aig
} // namespace Yise

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
