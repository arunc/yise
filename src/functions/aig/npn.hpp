/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : npn.hpp
 * @brief  : NPN computations 
 */

//------------------------------------------------------------------------------
#ifndef NPN_AIG_HPP
#define NPN_AIG_HPP

#include <types/aig.hpp>

#include <functions/aig/truth.hpp>

namespace Yise
{
namespace Aig
{

// Computes the NPN class for a given truth table. num_vars <= 6 only
Truth_t compute_npn ( const Truth_t &tt, const int num_vars );


//------------------------------------------------------------------------------
/*
  There are 2 levels of encoding.
  1. input variable encoding
  2. number of stage encoding

 */
//------------------------------------------------------------------------------
// Level 1 encoding
// Input var encoding for TT permuations and negations
const unsigned TT_ENC_CONST1 = 0xC;
const unsigned TT_ENC_CONST0 = 0xD;
const unsigned TT_ENC_DONTCARE = 0xE;

// TT is mapped to an input encoding scheme, represented by an integer.
// Last 24 bits of this integer is used to represent different permutations
// of input edges a, b, c, d, e and f.
// Each edge uses 4 bits in the order. a uses b0-b3, b uses b4-b7 etc
//
// Meaning of first 4 bits (LSB) is as follows
// 0000 => a = in0, 0001 => a = ~in0
// 0010 => a = in1, 0011 => a = ~in1
// 0100 => a = in2, 0101 => a = ~in2
// 0110 => a = in3, 0111 => a = ~in3
// 1000 => a = in4, 1001 => a = ~in4
// 1010 => a = in5, 1011 => a = ~in5
// 1100 => a =   1, 1101 => a = 0 (~1 = 0)
// 1110 => a =   X, 1111 => a = ~X = X ( don't care )
// similarly next 4 bits for b and so on.
//
// These permutations are retrived using a_input() to f_input() functions.
// negations using a_flipped() to f_flipped() functions.
//
// clang-format off

// a, b, c, d, e and f are the edges ( i.e., with negation )
inline unsigned encode_edges (
  unsigned a, unsigned b, unsigned c, unsigned d, unsigned e, unsigned f
  )
{
  assert ( a <= 0xE && a >= 0 ); assert ( b <= 0xE && b >= 0 );
  assert ( c <= 0xE && c >= 0 ); assert ( d <= 0xE && d >= 0 );
  assert ( e <= 0xE && e >= 0 ); assert ( f <= 0xE && f >= 0 );
  return ( a | ( b << 4 ) | ( c << 8 ) | ( d << 12 ) | ( e << 16 ) | ( f << 20 ) );
}

inline unsigned encode_edges ( const std::vector<unsigned> &_edges )
{
  assert ( _edges.size () < 7 );
  std::vector<unsigned> edges = {0xE, 0xE, 0xE, 0xE, 0xE, 0xE}; // don't cares
  for ( auto i = 0; i < _edges.size (); i++ )
  {
    edges[i] = _edges[i];
  }
  return encode_edges ( edges[0], edges[1], edges[2], edges[3], edges[4], edges[5] );
}


// TODO: next version, use char
inline unsigned a_pos ( const unsigned enc ) { return ( (enc & 0xE ) >> 1 ); }
inline unsigned b_pos ( const unsigned enc ) { return ( (enc & 0xE0 ) >> 5 ); }
inline unsigned c_pos ( const unsigned enc ) { return ( (enc & 0xE00 ) >> 9 ); }
inline unsigned d_pos ( const unsigned enc ) { return ( (enc & 0xE000 ) >> 13 ); }
inline unsigned e_pos ( const unsigned enc ) { return ( (enc & 0xE0000 ) >> 17 ); }
inline unsigned f_pos ( const unsigned enc ) { return ( (enc & 0xE00000 ) >> 21 ); }

inline bool a_flipped ( const unsigned enc ) { return ( enc & 1); }
inline bool b_flipped ( const unsigned enc ) { return ( ( enc >> 4)  & 1); }
inline bool c_flipped ( const unsigned enc ) { return ( ( enc >> 8)  & 1); }
inline bool d_flipped ( const unsigned enc ) { return ( ( enc >> 12) & 1); }
inline bool e_flipped ( const unsigned enc ) { return ( ( enc >> 16) & 1); }
inline bool f_flipped ( const unsigned enc ) { return ( ( enc >> 20) & 1); }

inline unsigned leaf_permutation ( const unsigned enc, const unsigned pos )
{
  assert ( pos < 6 );
  if ( pos == 0 ) return a_pos ( enc );
  if ( pos == 1 ) return b_pos ( enc );
  if ( pos == 2 ) return c_pos ( enc );
  if ( pos == 3 ) return d_pos ( enc );
  if ( pos == 4 ) return e_pos ( enc );
  return f_pos ( enc );
}

inline unsigned leaf_negation ( const unsigned enc, const unsigned pos )
{
  assert ( pos < 6 );
  if ( pos == 0 ) return a_flipped ( enc );
  if ( pos == 1 ) return b_flipped ( enc );
  if ( pos == 2 ) return c_flipped ( enc );
  if ( pos == 3 ) return d_flipped ( enc );
  if ( pos == 4 ) return e_flipped ( enc );
  return f_flipped ( enc );
}

inline std::vector<unsigned> decode_edges ( const unsigned enc )
{
  std::vector<unsigned> dec_edges;
  for ( auto i = 0; i < 6; i++ )
  {
    dec_edges.emplace_back (
      ( leaf_permutation ( enc, i ) << 1 ) | leaf_negation ( enc, i )
      ); 
  }
  return dec_edges;
}





//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Level 2 encoding
/*
MSB encoding for stage information. First 4 bits of the MSB



+------------+------------+------------+------------+
|stage       |#inputs     |1st-stage   |2nd-stage   |
+------------+------------+------------+------------+
|0           |3           |MAJ         |NONE        |
+------------+------------+------------+------------+
|1           |3           |AND         |AND         |
+------------+------------+------------+------------+
|2           |3           |AND         |OR          |
+------------+------------+------------+------------+
|3           |3           |AND         |XOR         |
+------------+------------+------------+------------+
|4           |3           |OR          |AND         |
+------------+------------+------------+------------+
|5           |3           |OR          |OR          |
+------------+------------+------------+------------+
|6           |3           |OR          |XOR         |
+------------+------------+------------+------------+
|7           |4           |MAJ         |AND         |
+------------+------------+------------+------------+
|8           |4           |MAJ         |OR          |
+------------+------------+------------+------------+
|9           |4           |MAJ         |XOR         |
+------------+------------+------------+------------+
|10          |5           |MAJ         |AND         |
+------------+------------+------------+------------+
|11          |5           |MAJ         |OR          |
+------------+------------+------------+------------+
|12          |5           |MAJ         |XOR         |
+------------+------------+------------+------------+
|13          |6           |MAJ         |AND         |
+------------+------------+------------+------------+
|14          |6           |MAJ         |OR          |
+------------+------------+------------+------------+
|15          |6           |MAJ         |XOR         |
+------------+------------+------------+------------+


If the MSB 4 bits is 0, it means form a normal 3-input MIG network, i.e. no 2nd stage
And apply the input permutation encoding in the 24 LSB bits

If the MSB 4 bits is 10, it means form 2 3-input MIG network, and connect the 
second stage as AND gate. Apply the permutation encoding at the inputs.

If MSB = 4, form first stage as OR gate as given in the LSB 24-bit encoding, 
further connect the 3rd input as a AND gate to the 2nd stage.
This way we can form 3-input AND, 3-input OR etc
 */

// 0x0FFFFFFF = 00001111111111111111111111111111 ( 32 bits total )
inline unsigned encode_stage ( unsigned enc, unsigned val )
{
  return ( enc & 0x0FFFFFFF ) | ( val << 28 ); 
}

inline unsigned decode_stage ( unsigned enc ) { return enc >> 28; }

//------------------------------------------------------------------------------

// clang-format on

}
}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
