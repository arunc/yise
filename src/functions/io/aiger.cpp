/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : aiger.cpp
 * @brief  : AIG read/write utils
 * Note    : corresponding function header is mig.hpp, aig.hpp and common.hpp
 *
 *         : functions
 *         : Aig::Aig read_aig (); Mig::Mig read_aig ();
 *         : void write_aig ( Aig::Aig ); void write_aig ( Mig::mig );
 *         : Aig::Aig to_aig ( const Mig &mig ); 
 */

/*-------------------------------------------------------------------------------*/

#include "common_io.hpp"

#include <functions/aig/tt_maps.hpp>
#include <functions/aig/cut.hpp>
#include <functions/aig/npn.hpp>

//------------------------------------------------------------------------------
// Basic AIGER read/write and lambdas
//------------------------------------------------------------------------------
// Many of these routines are adapted (~copied) from ABC
namespace Yise
{

inline void write_header ( FILE *ofs, const Aig::Aig &aig, bool ascii )
{
  assert ( ofs != NULL );
  std::string typ = ascii ? "aag" : "aig";
  std::fprintf (
    ofs, "%s %u %u %u %u %u\n",           // M I L O A
    typ.c_str (),                         // ascii or binary
    aig.num_nodes () - 1,                 // max variable index
    aig.num_pis (),                       // number of PIs
    0,                                    // number of latches
    aig.num_pos (),                       // number of POs
    aig.num_nodes () - aig.num_pis () - 1 // number of AND gates
    );
}

inline void write_symbol_table ( FILE *ofs, const Aig::Aig &aig )
{
  assert ( ofs != NULL );
  // write PIs
  for ( auto i = 0; i < aig.pi_list.size (); i++ )
  {
    std::fprintf ( ofs, "i%d %s\n", i, to_name ( aig, aig.pi_list[i] ).c_str () );
  }
  // write POs
  for ( auto i = 0; i< aig.po_list.size (); i++ )
  {
    std::fprintf ( ofs, "o%d %s\n", i, aig.po_list[i].name.c_str ()  );
  }
}

// Dump the ascii AAG format
void write_aag_int ( const Aig::Aig &aig, char * pFileName )
{
  // start the output stream
  auto ofs = fopen ( pFileName, "wb" );
  if ( ofs == NULL )
  {
    std::cout << "[e] Cannot open the output file:" << pFileName << "\n";
    return;
  }
  
  write_header ( ofs, aig, true );      // 1. Header
  for ( const auto &e : aig.pi_list )   // 2. Pis
  {
    std::fprintf ( ofs, "%u\n", e );
  }

  std::vector<unsigned> pos;
  for ( const auto &po : aig.po_list )   // 3. POs
  {
    std::fprintf ( ofs, "%u\n", swap_if_const ( po.edge ) );
  }

  for ( const auto &n : aig.node_list )  // 4. AND nodes
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    auto a = swap_if_const ( n.fanins[0] );
    auto b = swap_if_const ( n.fanins[1] );
    const auto &in0 = a > b ? a : b;
    const auto &in1 = a < b ? a : b;
    std::fprintf ( ofs, "%u %u %u\n", n.edge (), in0, in1 );
  }

  write_symbol_table ( ofs, aig );       // 5. write the symbol table
  // 6. write model, comment
  fprintf ( ofs, "c" );
  fprintf ( ofs, "\n%s", aig.name.c_str() );
  fprintf ( ofs, "\nWritten by Yise @ %s\n", curr_time_str ().c_str () );
  fclose ( ofs );
}


// Dump the AIG binary format
void write_aig_int ( const Aig::Aig &aig, char * pFileName )
{
  // start the output stream
  auto ofs = fopen ( pFileName, "wb" );
  if ( ofs == NULL )
  {
    std::cout << "[e] Cannot open the output file:" << pFileName << "\n";
    return;
  }
  
  write_header ( ofs, aig, false ); // 1. Header
  // 2. Pis M = I + L + A, hence no need to writeout PIs explicitly

  for ( const auto &po : aig.po_list )  // 3. POs
  {
    std::fprintf ( ofs, "%u\n", swap_if_const ( po.edge ) );
  }

  // 4. AND nodes
  // write the nodes into the buffer
  int Pos = 0;
  // need a large buffer to hold the characters
  int nBufferSize = 6 * aig.num_nodes () + 100; 
  unsigned char *pBuffer = __ABC__ALLOC__( unsigned char, nBufferSize );

  for ( const auto &n : aig.node_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    unsigned int uLit = n.edge ();
    unsigned int uLit0 = swap_if_const ( n.fanins[0] );
    unsigned int uLit1 = swap_if_const ( n.fanins[1] );
    if ( uLit0 > uLit1 )
    {
      unsigned Temp = uLit0;
      uLit0 = uLit1;
      uLit1 = Temp;
    }
    assert ( !Aig::is_complement ( uLit ) );
    assert( uLit1 < uLit );
    unsigned int delta0 = uLit - uLit1;
    unsigned int delta1 = uLit1 - uLit0;
    
    Pos = encode ( pBuffer, Pos, delta0 );
    Pos = encode ( pBuffer, Pos, delta1 );
    
    if ( Pos > nBufferSize - 10 )
    {
      printf( "[e] Aig dump failed; allocated buffer is too small.\n" );
      __ABC__FREE__( pBuffer );
      return;
    }
  }

  assert ( Pos < nBufferSize );
  fwrite ( pBuffer, 1, Pos, ofs );
  __ABC__FREE__( pBuffer );

  // write the symbol table
  write_symbol_table ( ofs, aig );
  // write model, comment
  fprintf ( ofs, "c" );
  fprintf ( ofs, "\n%s%c", aig.name.c_str(), '\0' );
  fprintf ( ofs, "\nWritten by Yise @ %s\n", curr_time_str ().c_str () );

  fclose ( ofs );
}

// Ported from ABC Io_ReadAiger
template <typename F>
NameDetails foreach_aig_object (
  char *pFileName,
  F f
  )
{
  int nTotal, nInputs, nOutputs, nLatches, nAnds;
  int nBad = 0, nConstr = 0, nJust = 0, nFair = 0;
  int nFileSize = -1, iTerm, nDigits, i;
  char *pContents, *pDrivers = NULL, *pSymbols, *pCur, *pName, *pType;
  unsigned uLit0, uLit1, uLit;
  int RetValue;
  pName = (char *)std::string ( "" ).c_str ();

  // read the file into the buffer
  nFileSize = get_fileSize ( pFileName );
  FILE *pFile = fopen ( pFileName, "rb" );
  pContents = __ABC__ALLOC__ ( char, nFileSize );
  RetValue = fread ( pContents, nFileSize, 1, pFile );
  fclose ( pFile );

  NameDetails dummy ( 0, 0, "NA", "NA" );

  // check if the input file format is correct
  if ( strncmp ( pContents, "aig", 3 ) != 0 || ( pContents[3] != ' ' && pContents[3] != '2' ) )
  {
    std::cout << "[e] Wrong input file format: " << pFileName << "!\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }

  // read the parameters (M I L O A + B C J F)
  pCur = pContents;
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of objects
  nTotal = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of inputs
  nInputs = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of latches
  nLatches = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  assert ( nLatches == 0 && "Latches not supported " );
  // read the number of outputs
  nOutputs = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of nodes
  nAnds = atoi ( pCur );
  while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nBad = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nBad;
  }
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nConstr = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nConstr;
  }
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nJust = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nJust;
  }
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nFair = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nFair;
  }
  if ( *pCur != '\n' )
  {
    std::cout << "[e] Parameter line is in wrong format: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }
  pCur++;

  // check the parameters
  if ( nTotal != nInputs + nLatches + nAnds )
  {
    std::cout << "[e] Number of objects do not match: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }
  if ( nJust || nFair )
  {
    std::cout << "[e] Liveness properties not supported: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }

  if ( nConstr > 0 )
  {
    std::cout << "[w] last " << nConstr
              << " outputs interpreted as constraints\n";
  }

  if ( pContents[3] != ' ' ) // standard AIGER
  {
    std::cout << "[e] Not a standard AIGER format: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }

  auto all_names = NameDetails ( nInputs, nOutputs, "NA", pFileName );

  // foreach on PIs
  for ( i = 0; i < nInputs; i++ )
  {
    f ( i, -1, DagObjType::PI );
  }

  pDrivers = pCur; // remember the beginning of latch/PO literals
  // scroll to the beginning of the binary data
  for ( i = 0; i < nLatches + nOutputs; )
  {
    if ( *pCur++ == '\n' ) i++;
  }

  // foreach on AND gates
  for ( i = 0; i < nAnds; i++ )
  {
    uLit = ( ( i + 1 + nInputs + nLatches ) << 1 );
    uLit1 = uLit - decode ( &pCur );
    uLit0 = uLit1 - decode ( &pCur );
    f ( uLit0, uLit1, DagObjType::NODE );
  }

  // remember the place where symbols begin
  pSymbols = pCur;

  pCur = pDrivers;
  // tag the POs
  for ( i = 0; i < nOutputs; i++ )
  {
    uLit0 = atoi ( pCur );
    while ( *pCur++ != '\n' )
      ;
    f ( i, uLit0, DagObjType::PO );
  }

  // read the names if present
  pCur = pSymbols;
  if ( pCur < pContents + nFileSize && *pCur != 'c' )
  {
    while ( pCur < pContents + nFileSize && *pCur != 'c' )
    {
      int term_type = -1; // 0 for pi, 1 for po
      // get the terminal type
      pType = pCur;
      if ( *pCur == 'i' )
        term_type = 0;
      else if ( *pCur == 'o' )
        term_type = 1;
      else
        assert ( false && "Wrong terminal type" );

      iTerm = atoi ( ++pCur ); // get the terminal number
      while ( *pCur++ != ' ' )
        ;

      if ( iTerm >= ( nInputs + nOutputs ) )
        assert ( false && "Out of bound terminal number" );

      pName = pCur; // assign the name
      while ( *pCur++ != '\n' )
        ;

      *( pCur - 1 ) = 0;
      if ( term_type == 0 ) // assign this name
        all_names.pi_names.emplace ( std::make_pair ( iTerm, pName ) );
      else if ( term_type == 1 )
        all_names.po_names.emplace ( std::make_pair ( iTerm, pName ) );
      else
        assert ( false );

    }
  }

  // read the name of the model if given
  //pCur = pSymbols;
  if ( (pCur + 1) < (pContents + nFileSize) && (*pCur == 'c') )
  {
    pCur++;
    if ( *pCur == '\n' )
    {
      pCur++;
      if ( strlen ( pCur ) > 0 ) // read model name
      {
        all_names.name = copy_char_string ( pCur );
      }
    }
  }

  // skip remaining comments
  __ABC__FREE__ ( pContents );
  assert ( !( nBad || nConstr || nJust || nFair ) );
  return all_names;
}

//------------------------------------------------------------------------------
} // Yise namespace

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// AIG
namespace Yise
{
namespace Aig
{

Mig::Edge add_mig_edge (
  const Aig &aig, Mig::Mig &mig,
  std::unordered_map<int, int> &aig_to_mig,
  const Edge aig_e
  )
{
  auto itr = aig_to_mig.find ( aig_e );
  if ( itr != aig_to_mig.end () ) return itr->second;

  const auto &fanins = all_fanins ( aig, aig_e ); // implicit -> regular (aig_e)
  auto a = add_mig_edge ( aig, mig, aig_to_mig, fanins[0] );
  auto b = add_mig_edge ( aig, mig, aig_to_mig, fanins[1] );

  auto mig_e = Mig::add_and ( mig, a, b );
  // all_fanins () returns the regular edge only.
  // i.e., aig_e is converted to regular ( aig_e ) before returning fanins.
  // Hence if the incoming aig_e is complemented that has to be reflected.
  if ( is_complement ( aig_e ) ) mig_e = Mig::complement ( mig_e );

  aig_to_mig [aig_e] = mig_e;
  aig_to_mig [complement ( aig_e )] = Mig::complement ( mig_e );
  return mig_e;
}

Mig::Mig to_mig ( const Aig &aig )
{
  Mig::Mig mig ( aig.name, aig.num_nodes () );
  std::unordered_map<int, int> aig_to_mig;
  aig_to_mig.reserve ( aig.num_nodes () << 1 );
  aig_to_mig[edge0] = Mig::edge0;
  aig_to_mig[edge1] = Mig::edge1;
  aig_to_mig[edge_undef] = Mig::edge_undef;

  for ( const auto &pi : aig.pi_list )
  {
    auto mig_pi = add_pi ( mig, to_name ( aig, pi ) );
    aig_to_mig[pi] = mig_pi;
    aig_to_mig[complement ( pi )] = Mig::complement ( mig_pi );
  }

  for ( const auto &aig_po : aig.po_list )
  {
    auto mig_po = add_mig_edge ( aig, mig, aig_to_mig, aig_po.edge );
    // Added po edge can be of opposite sign, dealt internally while creating nodes
    tag_as_po ( mig, mig_po, aig_po.name );
  }

  mig = strash ( mig );
  return mig;
}

//------------------------------------------------------------------------------
namespace OptMig
{
inline Node &node0  ( Node &n ) { 
  assert ( to_id ( n.fanins[0] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[0] )];
}
inline Node &node1  ( Node &n ) { 
  assert ( to_id ( n.fanins[1] ) < n.p_aig->node_list.size () );
  return n.p_aig->node_list[ to_id ( n.fanins[1] )];
}
inline Node &node_from_id ( Aig *p_aig, int id )
{
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}

inline unsigned cut_to_sumenc ( const Cut &cut, const Truth_t &tt )
{
  if ( cut.leaves.size () == 3 )
  {
    auto itr = TtMap::sum_tt.find ( tt );
    if ( itr != TtMap::sum_tt.end () ) return itr->second;
  }
  return 0u;
}

inline unsigned cut_to_enc ( const Cut &cut, const Truth_t &tt )
{
  if ( cut.leaves.size () == 3 )
  {
    auto itr = TtMap::cut_size_3.find ( tt );
    if ( itr != TtMap::cut_size_3.end () ) return itr->second;
  }
  else if ( cut.leaves.size () == 6 )
  {
    auto itr = TtMap::cut_size_6.find ( tt );
    if ( itr != TtMap::cut_size_6.end () ) return itr->second;
  }
  return 0u;
}


Mig::Edge add_enc_node (
  Mig::Mig &mig, const std::vector<Edge> &fanins,
  unsigned enc
  )
{

  // 2. encode the inputs, fanins to enc_fanins
  const auto xxx = Mig::edge_undef;
  std::vector<Mig::Edge> enc_fanins = {xxx, xxx, xxx, xxx, xxx, xxx};
  for ( auto i = 0; i < fanins.size (); i++ )
  {
    auto edge = fanins [ leaf_permutation ( enc, i ) ];
    enc_fanins[i] = leaf_negation ( enc, i ) ? complement ( edge ) : edge;
  }

  // 3. encode the stages and form the AIG
  Mig::Edge mig_e;
  const auto &stage = decode_stage ( enc );
  if ( stage < 7 && fanins.size () != 3 )
  {
    std::cout << "stage = " << to_hex_str ( enc ) << " fsize: " << fanins.size () << "\n";
  }
  if ( stage < 7 ) assert ( fanins.size () == 3 ); // check all schemes in sync
  if ( stage > 12 ) assert ( fanins.size () == 6 ); // check all schemes in sync
  assert ( stage < 7 || stage > 12 );

  if ( stage == 0 )
  {
    mig_e = Mig::add_maj ( mig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
  }
  else if ( stage == 1 )
  {
    auto and_e = Mig::add_and ( mig, enc_fanins[0], enc_fanins[1] );
    mig_e = Mig::add_and ( mig, and_e, enc_fanins[2] );
  }
  else if ( stage == 2 )
  {
    auto and_e = Mig::add_and ( mig, enc_fanins[0], enc_fanins[1] );
    mig_e = Mig::add_or ( mig, and_e, enc_fanins[2] );
  }
  else if ( stage == 3 )
  {
    auto and_e = Mig::add_and ( mig, enc_fanins[0], enc_fanins[1] );
    mig_e = Mig::add_xor ( mig, and_e, enc_fanins[2] );
  }
  else if ( stage == 4 )
  {
    auto and_e = Mig::add_or ( mig, enc_fanins[0], enc_fanins[1] );
    mig_e = Mig::add_and ( mig, and_e, enc_fanins[2] );
  }
  else if ( stage == 5 )
  {
    auto and_e = Mig::add_or ( mig, enc_fanins[0], enc_fanins[1] );
    mig_e = Mig::add_or ( mig, and_e, enc_fanins[2] );
  }
  else if ( stage == 6 )
  {
    auto and_e = Mig::add_or ( mig, enc_fanins[0], enc_fanins[1] );
    mig_e = Mig::add_xor ( mig, and_e, enc_fanins[2] );
  }
  else if ( stage == 13 )
  {
    auto m1 = Mig::add_maj ( mig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto m2 = Mig::add_maj ( mig, enc_fanins[3], enc_fanins[4], enc_fanins[5] );
    mig_e = Mig::add_and ( mig, m1, m2 );
  }
  else if ( stage == 14 )
  {
    auto m1 = Mig::add_maj ( mig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto m2 = Mig::add_maj ( mig, enc_fanins[3], enc_fanins[4], enc_fanins[5] );
    mig_e = Mig::add_or ( mig, m1, m2 );
  }
  else if ( stage == 15 )
  {
    auto m1 = Mig::add_maj ( mig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
    auto m2 = Mig::add_maj ( mig, enc_fanins[3], enc_fanins[4], enc_fanins[5] );
    mig_e = Mig::add_xor ( mig, m1, m2 );
  }
  else
  {
    assert ( false && "Unknown stage " );
  }

  return mig_e;
}


Mig::Edge add_sumenc_node (
  Mig::Mig &mig, const std::vector<Edge> &fanins,
  unsigned enc
  )
{

  // 2. encode the inputs, fanins to enc_fanins
  const auto xxx = Mig::edge_undef;
  std::vector<Mig::Edge> enc_fanins = {xxx, xxx, xxx, xxx, xxx, xxx};
  for ( auto i = 0; i < fanins.size (); i++ )
  {
    auto edge = fanins [ leaf_permutation ( enc, i ) ];
    enc_fanins[i] = leaf_negation ( enc, i ) ? complement ( edge ) : edge;
  }

  // 3. encode the stages and form the AIG
  Mig::Edge mig_e;
  const auto &stage = decode_stage ( enc );
  assert ( stage == 0 ); // only 1-bit adder supported for now

  auto p = Mig::add_maj ( mig, enc_fanins[0], enc_fanins[1], enc_fanins[2] );
  auto q = Mig::add_maj ( mig, enc_fanins[0], enc_fanins[1], complement ( enc_fanins[2] ) );
  mig_e = Mig::add_maj ( mig, complement ( p ), enc_fanins[2], q );
  return mig_e;
}



Edge add_opt_mig_node (
  Node &aig_node,
  Mig::Mig &mig,
  std::unordered_map<int, int> &aig_to_mig
  )
{
  auto itr = aig_to_mig.find ( to_edge ( aig_node.id ) );
  if ( itr != aig_to_mig.end () ) return itr->second;
  
  bool adder = false;
  // 1. find if a matching cut encoding is available
  auto cut = get_cut ( aig_node, 6 );
  const auto &tt = compute_cut_tt ( aig_node, cut );
  unsigned enc = cut_to_enc ( cut, tt );

  if ( enc == 0 ) // try adder encoding
  {
    enc = cut_to_sumenc ( cut, tt );
    adder = ( enc == 0 ) ? false : true;
  }

  if ( enc == 0 ) // now try 3 input cuts
  {
    cut = get_cut ( aig_node, 3 ); 
    const auto &tt = compute_cut_tt ( aig_node, cut );
    enc = cut_to_enc ( cut, tt );
  
    if ( enc == 0 ) // try adder encoding
    {
      enc = cut_to_sumenc ( cut, tt );
      adder = ( enc == 0 ) ? false : true;
    }
  }
  
  
  Mig::Edge mig_e;
  if ( enc == 0 ) // no matching encoding
  {
    auto f0 = add_opt_mig_node ( node0 ( aig_node ), mig, aig_to_mig );
    auto f1 = add_opt_mig_node ( node1 ( aig_node ), mig, aig_to_mig );
    if ( is_complement ( aig_node.fanins[0] ) ) f0 = complement ( f0 );
    if ( is_complement ( aig_node.fanins[1] ) ) f1 = complement ( f1 );
    
    mig_e = Mig::add_and ( mig, f0, f1 );
  }
  else
  {
    std::vector<int> leaves ( cut.leaves.begin (), cut.leaves.end () );
    std::sort ( leaves.begin (), leaves.end () ); // fix an order
    std::vector<Edge> fanins;
    // 1. justify all the leaves, leaves to fanins
    for ( const auto &l : leaves )
    {
      auto &leaf_node = node_from_id ( aig_node.p_aig, l ); // leaves are node ids
      fanins.emplace_back ( add_opt_mig_node ( leaf_node, mig, aig_to_mig ) );
    }
    if ( adder ) mig_e = add_sumenc_node ( mig, fanins, enc );
    else mig_e = add_enc_node ( mig, fanins, enc );
  }

  aig_to_mig [ to_edge ( aig_node.id )] = mig_e;
  aig_to_mig [ complement ( to_edge ( aig_node.id ) )] = complement ( mig_e );
  return mig_e;

}

}

Mig::Mig to_opt_mig ( Aig &aig )
{
  assert ( aig.good () );
  Mig::Mig mig ( aig.name, aig.num_nodes () );
  std::unordered_map<int, int> aig_to_mig;
  aig_to_mig.reserve ( aig.num_nodes () << 1 );
  aig_to_mig[edge0] = Mig::edge0;
  aig_to_mig[edge1] = Mig::edge1;
  aig_to_mig[edge_undef] = Mig::edge_undef;

  for ( const auto &pi : aig.pi_list )
  {
    auto mig_pi = Mig::add_pi ( mig, to_name ( aig, pi ) );
    aig_to_mig[pi] = mig_pi;
    aig_to_mig[complement ( pi )] = complement ( mig_pi );
  }

  for ( const auto &po : aig.po_list )
  {
    auto &n = aig.node_list[ po.node_id () ] ;
    auto mig_po = OptMig::add_opt_mig_node ( n, mig, aig_to_mig );
    if ( po.is_complement () ) mig_po = complement ( mig_po );
    tag_as_po ( mig, mig_po, po.name );
  }

  mig = strash ( mig );
  return mig;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void write_aig ( const Aig &aig, const std::string &file, bool ascii )
{
  if ( ascii )
    write_aag_int ( aig, (char *)file.c_str () );
  else
    write_aig_int ( aig, (char *)file.c_str () );  
}

Aig read_aig ( const std::string &file )
{
  auto num_lines = count_lines ( file );
  Aig aig ( "NA", num_lines );

  // 1. Aiger format names are at the end.
  // Hence names can be added only after parsing everything.
  // 2. Aiger format has POs before nodes.
  // tag_as_po() should be run after the node is entered.
  // instead of this we change the attribute afterwards
  // 3. literal-0 is Aig const1, and literal-1 is Aig const0

  const auto &all_names = foreach_aig_object (
    (char *)file.c_str (),
    [&]( int a, int b, const DagObjType &type ) {

      // 3. swap the constants
      a = swap_if_const ( a );
      b = swap_if_const ( b );

      if ( type == DagObjType::PI )        // all PIs comes first
      {
	add_pi ( aig );
      }
      else if ( type == DagObjType::NODE ) // all AND nodes
      {
	add_and ( aig, a, b );
      }
      else if ( type == DagObjType::PO )   //  finally all POs
      {
	Edge po = b;
	tag_as_po ( aig, po );
      }
    }
    );

  // change the attribute on all pos, ( needed? )
  for ( auto &po : aig.po_list )
  {
    Edge e = po.edge;
    assert ( po.node_id () < aig.num_nodes () );
    auto &node = aig.node_list[po.node_id ()];
    if ( is_const ( e ) )
    {
      assert ( node.type == NodeType::CONST1 );
    }
    else if ( is_pi ( aig, e ) )
    {
      node.type = NodeType::PI_PO;
    }
    else
    {
      node.type = NodeType::PO;
    }
  }

  // update the names
  for ( const auto &n : all_names.pi_names )
  {
    const auto &indx = n.first;
    const auto &pi = aig.pi_list[indx];
    aig.name_list[pi] = n.second;
  }

  for ( const auto &n : all_names.po_names )
  {
    const auto &indx = n.first;
    auto &po = aig.po_list[indx];
    po.name = n.second;
  }
  if ( !all_names.name.empty () ) aig.name = all_names.name;
  if ( !all_names.file.empty () ) aig.file = all_names.file;

  aig = strash ( aig );
  return aig;
}

} // Aig
} // Yise


//------------------------------------------------------------------------------
// MIG

namespace Yise
{
namespace Mig
{

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
Aig::Edge add_aig_edge (
  const Mig &mig, Aig::Aig &aig,
  std::unordered_map<int, int> &mig_to_aig,
  const Edge mig_e
  )
{
  auto itr = mig_to_aig.find ( mig_e );
  if ( itr != mig_to_aig.end () ) return itr->second;

  const auto &fanins = all_fanins ( mig, mig_e ); // implicit -> regular (mig_e)
  auto a = add_aig_edge ( mig, aig, mig_to_aig, fanins[0] );
  auto b = add_aig_edge ( mig, aig, mig_to_aig, fanins[1] );
  auto c = add_aig_edge ( mig, aig, mig_to_aig, fanins[2] );

  auto aig_e = Aig::add_maj ( aig, a, b, c );
  // all_fanins () returns the regular edge only.
  // i.e., mig_e is converted to regular ( mig_e ) before returning fanins.
  // Hence if the incoming mig_e is complemented that has to be reflected.
  if ( is_complement ( mig_e ) ) aig_e = Aig::complement ( aig_e );

  mig_to_aig [mig_e] = aig_e;
  mig_to_aig [complement ( mig_e )] = Aig::complement ( aig_e );
  return aig_e;
}

Aig::Aig to_aig ( const Mig &mig )
{
  Aig::Aig aig ( mig.name, mig.num_nodes () << 1 );
  std::unordered_map<int, int> mig_to_aig;
  mig_to_aig.reserve ( mig.num_nodes () << 1 );
  mig_to_aig[edge0] = Aig::edge0;
  mig_to_aig[edge1] = Aig::edge1;
  mig_to_aig[edge_undef] = Aig::edge_undef;

  for ( const auto &pi : mig.pi_list )
  {
    auto aig_pi = add_pi ( aig, to_name ( mig, pi ) );
    mig_to_aig[pi] = aig_pi;
    mig_to_aig[complement ( pi )] = Aig::complement ( aig_pi );
  }

  for ( const auto &mig_po : mig.po_list )
  {
    auto aig_po = add_aig_edge ( mig, aig, mig_to_aig, mig_po.edge );
    // Added po edge can be of opposite sign, dealt internally while creating nodes
    tag_as_po ( aig, aig_po, mig_po.name );
  }

  aig = strash ( aig );
  return aig;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void write_aig ( const Mig &mig, const std::string &file, bool ascii )
{
  auto aig = to_aig ( mig );

  if ( ascii )
    write_aag_int ( aig, (char *)file.c_str () );
  else
    write_aig_int ( aig, (char *)file.c_str () );  
}

Mig read_aig ( const std::string &file )
{
  auto num_lines = count_lines ( file );
  Mig mig ( "NA", num_lines );

  // 1. Aiger format names are at the end.
  // Hence names can be added only after parsing everything.
  // 2. Aiger format has POs before nodes.
  // tag_as_po() should be run after the node is entered.
  // instead of this we change the attribute afterwards
  // 3. literal-0 is Mig const1, and literal-1 is Mig const0
  const auto &all_names = foreach_aig_object (
    (char *)file.c_str (),
    [&]( int a, int b, const DagObjType &type ) {

      // 3. swap the constants
      a = swap_if_const ( a );
      b = swap_if_const ( b );
      
      if ( type == DagObjType::PI )        // all PIs comes first
      {
	add_pi ( mig );
      }
      else if ( type == DagObjType::NODE ) // all AND nodes
      {
	add_and ( mig, a, b );
      }
      else if ( type == DagObjType::PO )   //  finally all POs
      {
	Edge po = b;
	tag_as_po ( mig, po );
      }
    }
    );

  // change the attribute on all pos, ( needed? )
  for ( auto &po : mig.po_list )
  {
    Edge e = po.edge;
    assert ( po.node_id () < mig.num_nodes () );
    auto &node = mig.node_list[po.node_id ()];
    if ( is_const ( e ) )
    {
      assert ( node.type == NodeType::CONST1 );
    }
    else if ( is_pi ( mig, e ) )
    {
      node.type = NodeType::PI_PO;
    }
    else
    {
      node.type = NodeType::PO;
    }
  }

  // update the names
  for ( const auto &n : all_names.pi_names )
  {
    const auto &indx = n.first;
    const auto &pi = mig.pi_list[indx];
    mig.name_list[pi] = n.second;
  }

  for ( const auto &n : all_names.po_names )
  {
    const auto &indx = n.first;
    auto &po = mig.po_list[indx];
    po.name = n.second;
  }
  if ( !all_names.name.empty () ) mig.name = all_names.name;
  if ( !all_names.file.empty () ) mig.file = all_names.file;

  mig = strash ( mig );
  return mig;
}

} // Mig namespace
} // Yise namespace



//------------------------------------------------------------------------------
// QCA

namespace Yise
{
namespace Qca
{

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
Aig::Edge add_aig_edge (
  const Qca &qca, Aig::Aig &aig,
  std::unordered_map<int, int> &qca_to_aig,
  const Edge qca_e
  )
{
  auto itr = qca_to_aig.find ( qca_e );
  if ( itr != qca_to_aig.end () ) return itr->second;

  const auto &fanins = all_fanins ( qca, qca_e ); // implicit -> regular (qca_e)
  auto a = add_aig_edge ( qca, aig, qca_to_aig, fanins[0] );
  auto b = add_aig_edge ( qca, aig, qca_to_aig, fanins[1] );
  auto c = add_aig_edge ( qca, aig, qca_to_aig, fanins[2] );

  auto aig_e = Aig::add_maj ( aig, a, b, c );
  // all_fanins () returns the regular edge only.
  // i.e., qca_e is converted to regular ( qca_e ) before returning fanins.
  // Hence if the incoming qca_e is complemented that has to be reflected.
  if ( is_complement ( qca_e ) ) aig_e = Aig::complement ( aig_e );

  qca_to_aig [qca_e] = aig_e;
  qca_to_aig [complement ( qca_e )] = Aig::complement ( aig_e );
  return aig_e;
}

Aig::Aig to_aig ( const Qca &qca )
{
  Aig::Aig aig ( qca.name, qca.num_nodes () << 1 );
  std::unordered_map<int, int> qca_to_aig;
  qca_to_aig.reserve ( qca.num_nodes () << 1 );
  qca_to_aig[edge0] = Aig::edge0;
  qca_to_aig[edge1] = Aig::edge1;
  qca_to_aig[edge_undef] = Aig::edge_undef;

  for ( const auto &pi : qca.pi_list )
  {
    auto aig_pi = add_pi ( aig, to_name ( qca, pi ) );
    qca_to_aig[pi] = aig_pi;
    qca_to_aig[complement ( pi )] = Aig::complement ( aig_pi );
  }

  for ( const auto &qca_po : qca.po_list )
  {
    auto aig_po = add_aig_edge ( qca, aig, qca_to_aig, qca_po.edge );
    // Added po edge can be of opposite sign, dealt internally while creating nodes
    tag_as_po ( aig, aig_po, qca_po.name );
  }

  aig = strash ( aig );
  return aig;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void write_aig ( const Qca &qca, const std::string &file, bool ascii )
{
  auto aig = to_aig ( qca );

  if ( ascii )
    write_aag_int ( aig, (char *)file.c_str () );
  else
    write_aig_int ( aig, (char *)file.c_str () );  
}

Qca read_aig ( const std::string &file )
{
  auto num_lines = count_lines ( file );
  Qca qca ( "NA", num_lines );

  // 1. Aiger format names are at the end.
  // Hence names can be added only after parsing everything.
  // 2. Aiger format has POs before nodes.
  // tag_as_po() should be run after the node is entered.
  // instead of this we change the attribute afterwards
  // 3. literal-0 is Qca const1, and literal-1 is Qca const0
  const auto &all_names = foreach_aig_object (
    (char *)file.c_str (),
    [&]( int a, int b, const DagObjType &type ) {

      // 3. swap the constants
      a = swap_if_const ( a );
      b = swap_if_const ( b );
      
      if ( type == DagObjType::PI )        // all PIs comes first
      {
	add_pi ( qca );
      }
      else if ( type == DagObjType::NODE ) // all AND nodes
      {
	add_and ( qca, a, b );
      }
      else if ( type == DagObjType::PO )   //  finally all POs
      {
	Edge po = b;
	tag_as_po ( qca, po );
      }
    }
    );

  // change the attribute on all pos, ( needed? )
  for ( auto &po : qca.po_list )
  {
    Edge e = po.edge;
    assert ( po.node_id () < qca.num_nodes () );
    auto &node = qca.node_list[po.node_id ()];
    if ( is_const ( e ) )
    {
      assert ( node.type == NodeType::CONST1 );
    }
    else if ( is_pi ( qca, e ) )
    {
      node.type = NodeType::PI_PO;
    }
    else
    {
      node.type = NodeType::PO;
    }
  }

  // update the names
  for ( const auto &n : all_names.pi_names )
  {
    const auto &indx = n.first;
    const auto &pi = qca.pi_list[indx];
    qca.name_list[pi] = n.second;
  }

  for ( const auto &n : all_names.po_names )
  {
    const auto &indx = n.first;
    auto &po = qca.po_list[indx];
    po.name = n.second;
  }
  if ( !all_names.name.empty () ) qca.name = all_names.name;
  if ( !all_names.file.empty () ) qca.file = all_names.file;

  qca = strash ( qca );
  return qca;
}

} // Qca namespace
} // Yise namespace


// clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
