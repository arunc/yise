/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : common_io.hpp
 * @brief  : Common io functions
 * 
 */

/*-------------------------------------------------------------------------------*/

#ifndef COMMON_IO_HPP
#define COMMON_IO_HPP

#include <types/mig.hpp>
#include <types/aig.hpp>
#include <types/qca.hpp>
#include <types/common.hpp>

#include <utils/common_utils.hpp>

#include <cstdio>
#include <cstring>
#include <sstream>

//------------------------------------------------------------------------------
// Basic low level AIGER/MIGER read/write and lambdas
//------------------------------------------------------------------------------
// Many of these routines are adapted (~copied) from ABC

// clang-format off

#define __ABC__REALLOC__(type, obj, num) \
  ((obj) ? ((type *) realloc((char *)(obj), sizeof(type) * (num))) :	\
   ((type *) malloc(sizeof(type) * (num))))

#define __ABC__ALLOC__(type, num)     ((type *) malloc(sizeof(type) * (num)))
#define __ABC__FREE__(obj)            ((obj) ? (free((char *) (obj)), (obj) = 0) : 0)
#define __ABC__CALLOC__(type, num)    ((type *) calloc((num), sizeof(type)))

#define __ABC__SWAP__(Type, a, b)  { Type t = a; a = b; b = t; }

namespace Yise
{

enum class DagObjType
{
  PI,
  PO,
  NODE
};


inline int get_fileSize ( char *pFileName )
{
  FILE *pFile;
  int nFileSize;
  pFile = fopen ( pFileName, "rb" );
  if ( pFile == nullptr )
  {
    std::cout << "[e] File not available: " << pFileName << "\n";
    return 0;
  }
  fseek ( pFile, 0, SEEK_END );
  nFileSize = ftell ( pFile );
  fclose ( pFile );
  return nFileSize;
}

inline char *copy_char_string ( const char *s )
{
  if ( s == nullptr )
    return nullptr;
  else
    return strcpy ( __ABC__ALLOC__ ( char, strlen ( s ) + 1 ), s );
}

// decode from binary format
inline unsigned decode ( char **ppPos )
{
  unsigned x = 0, i = 0;
  unsigned char ch;

  while ( ( ch = *( *ppPos )++ ) & 0x80 ) x |= ( ch & 0x7f ) << ( 7 * i++ );

  return x | ( ch << ( 7 * i ) );
}

// encode to binary format
inline int encode ( unsigned char * pBuffer, int Pos, unsigned x )
{
  unsigned char ch;
  while (x & ~0x7f)
  {
    ch = (x & 0x7f) | 0x80;
    pBuffer[Pos++] = ch;
    x >>= 7;
  }
  ch = x;
  pBuffer[Pos++] = ch;
  return Pos;
}

// In Yise::Aig/Mig, 0 is const1 where as in Aiger, 0 is const0. (and vice versa)
inline unsigned swap_if_const ( const unsigned a )
{
  if ( a == 0u ) return 1u;
  else if ( a == 1u ) return 0u;
  else return a;
}

// POD to hold names in Aiger/Miger while reading
struct NameDetails
{
  std::unordered_map<int, std::string> pi_names;
  std::unordered_map<int, std::string> po_names;
  std::string name;
  std::string file;
  int num_pis, num_pos;

  NameDetails ( const int _num_pis, const int _num_pos,
                const std::string &_name, const std::string &_file )
  {
    pi_names.reserve ( _num_pis );
    po_names.reserve ( _num_pos );
    name = _name;
    file = _file;
    num_pis = _num_pis;
    num_pos = _num_pos;
  }
};

} // Yise

// clang-format off
  
#endif

// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:

