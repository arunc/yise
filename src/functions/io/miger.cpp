/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : aiger.cpp
 * @brief  : AIG read/write utils
 * Note    : corresponding function header is mig.hpp
 */

// MIGER formats
// We define 2 formats, ASCII MIG ( MAG ) and binary MIG
// These are inspired from AIGER format available at http://fmv.jku.at/aiger
// Only difference is instead of writing 2-input AND nodes we write 3-input MAJ nodes
// In binary format, instead of writing 2 deltas we write 3 deltas.

/*-------------------------------------------------------------------------------*/

#include "common_io.hpp"

#include <array>
#include <algorithm>
//------------------------------------------------------------------------------
// Basic low level MIGER read/write and lambdas
//------------------------------------------------------------------------------
// Many of these routines are adapted (~copied) from ABC
namespace Yise
{

inline void write_header ( FILE *ofs, const Mig::Mig &mig, bool ascii )
{
  assert ( ofs != NULL );
  std::string typ = ascii ? "mag" : "mig";
  std::fprintf (
    ofs, "%s %u %u %u %u %u\n",           // M I L O A
    typ.c_str (),                         // ascii or binary
    mig.num_nodes () - 1,                 // max variable index
    mig.num_pis (),                       // number of PIs
    0,                                    // number of latches
    mig.num_pos (),                       // number of POs
    mig.num_nodes () - mig.num_pis () - 1 // number of AND gates
    );
}

inline void write_symbol_table ( FILE *ofs, const Mig::Mig &mig )
{
  assert ( ofs != NULL );
  // write PIs
  for ( auto i = 0; i < mig.pi_list.size (); i++ )
  {
    std::fprintf ( ofs, "i%d %s\n", i, to_name ( mig, mig.pi_list[i] ).c_str () );
  }
  // write POs
  for ( auto i = 0; i< mig.po_list.size (); i++ )
  {
    std::fprintf ( ofs, "o%d %s\n", i, mig.po_list[i].name.c_str ()  );
  }
}


// Dump the ascii MAG format
void write_mag_int ( const Mig::Mig &mig, char * pFileName )
{
  // start the output stream
  auto ofs = fopen ( pFileName, "wb" );
  if ( ofs == NULL )
  {
    std::cout << "[e] Cannot open the output file:" << pFileName << "\n";
    return;
  }
  
  write_header ( ofs, mig, true );      // 1. Header
  for ( const auto &e : mig.pi_list )   // 2. Pis
  {
    std::fprintf ( ofs, "%u\n", e );
  }

  std::vector<unsigned> pos;
  for ( const auto &po : mig.po_list )   // 3. POs
  {
    std::fprintf ( ofs, "%u\n", swap_if_const ( po.edge ) );
  }

  for ( const auto &n : mig.node_list )  // 4. MAJ nodes
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    auto a = swap_if_const ( n.fanins[0] );
    auto b = swap_if_const ( n.fanins[1] );
    auto c = swap_if_const ( n.fanins[2] );
    std::fprintf ( ofs, "%u %u %u %u\n", n.edge (), c, b, a );
  }

  write_symbol_table ( ofs, mig );       // 5. write the symbol table
  // 6. write model, comment
  fprintf ( ofs, "c" );
  fprintf ( ofs, "\n%s", mig.name.c_str() );
  fprintf ( ofs, "\nWritten by Yise @ %s\n", curr_time_str ().c_str () );
  fprintf ( ofs, "MAG is similar to ASCII AIGER format (http://fmv.jku.at/aiger)\n" );
  fclose ( ofs );
}

// Dump the MIG binary format
void write_mig_int ( const Mig::Mig &mig, char * pFileName )
{
  // start the output stream
  auto ofs = fopen ( pFileName, "wb" );
  if ( ofs == NULL )
  {
    std::cout << "[e] Cannot open the output file:" << pFileName << "\n";
    return;
  }
  
  write_header ( ofs, mig, false ); // 1. Header
  // 2. Pis M = I + L + A, hence no need to writeout PIs explicitly

  for ( const auto &po : mig.po_list ) // 3. POs
  {
    std::fprintf ( ofs, "%u\n", swap_if_const ( po.edge ) );
  }

  // 4. MIG nodes
  // write the nodes into the buffer
  int Pos = 0;
  // need a large buffer to hold the characters
  int nBufferSize = 10 * mig.num_nodes () + 100; 
  unsigned char *pBuffer = __ABC__ALLOC__( unsigned char, nBufferSize );

  for ( const auto &n : mig.node_list )
  {
    if ( is_pi ( n ) ) continue;
    if ( is_const ( n ) ) continue;
    unsigned int uLit = n.edge ();
    std::array<unsigned int, 3> lits = {swap_if_const ( n.fanins[0] ),
					swap_if_const ( n.fanins[1] ),
					swap_if_const ( n.fanins[2] )};
    std::sort (
      lits.begin (), lits.end (),
      [&] ( const unsigned int a, const unsigned int b ) { return a > b; }
      );

    unsigned int uLit0 = lits[2];
    unsigned int uLit1 = lits[1];
    unsigned int uLit2 = lits[0];

    assert ( !Mig::is_complement ( uLit ) );
    assert ( uLit2 < uLit );
    unsigned int delta0 = uLit - uLit2;
    unsigned int delta1 = uLit2 - uLit1;
    unsigned int delta2 = uLit1 - uLit0;

    Pos = encode ( pBuffer, Pos, delta0 );
    Pos = encode ( pBuffer, Pos, delta1 );
    Pos = encode ( pBuffer, Pos, delta2 );
    
    if ( Pos > nBufferSize - 10 )
    {
      printf( "[e] Mig dump failed; allocated buffer is too small.\n" );
      __ABC__FREE__( pBuffer );
      return;
    }
  }

  assert ( Pos < nBufferSize );
  fwrite ( pBuffer, 1, Pos, ofs );
  __ABC__FREE__( pBuffer );

  // write the symbol table
  write_symbol_table ( ofs, mig );
  // write model, comment
  fprintf ( ofs, "c" );
  fprintf ( ofs, "\n%s%c", mig.name.c_str(), '\0' );
  fprintf ( ofs, "\nWritten by Yise @ %s\n", curr_time_str ().c_str () );
  fprintf ( ofs, "MIG is similar to binary AIGER format (http://fmv.jku.at/aiger)\n" );

  fclose ( ofs );
}

// Ported from ABC Io_ReadAiger
template <typename F>
NameDetails foreach_mig_object (
  char *pFileName,
  F f
  )
{
  int nTotal, nInputs, nOutputs, nLatches, nMajs;
  int nBad = 0, nConstr = 0, nJust = 0, nFair = 0;
  int nFileSize = -1, iTerm, nDigits, i;
  char *pContents, *pDrivers = NULL, *pSymbols, *pCur, *pName, *pType;
  unsigned uLit0, uLit1, uLit2, uLit;
  int RetValue;
  pName = (char *)std::string ( "" ).c_str ();

  // read the file into the buffer
  nFileSize = get_fileSize ( pFileName );
  FILE *pFile = fopen ( pFileName, "rb" );
  pContents = __ABC__ALLOC__ ( char, nFileSize );
  RetValue = fread ( pContents, nFileSize, 1, pFile );
  fclose ( pFile );

  NameDetails dummy ( 0, 0, "NA", "NA" );

  // check if the input file format is correct
  if ( strncmp ( pContents, "mig", 3 ) != 0 || ( pContents[3] != ' ' && pContents[3] != '2' ) )
  {
    std::cout << "[e] Wrong input file format: " << pFileName << "!\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }

  // read the parameters (M I L O A + B C J F)
  pCur = pContents;
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of objects
  nTotal = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of inputs
  nInputs = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of latches
  nLatches = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  assert ( nLatches == 0 && "Latches not supported " );
  // read the number of outputs
  nOutputs = atoi ( pCur );
  while ( *pCur != ' ' ) pCur++;
  pCur++;
  // read the number of nodes
  nMajs = atoi ( pCur );
  while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nBad = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nBad;
  }
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nConstr = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nConstr;
  }
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nJust = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nJust;
  }
  if ( *pCur == ' ' )
  {
    // read the number of properties
    pCur++;
    nFair = atoi ( pCur );
    while ( *pCur != ' ' && *pCur != '\n' ) pCur++;
    nOutputs += nFair;
  }
  if ( *pCur != '\n' )
  {
    std::cout << "[e] Parameter line is in wrong format: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }
  pCur++;

  // check the parameters
  if ( nTotal != nInputs + nLatches + nMajs )
  {
    std::cout << "[e] Number of objects do not match: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }
  if ( nJust || nFair )
  {
    std::cout << "[e] Liveness properties not supported: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }

  if ( nConstr > 0 )
  {
    std::cout << "[w] last " << nConstr
              << " outputs interpreted as constraints\n";
  }

  if ( pContents[3] != ' ' ) // standard MIGER
  {
    std::cout << "[e] Not a standard MIGER format: " << pFileName << "\n";
    __ABC__FREE__ ( pContents );
    return dummy;
  }

  auto all_names = NameDetails ( nInputs, nOutputs, "NA", pFileName );

  // foreach on PIs
  for ( i = 0; i < nInputs; i++ )
  {
    f ( i, -1, -1, DagObjType::PI );
  }

  pDrivers = pCur; // remember the beginning of latch/PO literals
  // scroll to the beginning of the binary data
  for ( i = 0; i < nLatches + nOutputs; )
  {
    if ( *pCur++ == '\n' ) i++;
  }

  // foreach on MIG gates
  for ( i = 0; i < nMajs; i++ )
  {
    uLit = ( ( i + 1 + nInputs + nLatches ) << 1 );
    uLit2 = uLit - decode ( &pCur );
    uLit1 = uLit2 - decode ( &pCur );
    uLit0 = uLit1 - decode ( &pCur );
    
    f ( uLit0, uLit1, uLit2, DagObjType::NODE );
  }

  // remember the place where symbols begin
  pSymbols = pCur;

  pCur = pDrivers;
  // tag the POs
  for ( i = 0; i < nOutputs; i++ )
  {
    uLit0 = atoi ( pCur );
    while ( *pCur++ != '\n' )
      ;
    f ( i, uLit0, -1, DagObjType::PO );
  }

  // read the names if present
  pCur = pSymbols;
  if ( pCur < pContents + nFileSize && *pCur != 'c' )
  {
    while ( pCur < pContents + nFileSize && *pCur != 'c' )
    {
      int term_type = -1; // 0 for pi, 1 for po
      // get the terminal type
      pType = pCur;
      if ( *pCur == 'i' )
        term_type = 0;
      else if ( *pCur == 'o' )
        term_type = 1;
      else
        assert ( false && "Wrong terminal type" );

      iTerm = atoi ( ++pCur ); // get the terminal number
      while ( *pCur++ != ' ' )
        ;

      if ( iTerm >= ( nInputs + nOutputs ) )
        assert ( false && "Out of bound terminal number" );

      pName = pCur; // assign the name
      while ( *pCur++ != '\n' )
        ;

      *( pCur - 1 ) = 0;
      if ( term_type == 0 ) // assign this name
        all_names.pi_names.emplace ( std::make_pair ( iTerm, pName ) );
      else if ( term_type == 1 )
        all_names.po_names.emplace ( std::make_pair ( iTerm, pName ) );
      else
        assert ( false );

    }
  }

  // read the name of the model if given
  //pCur = pSymbols;
  if ( (pCur + 1) < (pContents + nFileSize) && (*pCur == 'c') )
  {
    pCur++;
    if ( *pCur == '\n' )
    {
      pCur++;
      if ( strlen ( pCur ) > 0 ) // read model name
      {
        all_names.name = copy_char_string ( pCur );
      }
    }
  }

  // skip remaining comments
  __ABC__FREE__ ( pContents );
  assert ( !( nBad || nConstr || nJust || nFair ) );
  return all_names;
}



//------------------------------------------------------------------------------
} // Yise namespace

//------------------------------------------------------------------------------
// MIG

namespace Yise
{
namespace Mig
{

void write_mig ( const Mig &mig, const std::string &file, bool ascii )
{
  if ( ascii )
    write_mag_int ( mig, (char *)file.c_str () );
  else
    write_mig_int ( mig, (char *)file.c_str () );
}

Mig read_mig ( const std::string &file )
{
  auto num_lines = count_lines ( file );
  Mig mig ( "NA", num_lines );

  // 1. Miger format names are at the end.
  // Hence names can be added only after parsing everything.
  // 2. Miger format has POs before nodes.
  // tag_as_po() should be run after the node is entered.
  // instead of this we change the attribute afterwards
  // 3. literal-0 is Mig const1, and literal-1 is Mig const0
  const auto &all_names = foreach_mig_object (
    (char *)file.c_str (),
    [&]( int a, int b, int c, const DagObjType &type ) {

      // 3. swap the constants
      a = swap_if_const ( a );
      b = swap_if_const ( b );
      c = swap_if_const ( c );
      
      if ( type == DagObjType::PI )        // all PIs comes first
      {
	add_pi ( mig );
      }
      else if ( type == DagObjType::NODE ) // all MAJ nodes
      {
	add_maj ( mig, a, b, c );
      }
      else if ( type == DagObjType::PO )   //  finally all POs
      {
	Edge po = b;
	tag_as_po ( mig, po );
      }
    }
    );

  // change the attribute on all pos, ( needed? )
  for ( auto &po : mig.po_list )
  {
    Edge e = po.edge;
    assert ( po.node_id () < mig.num_nodes () );
    auto &node = mig.node_list[po.node_id ()];
    if ( is_const ( e ) )
    {
      assert ( node.type == NodeType::CONST1 );
    }
    else if ( is_pi ( mig, e ) )
    {
      node.type = NodeType::PI_PO;
    }
    else
    {
      node.type = NodeType::PO;
    }
  }

  // update the names
  for ( const auto &n : all_names.pi_names )
  {
    const auto &indx = n.first;
    const auto &pi = mig.pi_list[indx];
    mig.name_list[pi] = n.second;
  }

  for ( const auto &n : all_names.po_names )
  {
    const auto &indx = n.first;
    auto &po = mig.po_list[indx];
    po.name = n.second;
  }
  if ( !all_names.name.empty () ) mig.name = all_names.name;
  if ( !all_names.file.empty () ) mig.file = all_names.file;

  mig = strash ( mig );
  return mig;
}

} // Mig namespace
} // Yise namespace

// clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
