/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : cnf.cpp
 * @brief  : CNF read/write utils
 * Note    : 
 *
 */

/*-------------------------------------------------------------------------------*/

#include <types/cnf.hpp>

#include "common_io.hpp"

#include <sstream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
//------------------------------------------------------------------------------
// Basic CNF read/write and lambdas
//------------------------------------------------------------------------------
namespace Yise
{
namespace Cnf
{

inline int cnf_lit2var ( int lit ) { return (lit & 1) ? -(lit >> 1) : (lit >> 1); }

inline std::string to_str ( const Clause &cls )
{
  std::stringstream ss;
  for ( auto &lit : cls )
  {
    ss << lit << " ";
  }
  return ss.str ();
}


Cnf read_cnf ( const std::string &file )
{
  const auto ten_million = 10000000;
  std::vector<Clause> clauses;
  std::vector<int> literals;
  std::unordered_set<int> all_vars; // unique set to track all vars
  
  Cnf cnf;
  auto ofs = std::fopen ( (char *)file.c_str (), "rb" );
  if ( ofs == NULL )
  {
    std::cout << "[e] Cannot open file " << file << "\n";
    return cnf;
  }

  char *cbuffer = __ABC__ALLOC__ ( char, ten_million );
  char *tokens;
  auto line_num = 0;
  auto num_vars = -1;    // number of literals given in CNF file
  auto num_clauses = -1; // number of clauses given in CNF file
  auto num_lits = 0;     // total number of literals in CNF, after processing
  int var, lit;

  while ( std::fgets ( cbuffer, ten_million, ofs ) != NULL )
  {
    line_num++;
    if ( cbuffer[0] == 'c' ) continue; // skip comment 
    if ( cbuffer[0] == 'p' ) // model line
    {
      tokens = std::strtok ( cbuffer + 1, " \t" );
      if ( std::strcmp ( tokens, "cnf" ) )
      {
	std::cout << "[e] DIMACS format is not CNF\n";
	std::fclose ( ofs );
	__ABC__FREE__ ( cbuffer );
	return cnf;
      }
      tokens = strtok ( NULL, " \t" );
      num_vars = atoi ( tokens );
      tokens = strtok ( NULL, " \t" );
      num_clauses = atoi ( tokens );
      assert ( num_vars > 0 && num_clauses > 0 && "#vars, #clauses <= 0" );
      clauses.reserve ( num_clauses + 10 );
      all_vars.reserve ( num_vars + 2 );
      literals.reserve ( 100 );
      continue;
    }
    
    tokens = strtok ( cbuffer, " \t\r\n" );
    if ( tokens == NULL ) continue; // empty line
    
    while ( tokens )
    {
      var = std::atoi ( tokens );
      if ( var == 0 ) break; // clause ends here
      const auto &lit = ( var > 0 ) ? var2lit ( var, false ) : var2lit ( -var, true );
      if ( lit > ( 2 * num_vars + 1 ) )
      {
	std::cout << "[e] Lit " << lit << " out-of-bound for " << num_vars << " variables\n";
	std::fclose ( ofs );
	__ABC__FREE__ ( cbuffer );
	return cnf;
      }
      literals.emplace_back ( lit );
      num_lits++;
      all_vars.emplace ( lit2var ( lit ) );
      tokens = strtok ( NULL, " \t\r\n" );
    }
    if ( var != 0 )
    {
      std::cout << "[w] No 0-terminator in line " << line_num << "\n";
    }
    else
    {
      clauses.emplace_back ( literals );
      literals.clear (); // start a new clause
    }
  }
  
  // close and free the C datastructures
  std::fclose ( ofs );
  __ABC__FREE__ ( cbuffer );
  
  // check
  if ( clauses.size () != num_clauses )
  {
    std::cout << "[w] Number of clauses: " << clauses.size ()
	      << " different from declaration: " << num_clauses << "\n";
  }
  
  // create CNF
  std::vector<int> var_list;
  var_list.assign ( all_vars.begin (), all_vars.end () );
  std::sort ( var_list.begin (), var_list.end () );
  cnf.var_list = std::move ( var_list );
  cnf.num_lits = num_lits;
  cnf.clauses = clauses;
  return cnf;
}



void write_cnf (
  const Cnf &cnf, const std::string &file, 
  const std::vector<int> &forall_clauses, // QBF forall 
  const std::vector<int> &exists_clauses  // QBF exists
  )
{
  auto ofs = std::fopen ( (char *)file.c_str (), "w" );
  if ( ofs == NULL )
  {
    std::cout << "[e] I/O error\n";
    return;
  }
  std::fprintf ( ofs, "c Written by Yise @ %s\n", curr_time_str ().c_str () );
  std::fprintf ( ofs, "p cnf %d %d\n", cnf.num_vars (), cnf.num_clauses () );

  // write the QBF clauses
  if ( !forall_clauses.empty () )
  {
    std::fprintf ( ofs, "a " );
    for ( const auto &var_id : forall_clauses )
    {
      std::fprintf ( ofs, "%d ", var_id );
    }
    std::fprintf ( ofs, "0\n" );
  }
  if ( !exists_clauses.empty () )
  {
    std::fprintf ( ofs, "e " );
    for ( const auto &var_id : exists_clauses )
    {
      std::fprintf ( ofs, "%d ", var_id );
    }
    std::fprintf ( ofs, "0\n" );
  }

  for ( const auto &cls : cnf.clauses )
  {
    for ( auto &lit : cls )
    {
      std::fprintf ( ofs, "%d ", cnf_lit2var ( lit ) );
    }
    std::fprintf ( ofs, "0\n" );
  }
  
  std::fclose ( ofs );
}


void write_cnf ( const Cnf &cnf, const std::string &filename )
{
  std::vector<int> qbf_dummy;
  write_cnf ( cnf, filename, qbf_dummy, qbf_dummy );
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool write_cnf_to_solver ( Sat::sat_solver *solver, Cnf &cnf )
{
  assert ( solver != nullptr );
  Sat::sat_solver_setnvars ( solver, cnf.num_vars () );
  for ( auto &cls : cnf.clauses )
  {
    assert ( !cls.empty () );
    if ( !Sat::sat_solver_addclause ( solver, &cls[0], &(cls[cls.size()]) ) )
    {
      Sat::sat_solver_delete ( solver );
      return false;
    }
  }
  
  const auto &status = Sat::sat_solver_simplify ( solver );
  if ( !status )
  {
    Sat::sat_solver_delete ( solver );
    return false;
  }
  return true;
}

std::vector<int> get_cex ( Cnf &cnf, Sat::sat_solver *solver )
{
  std::vector<int> cex;
  cex.reserve ( cnf.var_list.size () );
  for ( auto &v : cnf.var_list )
  {
    assert ( v > 0 );
    const auto &val = Sat::sat_solver_var_value ( solver, v );
    cex.emplace_back ( val ? v : -v );
  }
  return cex;
}
  
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
inline Aig::Edge _edge ( const std::unordered_map<int, Aig::Edge> &cnf_map, int lit )
{
  auto itr = cnf_map.find ( lit );
  assert ( itr != cnf_map.end () );
  return itr->second;
}

inline void aig_add_or_cls ( // consume the clause until its empty
  Aig::Aig &aig, std::vector<int> &cls,
  std::unordered_map<int, Aig::Edge> &cnf_map,
  std::vector<Aig::Edge> &and_clauses
  )
{
  if ( cls.empty () ) return;
  if ( cls.size () == 1 )
  {
    auto lit = cls.back (); cls.pop_back ();
    and_clauses.emplace_back ( _edge ( cnf_map, lit ) );
  }
  else if ( cls.size () == 2 )
  {
    auto l0 = cls.back (); cls.pop_back ();
    auto l1 = cls.back (); cls.pop_back ();
    and_clauses.emplace_back (
      Aig::add_or ( aig, _edge (cnf_map, l0), _edge (cnf_map, l1) )
      );
  }
  else
  {
    auto l0 = cls.back (); cls.pop_back ();
    auto l1 = cls.back (); cls.pop_back ();
    auto l2 = cls.back (); cls.pop_back ();
    and_clauses.emplace_back (
      Aig::add_or (
	aig, _edge (cnf_map, l0), _edge (cnf_map, l1), _edge (cnf_map, l2) )
      );
  }
  
  aig_add_or_cls ( aig, cls, cnf_map, and_clauses );
}


inline void aig_add_and_cls ( // consume the clause until its empty
  Aig::Aig &aig, 
  std::vector<Aig::Edge> &and_clauses
  )
{
  assert ( !and_clauses.empty () );
  if ( and_clauses.size () == 1 )
  {
    return;
  }
  else if ( and_clauses.size () == 2 )
  {
    auto l0 = and_clauses.back (); and_clauses.pop_back ();
    auto l1 = and_clauses.back (); and_clauses.pop_back ();
    and_clauses.emplace_back ( Aig::add_and ( aig, l0, l1 ) );
  }
  else
  {
    auto l0 = and_clauses.back (); and_clauses.pop_back ();
    auto l1 = and_clauses.back (); and_clauses.pop_back ();
    auto l2 = and_clauses.back (); and_clauses.pop_back ();
    and_clauses.emplace_back ( Aig::add_and ( aig, l0, l1, l2 ) );
  }
  
  aig_add_and_cls ( aig, and_clauses );
}


// Aig conversions
Aig::Aig to_aig ( Cnf &orig_cnf, const std::string &name )
{
  Aig::Aig aig ( name, orig_cnf.num_lits );
  orig_cnf.p_dag = (void *) &aig;
  Cnf cnf = orig_cnf; // first take a copy

  std::unordered_map<int, Aig::Edge> cnf_map; // CNF vars need not start with 1
  cnf_map.reserve ( cnf.num_vars () << 1 );

  std::vector<Aig::Edge> and_clauses;
  and_clauses.reserve ( cnf.num_clauses () );
  for ( auto i = 0; i < cnf.num_vars (); i++ )
  {
    auto pi = Aig::add_pi ( aig );
    const auto &l = var2lit ( cnf.var_list[i], false ); 
    const auto &ln = var2lit ( cnf.var_list[i], true ); 
    cnf_map [l] = pi;
    cnf_map [ln] = Aig::complement ( pi );
  }

  for ( auto &cls : cnf.clauses )
  {
    aig_add_or_cls ( aig, cls, cnf_map, and_clauses );
  }
  
  aig_add_and_cls ( aig, and_clauses );
  assert ( and_clauses.size () == 1 );
  Aig::tag_as_po ( aig, and_clauses[0], "miter" );
  aig = Aig::strash ( aig );
  return aig;
}

//------------------------------------------------------------------------------

inline Aig::Node &node_from_id ( Aig::Aig *p_aig, int id )
{
  if ( id >= p_aig->node_list.size () )
  {
    std::cout << "Failed ID: " << id << " node_list.size(): "
	      << p_aig->node_list.size () << "\n";
    std::exit ( 0 );
  }
  assert ( id < p_aig->node_list.size () );
  return p_aig->node_list[id];
}


// Need Tseitin, will blow up the CNF
Cnf to_cnf ( Aig::Aig &miter ) // essentially DNF to CNF
{
  std::unordered_set<int> vars;
  for ( auto &pi : miter.pi_list )
  {
    vars.emplace ( Aig::to_id ( pi ) ); 
  }

  Cnf cnf;
  int num_lits = 0;
  for ( auto &n : miter.node_list )
  {
    if ( Aig::is_pi ( n ) ) continue;
    if ( Aig::is_const ( n ) ) continue;
    if ( Aig::is_po ( n ) ) continue;
    auto var = n.id;
    auto var0 = Aig::to_id ( n.fanins[0] );
    auto var1 = Aig::to_id ( n.fanins[1] );
    vars.emplace ( var );
    vars.emplace ( var0 );
    vars.emplace ( var1 );
    auto fCompl = false;
    auto fCompl0 = Aig::is_complement ( n.fanins[0] );
    auto fCompl1 = Aig::is_complement ( n.fanins[1] );
    
    cnf.clauses.push_back ( { var2lit (var, !fCompl), var2lit (var0, fCompl0) } );
    cnf.clauses.push_back ( { var2lit (var, !fCompl), var2lit (var1, fCompl1) } );
    cnf.clauses.push_back (
      { var2lit (var, fCompl), var2lit (var0, !fCompl0), var2lit (var1, !fCompl1) }
      );
    num_lits = num_lits + 7;
  }

  for ( auto &po : miter.po_list )
  {
    auto &n = node_from_id ( &miter, Aig::to_id ( po.edge ) );
    auto var = n.id;
    auto var0 = Aig::to_id ( n.fanins[0] );
    auto var1 = Aig::to_id ( n.fanins[1] );
    vars.emplace ( var );
    vars.emplace ( var0 );
    vars.emplace ( var1 );
    auto fCompl = po.is_complement ();
    auto fCompl0 = Aig::is_complement ( n.fanins[0] );
    auto fCompl1 = Aig::is_complement ( n.fanins[1] );
    
    cnf.clauses.push_back ( { var2lit (var, !fCompl), var2lit (var0, fCompl0) } );
    cnf.clauses.push_back ( { var2lit (var, !fCompl), var2lit (var1, fCompl1) } );
    cnf.clauses.push_back (
      { var2lit (var, fCompl), var2lit (var0, !fCompl0), var2lit (var1, !fCompl1) }
      );
    num_lits = num_lits + 7;
  }

  cnf.var_list.assign ( vars.begin (), vars.end () );
  cnf.num_lits = num_lits;
  cnf.p_dag = (void *) &miter;
  return cnf;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

} // Cnf namespace
} // Yise namespace


// clang-format on


// Local Variables:
// eval: (toggle-truncate-lines)
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
