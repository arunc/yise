/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : yig_truth.hpp
 * @brief  : Truth table manipulations for YIG
 */
//-------------------------------------------------------------------------------
#ifndef YIG_TRUTH_HPP
#define YIG_TRUTH_HPP

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <utility>

#include <ext-libs/abc/abc_api.hpp>
#include <types/yig.hpp>

namespace axekit
{
const int TT_ENC_CONST1 = 0xC;
const int TT_ENC_CONST0 = 0xD;
const int TT_ENC_DONTCARE = 0xE;
}

namespace Yise
{

// TT is mapped to an input encoding scheme, represented by an integer.
// Last 24 bits of this integer is used to represent different permutations
// of input variables a, b, c, d, e and f.
// Each variable uses 4 bits in the order. a uses b0-b3, b uses b4-b7 etc
//
// Meaning of first 3 bits (LSB) is as follows
// 0000 => a = in0, 0001 => a = ~in0
// 0010 => a = in1, 0011 => a = ~in1
// 0100 => a = in2, 0101 => a = ~in2
// 0110 => a = in3, 0111 => a = ~in3
// 1000 => a = in4, 1001 => a = ~in4
// 1010 => a = in5, 1011 => a = ~in5
// 1100 => a =   1, 1101 => a = ~0 = 1
// 1110 => a =   X, 1111 => a = ~X = X ( don't care )
// similarly next 4 bits for b and so on.
//
// These permutations are retrived using a_input() to f_input() functions.
// negations using a_flipped() to f_flipped() functions.
//
// clang-format off

inline int make_tt_encoding ( int a_pos, int b_pos, int c_pos,
			      int d_pos, int e_pos, int f_pos )
{
  assert ( a_pos <= 0xE && a_pos >= 0 ); assert ( b_pos <= 0xE && b_pos >= 0 );
  assert ( c_pos <= 0xE && c_pos >= 0 ); assert ( d_pos <= 0xE && d_pos >= 0 );
  assert ( e_pos <= 0xE && e_pos >= 0 ); assert ( f_pos <= 0xE && f_pos >= 0 );

  return ( a_pos | ( b_pos << 4 ) | ( c_pos << 8 ) | ( d_pos << 12 ) |
           ( e_pos << 16 ) | ( f_pos << 20 ) );
}

inline int make_tt_encoding ( const std::array<int, 6> &pos )
{
  return make_tt_encoding ( pos[0], pos[1], pos[2], pos[3], pos[4], pos[5] );
}


// TODO: next version, use char
inline int a_input ( const int enc ) { return ( ( enc & 0xE ) >> 1 ); }
inline int b_input ( const int enc ) { return ( ( enc & 0xE0 ) >> 5 ); }
inline int c_input ( const int enc ) { return ( ( enc & 0xE00 ) >> 9 ); }
inline int d_input ( const int enc ) { return ( ( enc & 0xE000 ) >> 13 ); }
inline int e_input ( const int enc ) { return ( ( enc & 0xE0000 ) >> 17 ); }
inline int f_input ( const int enc ) { return ( ( enc & 0xE00000 ) >> 21 ); }

inline bool a_flipped ( const int enc ) { return ( enc & 1); }
inline bool b_flipped ( const int enc ) { return ( ( enc >> 4)  & 1); }
inline bool c_flipped ( const int enc ) { return ( ( enc >> 8)  & 1); }
inline bool d_flipped ( const int enc ) { return ( ( enc >> 12) & 1); }
inline bool e_flipped ( const int enc ) { return ( ( enc >> 16) & 1); }
inline bool f_flipped ( const int enc ) { return ( ( enc >> 20) & 1); }

// first is the permutation and second is negation
inline std::pair<int, bool> a_term ( const int enc )
{
  return std::make_pair ( a_input ( enc ), a_flipped ( enc ) );
}
inline std::pair<int, bool> b_term ( const int enc )
{
  return std::make_pair ( b_input ( enc ), b_flipped ( enc ) );
}
inline std::pair<int, bool> c_term ( const int enc )
{
  return std::make_pair ( c_input ( enc ), c_flipped ( enc ) );
}
inline std::pair<int, bool> d_term ( const int enc )
{
  return std::make_pair ( d_input ( enc ), d_flipped ( enc ) );
}
inline std::pair<int, bool> e_term ( const int enc )
{
  return std::make_pair ( e_input ( enc ), e_flipped ( enc ) );
}
inline std::pair<int, bool> f_term ( const int enc )
{
  return std::make_pair ( f_input ( enc ), f_flipped ( enc ) );
}


inline int leaf_permutation ( const int enc, const int pos )
{
  assert ( pos < 6 );
  if ( pos == 0 ) return a_input ( enc );
  if ( pos == 1 ) return b_input ( enc );
  if ( pos == 2 ) return c_input ( enc );
  if ( pos == 3 ) return d_input ( enc );
  if ( pos == 4 ) return e_input ( enc );
  return f_input ( enc );
}

inline int leaf_negation ( const int enc, const int pos )
{
  assert ( pos < 6 );
  if ( pos == 0 ) return a_flipped ( enc );
  if ( pos == 1 ) return b_flipped ( enc );
  if ( pos == 2 ) return c_flipped ( enc );
  if ( pos == 3 ) return d_flipped ( enc );
  if ( pos == 4 ) return e_flipped ( enc );
  return f_flipped ( enc );
}

// clang-format on


void print_tt_map ( const std::unordered_map<abc::word, int> &tt_map, std::ostream &os );
void write_tt_map ( const std::unordered_map<abc::word, int> &tt_map, std::ofstream &ofs );
std::unordered_map<abc::word, int> generate_yig_tt ();
void generate_yig_tt ( abc::Gia_Man_t *aig );
} // Yise

#endif


//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
