/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : common_utils.hpp
 * @brief  : some common utilities
 */
//-------------------------------------------------------------------------------

#include "common_utils.hpp"


#ifdef __linux__

#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#endif

namespace Yise
{

#ifdef __linux__
// SO:478898
std::string exec ( const char *cmd )
{
  std::array<char, 128> buffer;
  std::string result;
  std::shared_ptr<FILE> pipe ( popen ( cmd, "r" ), pclose );
  if ( !pipe ) throw std::runtime_error ( "popen() failed!" );
  while ( !feof ( pipe.get () ) )
  {
    if ( fgets ( buffer.data (), 128, pipe.get () ) != NULL )
      result += buffer.data ();
  }
  return result;
}

#else

std::string exec ( const char *cmd )
{
  assert ( false && "ONLY Linux tried for now!\n");
}

#endif

std::string exec ( const std::string &str )
{
  return exec ( str.c_str () );
}

//------------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
