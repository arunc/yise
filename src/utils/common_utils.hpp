/**
 * Copyright (C) AGRA - University of Bremen
 *
 * LICENSE : MIT License
 *
 * @author : Arun <arun@uni-bremen.de>
 * @file   : common_utils.hpp
 * @brief  : some common utilities
 */
//-------------------------------------------------------------------------------

#ifndef COMMON_UTILS_HPP
#define COMMON_UTILS_HPP

#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <string>
#include <sstream>

namespace Yise
{

// Some utilities to print date and time
using axc_time = std::chrono::time_point<std::chrono::steady_clock>;
using axc_duration = std::chrono::duration<double, std::chrono::milliseconds::period>;
// using axc_duration = std::chrono::duration<double,
// std::chrono::seconds::period>;
inline axc_time curr_time ()
{
  return std::chrono::steady_clock::now ();
}
inline long diff_time ( const axc_time &t1, const axc_time &t2 )
{
  return axc_duration ( t2 - t1 ).count ();
}
inline long elapsed_time ( const axc_time &begin_time )
{
  auto t2 = curr_time ();
  return diff_time ( begin_time, t2 );
}
inline std::string curr_time_str () // from SO:38561
{
  auto now = std::chrono::system_clock::now ();
  auto in_time_t = std::chrono::system_clock::to_time_t ( now );
  std::stringstream ss;
  ss << std::put_time ( std::localtime ( &in_time_t ), "%d-%m-%Y %X" );
  return ss.str ();
}

//------------------------------------------------------------------------------

inline unsigned count_lines ( const std::string &filename )
{
  std::ifstream file ( filename );
  return std::count ( std::istreambuf_iterator<char> ( file ),
                      std::istreambuf_iterator<char> (), '\n' );
}

std::string exec ( const char *cmd );
std::string exec ( const std::string &str );


//------------------------------------------------------------------------------
}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
