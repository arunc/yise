// Basic Y Inverter Gate : verilog

module ygate_3 ( a, b, c, d, e, f, y );
   input a, b, c, d, e, f;
   output y;
   
   wire   m1, m2, m3;

   wire   aa, bb, cc, dd, ee, ff;

   assign aa = a;
   assign bb = b;
   assign cc = ~c;
   assign dd = ~d;
   assign ee = ~e;
   assign ff = f;
   
   
   assign m1 = ( aa & bb ) | ( bb & cc ) | ( cc & aa );
   assign m2 = ( bb & dd ) | ( dd & ee ) | ( ee & bb );
   assign m3 = ( cc & ee ) | ( ee & ff ) | ( ff & cc );
   
   assign y = ( m1 & m2 ) | ( m2 & m3 ) | ( m3 & m1 );

endmodule // ygate

