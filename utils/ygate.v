// Basic Y Inverter Gate : verilog

module ygate ( a, b, c, d, e, f, y );
   input a, b, c, d, e, f;
   output y;
   
   wire   m1, m2, m3;
   
   assign m1 = ( a & b ) | ( b & c ) | ( c & a );
   assign m2 = ( b & d ) | ( d & e ) | ( e & b );
   assign m3 = ( c & e ) | ( e & f ) | ( f & c );
   
   assign y = ( m1 & m2 ) | ( m2 & m3 ) | ( m3 & m1 );

endmodule // ygate

