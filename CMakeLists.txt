# Copyright (C) AGRA - University of Bremen
# Arun <arun@uni-bremen.de>
# LICENSE : MIT License

# Top level CMake file

project("yise" CXX)
cmake_minimum_required(VERSION 3.0)

include(CMakeToolsHelpers OPTIONAL)

include(utils/cmake/CCache.cmake)
include(utils/cmake/CompilerFlags.cmake)
include(utils/cmake/ListDir.cmake)
include(utils/cmake/CMakeProcedures.cmake)

set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/utils/cmake/modules/")

message ("TOP-DIR :: ${CMAKE_SOURCE_DIR}")
add_subdirectory(ext)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/programs )

# Options
option(yise_ENABLE_PROGRAMS "build programs" on)
option(yise_BUILD_SHARED "build shared libraries" on)
set(yise_PACKAGES "" CACHE STRING "if non-empty, then only the packages in the semicolon-separated lists are build")

# Libraries (include)
set (abc_INCLUDE ${CMAKE_SOURCE_DIR}/src/ext-libs/abc)
include_directories(${abc_INCLUDE})
include_directories(src)

add_subdirectory(src)

