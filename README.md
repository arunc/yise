Yise : A package for Boolean Y functions
========================================================================

> ### Author: Arun <arun@uni-bremen.de>


Copyright
----------
MIT License.
AGRA - Group of Computer Architecture,  University of Bremen, Germany.


Publication: Arun Chandrasekharan, Daniel Grosse and Rolf Drechsler, "Yise - A
novel Framework for Boolean Networks using Y-Inverter Graphs", In 15th ACM-IEEE
International Conference on Formal Methods and Models for System Design, 2017.


How to build and run
--------------------

## Dependencies:

* C++11 compiler  ( Makeflow uses GCC, and targets a Linux system )
* CMake 3+        ( In Ubuntu, sudo apt-get install cmake )
* Ninja build flow or GNU Make ( In Ubuntu, sudo apt-get install ninja-build build-essential )
* Zlib        ( In Ubuntu, sudo apt-get install libz-dev, zlibc )
* Flex, Bison ( In Ubuntu, sudo apt-get install flex bison )



## Build Steps:
* cd build ( from top directory )
    * cmake -G Ninja ..
    * ninja yise


## Run Steps:
* cd build/programs
* ./yise
* ./yise ../../utils/all_gates.aig all_gates.yig # Example

Short Overview
--------------------

Yise is a package for Boolean Y-functions. Y functions are single output, six
input Boolean gates composed of three majority gates connected in a Y fashion.
Boolean networks based on Y Inverter Gates (YIG) offer an alternate
representation for logic in CAD tools. This package, Yise, translates circuits to Y
inverter gates which forms the basis for logic synthesis, formal verification etc.


Currently only AIGER input format is supported.


Further details are available in the IWLS-2017 competition website.

  
### Notes

   
   
### Others:   

   
### Copyright:  Group of Computer Architecture, Uni Bremen
   
   
   
   


---------------------------------------------------------------



