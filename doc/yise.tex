\documentclass[a4paper,10pt]{article} \usepackage[utf8]{inputenc}
\renewcommand\thesubsection{\arabic{subsection}.}
\renewcommand\thesection{\Roman{section}.}
\renewcommand\thesubsubsection{\roman{subsubsection}.}
\usepackage{graphicx}
\usepackage{url}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{mathtools}
\usepackage{dsfont}
\usepackage{tabulary}
\usepackage{booktabs}


% Bold font for Boolean Algebra. e.g., $\bbbb^n \to \bbbb^m $
\newcommand{\bbbb}{\ensuremath{\mathds{B}}}

% Put non-italics with proper dash inside math. $\dash{a-b}$
\newcommand{\dash}[1]{{\operatorname{#1}}}

% Scriptsize inside math or equation environment.
\newcommand{\st}[1]{{\scriptstyle{#1}}}



% Title Page
\title{Yise \\ \ \\
  \large { \textit{
      (A package for Y-functions 
      )}}
} \author{Arun \\ (arun@uni-bremen.de) }

\begin{document}
\maketitle

\section{Yise}
Yise is a package for Boolean Y-functions. Y functions are single output, six
input Boolean gates composed of three majority gates connected in a Y fashion.
Boolean networks based on Y Inverter Gates (YIG) offer an alternate
representation for logic in CAD tools.  This package, Yise, translates circuits
to Y inverter gates which forms the basis for logic synthesis, formal
verification etc.

Currently only AIGER format is supported in Yise. 

\section{Build Yise}

Yise is available at:  \texttt{https://gitlab.com/arunc/yise}

\subsection{Dependencies}
Yise has the following dependencies
\begin{enumerate}
\item C++11 compiler
  \begin{itemize}
  \item Makeflow uses GCC, and targets a Linux system.
  \end{itemize}
\item CMake 3+
  \begin{itemize}
  \item In Ubuntu, sudo apt-get install cmake 
  \end{itemize}
\item Ninja build flow or GNU Make
  \begin{itemize}
  \item In Ubuntu, sudo apt-get install ninja-build build-essential
  \end{itemize}
\item  Zlib
  \begin{itemize}
  \item In Ubuntu, sudo apt-get install libz-dev, zlibc
  \end{itemize}
\item Flex, Bison, Readline
  \begin{itemize}
  \item In Ubuntu, sudo apt-get install flex bison libreadline libreadline-dev
  \end{itemize}
\end{enumerate}

\subsection{Build Yise}
To build Yise, first clone the git repository.
Go to ``build'' directory, configure cmake, \texttt{cmake -G Ninja ..},
first build external dependencies \texttt{ninja external}, then Yise
\texttt{ninja yise}.


\begin{verbatim}
#########################################
$ git clone git@gitlab.com:arunc/yise.git
$ cd yise

$ cd build
$ cmake -G Ninja ..  # configure CMake
$ ninja external     # build external dependencies
$ ninja yise         # build yise

$ ./programs/yise 
#########################################
\end{verbatim}


\section{Use Yise}

There is an example AIGER file provided in the \textit{utils}
directory, e.g., \texttt{all\_gates.aig}.
To run this example with Yise,
\begin{verbatim}
$ ./programs/yise ../utils/all_gates.aig  all_gates.yig all_gates.v
\end{verbatim}

The above example will read \texttt{all\_gates.aig}, write out the
converted YIG format to \texttt{all\_gates.yig} and also writes out a verilog
file for equivalence checking. Writing out verilog is optional.

Note: IWLS-2017 provides a separate YIG to Verilog converter.
YIG format is as per the IWLS-2017 competition. The format is rather self
explanatory, any case please check the competition website for more details.

\section{Package details}
The main class in Yise is \texttt{Yig} which bundles all the necessary
information of Y-inverter graph. You need to include \textit{src/types/yig.hpp}
for all utilities in Yise. Further all methods are organized into a
\texttt{namespace Yise}. The header \texttt{yig.hpp} and
\texttt{libyise\_all.so} (available in \textit{build/src/libyise\_all.so}) are
enough for building applications using Yise.

In Yise, the basic unit is a \texttt{Yig}, Yig is a DAG, with nodes and edges.
Primary inputs (PI) and internal nodes are valid regular nodes. In addition,
\textit{constant0} is a special node. Edges have an attribute
\textit{complement}, which indicates complemented edges. Primary outputs (PO) are
regular edges tagged with a name. Note that there are no special nodes for PO,
rather any node can be tagged as a primary output, with a corresponding
complement if needed. Each node ( i.e., either a PI or a regular edge ), has 6
input edges. Further each node is provided with a node-id, which is a unique
integer representing that node.

\newpage
Important methods in Yise are
\begin{verbatim}
######################################################################
# Minimal list of methods to start on Yise.
# Further procedures are available in yig.hpp
  
// Basic stuffs
void init ( Yig &yig, std::string circuit_name )

int pi_count ( const Yig &yig )      // number of primary inputs
int po_count ( const Yig &yig )      // number of primary outputs
int node_count ( const Yig &yig )    // total number of nodes
int edge_count ( const Yig &yig )    // total number of edges
Edge edge_one  ()                    // special edge constant-1
Edge edge_zero ()                    // special edge constant-0

// for C++11 iterations.
std::vector<Node> all_nodes   ( const Yig &yig )    // list of all nodes
std::vector<int>  all_pi_ids  ( const Yig &yig )    // list of all PI ids
std::vector<po_info_type> all_pos ( const Yig &yig )
etc...

// Adding/Updating new nodes, PI, POs
int add_yig ( Yig &yig, std::array<Edge, 6> &fanins ) // returns a new node-id
int add_pi  ( Yig &yig, const std::string &name = "") // returns a new node-id
void tag_as_po  ( Yig &yig, const Edge &edge, const std::string &po_name)

// all the below routines returns a new node-id
int add_maj ( Yig &yig, Edge &a, Edge &b, Edge &c ) // Majority gate, 
int add_and ( Yig &yig, Edge &a, Edge &b )  //  2-input AND
int add_or  ( Yig &yig, Edge &a, Edge &b )  // 2-input OR
int add_and ( Yig &yig, Edge &x, Edge &y, Edge &z ) // 3-input AND
int add_or ( Yig &yig, Edge &x, Edge &y, Edge &z ) // 3-input OR
etc...

// Edge-node-id conversions
Edge to_edge  ( const Node &node, bool value )
Edge to_regular_edge ( const Node &node ) 
Edge to_complement_edge ( const Node &node )
int to_id ( const Node &node ) 
int to_id ( const Edge &edge ) 
etc...

// boolean predictes to check conditions
bool is_regular ( const Edge &edge )
bool is_complement ( const Edge &edge )
bool is_const0 ( const Node &node )
bool is_pi ( const Node &node )
etc...
######################################################################
\end{verbatim}

\newpage


\end{document}          

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% mode: reftex
%%% mode: auto-fill
%%% TeX-master: t
%%% End:


